<?php

/*
 * Template Name: Cardcom Payment Successful
 * */

get_header();
do_action( 'wpk/d5657781/payment/successful' );
?>
	<div class="post row">
		<div class="single-content">
            <?php the_content(); ?>
		</div>
	</div>
<?php
get_footer();
