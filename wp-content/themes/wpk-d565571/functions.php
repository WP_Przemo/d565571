<?php

use Wpk\d565571\Core;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

require_once 'vendor/autoload.php';
require_once 'includes/wpk_constants.php';
require_once 'libs/wprentals/index.php';

add_action( 'wp_enqueue_scripts', function () {
    wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', [
        'wpestate_bootstrap',
        'wpestate_bootstrap-theme',
    ] );
} );

add_action( 'after_setup_theme', function () {
    load_child_theme_textdomain( 'wpk', get_stylesheet_directory() . '/languages' );
} );

/**
 * Helper function for accessing core.
 *
 * @return Core
 */
function Core() {
    return Core::getInstance();
}


$core = Core();

/* Release the kraken! */
$core->init();

