<div class="dropdown {{ $icon ? $icon : '' }}">

    <div data-toggle="dropdown" id="{{ $id }}" class="filter_menu_trigger" data-value="{{ $value }}">
        {{ $label }}
        <span class="caret caret_filter"></span>
    </div>
    <input type="hidden" name="{{ $id }}" id="{{ $id }}_input" value="{{ $value }}">
    <ul class="dropdown-menu filter_menu" role="menu" aria-labelledby="{{ $id }}">
        @foreach($options as $key => $option)
            <li role="presentation" data-value="{{ $key }}">{{ $option }}</li>
        @endforeach
    </ul>

</div>
