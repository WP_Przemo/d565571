<script>
    if ( window.top !== window.self ) {
        window.top.location = window.self.location.href;
    } else {
        setTimeout( function() {
            window.location = '{!! $redirectUrl !!}';
        }, 5000 );
    }
</script>
