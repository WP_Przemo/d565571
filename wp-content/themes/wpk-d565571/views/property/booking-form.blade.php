<?php
/**
 * @var \Wpk\d565571\Models\Property $post
 */
?>

<form action="#" method="POST" class="wpk-request-form {{ $instantBooking ? 'wpk-instant-booking' : 'wpk-normal-booking' }}" data-logged="{{ is_user_logged_in() ? '1' : '0' }}">
    <div class="booking_form_request wpk" id="booking_form_request">
        <div id="booking_form_request_mess">

        </div>
        <h3>{{ $instantBooking ? __( 'Book Now', 'wpestate-child' ) : __( 'Request Booking', 'wpestate' ) }}</h3>

        <section class="wpk-time-section">
            <div class="has_calendar calendar_icon">
                <input type="text" id="start_date" placeholder="{{ __( 'Check In', 'wpestate' ) }}" class="form-control calendar_icon" size="40" name="start_date"
                       value="{{ $checkIn }}">
            </div>

            <div class="wpk-clock-icon wpk-booking-hours">
                <select name="start_hour" id="start_hour">
                    <option selected disabled value="">{{ __('Hour') }}</option>
                    @foreach($hours as $hour)
                        <option {{ selected($hour, $checkInHour, false) }} value="{{ $hour }}">{{ $hour }}</option>
                    @endforeach
                </select>
            </div>
        </section>

        <section class="wpk-time-section">
            <div class="has_calendar calendar_icon">
                <input type="text" id="end_date" disabled placeholder="{{ __( 'Check Out', 'wpestate' ) }}" class="form-control calendar_icon" size="40" name="end_date"
                       value="{{ $checkOut }}">
            </div>

            <div class="wpk-clock-icon wpk-booking-hours">
                <select name="end_hour" id="end_hour">
                    @foreach($hours as $hour)
                        <option {{ selected($hour, $checkOutHour, false) }}  value="{{ $hour }}">{{ $hour }}</option>
                    @endforeach
                </select>
            </div>
        </section>

        <div class="has_calendar guest_icon">

            <select name="guests_no" id="guests_no">

                @for( $i = 1; $i < $guests; $i++ )
                    <option value="{{ $i }}">{{ $i }} {{ $i > 1 ? __('attendee', 'wpk') : __('attendees', 'wpk') }}</option>
                @endfor

            </select>

        </div>
    </div>

    @if(!empty($extraOptions) && is_array($extraOptions))
        @foreach($extraOptions as $key=>$extraOption)
            <div class="cost_row cost_row_extra" data-value_add="{{ $extraOption[1] }}" data-value_how="{{ $extraOption[2] }}" data-value_name="{{ $extraOption[0] }}">

                <div class="cost_explanation">
                    <input type="checkbox" value="{{ $key }}" class="form-control" value="1" name="extra_option[]"/>
                    {{ $extraOption[0] }}
                </div>

                <div class="cost_value">
                    <div class="cost_value_show">{!! wpestate_show_price_booking( $extraOption[1], $currency, $whereCurrency, 1 ) !!}</div>
                    {!! wpestate_extra_options_exp( $extraOption[2] ) !!}
                </div>

            </div>

        @endforeach
    @endif
    <div class="space_extra_opt"><input type="hidden" id="extra_options_key" value=""></input></div>

    <p class="full_form" id="add_costs_here"></p>

    <input type="hidden" id="listing_edit" name="listing_edit" value="{{ $post->ID }}"/>

    <div class="submit_booking_front_wrapper">


        <div id="submit_booking_front_instant_wrap">
            <input type="hidden" name="max_guests" id="max_guests" value="{{ $maxGuests }}">
            <input type="hidden" name="overload" id="overload" value="{{ $overloadGuest }}">
            <input type="hidden" name="guest_from_one" id="guest_from_one" value="{{ $pricePerGuestFromOne }}">
            <input type="submit" id="submit_booking_front_instant" data-maxguest="{{ $maxGuests }}" data-overload="{{ $overloadGuest }}" data-guestfromone="{{ $pricePerGuestFromOne }}" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" value="{{ $instantBooking ? __( 'Instant Booking', 'wpestate' ) : __( 'Book Now', 'wpestate' ) }}"/>
        </div>


        {!!  wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' ); !!}
    </div>
</form>

<div class="third-form-wrapper">
    <div class="col-md-6 reservation_buttons">
        <div id="add_favorites" class="{{ $favoriteClass }}" data-postid="{{ $post->ID }}">
            {!! $favoriteText !!}
        </div>
    </div>

    <div class="col-md-6 reservation_buttons">
        <div id="contact_host" class="col-md-6" data-postid="{{ $post->ID }}">
            {{ __( 'Contact Owner', 'wpestate' ) }}
        </div>
    </div>
</div>

<div class="prop_social">
    <span class="prop_social_share">{{ __( 'Share', 'wpestate' ) }}</span>
    <a href="http://www.facebook.com/sharer.php?u={{ esc_url($permalink) }}&amp;t={!! urlencode( $title ) !!}" target="_blank" class="share_facebook">
        <i class="fab fa-facebook-f"></i>
    </a>
    <a href="http://twitter.com/home?status={{ urlencode("$title $permalink") }}" class="share_tweet" target="_blank">
        <i class="fab fa-twitter fa-2"></i>
    </a>
    <a href="https://plus.google.com/share?url={{ $permalink }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="share_google">
        <i class="fab fa-google-plus-g fa-2"></i>
    </a>
    @isset($pinterest[0])
        <a href="http://pinterest.com/pin/create/button/?url={{ $permalink }}&amp;media={{ $pinterest[0] }}&amp;description={{  urlencode( $title ) }}" target="_blank" class="share_pinterest">
            <i class="fab fa-pinterest-p fa-2"></i>
        </a>
    @endisset
</div>
