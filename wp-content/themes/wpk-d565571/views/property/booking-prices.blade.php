<div class="show_cost_form" id="show_cost_form">
    <div class="cost_row">
        <div class="cost_explanation">
            @if( $prices['price_per_guest_from_one'] == 1 )

                @if( $prices['custom_period_quest'] != 1 )
                    {!! $pricesDisplay['extra_price_per_guest'] !!}
                @endif

                {{ $prices['count_hours'] }} {{ __( 'hours','wpestate') }} x {{ $prices['curent_guest_no'] }} {{ __( 'guests','wpestate') }}

                @if( $prices['custom_period_quest'] == 1 )
                    - {{ __( ' period with custom price per guest','wpestate') }}
                @endif

            @else

                @if( $prices['has_custom'] == 1 )
                    {{ $prices['count_hours'] }} {{ __( 'hours with custom price','wpestate') }}
                @elseif( $prices['has_wkend_price']===1 && $prices['cover_weekend']===1)
                    {{ $prices['count_hours'] }} {{ __( 'hours with weekend price','wpestate') }}
                @else
                    {{ $prices['count_hours'] }} {{ __( 'hours','wpestate') }}
                @endif

            @endif
        </div>
        <div class="cost_value">{!! $pricesDisplay['inter_price']  !!}</div>
    </div>


    @if( $prices['has_guest_overload'] != 0 && $prices['total_extra_price_per_guest'] != 0 )

        <div class="cost_row">
            <div class="cost_explanation">
                {{ __( 'Costs for ','wpestate') }} {{ $prices['extra_guests'] }} {{ __('extra guests','wpestate') }}
            </div>
            <div class="cost_value">{!! $pricesDisplay['total_extra_price_per_guest']  !!}</div>
        </div>
    @endif


    @if( $prices['cleaning_fee'] != 0 && $prices['cleaning_fee'] != '' )

        <div class="cost_row">
            <div class="cost_explanation">
                {{ __( 'Cleaning Fee','wpestate') }}
            </div>
            <div class="cost_value cleaning_fee_value" data_cleaning_fee="{{ $prices['cleaning_fee'] }}">{!! $pricesDisplay['cleaning_fee'] !!}</div>
        </div>

    @endif

    @if( $prices['city_fee'] != 0 && $prices['city_fee'] != '' )

        <div class="cost_row">
            <div class="cost_explanation">
                {{ __( 'City Fee','wpestate') }}
            </div>
            <div class="cost_value cleaning_fee_value" data_city_fee="{{ $prices['city_fee'] }}">{!! $pricesDisplay['city_fee'] !!}</div>
        </div>

    @endif


    @if( $prices['security_deposit'] != 0 && $prices['security_deposit'] != '' )

        <div class="cost_row">
            <div class="cost_explanation">{{ __( 'Security Deposit (*refundable)', 'wpestate') }}</div>
            <div class="cost_value">{!! $pricesDisplay['security_deposit'] !!}</div>
        </div>

    @endif

    @if( $prices['early_bird_discount'] != 0 && $prices['early_bird_discount'] != '' )

        <div class="cost_row">
            <div class="cost_explanation">{{ __( 'Early Bird Discount','wpestate') }}</div>
            <div class="cost_value" id="early_bird_discount" data-early-bird="'.$prices['early_bird_discount'].'">
                {!! $pricesDisplay['early_bird_discount'] !!}
            </div>
        </div>

    @endif

    @if(!empty($prices['custom_prices_display']))
        @foreach($prices['custom_prices_display'] as $key => $price)
            <div class="cost_row">
                <div class="cost_explanation">{{ $key }}</div>
                <div class="cost_value" data_total_price="'.$prices['total_price'].'">
                    {!! $price !!}
                </div>
            </div>
        @endforeach
    @endif

    <div class="cost_row" id="total_cost_row">
        <div class="cost_explanation"><strong>{{ __( 'TOTAL','wpestate') }}</strong></div>
        <div class="cost_value" data_total_price="'.$prices['total_price'].'">
            {!! $pricesDisplay['total_price'] !!}
        </div>
    </div>

</div>


<div class="cost_row_instant instant_depozit">
    {{ __( 'Deposit for instant booking','wpestate') }}:
    <span class="instant_depozit_value">
        {{ empty($prices['deposit']) ? '0' : $pricesDisplay['deposit'] }}
   </span>

</div>

@if(!empty($prices['balance']))
    <div class="cost_row_instant instant_balance">{{ __( 'Balance remaining','wpestate') }}:
        <span class="instant_balance_value">
            {!! $pricesDisplay['balance'] !!}
        </span>
    </div>
@endif

<div class="instant_book_info"
     data-total_price="{{ $prices['total_price'] }}"
     data-deposit="{{ $prices['deposit'] }}"
     data-balance="{{ $prices['balance'] }}">
</div>
