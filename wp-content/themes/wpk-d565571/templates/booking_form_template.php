<?php
global $post;
global $current_user;
global $feature_list_array;
global $propid;
global $post_attachments;
global $options;
global $where_currency;
global $property_description_text;
global $property_details_text;
global $property_features_text;
global $property_adr_text;
global $property_price_text;
global $property_pictures_text;
global $propid;
global $gmap_lat;
global $gmap_long;
global $unit;
global $currency;
global $use_floor_plans;
global $favorite_text;
global $favorite_class;
global $property_action_terms_icon;
global $property_action;
global $property_category_terms_icon;
global $property_category;
global $guests;
global $bedrooms;
global $bathrooms;
global $show_sim_two;
global $guest_list;
global $post_id;

?>

<div itemprop="price" class="listing_main_image_price">
	<?php
	$price_per_guest_from_one = floatval( get_post_meta( $post->ID, 'price_per_guest_from_one', true ) );
	$price                    = floatval( get_post_meta( $post->ID, 'property_price', true ) );
	wpestate_show_price( $post->ID, $currency, $where_currency, 0 );
	if ( $price != 0 ) {
		if ( $price_per_guest_from_one == 1 ) {
			echo ' ' . esc_html__( 'per guest', 'wpestate' );
		} else {
			echo ' ' . esc_html__( 'per hour', 'wpestate' );
		}
	}
	?>
</div>

<?php do_action( 'wpk/d565581/bookingForm', get_the_ID() ) ?>

