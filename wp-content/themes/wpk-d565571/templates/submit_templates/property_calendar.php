<?php
global $feature_list_array;
global $edit_id;
global $moving_array;
global $property_icalendar_import;
global $property_icalendar_import_multi;

$reservation_array      = get_post_meta( $edit_id, 'booking_dates', true );
$booking_dates_formated = (array) get_post_meta( $edit_id, 'booked_dates_formated', true );


$property_icalendar_import_multi = get_post_meta( $edit_id, 'property_icalendar_import_multi', true );

$calendars         = \Wpk\d565571\Api\Google\Calendar::getForProperty( \Wpk\d565571\Models\Property::find( $edit_id ) );
$property_calendar = get_post_meta( $edit_id, 'google_calendar_id', true );
$google_token      = get_post_meta( $edit_id, 'google_access_token', true );

//update from singular feed to multifeed
if ( $property_icalendar_import != '' ) {
    $tmp_feed_array           = [];
    $all_feeds                = [];
    $tmp_feed_array[ 'feed' ] = esc_url_raw( $value );
    $tmp_feed_array[ 'name' ] = esc_html( $_POST[ 'array_labels' ][ $key ] );
    $all_feeds[]              = $tmp_feed_array;
    update_post_meta( $edit_id, 'property_icalendar_import_multi', $all_feeds );
    update_post_meta( $edit_id, 'property_icalendar_import', '' );
    wpestate_clear_ical_imported( $edit_id );
    wpestate_import_calendar_feed_listing_global( $edit_id );
}


?>


<div class="col-md-12">

	<div class="user_dashboard_panel">

		<h4 class="user_dashboard_panel_title"><?php esc_html_e( 'Synchronize with Google Calendar (recommended)', 'wpk' ); ?> </h4>

        <?php if ( ! empty( $google_token ) ): ?>

			<div class="wpk-google-calendar-sync col-md-12">
				<select class="wpk-select" name="google_calendars" id="google_calendars">
					<option value=""><?php esc_html_e( 'Select calendar', 'wpk' ) ?></option>
                    <?php foreach ( $calendars as $calendar ): ?>
						<option <?php selected( $property_calendar, $calendar->id, true ) ?> value="<?php echo $calendar->id ?>"><?php echo $calendar->summary ?></option>
                    <?php endforeach; ?>
				</select>
				<div class="wpk-input-notice">
					<small><?php esc_html_e( 'Be careful, the synchronization works in two ways. If you remove booking from your Google Calendar it will be removed from site as well.' ) ?></small>
					<br>
					<small>
                        <?php esc_html_e( 'Also note, that if you would change the dates the price of booking won\'t change.' ) ?>
					</small>
				</div>
				<button class="wpk-disconnect-google-calendar"><?php esc_html_e( 'Disconnect google account', 'wpk' ) ?></button>
				<button class="wpk-save-calendar">
                    <?php esc_html_e( 'Save', 'wpk' ) ?>
				</button>
				<input type="hidden" id="wpk_disconnect_nonce" name="wpk_disconnect_nonce" value="<?php echo wp_create_nonce( 'wpk_disconnect_google_calendar' ) ?>">
			</div>
        <?php else: ?>

			<div class="col-md-12">
				<div class="wpk-notice">
					<span><?php esc_html_e( 'In order to synchronize your property with your Google Calendar you need to provide access to your Google Account. After doing that you will be able to select your calendar.' ) ?></span>
				</div>
				<a
						href="<?php echo \Wpk\d565571\Api\Google\Auth::getUrl( [ 'https://www.googleapis.com/auth/calendar', ], "property|$edit_id" ) ?>"
						class="wpk-google-connect">
                    <?php esc_html_e( 'Connect with google' ) ?></a>
			</div>

        <?php endif; ?>

		<h4 class="user_dashboard_panel_title"><?php esc_html_e( 'When is your listing available?', 'wpestate' ); ?></h4>
		<div class="price_explaning"> <?php esc_html_e( '*Click to select the period you wish to mark as booked for visitors.', 'wpestate' ); ?></div>
		<div class="col-md-12" id="profile_message"></div>


		<div class="booking-calendar-wrapper-in-wrapper booking-calendar-set">

            <?php

            $reservation_array = get_post_meta( $edit_id, 'booking_dates', true );

            if ( ! is_array( $reservation_array ) ) {
                $reservation_array = [];
            }

            wpestate_get_calendar_custom2( $reservation_array, $booking_dates_formated, true, true );
            ?>


			<div id="calendar-prev-internal-set" class="internal-calendar-left"><i class="fa fa-angle-left"></i></div>
			<div id="calendar-next-internal-set" class="internal-calendar-right"><i class="fa fa-angle-right"></i></div>
			<div style="clear: both;"></div>
		</div>

		<div class="col-md-12 calendar-actions">
			<div class="calendar-legend-today"></div>
			<span><?php esc_html_e( 'Today', 'wpestate' ); ?></span>
			<div class="calendar-legend-reserved"></div>
			<span><?php esc_html_e( 'Dates Booked', 'wpestate' ); ?></span>

		</div>

		<div class="col-md-12">
			<div id="profile_message2"></div>
		</div>

		<h4 class="user_dashboard_panel_title"><?php esc_html_e( 'Import/Export iCalendar feeds', 'wpestate' ); ?> </h4>

		<div class="export_ical">
			<strong> <?php esc_html_e( 'This is the property iCalendar feed to export', 'wpestate' ); ?> </strong>

            <?php
            $unique_code_ical = get_post_meta( $edit_id, 'unique_code_ica', true );
            if ( $unique_code_ical == '' ) {
                $unique_code_ical = md5( uniqid( mt_rand(), true ) );
                update_post_meta( $edit_id, 'unique_code_ica', $unique_code_ical );
            }

            $icalendar_feed = wpestate_get_template_link( 'ical.php' );
            $icalendar_feed = esc_url_raw( add_query_arg( 'ical', $unique_code_ical, $icalendar_feed ) );
            print ': ' . $icalendar_feed;

            ?>
		</div>

		<div class="import_ical">

			<!--         <p>
            <label for="property_icalendar_import"><?php esc_html_e( 'iCalendar import feed(feed will be read every 3 hours and when you hit save)', 'wpestate' ); ?></label>
            <input type="text" id="property_icalendar_import" class="form-control" size="40" width="200" name="property_icalendar_import" value="<?php echo $property_icalendar_import; ?>">
            <a href="" id="delete_imported_dates" data-edit-id="<?php echo $edit_id; ?>"><?php esc_html_e( 'delete imported dates', 'wpestate' ); ?></a>
        </p>
    -->


			<div id="icalfeed_wrapper">
                <?php

                if ( is_array( $property_icalendar_import_multi ) ) {
                    foreach ( $property_icalendar_import_multi as $key => $feed_data ) {
                        print '
                    <div class="icalfeed">
                        <input type="text" class="form-control property_icalendar_import_name_new" size="40" width="200"  name="property_icalendar_import_name[]" value="' . $feed_data[ 'name' ] . '">
                        <input type="text"  class="form-control property_icalendar_import_feed_new" size="40" width="200" name="property_icalendar_import_feed[]" value="' . $feed_data[ 'feed' ] . '">
                        <span class="delete_imported_dates_singular" data-edit-id="' . $edit_id . '" data-edit-id-key="' . $key . '">' . esc_html__( 'delete imported dates', 'wpestate' ) . '</span>
                    </div>';
                    }

                } else {
                    esc_html_e( 'There are no calendar feeds', 'wpestate' );
                }

                ?>
			</div>

			<div class="icalfeed">
				<label for="property_icalendar_import"><?php esc_html_e( 'iCalendar import feeds (feed will be read every 3 hours and when you hit save)', 'wpestate' ); ?></label>
				<input type="text" class="form-control property_icalendar_import_name_new" size="40" width="200" id="property_icalendar_import_name_new" name="property_icalendar_import_name_new" value="" placeholder=" <?php esc_html_e( 'feed name', 'wpestate' ); ?> ">
				<input type="text" class="form-control property_icalendar_import_feed_new" size="40" width="200" id="property_icalendar_import_feed_new" name="property_icalendar_import_feed_new" value="" placeholder=" <?php esc_html_e( 'feed url', 'wpestate' ); ?>">
				<span id="add_extra_feed"><?php esc_html_e( 'Add new feed', 'wpestate' ); ?></span>

			</div>


			<input type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="edit_calendar" value="<?php esc_html_e( 'Save', 'wpestate' ) ?>">
		</div>


		<div class="col-md-12" style="display: inline-block;">
			<input type="hidden" name="" id="listing_edit" value="<?php echo $edit_id; ?>">
		</div>

		<div class="col-md-12" style="display: inline-block;">
			<a href="<?php echo wpestate_get_template_link('user_dashboard.php') ?>" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button">
                <?php esc_html_e( 'Save', 'wprentals' ) ?>
			</a>
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="owner_reservation_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" id="close_reservation_internal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h2 class="modal-title_big"><?php esc_html_e( 'Reserve a period', 'wpestate' ); ?></h2>
					<h4 class="modal-title" id="myModalLabel"><?php esc_html_e( 'Mark dates as booked.', 'wpestate' ); ?></h4>
				</div>

				<div class="modal-body">
					<form action="#" method="POST" id="booking_form_owner" class="wpk-request-form">
						<div id="booking_form_request_mess_modal"></div>

						<label for="start_date_owner_book"><?php esc_html_e( 'Check In', 'wpestate' ); ?></label>
						<input type="text" id="start_date_owner_book" size="40" name="booking_from_date" class="form-control" value="" readonly>

						<label for="start_hour_owner_book"><?php esc_attr_e( 'Check in Hour', 'wpk' ) ?></label>
						<select name="start_hour_owner_book" id="start_hour_owner_book">
                            <?php foreach ( \Wpk\d565571\Helpers\Booking::getHours() as $hour ) : ?>
								<option value="<?php echo $hour ?>"><?php echo $hour ?></option>
                            <?php endforeach; ?>
						</select>


						<label for="end_date_owner_book"><?php esc_html_e( 'Check Out', 'wpestate' ); ?></label>
						<input type="text" id="end_date_owner_book" size="40" name="booking_to_date" class="form-control" value="" readonly>

						<label for="start_hour_owner_book"><?php esc_attr_e( 'Check out Hour', 'wpk' ) ?></label>
						<select name="end_hour_owner_book" id="end_hour_owner_book">
                            <?php foreach ( \Wpk\d565571\Helpers\Booking::getHours() as $hour ) : ?>
								<option value="<?php echo $hour ?>"><?php echo $hour ?></option>
                            <?php endforeach; ?>
						</select>

						<input type="hidden" id="property_id" name="property_id" value="<?php echo $edit_id ?>""/>
						<input name="prop_id" type="hidden" id="agent_property_id" value=">


						<p class=" full_form">
						<label for="coment"><?php esc_html_e( 'Your notes', 'wpestate' ); ?></label>
						<textarea id="book_notes" name="booking_mes_mess" cols="50" rows="6" class="form-control"></textarea>
						</p>
						<button type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button"><?php esc_html_e( 'Book Period', 'wpestate' ); ?></button>
					</form>

				</div><!-- /.modal-body -->


			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


    <?php
    global $start_reservation;
    global $end_reservation;
    global $reservation_class;

    $start_reservation = '';
    $end_reservation   = '';
    $reservation_class = '';

    function wpestate_get_calendar_custom2( $reservation_array, $booking_dates_formated, $initial = true, $echo = true ) {
        global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;
        $daywithpost = [];
        // week_begins = 0 stands for Sunday


        $time_now = current_time( 'timestamp' );
        $now      = date( 'Y-m-d' );
        $date     = new DateTime();

        $thismonth = gmdate( 'm', $time_now );
        $thisyear  = gmdate( 'Y', $time_now );
        $unixmonth = mktime( 0, 0, 0, $thismonth, 1, $thisyear );
        $last_day  = date( 't', $unixmonth );

        $max_month_no = intval( get_option( 'wp_estate_month_no_show', '' ) );
        $month_no     = 1;
        while ( $month_no < $max_month_no ) {

            wpestate_draw_month( $month_no, $reservation_array, $booking_dates_formated, $unixmonth, $daywithpost, $thismonth, $thisyear, $last_day );

            $date->modify( 'first day of next month' );
            $thismonth = $date->format( 'm' );
            $thisyear  = $date->format( 'Y' );
            $unixmonth = mktime( 0, 0, 0, $thismonth, 1, $thisyear );
            $month_no ++;
        }

    }


    function wpestate_draw_month( $month_no, $reservation_array, $booking_dates_formated, $unixmonth, $daywithpost, $thismonth, $thisyear, $last_day ) {
        global $wp_locale;
        global $end_reservation;
        global $reservation_class;
        $week_begins = intval( get_option( 'start_of_week' ) );


        $initial = true;
        $echo    = true;

        $table_style = '';
        if ( $month_no > 1 ) {
            $table_style = 'style="display:none;"';
        }

        $calendar_output = '<div class="booking-calendar-wrapper-in col-md-12" data-mno="' . $month_no . '" ' . $table_style . '>
                <div class="month-title"> ' . date_i18n( "F", mktime( 0, 0, 0, $thismonth, 10 ) ) . ' ' . $thisyear . ' </div>
                <table class="wp-calendar booking-calendar">

            <thead>
            <tr>';

        $myweek = [];

        for ( $wdcount = 0; $wdcount <= 6; $wdcount ++ ) {
            $myweek[] = $wp_locale->get_weekday( ( $wdcount + $week_begins ) % 7 );
        }

        foreach ( $myweek as $wd ) {
            $day_name        = ( true == $initial ) ? $wp_locale->get_weekday_initial( $wd ) : $wp_locale->get_weekday_abbrev( $wd );
            $wd              = esc_attr( $wd );
            $calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\">$day_name</th>";
        }

        $calendar_output .= '
            </tr>
            </thead>

            <tfoot>
            <tr>';

        $calendar_output .= '
            </tr>
            </tfoot>
            <tbody>
            <tr>';


        // See how much we should pad in the beginning
        $pad = calendar_week_mod( date( 'w', $unixmonth ) - $week_begins );
        if ( 0 != $pad ) {
            $calendar_output .= "\n\t\t" . '<td colspan="' . esc_attr( $pad ) . '" class="pad">&nbsp;</td>';
        }

        $daysinmonth = intval( date( 't', $unixmonth ) );
        for ( $day = 1; $day <= $daysinmonth; ++ $day ) {

            $timestamp_java = strtotime( $day . '-' . $thismonth . '-' . $thisyear );

            //WPK d565571 Include formatted date
            $date_formated = date( 'Y-m-d', $timestamp_java );

            if ( isset( $newrow ) && $newrow ) {
                $calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
            }

            $newrow = false;

            if ( $timestamp_java < ( time() - 24 * 60 * 60 ) ) {
                $has_past_class = "has_past";
            } else {
                $has_past_class = "has_future";
            }

            $reservation_class = '';

            if ( $day == gmdate( 'j', current_time( 'timestamp' ) ) && $thismonth == gmdate( 'm', current_time( 'timestamp' ) ) && $thisyear == gmdate( 'Y', current_time( 'timestamp' ) ) ) {
                // if is today check for reservation


                if ( array_key_exists( $timestamp_java, $reservation_array ) ) {
                    $calendar_output .= '<td class="calendar-reserved ' . $has_past_class . ' "     data-curent-date="' . $timestamp_java . '">' . wpestate_draw_reservation( $reservation_array[ $timestamp_java ] );
                } else {
                    $calendar_output .= '<td class="calendar-today ' . $has_past_class . ' "        data-curent-date="' . $timestamp_java . '">';
                }
                //WPK d565571 Check for reservations in our custom array rather
            } else if ( array_key_exists( $date_formated, $booking_dates_formated ) ) {

                $clone_arr  = $booking_dates_formated[ $date_formated ];
                $booking_id = array_shift( $clone_arr );

                $is_google_event    = (bool) get_post_meta( $booking_id, 'created_from_google_event', true );
                $google_event_class = $is_google_event ? ' wpk-google-event ' : '';

                //All hours are booked, so block the day
                if ( count( $booking_dates_formated[ $date_formated ] ) === 24 ) {
                    $end_reservation = 1;


                    $calendar_output .= '<td class="calendar-reserved ' . $has_past_class . $reservation_class . $google_event_class . ' "     data-curent-date="' . $timestamp_java . '">' . wpestate_draw_reservation( $reservation_array[ $timestamp_java ] );

                    //Part of hour are booked, mark this in calendar
                } else {
                    $calendar_output .= '<td class="end_reservation ' . $has_past_class . $reservation_class . $google_event_class . '"          data-curent-date="' . $timestamp_java . '">';
                }

                //WPK d565571 end
            } else {//no reservations


                $calendar_output .= '<td class="calendar-free ' . $has_past_class . $reservation_class . '"          data-curent-date="' . $timestamp_java . '">';
            }


            if ( in_array( $day, $daywithpost ) ) // any posts today?
            {
                $calendar_output .= '<a href="' . get_day_link( $thisyear, $thismonth, $day ) . '" title="' . esc_attr( $ak_titles_for_day[ $day ] ) . "\">$day</a>";
            } else {
                $calendar_output .= $day;
            }
            $calendar_output .= '</td>';

            if ( 6 == calendar_week_mod( date( 'w', mktime( 0, 0, 0, $thismonth, $day, $thisyear ) ) - $week_begins ) ) {
                $newrow = true;
            }
        }

        $pad = 7 - calendar_week_mod( date( 'w', mktime( 0, 0, 0, $thismonth, $day, $thisyear ) ) - $week_begins );
        if ( $pad != 0 && $pad != 7 ) {
            $calendar_output .= "\n\t\t" . '<td class="pad" colspan="' . esc_attr( $pad ) . '">&nbsp;</td>';
        }

        $calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table></div>";


        if ( $echo ) {
            echo apply_filters( 'get_calendar', $calendar_output );
        } else {
            return apply_filters( 'get_calendar', $calendar_output );
        }
    }

    // wpestate_draw_reservation($reservation_array[$timestamp_java])

    function wpestate_draw_reservation( $reservation_note ) {
        //$reservation_array[$timestamp_java]

        if ( is_numeric( $reservation_note ) != 0 ) {
            return '<div class="rentals_reservation" >' . esc_html__( 'Booking id', 'wpestate' ) . ': ' . $reservation_note . '</div>';
        } else {

//        if (strpos($reservation_note,'@') !== false) {
//            $reservation_array=  explode('@', $reservation_note);
//            return '<div class="rentals_reservation external_reservation">'.$reservation_array[1].'</div>';
//        }else{
//            return '<div class="rentals_reservation external_reservation">'.esc_html__('External Booking','wpestate').'</div>';
//        }
            return '<div class="rentals_reservation external_reservation">' . $reservation_note . '</div>';

        }

    }


    ?>
