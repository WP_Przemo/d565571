<?php
$current_user            = wp_get_current_user();
$userID                  = $current_user->ID;
$user_login              = $current_user->user_login;
$first_name              = get_the_author_meta( 'first_name', $userID );
$last_name               = get_the_author_meta( 'last_name', $userID );
$user_email              = get_the_author_meta( 'user_email', $userID );
$user_mobile             = get_the_author_meta( 'mobile', $userID );
$user_phone              = get_the_author_meta( 'phone', $userID );
$description             = get_the_author_meta( 'description', $userID );
$facebook                = get_the_author_meta( 'facebook', $userID );
$twitter                 = get_the_author_meta( 'twitter', $userID );
$linkedin                = get_the_author_meta( 'linkedin', $userID );
$pinterest               = get_the_author_meta( 'pinterest', $userID );
$user_skype              = get_the_author_meta( 'skype', $userID );
$bank_account            = get_user_meta( $userID, 'bank_account', true );
$bank_account_owner_name = get_user_meta( $userID, 'bank_account_owner_name', true );
$bank_code               = get_user_meta( $userID, 'bank_code', true );
$branch                  = get_user_meta( $userID, 'branch', true );

$user_title          = get_the_author_meta( 'title', $userID );
$user_custom_picture = get_the_author_meta( 'custom_picture', $userID );
$user_small_picture  = get_the_author_meta( 'small_custom_picture', $userID );
$image_id            = get_the_author_meta( 'small_custom_picture', $userID );
$user_id_picture     = get_the_author_meta( 'user_id_image', $userID );
$id_image_id         = get_the_author_meta( 'user_id_image_id', $userID );
$about_me            = get_the_author_meta( 'description', $userID );
$live_in             = get_the_author_meta( 'live_in', $userID );
$i_speak             = get_the_author_meta( 'i_speak', $userID );
$paypal_payments_to  = get_the_author_meta( 'paypal_payments_to', $userID );
$payment_info        = get_the_author_meta( 'payment_info', $userID );

$professions     = [
    'Psychologist',
    'Physiotherapist',
    'Occupational Therapist',
    'Speech Therapist',
    'Communications clinician',
    'Nutrionist',
    'Dietician',
    'Lawyer',
    'Mediator',
    'Life Coach',
    'Real Estate agent',
    'Broker',
    'Programmer',
    'Designer',
    'Business man',
    'Consultant',
    'Accountant',
    'Salesman',
    'Insurance agent',
    'Workshop Facilitator',
    'Human Resources Manager',
];
$user_profession = get_user_meta( $userID, 'profession', true );

if ( $user_custom_picture == '' ) {
    $user_custom_picture = get_template_directory_uri() . '/img/default_user.png';
}

if ( $user_id_picture == '' ) {
    $user_id_picture = get_template_directory_uri() . '/img/default_user.png';
}


?>


<div class="user_profile_div">


	<div class=" row">

		<div class="col-md-12">

            <?php

            $sms_verification = esc_html( get_option( 'wp_estate_sms_verification', '' ) );
            if ( $sms_verification === 'yes' ) {
                $check_phone = get_the_author_meta( 'check_phone_valid', $userID );

                if ( $check_phone != 'yes' ) {

                    ?>
					<div class="sms_wrapper">
						<h4 class="user_dashboard_panel_title"><?php esc_html_e( ' Validate your Mobile Phone Number to receive SMS Notifications', 'wpestate' ); ?></h4>
						<div class="col-md-12" id="sms_profile_message"></div>
						<div class="col-md-9">
                            <?php //echo get_user_meta( $userID, 'validation_pin',true). '</br>';
                            esc_html_e( '1. Add your Mobile no in Your Details section. Make sure you add it with country code.', 'wpestate' );
                            echo '</br>';
                            esc_html_e( '2. Click on the button "Send me validation code".', 'wpestate' );
                            echo '</br>';
                            esc_html_e( '3. You will get a 4 digit code number via sms at', 'wpestate' );
                            echo ' ' . $user_mobile . '.</br> ';
                            esc_html_e( '4. Add the 4 digit code in the form below and click "Validate Mobile Phone Number"', 'wpestate' );

                            ?>
							<input type="text" style="max-width:250px;" id="validate_phoneno" class="form-control" value="" name="validate_phoneno">
							<button class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="send_sms_pin"><?php esc_html_e( 'Send me validation code', 'wpestate' ); ?></button>
							<button class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="validate_phone"><?php esc_html_e( 'Validate Mobile Phone Number', 'wpestate' ); ?></button>
                            <?php echo '</br>';
                            esc_html_e( '*** If you don\'t receive the SMS, please check that your mobile phone number has the proper format (use the country code ex: +1 3232 232)', 'wpestate' );
                            echo '</br>'; ?>
						</div>


						<div class="col-md-6"></div>
					</div>
                    <?php
                }
            }


            ?>


			<div class="user_dashboard_panel">
				<h4 class="user_dashboard_panel_title"><?php esc_html_e( 'Your details', 'wpestate' ); ?></h4>

				<div class="col-md-12" id="profile_message"></div>

				<div class="col-md-4">
					<p>
						<label for="firstname"><?php esc_html_e( 'First Name', 'wpestate' ); ?></label>
						<input type="text" id="firstname" class="form-control" value="<?php echo $first_name; ?>" name="firstname">
					</p>

					<p>
						<label for="secondname"><?php esc_html_e( 'Last Name', 'wpestate' ); ?></label>
						<input type="text" id="secondname" class="form-control" value="<?php echo $last_name; ?>" name="firstname">
					</p>

					<p>
						<label for="useremail"><?php esc_html_e( 'Email', 'wpestate' ); ?></label>
						<input type="text" id="useremail" class="form-control" value="<?php echo $user_email; ?>" name="useremail">
					</p>

					<p>
						<label for="about_me"><?php esc_html_e( 'About Me', 'wpestate' ); ?></label>
						<textarea id="about_me" class="form-control about_me_profile" name="about_me"><?php echo $about_me; ?></textarea>
					</p>

					<p>
						<label for="live_in"><?php esc_html_e( 'I live in', 'wpestate' ); ?></label>
						<input type="text" id="live_in" class="form-control" value="<?php echo $live_in; ?>" name="live_in">
					</p>

					<p>
						<label for="i_speak"><?php esc_html_e( 'I speak', 'wpestate' ); ?></label>
						<input type="text" id="i_speak" class="form-control" value="<?php echo $i_speak; ?>" name="i_speak">
					</p>
					<p>
						<label for="payment_info"><?php esc_html_e( 'Payment Info/Hidden Field', 'wpestate' ); ?></label>
						<textarea id="payment_info" class="form-control" name="payment_info" cols="70" rows="3"><?php echo $payment_info; ?></textarea>
					</p>

					<p>
						<label for="paypal_payments_to"><?php esc_html_e( 'Email for receiving Paypal Payments (booking fees, refund security fees, etc)', 'wpestate' ); ?></label>
						<input type="text" id="paypal_payments_to" class="form-control" value="<?php echo $paypal_payments_to; ?>" name="paypal_payments_to">
					</p>
					<p>
						<i><?php esc_html_e( '*all property owners are required to fill in their bank account details - this is where the funds will be transferred', 'wpestate' ) ?></i>
					</p>
					<p>
						<label for="bank_account_owner_name">
                            <?php esc_html_e( 'Bank account owner name', 'wpk' ) ?>
						</label>
						<input class="form-control" type="text" id="bank_account_owner_name" name="bank_account_owner_name" value="<?php echo $bank_account_owner_name ?>">
					</p>
					<p>
						<label for="bank_code">
                            <?php esc_html_e( 'Bank code', 'wpk' ) ?>
						</label>
						<input class="form-control" type="text" id="bank_code" name="bank_code" value="<?php echo $bank_code ?>">
					</p>
					<p>
						<label for="branch">
                            <?php esc_html_e( 'Branch', 'wpk' ) ?>
						</label>
						<input class="form-control" type="text" id="branch" name="branch" value="<?php echo $branch ?>">
					</p>
					<p>
						<label for="bank_account">
                            <?php esc_html_e( 'Bank account number', 'wpk' ) ?>
						</label>
						<input class="form-control" type="text" id="bank_account" name="bank_account" value="<?php echo $bank_account ?>">
					</p>
					<p>
						<label for="profession">
                            <?php esc_html_e( 'Profession', 'wpk' ) ?>
						</label>
						<select name="profession" id="profession" class="form-control">
							<option value=""><?php esc_html_e( 'Select profession', 'wpk' ) ?></option>
                            <?php foreach ( $professions as $profession ): ?>
								<option <?php selected( $user_profession, $profession, true ) ?> value="<?php echo $profession ?>">
                                    <?php echo $profession ?>
								</option>
                            <?php endforeach; ?>
						</select>
					</p>
				</div>

				<div class="col-md-4">
					<p>
						<label for="userphone"><?php esc_html_e( 'Phone', 'wpestate' ); ?></label>
						<input type="text" id="userphone" class="form-control" value="<?php echo $user_phone; ?>" name="userphone">
					</p>
					<p>
						<label for="usermobile"><?php esc_html_e( 'Mobile (*Add the country code format Ex :+1 232 3232)', 'wpestate' ); ?></label>
						<input type="text" id="usermobile" class="form-control" value="<?php echo $user_mobile; ?>" name="usermobile">
					</p>

					<p>
						<label for="userskype"><?php esc_html_e( 'Skype', 'wpestate' ); ?></label>
						<input type="text" id="userskype" class="form-control" value="<?php echo $user_skype; ?>" name="userskype">
					</p>

					<p>
						<label for="userfacebook"><?php esc_html_e( 'Facebook Url', 'wpestate' ); ?></label>
						<input type="text" id="userfacebook" class="form-control" value="<?php echo $facebook; ?>" name="userfacebook">
					</p>

					<p>
						<label for="usertwitter"><?php esc_html_e( 'Twitter Url', 'wpestate' ); ?></label>
						<input type="text" id="usertwitter" class="form-control" value="<?php echo $twitter; ?>" name="usertwitter">
					</p>

					<p>
						<label for="userlinkedin"><?php esc_html_e( 'Linkedin Url', 'wpestate' ); ?></label>
						<input type="text" id="userlinkedin" class="form-control" value="<?php echo $linkedin; ?>" name="userlinkedin">
					</p>

					<p>
						<label for="userpinterest"><?php esc_html_e( 'Pinterest Url', 'wpestate' ); ?></label>
						<input type="text" id="userpinterest" class="form-control" height="100" value="<?php echo $pinterest; ?>" name="userpinterest">
					</p>
				</div>
                <?php wp_nonce_field( 'profile_ajax_nonce', 'security-profile' ); ?>


				<div class="col-md-4">
					<div id="profile-div" class="feature-media-upload">

                        <?php print '<img id="profile-image" src="' . $user_custom_picture . '" alt="user image" data-profileurl="' . $user_custom_picture . '" data-smallprofileurl="' . $image_id . '" >'; ?>

						<div id="upload-container">
							<div id="aaiu-upload-container">

								<button id="aaiu-uploader" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button"><?php esc_html_e( 'Upload Image', 'wpestate' ); ?></button>
								<div id="profile-div-upload-imagelist">
									<ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
								</div>
								F
							</div>
						</div>
						<span class="upload_explain"><?php esc_html_e( '* recommended size: minimum 550px ', 'wpestate' ); ?></span>

					</div>


				</div>

				<div class="col-md-4">
                    <?php
                    $user_verified = get_user_meta( $userID, 'user_id_verified', true );
                    $user_id_class = ( $user_verified == 1 ) ? 'verified' : 'feature-media-upload';
                    ?>

					<div id="user-id" class="<?php print esc_attr( $user_id_class ); ?>">

                        <?php print '<img id="user-id-image" src="' . $user_id_picture . '" alt="user ID image" data-useridurl="' . $user_id_picture . '" data-useridimageid="' . $id_image_id . '" >'; ?>
                        <?php if ( ! $user_verified ) { ?>
							<div id="user-id-upload-container-wrap">
								<div id="user-id-upload-container">

									<button id="user-id-uploader" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button image_id_uppload "><?php esc_html_e( 'Upload an Id Scan', 'wpestate' ); ?></button>
									<div id="user-id-upload-imagelist">
										<ul id="user-id-ul-list" class="aaiu-upload-list"></ul>
									</div>
								</div>
							</div>
							<span class="upload_explain"><?php esc_html_e( '* recommended size: minimum 550px ', 'wpestate' ); ?></span>
                        <?php } else { ?>
							<div class="verified-id"><?php esc_html_e( 'You have been verified', 'wpestate' ); ?></div>
                        <?php } ?>

					</div>
				</div>


				<p class="fullp-button">
					<button class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="update_profile"><?php esc_html_e( 'Update profile', 'wpestate' ); ?></button>
					<br>
					<button class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="delete_profile"><?php esc_html_e( 'Delete account', 'wpestate' ); ?></button>
				</p>


			</div>
		</div>


		<div class="col-md-12">
			<div class="user_dashboard_panel">


			</div>
		</div>


		<div class="col-md-12">
			<div class="user_dashboard_panel">
				<h4 class="user_dashboard_panel_title"><?php esc_html_e( 'Change Password', 'wpestate' ); ?></h4>


				<div class="col-md-12" id="profile_pass">
                    <?php esc_html_e( '*After you change the password you will have to login again.', 'wpestate' ); ?>

				</div>

				<p class="col-md-4">
					<label for="oldpass"><?php esc_html_e( 'Old Password', 'wpestate' ); ?></label>
					<input id="oldpass" value="" class="form-control" name="oldpass" type="password">
				</p>

				<p class="col-md-4">
					<label for="newpass"><?php esc_html_e( 'New Password ', 'wpestate' ); ?></label>
					<input id="newpass" value="" class="form-control" name="newpass" type="password">
				</p>
				<p class="col-md-4">
					<label for="renewpass"><?php esc_html_e( 'Confirm New Password', 'wpestate' ); ?></label>
					<input id="renewpass" value="" class="form-control" name="renewpass" type="password">
				</p>

                <?php wp_nonce_field( 'pass_ajax_nonce', 'security-pass' ); ?>
				<p class="fullp-button">
					<button class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="change_pass"><?php esc_html_e( 'Reset Password', 'wpestate' ); ?></button>
				</p>
			</div>
		</div>


	</div>
