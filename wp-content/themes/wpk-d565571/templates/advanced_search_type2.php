<?php
global $post;
$adv_search_what         = get_option( 'wp_estate_adv_search_what', '' );
$show_adv_search_visible = get_option( 'wp_estate_show_adv_search_visible', '' );
$close_class             = '';

//WPK d565571 Grab start and end hours from query
$start_hour = ! empty( $_GET[ 'start_hour' ] ) ? $_GET[ 'start_hour' ] : '';
$end_hour   = ! empty( $_GET[ 'end_hour' ] ) ? $_GET[ 'end_hour' ] : '';

$guests         = (int) get_option( 'wp_estate_guest_dropdown_no' );
$searchedGuests = (int) isset( $_GET[ 'guest_no' ] ) ? $_GET[ 'guest_no' ] : false;

$propertyTypes = get_terms( [
    'taxonomy' => 'property_category',
] );
$searchedType  = isset( $_GET[ 'category_values' ] ) ? $_GET[ 'category_values' ] : 'meeting-room';


if ( $show_adv_search_visible == 'no' ) {
    $close_class = 'adv-search-1-close';
}

if ( isset( $post->ID ) ) {
    $post_id = $post->ID;
} else {
    $post_id = '';
}

$extended_search = get_option( 'wp_estate_show_adv_search_extended', '' );
$extended_class  = '';

if ( $extended_search == 'yes' ) {
    $extended_class = 'adv_extended_class';
    if ( $show_adv_search_visible == 'no' ) {
        $close_class = 'adv-search-1-close-extended';
    }
}
$header_type = '';
if ( isset( $post->ID ) ) {
    $header_type = get_post_meta( $post->ID, 'header_type', true );
}
$global_header_type = get_option( 'wp_estate_header_type', '' );

$google_map_lower_class = '';
if ( ! $header_type == 0 ) {  // is not global settings
    if ( $header_type == 5 ) {
        $google_map_lower_class = 'adv_lower_class';
    }
} else {    // we don't have particular settings - applt global header
    if ( $global_header_type == 4 ) {
        $google_map_lower_class = 'adv_lower_class';
    }
} // end if header


?>

<div class="adv-2-header">
    <?php esc_html_e( 'Make a Reservation', 'wpestate' ); ?>
</div>

<div class="adv-2-wrapper">
</div>


<div class="adv-search-2 <?php echo $google_map_lower_class . ' ' . $close_class . ' ' . $extended_class; ?>" id="adv-search-1" data-postid="<?php echo $post_id; ?>">


	<form method="get" id="main_search" action="<?php print $adv_submit; ?>">
        <?php
        if ( function_exists( 'icl_translate' ) ) {
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>
		<div class="col-md-12 types_icon">
			<select name="category_values" id="category_values" class="wpk-select">
				<option value=""><?php esc_html_e( 'Select Property Unit Type', 'wpestate' ) ?></option>
                <?php
                /** @var WP_Term $type */
                foreach ( $propertyTypes as $type ): ?>
					<option
                        <?php selected( $searchedType, $type->slug ) ?>
							value="<?php echo $type->slug ?>">
                        <?php echo $type->name ?>
					</option>
                <?php endforeach; ?>
			</select>
		</div>
		<div class="col-md-12 map_icon">
            <?php
            $show_adv_search_general  = get_option( 'wp_estate_wpestate_autocomplete', '' );
            $wpestate_internal_search = '';
            if ( $show_adv_search_general == 'no' ) {
                $wpestate_internal_search = '_autointernal';
                print '<input type="hidden" class="stype" id="stype" name="stype" value="tax">';
            }
            ?>
			<input type="text" id="search_location<?php echo $wpestate_internal_search; ?>" class="form-control" name="search_location" placeholder="<?php esc_html_e( 'Where do you want to go ?', 'wpestate' ); ?>" value="">
			<input type="hidden" id="advanced_city" class="form-control" name="advanced_city" data-value="" value="">
			<input type="hidden" id="advanced_area" class="form-control" name="advanced_area" data-value="" value="">
			<input type="hidden" id="advanced_country" class="form-control" name="advanced_country" data-value="" value="">
			<input type="hidden" id="property_admin_area" name="property_admin_area" value="">
		</div>
		<section class="wpk-time-section">

			<div class="col-md-12 has_calendar calendar_icon">
				<input type="text" id="check_in" class="form-control " name="check_in" placeholder="<?php esc_html_e( 'Check In', 'wpestate' ); ?>"
					   value="<?php
                       if ( isset( $_GET[ 'check_in' ] ) ) {
                           echo esc_attr( $_GET[ 'check_in' ] );
                       } ?>">
			</div>
            <?php
            //WPK D565571 add start hour dropdown
            ?>
			<div class="wpk-clock-icon wpk-booking-hours col-md-12">
				<select name="start_hour" id="start_hour">
					<option selected disabled><?php esc_html_e( 'Hour' ) ?></option>
                    <?php foreach ( \Wpk\d565571\Helpers\Booking::getHours() as $hour ): ?>
						<option <?php selected( $start_hour, $hour ) ?> value="<?php echo $hour ?>"><?php echo $hour ?></option>
                    <?php endforeach; ?>
				</select>
			</div>

		</section>
		<!--	<section class="wpk-time-section">

			<div class="col-md-12 has_calendar calendar_icon">
				<input type="text" id="check_out" disabled class="form-control" name="check_out" placeholder="<?php /*esc_html_e( 'Check Out', 'wpestate' ); */ ?>"
					   value="<?php
        /*                       if ( isset( $_GET[ 'check_out' ] ) ) {
                                   echo esc_attr( $_GET[ 'check_out' ] );
                               } */ ?>">
			</div>

            <?php
        /*            //WPK D565571 add end hour dropdown
                    */ ?>
			<div class="wpk-clock-icon wpk-booking-hours col-md-12">
				<select name="end_hour" id="end_hour">
                    <?php /*foreach ( \Wpk\d565571\Helpers\Booking::getHours() as $hour ): */ ?>
						<option <?php /*selected( $end_hour, $hour ) */ ?> value="<?php /*echo $hour */ ?>"><?php /*echo $hour */ ?></option>
                    <?php /*endforeach; */ ?>
				</select>
			</div>

		</section>-->
		<!--<div class="col-md-12 guest_icon">

			<select name="guest_no" class="wpk-select" id="guest_no">

				<option selected value="any"><?php /*esc_html_e( 'any', 'wpk' ) */ ?></option>

                <?php /*for ( $i = 1; $i < $guests; $i ++ ) : */ ?>
					<option <?php /*selected( $searchedGuests, $i ) */ ?> value="<?php /*echo $i */ ?>">
                        <?php /*echo $i */ ?>
                        <?php /*echo $i > 1 ? __( 'attendees', 'wpk' ) : __( 'attendee', 'wpk' ) */ ?>
					</option>
                <?php /*endfor; */ ?>

			</select>

		</div>-->
		<div class="col-md-12">
			<input name="submit" type="submit" class="wpb_btn-info wpb_btn-small wpestate_vc_button  vc_button" id="advanced_submit_2" value="<?php esc_html_e( 'Search', 'wpestate' ); ?>">
		</div>

		<div id="results">
            <?php esc_html_e( 'We found ', 'wpestate' ) ?>
			<span id="results_no">0</span> <?php esc_html_e( 'results.', 'wpestate' ); ?>
			<span id="showinpage"> <?php esc_html_e( 'Show the results now ?', 'wpestate' ); ?> </span>
		</div>

	</form>

</div>
<?php get_template_part( 'libs/internal_autocomplete_wpestate' ); ?>
