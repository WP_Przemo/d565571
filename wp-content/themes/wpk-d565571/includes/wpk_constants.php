<?php

if ( !defined( 'THEME_PATH' ) ) {
    define( 'THEME_PATH', rtrim( get_stylesheet_directory(), '/' ) . '/' );
}

if ( !defined( 'THEME_URL' ) ) {
    define( 'THEME_URL', rtrim( get_stylesheet_directory_uri(), '/' ) . '/' );
}
