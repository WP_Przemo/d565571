const path = require( 'path' );

module.exports = {
    mode:      'production',
    entry:     './src/js/main.js',
    output:    {
        filename: 'wpk-d565571.js',
        path:     path.resolve( __dirname, 'assets/js' )
    },
    resolve:   {
        alias: {
            'assets': path.resolve( __dirname, 'assets' )
        }
    },
    module:    {
        rules: [
            {
                test:    /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use:     {
                    loader:  'babel-loader',
                    options: {
                        presets: [ '@babel/preset-env' ],
                    }
                }
            },
            {
                test: /\.(s*)css$/,
                use:  [ 'style-loader', 'css-loader', 'sass-loader' ],
            },
            {
                test: /\.(png|jpeg|ttf|...)$/,
                use:  [
                    { loader: 'url-loader', options: { limit: 8192 } }
                    // limit => file.size =< 8192 bytes ? DataURI : File
                ]
            }
        ]
    },
    externals: {
        jquery: 'jQuery'
    }
};
