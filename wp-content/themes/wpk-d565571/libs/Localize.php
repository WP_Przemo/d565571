<?php

namespace Wpk\d565571;

/**
 * Helper function for localizing scripts
 *
 * @author Przemysław Żydek
 */
class Localize {


    /**
     * @return void
     */
    public static function wpEstateLocalizeMapFunctions() {

        global $post;

        $pin_images         = wpestate_pin_images();
        $geolocation_radius = esc_html( get_option( 'wp_estate_geolocation_radius', '' ) );
        if ( $geolocation_radius == '' ) {
            $geolocation_radius = 1000;
        }
        $pin_cluster_status = esc_html( get_option( 'wp_estate_pin_cluster', '' ) );
        $zoom_cluster       = esc_html( get_option( 'wp_estate_zoom_cluster ', '' ) );
        $show_adv_search    = esc_html( get_option( 'wp_estate_show_adv_search_map_close', '' ) );

        $header_type            = '';
        $global_header_type     = '';
        $adv_search_type_status = '';
        $slugs                  = [];
        $hows                   = [];
        $show_price_slider      = '';

        if ( isset( $post->ID ) ) {
            $page_lat              = wpestate_get_page_lat( $post->ID );
            $page_long             = wpestate_get_page_long( $post->ID );
            $page_custom_zoom      = wpestate_get_page_zoom( $post->ID );
            $page_custom_zoom_prop = get_post_meta( $post->ID, 'page_custom_zoom', true );
            $closed_height         = wpestate_get_current_map_height( $post->ID );
            $open_height           = wpestate_get_map_open_height( $post->ID );
            $open_close_status     = wpestate_get_map_open_close_status( $post->ID );
        } else {
            $page_lat              = esc_html( get_option( 'wp_estate_general_latitude', '' ) );
            $page_long             = esc_html( get_option( 'wp_estate_general_longitude', '' ) );
            $page_custom_zoom      = esc_html( get_option( 'wp_estate_default_map_zoom', '' ) );
            $page_custom_zoom_prop = 15;
            $closed_height         = intval( get_option( 'wp_estate_min_height', '' ) );
            $open_height           = get_option( 'wp_estate_max_height', '' );
            $open_close_status     = esc_html( get_option( 'wp_estate_keep_min', '' ) );
        }

        $bypass_fit_bounds = 0;
        if ( isset( $post->ID ) ) {
            $bypass_fit_bounds = intval( get_post_meta( $post->ID, 'bypass_fit_bounds', true ) );
        }

        $is_tax = 0;
        if ( is_tax() ) {
            $bypass_fit_bounds = 1;
            $is_tax            = 1;
        }

        $is_property_list = 0;
        if ( is_page_template( 'property_list.php' ) || is_page_template( 'property_list_half.php' ) || is_page_template( 'advanced_search_results.php' ) ) {
            $is_property_list = 1;
        }

        $slider_price_position = 0;

        $custom_advanced_search = get_option( 'wp_estate_custom_advanced_search', '' );
        if ( $custom_advanced_search == 'yes' ) {
            $adv_search_what       = get_option( 'wp_estate_adv_search_what', '' );
            $adv_search_label      = get_option( 'wp_estate_adv_search_label', '' );
            $adv_search_how        = get_option( 'wp_estate_adv_search_how', '' );
            $show_price_slider     = get_option( 'wp_estate_show_slider_price', '' );
            $slider_price_position = 0;
            $counter               = 0;
            foreach ( $adv_search_what as $key => $search_field ) {
                $counter ++;
                if ( $search_field == 'types' ) {
                    $slugs[] = 'adv_actions';
                } else if ( $search_field == 'categories' ) {
                    $slugs[] = 'adv_categ';
                } else if ( $search_field == 'cities' ) {
                    $slugs[] = 'advanced_city';
                } else if ( $search_field == 'areas' ) {
                    $slugs[] = 'advanced_area';
                } else if ( $search_field == 'property price' && $show_price_slider == 'yes' ) {
                    $slugs[]               = 'property_price';
                    $slugs[]               = 'property_price';
                    $slider_price_position = $counter;

                } else {
                    $string  = wpestate_limit45( sanitize_title( $adv_search_label[ $key ] ) );
                    $slug    = sanitize_key( $string );
                    $slugs[] = $slug;
                }
            }

            foreach ( $adv_search_how as $key => $search_field ) {
                $hows[] = $adv_search_how[ $key ];

            }
        }

        $show_g_search_status = esc_html( get_option( 'wp_estate_show_g_search', '' ) );
        $measure_sys          = get_option( 'wp_estate_measure_sys', '' );

        $listing_map = 'internal';

        if ( $header_type == 5 || $global_header_type == 4 ) {
            $listing_map = 'top';
        }

        $use_generated_pins = 0;
        $load_extra         = 0;
        $post_type          = get_post_type();

        if ( is_page_template( 'advanced_search_results.php' ) || is_page_template( 'property_list_half.php' ) || is_page_template( 'property_list.php' ) || is_tax() || $post_type == 'estate_agent' ) {    // search results -> pins are added  from template
            $use_generated_pins = 1;
            $json_string        = [];
            $json_string        = json_encode( $json_string );
        } else {
            // google maps pins
            if ( get_option( 'wp_estate_readsys', '' ) == 'yes' ) {

                $path        = wpestate_get_pin_file_path_read();
                $request     = wp_remote_get( $path );
                $json_string = wp_remote_retrieve_body( $request );
            } else {

                $json_string = wpestate_listing_pins();
            }
        }

        wp_localize_script( 'wpk-wpestate-mapfunctions', 'mapfunctions_vars',
            [
                'path'                      => trailingslashit( get_template_directory_uri() ) . '/css/css-images',
                'pin_images'                => $pin_images,
                'geolocation_radius'        => $geolocation_radius,
                'adv_search'                => $adv_search_type_status,
                'in_text'                   => esc_html__( ' in ', 'wpestate' ),
                'zoom_cluster'              => intval( $zoom_cluster ),
                'user_cluster'              => $pin_cluster_status,
                'open_close_status'         => $open_close_status,
                'open_height'               => $open_height,
                'closed_height'             => $closed_height,
                'generated_pins'            => $use_generated_pins,
                'geo_no_pos'                => esc_html__( 'The browser couldn\'t detect your position!', 'wpestate' ),
                'geo_no_brow'               => esc_html__( 'Geolocation is not supported by this browser.', 'wpestate' ),
                'geo_message'               => esc_html__( 'm radius', 'wpestate' ),
                'show_adv_search'           => $show_adv_search,
                'custom_search'             => $custom_advanced_search,
                'listing_map'               => $listing_map,
                'slugs'                     => $slugs,
                'hows'                      => $hows,
                'measure_sys'               => $measure_sys,
                'close_map'                 => esc_html__( 'close map', 'wpestate' ),
                'show_g_search_status'      => $show_g_search_status,
                'slider_price'              => $show_price_slider,
                'slider_price_position'     => $slider_price_position,
                'map_style'                 => stripslashes( get_option( 'wp_estate_map_style', '' ) ),
                'is_tax'                    => $is_tax,
                'is_property_list'          => $is_property_list,
                'bypass_fit_bounds'         => $bypass_fit_bounds,
                'useprice'                  => esc_html( get_option( 'wp_estate_use_price_pins', '' ) ),
                'use_price_pins_full_price' => esc_html( get_option( 'wp_estate_use_price_pins_full_price', '' ) ),

            ]
        );

    }

    /**
     * @return void
     */
    public static function wpEstateLocalizeAjaxCallsAdd() {

        $custom_fields         = get_option( 'wp_estate_custom_fields', true );
        $tranport_custom_array = [];
        $i                     = 0;

        if ( ! empty( $custom_fields ) ) {
            while ( $i < count( $custom_fields ) ) {
                $name = $custom_fields[ $i ][ 0 ];

                $slug = wpestate_limit45( sanitize_title( $name ) );
                $slug = sanitize_key( $slug );
                $i ++;
                $tranport_custom_array[] = $slug;
            }
        }

        $feature_list       = esc_html( get_option( 'wp_estate_feature_list' ) );
        $feature_list_array = explode( ',', $feature_list );
        $moving_array_amm   = [];

        foreach ( $feature_list_array as $key => $value ) {
            $post_var_name      = str_replace( ' ', '_', trim( $value ) );
            $post_var_name      = wpestate_limit45( sanitize_title( $post_var_name ) );
            $post_var_name      = sanitize_key( $post_var_name );
            $moving_array_amm[] = $post_var_name;
        }

        wp_localize_script( 'wpk-wpestate-ajaxcalls-add', 'ajaxcalls_add_vars',
            [
                'admin_url'                  => get_admin_url(),
                'tranport_custom_array'      => json_encode( $tranport_custom_array ),
                'transport_custom_array_amm' => json_encode( $moving_array_amm ),
                'wpestate_autocomplete'      => get_option( 'wp_estate_wpestate_autocomplete', '' ),
                'home_url'                   => home_url(),
            ]
        );;

    }

    /**
     * @return void
     */
    public static function wpEstateLocalizeAjaxcalls() {

        wp_localize_script( 'wpk-wpestate-ajaxcalls', 'ajaxcalls_vars',
            [
                'contact_name'      => esc_html__( 'Your Name', 'wpestate' ),
                'contact_email'     => esc_html__( 'Your Email', 'wpestate' ),
                'contact_phone'     => esc_html__( 'Your Phone', 'wpestate' ),
                'contact_comment'   => esc_html__( 'Your Message', 'wpestate' ),
                'adv_contact_name'  => esc_html__( 'Your Name', 'wpestate' ),
                'adv_email'         => esc_html__( 'Your Email', 'wpestate' ),
                'adv_phone'         => esc_html__( 'Your Phone', 'wpestate' ),
                'adv_comment'       => esc_html__( 'Your Message', 'wpestate' ),
                'adv_search'        => esc_html__( 'Send Message', 'wpestate' ),
                'admin_url'         => get_admin_url(),
                'login_redirect'    => home_url(),
                'login_loading'     => esc_html__( 'Sending user info, please wait...', 'wpestate' ),
                'userid'            => get_current_user_id(),
                'prop_featured'     => esc_html__( 'Property is featured', 'wpestate' ),
                'no_prop_featured'  => esc_html__( 'You have used all the "Featured" listings in your package.', 'wpestate' ),
                'favorite'          => esc_html__( 'Favorite', 'wpestate' ) . '<i class="fa fa-heart"></i>',
                'add_favorite'      => esc_html__( 'Add to Favorites', 'wpestate' ),
                'remove_favorite'   => esc_html__( 'remove from favorites', 'wpestate' ),
                'add_favorite_unit' => esc_html__( 'add to favorites', 'wpestate' ),
                'saving'            => esc_html__( 'saving..', 'wpestate' ),
                'sending'           => esc_html__( 'sending message..', 'wpestate' ),
                'reserve'           => esc_html__( 'Reserve Period', 'wpestate' ),
                'paypal'            => esc_html__( 'Connecting to Paypal! Please wait...', 'wpestate' ),
                'stripecancel'      => esc_html__( 'subscription will be cancelled at the end of the current period', 'wpestate' ),
                'max_month_no'      => intval( get_option( 'wp_estate_month_no_show', '' ) ),
                'processing'        => esc_html__( 'processing..', 'wpestate' ),
                'home'              => get_site_url(),
                'delete_account'    => esc_html__( 'Confirm your ACCOUNT DELETION request! Clicking the button below will result your account data will be deleted. This means you will no longer be able to login to your account and access your account information: My Profile, My Reservations, My bookings, Invoices. This operation CAN NOT BE REVERSED!', 'wpestate' ),
            ]
        );;
    }

    /**
     * @return void
     */
    public static function wpEstatePropertyLocalize() {

        global $post;

        wp_localize_script( 'wpk-wpestate-property', 'property_vars', [
            'plsfill'        => esc_html__( 'Please fill all the forms:', 'wpestate' ),
            'sending'        => esc_html__( 'Sending Request...', 'wpestate' ),
            'logged_in'      => is_user_logged_in(),
            'notlog'         => esc_html__( 'You need to log in order to book a listing!', 'wpestate' ),
            'viewless'       => esc_html__( 'View less', 'wpestate' ),
            'viewmore'       => esc_html__( 'View more', 'wpestate' ),
            'nostart'        => esc_html__( 'Check in date cannot be bigger than check Out', 'wpestate' ),
            'noguest'        => esc_html__( 'Please select the number of guests', 'wpestate' ),
            'guestoverload'  => esc_html__( 'The number of guests is greater than the property capacity - ', 'wpestate' ),
            'guests'         => esc_html__( 'guests', 'wpestate' ),
            'early_discount' => (float) get_post_meta( $post->ID, 'early_bird_percent', true ),
        ] );;

    }

    /**
     * @return void
     */
    public static function wpEstateControlLocalize() {

        global $post;

        $login_redirect            = home_url();
        $show_adv_search_map_close = esc_html( get_option( 'wp_estate_show_adv_search_map_close', '' ) );
        $current_user              = wp_get_current_user();
        $userID                    = $current_user->ID;

        $week_days_control = [
            '0' => esc_html__( 'None', 'wpestate' ),
            '1' => esc_html__( 'Monday', 'wpestate' ),
            '2' => esc_html__( 'Tuesday', 'wpestate' ),
            '3' => esc_html__( 'Wednesday', 'wpestate' ),
            '4' => esc_html__( 'Thursday', 'wpestate' ),
            '5' => esc_html__( 'Friday', 'wpestate' ),
            '6' => esc_html__( 'Saturday', 'wpestate' ),
            '7' => esc_html__( 'Sunday', 'wpestate' ),
        ];

        $submission_curency = wpestate_curency_submission_pick();


        $booking_array                = [];
        $custom_price                 = '';
        $default_price                = '';
        $cleaning_fee_per_day         = '';
        $city_fee_per_day             = '';
        $price_per_guest_from_one     = '';
        $checkin_change_over          = '';
        $checkin_checkout_change_over = '';
        $min_days_booking             = '';
        $extra_price_per_guest        = '';
        $price_per_weekeend           = '';
        $mega_details                 = '';

        if ( isset( $post->ID ) ) {
            $custom_price = json_encode( wpml_custom_price_adjust( $post->ID ) );


            $booking_array = json_encode( get_post_meta( $post->ID, 'booking_dates', true ) );
            $default_price = get_post_meta( $post->ID, 'property_price', true );

            $cleaning_fee_per_day         = intval( get_post_meta( $post->ID, 'cleaning_fee_per_day', true ) );
            $city_fee_per_day             = intval( get_post_meta( $post->ID, 'city_fee_per_day', true ) );
            $price_per_guest_from_one     = intval( get_post_meta( $post->ID, 'price_per_guest_from_one', true ) );
            $checkin_change_over          = intval( get_post_meta( $post->ID, 'checkin_change_over', true ) );
            $checkin_checkout_change_over = intval( get_post_meta( $post->ID, 'checkin_checkout_change_over', true ) );
            $min_days_booking             = intval( get_post_meta( $post->ID, 'min_days_booking', true ) );
            $extra_price_per_guest        = intval( get_post_meta( $post->ID, 'extra_price_per_guest', true ) );
            $price_per_weekeend           = intval( get_post_meta( $post->ID, 'price_per_weekeend', true ) );
            $mega_details                 = json_encode( wpml_mega_details_adjust( $post->ID ) );
        }

        //$direct_payment_details         =   wp_kses( get_option('wp_estate_direct_payment_details','') ,$argsx);
        if ( function_exists( 'icl_translate' ) ) {
            $mes                    = stripslashes( esc_html( get_option( 'wp_estate_direct_payment_details', '' ) ) );
            $direct_payment_details = icl_translate( 'wpestate', 'wp_estate_property_direct_payment_text', $mes );
        } else {
            $direct_payment_details = stripslashes( esc_html( get_option( 'wp_estate_direct_payment_details', '' ) ) );
        }

        $wp_estate_book_down           = floatval( get_option( 'wp_estate_book_down', '' ) );
        $wp_estate_book_down_fixed_fee = floatval( get_option( 'wp_estate_book_down_fixed_fee', '' ) );
        $include_expeses               = esc_html( get_option( 'wp_estate_include_expenses', '' ) );

        $dates_types = [
            '0' => 'yy-mm-dd',
            '1' => 'yy-dd-mm',
            '2' => 'dd-mm-yy',
            '3' => 'mm-dd-yy',
            '4' => 'dd-yy-mm',
            '5' => 'mm-yy-dd',

        ];

        wp_localize_script( 'wpk-wpestate-control', 'control_vars',
            [
                'searchtext'                    => esc_html__( 'SEARCH', 'wpestate' ),
                'searchtext2'                   => esc_html__( 'Search here...', 'wpestate' ),
                'path'                          => get_template_directory_uri(),
                'search_room'                   => esc_html__( 'Type Bedrooms No.', 'wpestate' ),
                'search_bath'                   => esc_html__( 'Type Bathrooms No.', 'wpestate' ),
                'search_min_price'              => esc_html__( 'Type Min. Price', 'wpestate' ),
                'search_max_price'              => esc_html__( 'Type Max. Price', 'wpestate' ),
                'contact_name'                  => esc_html__( 'Your Name', 'wpestate' ),
                'contact_email'                 => esc_html__( 'Your Email', 'wpestate' ),
                'contact_phone'                 => esc_html__( 'Your Phone', 'wpestate' ),
                'contact_comment'               => esc_html__( 'Your Message', 'wpestate' ),
                'zillow_addres'                 => esc_html__( 'Your Address', 'wpestate' ),
                'zillow_city'                   => esc_html__( 'Your City', 'wpestate' ),
                'zillow_state'                  => esc_html__( 'Your State Code (ex CA)', 'wpestate' ),
                'adv_contact_name'              => esc_html__( 'Your Name', 'wpestate' ),
                'adv_email'                     => esc_html__( 'Your Email', 'wpestate' ),
                'adv_phone'                     => esc_html__( 'Your Phone', 'wpestate' ),
                'adv_comment'                   => esc_html__( 'Your Message', 'wpestate' ),
                'adv_search'                    => esc_html__( 'Send Message', 'wpestate' ),
                'admin_url'                     => get_admin_url(),
                'login_redirect'                => $login_redirect,
                'login_loading'                 => esc_html__( 'Sending user info, please wait...', 'wpestate' ),
                'street_view_on'                => esc_html__( 'Street View', 'wpestate' ),
                'street_view_off'               => esc_html__( 'Close Street View', 'wpestate' ),
                'userid'                        => $userID,
                'show_adv_search_map_close'     => $show_adv_search_map_close,
                'close_map'                     => esc_html__( 'close map', 'wpestate' ),
                'open_map'                      => esc_html__( 'open map', 'wpestate' ),
                'fullscreen'                    => esc_html__( 'Fullscreen', 'wpestate' ),
                'default'                       => esc_html__( 'Default', 'wpestate' ),
                'addprop'                       => esc_html__( 'Please wait while we are processing your submission!', 'wpestate' ),
                'deleteconfirm'                 => esc_html__( 'Are you sure you wish to delete?', 'wpestate' ),
                'terms_cond'                    => esc_html__( 'You must to agree with terms and conditions!', 'wpestate' ),
                'slider_min'                    => floatval( get_option( 'wp_estate_show_slider_min_price', '' ) ),
                'slider_max'                    => floatval( get_option( 'wp_estate_show_slider_max_price', '' ) ),
                'bookconfirmed'                 => esc_html__( 'Booking request sent. Please wait for owner\'s confirmation!', 'wpestate' ),
                'bookdenied'                    => esc_html__( 'The selected period is already booked. Please choose a new one!', 'wpestate' ),
                'to'                            => esc_html__( 'to', 'wpestate' ),
                'curency'                       => esc_html( get_option( 'wp_estate_currency_label_main', '' ) ),
                'where_curency'                 => esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) ),
                'price_separator'               => esc_html( get_option( 'wp_estate_prices_th_separator', '' ) ),
                'datepick_lang'                 => esc_html( get_option( 'wp_estate_date_lang', '' ) ),
                'custom_price'                  => $custom_price,
                'booked_dates_formated'         => json_encode( get_post_meta( $post->ID, 'booked_dates_formated', true ) ),
                'booking_array'                 => $booking_array,
                'default_price'                 => $default_price,
                'transparent_logo'              => get_option( 'wp_estate_transparent_logo_image', '' ),
                'normal_logo'                   => get_option( 'wp_estate_logo_image', '' ),
                'cleaning_fee_per_day'          => $cleaning_fee_per_day,
                'city_fee_per_day'              => $city_fee_per_day,
                'price_per_guest_from_one'      => $price_per_guest_from_one,
                'checkin_change_over'           => $checkin_change_over,
                'checkin_checkout_change_over'  => $checkin_checkout_change_over,
                'min_days_booking'              => $min_days_booking,
                'extra_price_per_guest'         => $extra_price_per_guest,
                'price_per_weekeend'            => $price_per_weekeend,
                'setup_weekend_status'          => esc_html( get_option( 'wp_estate_setup_weekend', '' ) ),
                'mega_details'                  => $mega_details,
                'mindays'                       => esc_html__( 'The selected period is shorter than the minimum required period!', 'wpestate' ),
                'weekdays'                      => json_encode( $week_days_control ),
                'stopcheckin'                   => esc_html__( 'Check in date is not correct', 'wpestate' ),
                'stopcheckinout'                => esc_html__( 'Check in/Check out dates are not correct', 'wpestate' ),
                'from'                          => esc_html__( 'from', 'wpestate' ),
                'separate_users'                => esc_html( get_option( 'wp_estate_separate_users', '' ) ),
                'captchakey'                    => get_option( 'wp_estate_recaptha_sitekey', '' ),
                'usecaptcha'                    => get_option( 'wp_estate_use_captcha', '' ),
                'unavailable_check'             => esc_html__( 'Unavailable/Only Check Out', 'wpestate' ),
                'unavailable'                   => esc_html__( 'Unavailable', 'wpestate' ),
                'submission_curency'            => $submission_curency,
                'direct_price'                  => esc_html__( 'To be paid', 'wpestate' ),
                'send_invoice'                  => esc_html__( 'Send me the invoice', 'wpestate' ),
                'direct_pay'                    => $direct_payment_details,
                'direct_title'                  => esc_html__( 'Direct payment instructions', 'wpestate' ),
                'direct_thx'                    => esc_html__( 'Thank you. Please check your email for payment instructions.', 'wpestate' ),
                'guest_any'                     => esc_html__( 'any', 'wpestate' ),
                'my_reservations_url'           => wpestate_get_template_link( 'user_dashboard_my_reservations.php' ),
                'pls_wait'                      => esc_html__( 'processing, please wait...', 'wpestate' ),
                'wp_estate_book_down'           => $wp_estate_book_down,
                'wp_estate_book_down_fixed_fee' => $wp_estate_book_down_fixed_fee,
                'include_expeses'               => $include_expeses,
                'date_format'                   => $dates_types[ intval( get_option( 'wp_estate_date_format', '' ) ) ],


            ]
        );;

    }

    /**
     * @return void
     */
    public static function wpEstateLocalizeDashboardControl() {

        $wp_estate_book_down             = floatval( get_option( 'wp_estate_book_down', '' ) );
        $book_down_fixed_fee             = floatval( get_option( 'wp_estate_book_down_fixed_fee', '' ) );
        $wp_estate_service_fee_fixed_fee = floatval( get_option( 'wp_estate_service_fee_fixed_fee', '' ) );
        $wp_estate_service_fee           = floatval( get_option( 'wp_estate_service_fee', '' ) );

        wp_localize_script( 'wpk-wpestate-dashbord-control', 'dashboard_vars',
            [
                'deleting'              => esc_html__( 'deleting...', 'wpestate' ),
                'searchtext2'           => esc_html__( 'Search here...', 'wpestate' ),
                'currency_symbol'       => wpestate_curency_submission_pick(),
                'where_currency_symbol' => esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) ),
                'book_down'             => $wp_estate_book_down,
                'book_down_fixed_fee'   => $book_down_fixed_fee,
                'discount'              => esc_html__( 'Discount', 'wpestate' ),
                'delete_inv'            => esc_html__( 'Delete Invoice', 'wpestate' ),
                'issue_inv'             => esc_html__( 'Invoice Issued', 'wpestate' ),
                'confirmed'             => esc_html__( 'Confirmed', 'wpestate' ),
                'issue_inv1'            => esc_html__( 'Issue invoice', 'wpestate' ),
                'sending'               => esc_html__( 'sending message...', 'wpestate' ),
                'send_reply'            => esc_html__( 'Send Reply', 'wpestate' ),
                'plsfill'               => esc_html__( 'Please fill in all the fields', 'wpestate' ),
                'datesb'                => esc_html__( 'Dates are already booked. Please check the calendar for free days!', 'wpestate' ),
                'datepast'              => esc_html__( 'You cannot select a date in the past! ', 'wpestate' ),
                'bookingstart'          => esc_html__( 'Start date cannot be greater than end date !', 'wpestate' ),
                'selectprop'            => esc_html__( 'Please select a property !', 'wpestate' ),
                'err_title'             => esc_html__( 'Please submit a title !', 'wpestate' ),
                'err_category'          => esc_html__( 'Please pick a category !', 'wpestate' ),
                'err_type'              => esc_html__( 'Please pick a typr !', 'wpestate' ),
                'err_guest'             => esc_html__( 'Please select the guest no !', 'wpestate' ),
                'err_city'              => esc_html__( 'Please pick a city !', 'wpestate' ),
                'sending'               => esc_html__( 'sending...', 'wpestate' ),
                'doublebook'            => esc_html__( 'This period is already booked', 'wpestate' ),
                'deleted_feed'          => esc_html__( 'Delete imported dates', 'wpestate' ),
                'sent'                  => esc_html__( 'done', 'wpestate' ),
                'service_fee_fixed_fee' => $wp_estate_service_fee_fixed_fee,
                'service_fee'           => $wp_estate_service_fee,

            ]
        );

    }

}
