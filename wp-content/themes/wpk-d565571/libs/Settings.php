<?php


namespace Wpk\d565571;

/**
 * Manages settings using ACF plugin
 *
 * @author Przemysław Żydek
 */
class Settings {

    /** @var array Stores option args */
    protected $optionArgs = [];

    /** @var array Stores sub option menu args */
    protected $subOptionArgs = [];

    /** @var bool Determines if ACF plugin exists on site */
    public static $isAcf = false;

    /**
     * Settings constructor.
     */
    public function __construct() {

        self::$isAcf = function_exists( 'get_field' );

        if ( self::$isAcf ) {
            $this->optionArgs = [
                [
                    'page_title' => 'WPK Cardcom Settings',
                    'menu_title' => 'WPK Cardcom Settings',
                    'menu_slug'  => 'wpk_cardcom_settings',
                    'capability' => 'manage_options',
                ],
                [
                    'page_title' => 'WPK Search Settings',
                    'menu_title' => 'WPK Search Settings',
                    'menu_slug'  => 'wpk_search_settings',
                    'capability' => 'manage_options',
                ],
            ];

            $this->subOptionArgs = [
             /*   [
                    'page_title'  => '',
                    'menu_title'  => '',
                    'menu_slug'   => '',
                    'capability'  => 'manage_options',
                    'parent_slug' => '',
                ],*/
            ];
            add_action( 'init', [ $this, 'addOptionsPage' ] );
        }

    }


    /**
     * Add options pages
     */
    public function addOptionsPage() {

        foreach ( $this->optionArgs as $option_arg ) {
            acf_add_options_page( $option_arg );
        }

        foreach ( $this->subOptionArgs as $sub_option_arg ) {
            acf_add_options_sub_page( $sub_option_arg );
        }


    }

    /**
     * Perform conditional formatting for setting
     *
     * @param string $key
     * @param mixed  $value
     *
     * @return mixed
     */
    protected static function formatSetting( $key, $value ) {

        switch ( $key ) {

            default:
                return $value;

        }

    }

    /**
     * Get setting via its key
     *
     * @param string|int $key
     * @param mixed      $default Default value for option
     *
     * @return bool|mixed
     */
    public static function get( $key, $default = false ) {

        $value = self::getField( $key );
        $value = empty( $value ) ? $default : $value;


        return self::formatSetting( $key, $value );

    }

    /**
     * Update setting value
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return bool|int
     */
    public static function update( $key, $value ) {
        return self::updateField( $key, $value, 'option' );
    }

    /**
     * @param array $values
     *
     * @return bool
     */
    public static function updateArray( $values = [] ) {

        foreach ( $values as $key => $value ) {
            self::update( $key, $value );
        }

        return true;

    }

    /**
     * Helper function for accessing acf fields in options
     *
     * @param mixed  $key
     * @param string $prefix Option prefix
     *
     * @return mixed|false
     */
    public static function getField( $key, $prefix = 'wpk' ) {

        if ( ! self::$isAcf ) {
            return false;
        }

        return get_field( "{$prefix}_{$key}", 'option' );

    }

    /**
     * Helper function for updating ACF field value
     *
     * @param mixed  $key
     * @param mixed  $value
     * @param mixed  $postID
     * @param string $prefix
     *
     * @return bool|int
     */
    public static function updateField( $key, $value, $postID, $prefix = 'wpk' ) {

        if ( ! self::$isAcf ) {
            return false;
        }

        return update_field( "{$prefix}_{$key}", $value, $postID );

    }


}
