<?php


namespace Wpk\d565571\Interfaces;

/**
 * Converts object to json
 */
interface Jsonable {

    /**
     * @return string
     */
    public function toJson();

}