<?php


namespace Wpk\d565571\Interfaces;

/**
 * Converts object to array
 */
interface Arrayable {

    /**
     * @return array
     */
    public function toArray();


}