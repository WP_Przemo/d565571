<?php


namespace Wpk\d565571\Controllers;

use Wpk\d565571\Helpers\Response;
use Wpk\d565571\Models\Post;
use Wpk\d565571\Models\User;
use Wpk\d565571\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Controller {

    use Request;

    /** @var array Array with middleware classes to use */
    protected $middleware = [];

    /** @var Response */
    protected $response;

    /** @var User Stores current user */
    protected $user;

    /**
     * Controller constructor.
     */
    public function __construct() {

        $this->loadMiddleware();

        $this->response = new Response();
        $this->user     = User::current();

    }

    /**
     * Perform load of middleware modules
     *
     * @return void
     */
    protected function loadMiddleware() {

        foreach ( $this->middleware as $key => $middleware ) {
            $this->middleware[ $key ] = new $middleware();
        }

    }

    /**
     * Get controller middleware
     *
     * @param string $middleware
     *
     * @return bool|mixed
     */
    protected function middleware( $middleware ) {
        return isset( $this->middleware[ $middleware ] ) ? $this->middleware[ $middleware ] : false;
    }

}