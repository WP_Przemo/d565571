<?php

namespace Wpk\d565571\Controllers\Cardcom;

use Wpk\d565571\Api\Cardcom\Invoice;
use Wpk\d565571\Api\Cardcom\Storage;
use Wpk\d565571\Api\Cardcom\Validate;
use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Post;
use Wpk\d565571\Models\Property;
use Wpk\d565571\Models\User;
use Wpk\d565571\Utility;

/**
 * Handles and verifies cardcom payment
 *
 * @author Przemysław Żydek
 */
class PaymentSuccess extends Controller {

    /**
     * PaymentSuccess constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/d5657781/payment/successful', [ $this, 'verify' ] );
    }

    /**
     * Verifies whenever payment was completed successfuly
     *
     * @return void
     */
    public function verify() {

        $requiredKeys = [
            'terminalnumber',
            'lowprofilecode',
            'ResponseCode',
            'Operation',
            'Status',
        ];

        $redirectUrl = wpestate_get_template_link( 'user_dashboard_my_reservations.php' );

        $content = Post::find( get_the_ID() )->post_content;
        $content = apply_filters( 'the_content', $content );

        foreach ( $requiredKeys as $key ) {
            if ( ! array_key_exists( $key, $_GET ) ) {
                return;
            }
        }

        $responseCode   = (int) $this->getQueryParam( 'ResponseCode' );
        $lowProfileCode = $this->getQueryParam( 'lowprofilecode' );

        $paymentData = Storage::get( $this->user->ID );

        if ( empty( $paymentData ) ) {
            $this->response->render( 'payment.success', [
                'redirectUrl' => $redirectUrl,
                'content'     => $content,
            ] );

            return;
        }

        Storage::delete( $this->user->ID );

        if ( $responseCode === 0 && Validate::wasSuccessful( $lowProfileCode ) ) {

            $contextID   = $paymentData[ 'context_id' ];
            $redirectUrl = '';

            switch ( $paymentData[ 'action' ] ) {

                case 'deposit':
                case 'booking':
                    $this->handleBooking( $contextID );

                    $redirectUrl = get_permalink( Utility::getPageByTemplate( 'user_dashboard_my_reservations.php' ) );
                    break;

                case 'submit-property':
                case 'submit-property-featured':

                    $isFeatured = $paymentData[ 'action' ] === 'submit-property-featured';

                    $this->handlePropertySubmission( $contextID, $isFeatured );

                    $redirectUrl = get_permalink( Utility::getPageByTemplate( 'user_dashboard.php' ) );


                    break;

            }

            $this->response->render( 'payment.success', [
                'redirectUrl' => $redirectUrl,
                'content'     => $content,
            ] );

        }

    }

    /**
     * Handles property submission
     *
     * @param int  $propertyID
     * @param bool $isFeatured
     *
     * @return void
     */
    protected function handlePropertySubmission( $propertyID, $isFeatured = false ) {

        $property = Property::find( $propertyID );

        $paidSubmissionStatus  = esc_html( get_option( 'wp_estate_paid_submission', '' ) );
        $adminSubmissionStatus = esc_html( get_option( 'wp_estate_admin_submission', '' ) );

        $date = date( 'Y-m-d H:i:s' );

        if ( $paidSubmissionStatus == 'per listing' ) {

            $property->updateMeta( 'pay_status', 'paid' );

            if ( $property->post_status !== 'publish' && $adminSubmissionStatus === 'no' ) {

                $property = $property->status( 'publish' )->update();

                if ( $isFeatured ) {

                    $property->updateMeta( 'prop_featured', 1 );

                    $invoiceID = wpestate_insert_invoice(
                        'Publish Listing with Featured',
                        esc_html__( 'One Time', 'wpestate' ),
                        $propertyID,
                        $date,
                        $this->user->ID,
                        1,
                        0,
                        '' );

                    update_post_meta( $invoiceID, 'invoice_status', 'confirmed' );

                } else {

                    $invoiceID = wpestate_insert_invoice(
                        'Listing',
                        esc_html__( 'One Time', 'wpestate' ),
                        $propertyID,
                        $date,
                        $this->user->ID,
                        0,
                        0,
                        '' );

                    update_post_meta( $invoiceID, 'invoice_status', 'confirmed' );
                }

                wpestate_email_to_admin( 0 );

            } else if ( $isFeatured ) {

                $property->updateMeta( 'prop_featured', 1 );

                $invoiceID = wpestate_insert_invoice(
                    'Upgrade to Featured',
                    esc_html__( 'One Time', 'wpestate' ),
                    $propertyID,
                    $date,
                    $this->user->ID,
                    0,
                    1,
                    '' );

                update_post_meta( $invoiceID, 'invoice_status', 'confirmed' );

                wpestate_email_to_admin( 1 );

            }

        }

    }

    /**
     * Launches after deposit was paid successfuly
     *
     * @param int $bookingID
     *
     * @return void
     */
    protected function handleBooking( $bookingID ) {

        $booking   = Booking::find( $bookingID );
        $invoiceID = $booking->meta( 'booking_invoice_no' );
        $ownerID   = $booking->meta( 'buyer_id' );

        $owner      = $booking->getProperty()->getAuthor();
        $ownerEmail = $owner->user_email;
        $deposit    = (float) get_post_meta( $invoiceID, 'depozit_to_be_paid', true );

        wpestate_booking_mark_confirmed( $bookingID, $invoiceID, $ownerID, $deposit, $ownerEmail );
        wpestate_select_email_type( $ownerEmail, 'purchase_activated', [] );

    }

}
