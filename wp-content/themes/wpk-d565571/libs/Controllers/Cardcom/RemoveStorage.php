<?php

namespace Wpk\d565571\Controllers\Cardcom;

use Wpk\d565571\Api\Cardcom\Storage;
use Wpk\d565571\Controllers\Controller;

/**
 * @author Przemysław Żydek
 */
class RemoveStorage extends Controller {

    /**
     * RemoveStorage constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wp_ajax_wpk_remove_cardcom_storage', [ $this, 'handle' ] );
    }

    /**
     * @return void
     */
    public function handle() {

        Storage::delete( $this->user->ID );

        $this->response->setResult( true )->sendJson();

    }

}
