<?php

namespace Wpk\d565571\Controllers\Cardcom;

use Wpk\d565571\Api\Cardcom\Iframe as CardcomIframe;
use Wpk\d565571\Api\Cardcom\Storage;
use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;

/**
 * Handles fetching cardcom iframe URL
 *
 * @author Przemysław Żydek
 */
class Iframe extends Controller {

    /**
     * CardcomIframe constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wp_ajax_wpk_get_cardcom_iframe_url', [ $this, 'handle' ] );
    }

    /**
     * @return void
     */
    public function handle() {

        $response = $this->response;

        $paymentAction = $this->getPostParam( 'payment_action' );
        $bookingID     = (int) $this->getPostParam( 'booking_id' );

        if ( ! is_user_logged_in() || empty( $paymentAction ) ) {
            $response->sendJson();
        }

        if ( in_array( $paymentAction, [ 'deposit', 'booking' ] ) ) {
            $this->handleBooking( $bookingID, $paymentAction );
        } else if ( $paymentAction === 'submit-property' ) {

            $propertyID = (int) $this->getPostParam( 'property_id' );
            $isFeatured = (bool) $this->getPostParam( 'is_featured', false );

            $this->handleSubmitProperty( $propertyID, $paymentAction, $isFeatured );

        }

        $response->addError(
            esc_html__( 'Invalid payment action', 'wpk' ),
            'alert',
            true );

    }

    /**
     * Handle payment for property submission
     *
     * @param int    $propertyID
     * @param string $paymentAction
     * @param bool   $isFeatured
     *
     * @return void
     */
    protected function handleSubmitProperty( $propertyID, $paymentAction, $isFeatured = false ) {

        $response = $this->response;

        $property = Property::find( $propertyID );
        $response->validateAuthor( $this->user, $property );

        $price = (float) get_option( 'wp_estate_price_submission', '' );

        $description = $property->post_status !== 'publish' ? esc_html__( 'Publish listing', 'wpk' ) : esc_html__( 'Listing', 'wpk' );

        if ( $isFeatured ) {
            $paymentAction = 'submit-property-featured';

            $description .= esc_html__( ' with featured', 'wpk' );
        }

        Storage::set( $this->user->ID, $propertyID, $paymentAction );

        if ( $isFeatured ) {
            $featuredPrice = (float) get_option( 'wp_estate_price_featured_submission', 0 );

            $price += $featuredPrice;
        }

        $url = CardcomIframe::getUrl( $price, $description );

        $response->setResult( $url )->sendJson();

    }

    /**
     * Handles booking payment actions
     *
     * @param int    $bookingID
     * @param string $paymentAction
     *
     * @return void
     */
    protected function handleBooking( $bookingID, $paymentAction ) {

        $response = $this->response;

        $url = '';

        $booking = Booking::find( $bookingID );

        $response->validateAuthor( $this->user, $booking );

        if ( in_array( $paymentAction, [ 'deposit', 'booking' ] ) ) {
            Storage::set( $this->user->ID, $bookingID, $paymentAction );
        }

        if ( $paymentAction === 'deposit' ) {
            $price = (float) $booking->meta( 'to_be_paid' );

            $title = sprintf(
                esc_html__( 'Reservation deposit for booking for property %s on : %s - %s', 'wpk' ),
                $booking->getProperty()->post_title,
                $booking->getStartDate()->format( 'd-m-Y H:i:s' ),
                $booking->getEndDate()->format( 'd-m-Y H:i:s' )
            );

            $url = CardcomIframe::getUrl( $price, $title );
        } else if ( $paymentAction === 'booking' ) {

            $toBePaid   = (float) $booking->meta( 'to_be_paid' );
            $totalPrice = (float) $booking->meta( 'total_price' );

            $title = sprintf(
                esc_html__( 'Reservation deposit for booking for property %s on : %s - %s', 'wpk' ),
                $booking->getProperty()->post_title,
                $booking->getStartDate()->format( 'd-m-Y H:i:s' ),
                $booking->getEndDate()->format( 'd-m-Y H:i:s' )
            );

            $price = $totalPrice - $toBePaid;

            $url = CardcomIframe::getUrl( $price, $title );

        }

        $response->setResult( $url )->sendJson();

    }

}
