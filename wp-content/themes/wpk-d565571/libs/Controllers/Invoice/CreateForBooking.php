<?php

namespace Wpk\d565571\Controllers\Invoice;

use Wpk\d565571\Api\Cardcom\Client;
use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Booking as BookingModel;
use Wpk\d565571\Models\Invoice;

/**
 * Handles creating new invoice for booking
 *
 * @author Przemysław Żydek
 */
class CreateForBooking extends Controller {

    /**
     * CreateForBooking constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/d5657781/bookingCreated', [ $this, 'handle' ], 10, 2 );
    }

    /**
     * @param BookingModel $booking
     * @param array        $prices
     *
     * @return void
     */
    public function handle( BookingModel $booking, $prices ) {

        $property = $booking->getProperty();

        //Don't generate invoices for bookings created from google events
        if ( ! empty( $booking->meta( 'google_event_id' ) ||
                      ! $property->meta( 'instant_booking' ) ) ) {
            return;
        }

        $userID     = $this->user->ID;
        $booking_id = $booking->ID;

        $extraOptionsArray = $booking->meta( 'extra_options' );

        $bookingDatesFormated = $property->meta( 'booked_dates_formated' );

        /* WP Rentals part. Blame developers for spaghetti code. Yummy! */

        $currency      = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
        $whereCurrency = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );

        $details[] = [ esc_html__( 'Subtotal', 'wpestate' ), $prices[ 'inter_price' ] ];
        if ( is_array( $extraOptionsArray ) && ! empty ( $extraOptionsArray ) ) {

            $extra_pay_options = $property->meta( 'extra_pay_options' );

            $options_array_explanations = [
                0 => esc_html__( 'Single Fee', 'wpestate' ),
                1 => esc_html__( 'Per Hour', 'wpestate' ),
                2 => esc_html__( 'Per Guest', 'wpestate' ),
                3 => esc_html__( 'Per Hour per Guest', 'wpestate' ),
            ];

            foreach ( $extraOptionsArray as $key => $value ) {

                if ( isset( $extra_pay_options[ $value ][ 0 ] ) ) {

                    $value_computed = wpestate_calculate_extra_options_value( $prices[ 'count_hours' ], $booking->meta( 'booking_guests' ), $extra_pay_options[ $value ][ 2 ], $extra_pay_options[ $value ][ 1 ] );

                    $extra_option_value_show_single = wpestate_show_price_booking_for_invoice( $extra_pay_options[ $value ][ 1 ], $currency, $whereCurrency, 0, 1 );

                    $temp_array = [
                        $extra_pay_options[ $value ][ 0 ],
                        $value_computed,
                        $extra_option_value_show_single . ' ' . $options_array_explanations [ $extra_pay_options[ $value ][ 2 ] ],
                    ];
                    $details[]  = $temp_array;

                }

            }

        }


        if ( intval( $prices[ 'security_deposit' ] ) != 0 ) {
            $sec_array = [ __( 'Security Deposit', 'wpestate' ), $prices[ 'security_deposit' ] ];
            $details[] = $sec_array;
        }

        if ( intval( $prices[ 'early_bird_discount' ] ) != 0 ) {
            $sec_array = [ __( 'Early Bird Discount', 'wpestate' ), $prices[ 'early_bird_discount' ] ];
            $details[] = $sec_array;
        }


        $billing_for = esc_html__( 'Reservation fee', 'wpestate' );
        $type        = esc_html__( 'One Time', 'wpestate' );

        $time    = time();
        $date    = date( 'Y-m-d H:i:s', $time );
        $user_id = wpse119881_get_author( $booking_id );

        $is_featured   = '';
        $is_upgrade    = '';
        $paypal_tax_id = '';


        $invoice_id = wpestate_booking_insert_invoice( $billing_for, $type, $booking_id, $date, $user_id, $is_featured, $is_upgrade, $paypal_tax_id, $details, $prices[ 'total_price' ], $booking->post_author );

        if ( $booking->post_author !== $property->post_author ) {

            $meta = [
                'booking_status'     => 'waiting',
                'booking_invoice_no' => $invoice_id,
            ];

            $booking->updateMetas( $meta );
            rcapi_edit_booking( $booking_id, $booking->getRcapiBookingID(), $meta );

        }

        //update invoice data
        update_post_meta( $invoice_id, 'early_bird_percent', 0 );
        update_post_meta( $invoice_id, 'early_bird_days', 0 );
        update_post_meta( $invoice_id, 'booking_taxes', $property->getTaxes() );
        update_post_meta( $invoice_id, 'booking_taxes', $prices[ 'taxes' ] );
        update_post_meta( $invoice_id, 'service_fee', $prices[ 'service_fee' ] );
        update_post_meta( $invoice_id, 'youearned', $prices[ 'youearned' ] );
        update_post_meta( $invoice_id, 'depozit_to_be_paid', $prices[ 'deposit' ] );
        update_post_meta( $invoice_id, 'item_price', $prices[ 'total_price' ] );
        update_post_meta( $invoice_id, 'custom_price_array', $prices[ 'custom_price_array' ] );
        update_post_meta( $invoice_id, 'balance', $prices[ 'balance' ] );
        update_post_meta( $invoice_id, 'invoice_percent', floatval( get_option( 'wp_estate_book_down', '' ) ) );
        update_post_meta( $invoice_id, 'invoice_percent_fixed_fee', floatval( get_option( 'wp_estate_book_down_fixed_fee', '' ) ) );

        // send notifications
        $receiver       = get_userdata( $booking->post_author );
        $receiver_email = $receiver->user_email;
        $to             = $user_id;
        $subject        = esc_html__( 'New Invoice', 'wpestate' );
        $description    = esc_html__( 'A new invoice was generated for your booking request', 'wpestate' );

        wpestate_add_to_inbox( $userID, $userID, $to, $subject, $description, 1 );
        wpestate_send_booking_email( 'newinvoice', $receiver_email );

        $currency       = esc_html( get_post_meta( $invoice_id, 'invoice_currency', true ) );
        $where_currency = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
        $default_price  = get_post_meta( $invoice_id, 'default_price', true );

        $booking_from_date = esc_html( get_post_meta( $booking_id, 'booking_from_date', true ) );
        $property_id       = esc_html( get_post_meta( $booking_id, 'booking_id', true ) );

        $booking_to_date = esc_html( get_post_meta( $booking_id, 'booking_to_date', true ) );
        $booking_guests  = floatval( get_post_meta( $booking_id, 'booking_guests', true ) );

        if ( $prices[ 'numberDays' ] >= 7 && $prices[ 'numberDays' ] < 30 ) {
            if ( $prices[ 'week_price' ] > 0 ) {
                $default_price = $prices[ 'week_price' ];
            }
        } else if ( $prices[ 'numberDays' ] >= 30 ) {
            if ( $prices[ 'month_price' ] > 0 ) {
                $default_price = $prices[ 'month_price' ];
            }
        }

        $wp_estate_book_down           = get_post_meta( $invoice_id, 'invoice_percent', true );
        $wp_estate_book_down_fixed_fee = get_post_meta( $invoice_id, 'invoice_percent_fixed_fee', true );
        $invoice_price                 = floatval( get_post_meta( $invoice_id, 'item_price', true ) );

        $include_expeses = esc_html( get_option( 'wp_estate_include_expenses', '' ) );

        if ( $include_expeses == 'yes' ) {
            $total_price_comp = $invoice_price;
        } else {
            $total_price_comp = $invoice_price - $prices[ 'city_fee' ] - $prices[ 'cleaning_fee' ];
        }

        $depozit = wpestate_calculate_deposit( $wp_estate_book_down, $wp_estate_book_down_fixed_fee, $total_price_comp );

        $balance = $invoice_price - $depozit;

        $price_per_weekeend = floatval( get_post_meta( $property_id, 'price_per_weekeend', true ) );

        $price_show              = wpestate_show_price_booking_for_invoice( $default_price, $currency, $where_currency, 0, 1 );
        $price_per_weekeend_show = wpestate_show_price_booking_for_invoice( $price_per_weekeend, $currency, $where_currency, 0, 1 );
        $total_price_show        = wpestate_show_price_booking_for_invoice( $invoice_price, $currency, $where_currency, 0, 1 );
        $depozit_show            = wpestate_show_price_booking_for_invoice( $depozit, $currency, $where_currency, 0, 1 );
        $balance_show            = wpestate_show_price_booking_for_invoice( $balance, $currency, $where_currency, 0, 1 );
        $city_fee_show           = wpestate_show_price_booking_for_invoice( $prices[ 'city_fee' ], $currency, $where_currency, 0, 1 );
        $cleaning_fee_show       = wpestate_show_price_booking_for_invoice( $prices[ 'cleaning_fee' ], $currency, $where_currency, 0, 1 );
        $inter_price_show        = wpestate_show_price_booking_for_invoice( $prices[ 'inter_price' ], $currency, $where_currency, 0, 1 );
        $total_guest             = wpestate_show_price_booking_for_invoice( $prices[ 'total_extra_price_per_guest' ], $currency, $where_currency, 1, 1 );
        $guest_price             = wpestate_show_price_booking_for_invoice( $prices[ 'extra_price_per_guest' ], $currency, $where_currency, 1, 1 );
        $extra_price_per_guest   = wpestate_show_price_booking( $prices[ 'extra_price_per_guest' ], $currency, $where_currency, 1 );


        $depozit_stripe = $depozit * 100;
        $details        = get_post_meta( $invoice_id, 'renting_details', true );


        // strip details generation
        require_once get_template_directory() . '/libs/stripe/lib/Stripe.php';
        $stripe_secret_key      = esc_html( get_option( 'wp_estate_stripe_secret_key', '' ) );
        $stripe_publishable_key = esc_html( get_option( 'wp_estate_stripe_publishable_key', '' ) );

        $stripe = [
            "secret_key"      => $stripe_secret_key,
            "publishable_key" => $stripe_publishable_key,
        ];

        \Stripe::setApiKey( $stripe[ 'secret_key' ] );

        $pages = get_pages( [
            'meta_key'   => '_wp_page_template',
            'meta_value' => 'stripecharge.php',
        ] );

        if ( $pages ) {
            $processor_link = esc_url( get_permalink( $pages[ 0 ]->ID ) );
        } else {
            $processor_link = esc_html( home_url() );
        }

        $booking_array = $prices;
        $html          = '';

        //This is where te fun begins.

        $html .= '              
            <div class="create_invoice_form wpk">
                   <h3>' . esc_html__( 'Invoice INV', 'wpestate' ) . $invoice_id . '</h3>

                   <div class="invoice_table">
                        <div class="invoice_data">
                                <div class="date_interval">
                                    <div class="invoice_data_legend">
                                        ' . esc_html__( 'Period', 'wpestate' ) . ' : 
                                    </div>
                                    <div style="width: 100%; float: none; clear: both;">
                                        '.esc_html__('From '). wpestate_convert_dateformat_reverse( $booking_from_date ).'
                                    </div>
                                    <div style="width: 100%; float: none;">
                                     '.esc_html__('To '). wpestate_convert_dateformat_reverse( $booking_to_date ).'
                                    </div>
                            <div>
                              
                            </div>
                                </div>
                                <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'No of hours', 'wpestate' ) . ': </span>' . $booking_array[ 'count_hours' ] . '</span>
                                <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'No of guests', 'wpestate' ) . ': </span>' . $booking_guests . '</span>';
        if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
            $html .= '    
                                    <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Price per Guest', 'wpestate' ) . ': </span>';
            $html .= $extra_price_per_guest;
            $html .= '</span>';
        } else {
            $html .= '    
                                    <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Price per hour', 'wpestate' ) . ': </span>';
            $html .= $price_show;
            if ( $booking_array[ 'has_custom' ] ) {
                $html .= ', ' . esc_html__( 'has custom price', 'wpestate' );
            }
            if ( $booking_array[ 'cover_weekend' ] ) {
                $html .= ', ' . esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
            }
            $html .= '</span>';
        }
        if ( $booking_array[ 'has_custom' ] ) {
            $html .= '<span class="invoice_data_legend">' . __( 'Price details:', 'wpestate' ) . '</span>';
            foreach ( $booking_array[ 'custom_price_array' ] as $date => $price ) {


                $day_price = wpestate_show_price_booking_for_invoice( $price, $currency, $where_currency, 1, 1 );
                $html      .= '<span class="price_custom_explained">' . __( 'on', 'wpestate' ) . ' ' . wpestate_convert_dateformat_reverse( date( "Y-m-d", $date ) ) . ' ' . __( 'price is', 'wpestate' ) . ' ' . $day_price . '</span>';


            }
        }


        $html .= '    
                        </div>

                        <div class="invoice_details">
                            <div class="invoice_row header_legend">
                               <span class="inv_legend">' . esc_html__( 'Cost', 'wpestate' ) . '</span>
                               <span class="inv_data">  ' . esc_html__( 'Price', 'wpestate' ) . '</span>
                               <span class="inv_exp">   ' . esc_html__( 'Detail', 'wpestate' ) . '</span>
                            </div>';

        $html .= '
                        <div class="invoice_row invoice_content">
                            <span class="inv_legend">   ' . esc_html__( 'Subtotal', 'wpestate' ) . '</span>
                            <span class="inv_data">   ' . $inter_price_show . '</span>';

        if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
            $html .= $extra_price_per_guest . ' x ' . $booking_array[ 'count_days' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' x ' . $booking_array[ 'curent_guest_no' ] . ' ' . esc_html__( 'guests', 'wpestate' );
        } else {
            if ( $booking_array[ 'cover_weekend' ] ) {
                $new_price_to_show = esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
            } else {
                if ( $booking_array[ 'has_custom' ] ) {
                    $new_price_to_show = esc_html__( "custom price", "wpestate" );
                } else {
                    $new_price_to_show = $price_show . ' ' . esc_html__( 'per hour', 'wpestate' );
                }
            }


            if ( $booking_array[ 'count_hours' ] == 1 ) {
                $html .= ' <span class="inv_exp">   (' . $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hour', 'wpestate' ) . ' | ' . $new_price_to_show . ') </span>';
            } else {
                $html .= ' <span class="inv_exp">   (' . $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' | ' . $new_price_to_show . ') </span>';
            }
        }

        if ( $booking_array[ 'custom_period_quest' ] == 1 ) {
            esc_html_e( " period with custom price per guest", "wpestate" );
        }

        $html .= '</div>';

        // $html .=_r($booking_array);

        if ( $booking_array[ 'has_guest_overload' ] != 0 && $booking_array[ 'total_extra_price_per_guest' ] != 0 ) {
            $html      .= '
                                <div class="invoice_row invoice_content">
                                    <span class="inv_legend">   ' . esc_html__( 'Extra Guests', 'wpestate' ) . '</span>
                                    <span class="inv_data" id="extra-guests" data-extra-guests="' . $booking_array[ 'total_extra_price_per_guest' ] . '">  ' . $total_guest . '</span>
                                    <span class="inv_exp">   (' . $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' | ' . $booking_array[ 'extra_guests' ] . ' ' . esc_html__( 'extra guests', 'wpestate' ) . ' ) </span>
                                </div>';
            $details[] = [ esc_html__( 'Extra Guests' ), $booking_array[ 'total_extra_price_per_guest' ] ];
        }


        if ( $booking_array[ 'cleaning_fee' ] != 0 && $booking_array[ 'cleaning_fee' ] != '' ) {
            $html .= '
                               <div class="invoice_row invoice_content">
                                   <span class="inv_legend">   ' . esc_html__( 'Cleaning fee', 'wpestate' ) . '</span>
                                   <span class="inv_data" id="cleaning-fee" data-cleaning-fee="' . $booking_array[ 'cleaning_fee' ] . '">  ' . $cleaning_fee_show . '</span>
                               </div>';
        }
        $details[] = [ esc_html__( 'Cleaning fee', 'wpestate' ), $booking_array[ 'cleaning_fee' ] ];


        if ( $booking_array[ 'city_fee' ] != 0 && $booking_array[ 'city_fee' ] != '' ) {
            $html .= '
                               <div class="invoice_row invoice_content">
                                   <span class="inv_legend">   ' . esc_html__( 'City fee', 'wpestate' ) . '</span>
                                   <span class="inv_data" id="city-fee" data-city-fee="' . $booking_array[ 'city_fee' ] . '">  ' . $city_fee_show . '</span>
                               </div>';
        }
        $details[] = [ esc_html__( 'City fee', 'wpestate' ), $booking_array[ 'city_fee' ] ];

        update_post_meta( $invoice_id, 'renting_details', $details );


        $options_array_explanations = [
            0 => esc_html__( 'Single Fee', 'wpestate' ),
            1 => esc_html__( 'Per Hour', 'wpestate' ),
            2 => esc_html__( 'Per Guest', 'wpestate' ),
            3 => esc_html__( 'Per Hour per Guest', 'wpestate' ),
        ];

        $extra_options_array = $booking->meta( 'extra_options' );

//extra_options$extra_options_array
        foreach ( $extra_options_array as $key => $value ) {
            if ( isset( $extra_pay_options[ $value ][ 0 ] ) ) {
                $extra_option_value             = wpestate_calculate_extra_options_value( $booking_array[ 'count_days' ], $booking_guests, $extra_pay_options[ $value ][ 2 ], $extra_pay_options[ $value ][ 1 ] );
                $extra_option_value_show        = wpestate_show_price_booking_for_invoice( $extra_option_value, $currency, $where_currency, 1, 1 );
                $extra_option_value_show_single = wpestate_show_price_booking_for_invoice( $extra_pay_options[ $value ][ 1 ], $currency, $where_currency, 0, 1 );

                $html .= '
                                    <div class="invoice_row invoice_content">
                                        <span class="inv_legend">   ' . $extra_pay_options[ $value ][ 0 ] . '</span>
                                        <span class="inv_data">  ' . $extra_option_value_show . '</span>
                                        <span class="inv_data">' . $extra_option_value_show_single . ' ' . $options_array_explanations[ $extra_pay_options[ $value ][ 2 ] ] . '</span>
                                    </div>';
            }
        }


        if ( $booking_array[ 'security_deposit' ] != 0 ) {
            $security_depozit_show = wpestate_show_price_booking_for_invoice( $booking_array[ 'security_deposit' ], $currency, $where_currency, 1, 1 );
            $html                  .= '
                                <div class="invoice_row invoice_content">
                                    <span class="inv_legend">   ' . __( 'Security Deposit', 'wpestate' ) . '</span>
                                    <span class="inv_data">  ' . $security_depozit_show . '</span>
                                    <span class="inv_data">' . __( '*refundable', 'wpestate' ) . '</span>
                                </div>';
        }


        if ( $booking_array[ 'early_bird_discount' ] > 0 ) {
            $early_bird_discount_show = wpestate_show_price_booking_for_invoice( $booking_array[ 'early_bird_discount' ], $currency, $where_currency, 1, 1 );
            $html                     .= '
                                <div class="invoice_row invoice_content">
                                    <span class="inv_legend">   ' . __( 'Early Bird Discount', 'wpestate' ) . '</span>
                                    <span class="inv_data">  ' . $early_bird_discount_show . '</span>
                                    <span class="inv_data"></span>
                                </div>';
        }


        $html .= ' 
                            <div class="invoice_row invoice_total total_inv_span total_invoice_for_payment">
                               <span class="inv_legend"><strong>' . esc_html__( 'Total', 'wpestate' ) . '</strong></span>
                               <span class="inv_data" id="total_amm" data-total="' . $invoice_price . '">' . $total_price_show . '</span></br>
                               
                               <span class="inv_legend">' . esc_html__( 'Reservation Fee Required', 'wpestate' ) . ':</span> <span class="inv_depozit depozit_show" data-value="' . $depozit . '"> ' . $depozit_show . '</span></br>
                               <span class="inv_legend">' . esc_html__( 'Balance Owing', 'wpestate' ) . ':</span> <span class="inv_depozit balance_show"  data-value="' . $balance . '">' . $balance_show . '</span>
                           </div>
                       </div>';

        $submission_curency_status = esc_html( get_option( 'wp_estate_submission_curency', '' ) );


        if ( floatval( $depozit ) == 0 ) {
            $html .= '<span id="confirm_zero_instant_booking"   data-propid="' . $property_id . '" data-bookid="' . $booking_id . '" data-invoiceid="' . $invoice_id . '">' . esc_html__( 'Confirm Booking - Nothing To Pay', 'wpestate' ) . '</span>';

        } else {
            $is_paypal_live = esc_html( get_option( 'wp_estate_enable_paypal', '' ) );
            $is_stripe_live = esc_html( get_option( 'wp_estate_enable_stripe', '' ) );

            $html .= '<span class="pay_notice_booking">' . esc_html__( 'Pay Deposit & Confirm Reservation', 'wpestate' ) . '</span>';
            if ( $is_stripe_live == 'yes' ) {
                $html .= ' 
                            <form action="' . $processor_link . '" method="post" class="booking_form_stripe">
                                <script src="https://checkout.stripe.com/checkout.js" 
                                class="stripe-button"
                                data-locale="auto"
                                data-key="' . $stripe[ 'publishable_key' ] . '"
                                data-amount="' . $depozit_stripe . '" 
                                data-zip-code="true"
                                data-email="' . $user_email . '"
                                data-currency="' . $submission_curency_status . '"
                                data-label="' . esc_html__( 'Pay with Credit Card', 'wpestate' ) . '"
                                data-description="Reservation Payment">
                                </script>
                                <input type="hidden" name="booking_id" value="' . $booking_id . '">
                                <input type="hidden" name="invoice_id" value="' . $invoice_id . '">
                                <input type="hidden" name="userID" value="' . $userID . '">
                                <input type="hidden" name="depozit" value="' . $depozit_stripe . '">
                            </form>';
            }
            if ( $is_paypal_live == 'yes' ) {
                $html .= '<span id="paypal_booking" data-deposit="' . $depozit . '"  data-propid="' . $property_id . '" data-bookid="' . $booking_id . '" data-invoiceid="' . $invoice_id . '">' . esc_html__( 'Pay with Paypal', 'wpestate' ) . '</span>';
            }

            if ( Client::hasFilledSettings() ) {
                $html .= '<button class="wpk-cardcom-payment" data-payment-action="deposit" data-propid="' . $property_id . '" data-bookid="' . $booking_id . '" data-invoiceid="' . $invoice_id . '">' . esc_html__( 'Pay with Cardcom', 'wpestate' ) . '</button>';
            }

            $enable_direct_pay = esc_html( get_option( 'wp_estate_enable_direct_pay', '' ) );

            if ( $enable_direct_pay == 'yes' ) {
                //  $html .= '<span id="direct_pay_booking" data-propid="'.$property_id.'" data-bookid="'.$booking_id.'" data-invoiceid="'.$invoice_id.'">'.esc_html__( 'Wire Transfer','wpestate').'</span>';
            }
        }
        $html .= '
                  </div>


            </div>';

        $rcapi_booking_id = get_post_meta( $booking_id, 'rcapi_booking_id', true );

        //Store the html in booking meta so that we can show it in response in Property@Booking controller
        $booking->updateMeta( 'invoice_html', $html );

        $invoice_details = [
            "invoice_status"            => "issued",
            "purchase_date"             => $date,
            "buyer_id"                  => $userID,
            "item_price"                => $booking_array[ 'total_price' ],
            "rcapi_booking_id"          => $rcapi_booking_id,
            "orignal_invoice_id"        => $invoice_id,
            "billing_for"               => $billing_for,
            "type"                      => $type,
            "pack_id"                   => $booking_id,
            "date"                      => $date,
            "user_id"                   => $user_id,
            "is_featured"               => $is_featured,
            "is_upgrade"                => $is_upgrade,
            "paypal_tax_id"             => $paypal_tax_id,
            "details"                   => $details,
            "price"                     => $booking_array[ 'total_price' ],
            "to_be_paid"                => $booking_array[ 'deposit' ],
            "submission_curency_status" => $submission_curency_status,
            "bookid"                    => $booking_id,
            //Still not clear what "author_id" should store...
            "author_id"                 => $booking->post_author,
            "youearned"                 => $booking_array[ 'youearned' ],
            "service_fee"               => $booking_array[ 'service_fee' ],
            "booking_taxes"             => $booking_array[ 'taxes' ],
            "security_deposit"          => $booking_array[ 'security_deposit' ],
            "renting_details"           => $details,
            "custom_price_array"        => $booking_array[ 'custom_price_array' ],
            "balance"                   => $booking_array[ 'balance' ],
        ];

        if ( $booking_array[ 'balance' ] > 0 ) {
            update_post_meta( $invoice_id, 'invoice_status_full', 'waiting' );
            $invoice_details[ 'invoice_status_full' ] = 'waiting';
        }

        if ( $booking_array[ 'balance' ] == 0 ) {
            update_post_meta( $invoice_id, 'is_full_instant', 1 );
            update_post_meta( $booking_id, 'is_full_instant', 1 );
        }


        if ( $userID != $property->post_author ) {
            rcapi_invoice_booking( $invoice_id, $invoice_details );
        }

    }

}
