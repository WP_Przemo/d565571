<?php

namespace Wpk\d565571\Controllers\Property;

use Wpk\d565571\Api\Google\Calendar;
use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;

/**
 * @author Przemysław Żydek
 */
class DisconnectGoogleCalendar extends Controller {

    /**
     * DisconnectGoogleCalendar constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wp_ajax_wpk_disconnect_google_calendar', [ $this, 'handle' ] );
    }

    /**
     * @return void
     */
    public function handle() {

        $propertyID = (int) $this->getPostParam( 'property_id' );
        $response   = $this->response;

        $response->checkNonce( 'wpk_disconnect_google_calendar', 'nonce' );

        if ( empty( $propertyID ) ) {
            $response->addError( esc_html__( 'No property provided', 'wpk' ), 'alert', true );
        }

        $property = Property::find( $propertyID );

        $response->validateAuthor( $this->user, $property );

        $property->disconnectGoogleCalendar();

        $response->setResult( true )->sendJson();

    }

}
