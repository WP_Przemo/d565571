<?php


namespace Wpk\d565571\Controllers\Property;

use Wpk\d565571\Controllers\Controller;
use \Wpk\d565571\Models\Booking as BookingModel;

/**
 * Handles property reservations array
 *
 * @author Przemysław Żydek
 */
class ReservationsArray extends Controller {

    /**
     * ReservationsArray constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/d5657781/bookingCreated', [ $this, 'refresh' ] );
        add_action( 'wpk/d5657781/bookingCreatedInternal', [ $this, 'refresh' ] );
    }

    /**
     * On certain action refresh property reservation array
     *
     * @param BookingModel $booking
     */
    public function refresh( BookingModel $booking ) {
        $booking->getProperty()->updateReservationArray();
    }

}