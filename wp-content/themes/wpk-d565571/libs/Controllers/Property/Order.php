<?php

namespace Wpk\d565571\Controllers\Property;

use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Helpers\WpRentals\Order as PostsOrder;

/**
 * @author Przemysław Żydek
 */
class Order extends Controller {

    /**
     * Order constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'wp_ajax_wpk_order_search', [ $this, 'handle' ] );
        add_action( 'wp_ajax_nopriv_wpk_order_search', [ $this, 'handle' ] );

    }

    /**
     * @return void
     */
    public function handle() {

        $order = (int) $this->getPostParam( 'order' );

        $page    = (int) $this->getPostParam( 'page', 1 );
        $perPage = $order === 7 ? - 1 : intval( get_option( 'wp_estate_prop_no', '' ) );

        $IDs = explode( ',', $this->getPostParam( 'ids' ) );

        $order = (int) wp_kses( $_POST[ 'order' ], $allowed_html );
        switch ( $order ) {
            case 1:
                $metaOrder      = 'property_price';
                $metaDirections = 'DESC';
                break;
            case 2:
                $metaOrder      = 'property_price';
                $metaDirections = 'ASC';
                break;
            case 3:
                $metaOrder      = 'property_size';
                $metaDirections = 'DESC';
                break;
            case 4:
                $metaOrder      = 'property_size';
                $metaDirections = 'ASC';
                break;
            case 5:
                $metaOrder      = 'property_bedrooms';
                $metaDirections = 'DESC';
                break;
            case 6:
                $metaOrder      = 'property_bedrooms';
                $metaDirections = 'ASC';
                break;
            default:
                $metaOrder      = 'prop_featured';
                $metaDirections = 'DESC';
        }

        $args = [
            'post__in'       => $IDs,
            'orderby'        => 'meta_value',
            'meta_key'       => $metaOrder,
            'order'          => $metaDirections,
            'post_type'      => 'estate_property',
            'paged'          => $page,
            'posts_per_page' => $perPage,
        ];

        $posts = new \WP_Query( $args );

        //Custom ordering by distance
        if ( $order == 7 ) {

            $userLat = (float) $_POST[ 'user_lat' ];
            $userLng = (float) $_POST[ 'user_lng' ];

            $posts = PostsOrder::byUserDistance( $userLat, $userLng, $page, $posts );
        }

    }

    /**
     * @return string
     */
    protected function generateHTML() {

    }

}
