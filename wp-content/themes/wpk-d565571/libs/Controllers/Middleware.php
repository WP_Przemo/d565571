<?php

namespace Wpk\d565571\Controllers;

use Wpk\d565571\Traits\Request;

/**
 * @author Przemysław Żydek
 */
abstract class Middleware {

    use Request;

}