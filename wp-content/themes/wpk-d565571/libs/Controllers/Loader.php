<?php

namespace Wpk\d565571\Controllers;

/**
 * Loads our controllers
 *
 * @author Przemysław Żydek
 */
class Loader {

    /** @var bool */
    protected static $loaded = false;

    /** @var array */
    protected static $controllers = [
        Booking\PropertyForm::class,
        Booking\Dates::class,
        Booking\Internal::class,
        Booking\GoogleCalendar::class,
        Booking\Removal::class,
        Property\ReservationsArray::class,
        Property\DisconnectGoogleCalendar::class,
        Property\Order::class,
        Invoice\CreateForBooking::class,
        Cardcom\Iframe::class,
        Cardcom\PaymentSuccess::class,
        Cardcom\RemoveStorage::class,
    ];

    /**
     * @return void
     */
    public static function load() {

        if ( ! self::$loaded ) {

            foreach ( self::$controllers as $controller ) {
                new $controller();
            }

            self::$loaded = true;

        }

    }

}
