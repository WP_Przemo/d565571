<?php

namespace Wpk\d565571\Controllers\Booking;

use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Helpers\DateTime;
use Wpk\d565571\Models\Property;
use \Wpk\d565571\Models\Booking as BookingModel;

class PropertyForm extends Controller {

    /**
     * Booking constructor.
     */
    public function __construct() {

        parent::__construct();

        add_action( 'wpk/d565581/bookingForm', [ $this, 'showForm' ] );

        add_action( 'wp_ajax_wpk_booking_get_prices', [ $this, 'getPrices' ] );
        add_action( 'wp_ajax_nopriv_wpk_booking_get_prices', [ $this, 'getPrices' ] );

        add_action( 'wp_ajax_wpk_book_property', [ $this, 'handleForm' ] );
        add_action( 'wp_ajax_nopriv_wpk_book_property', [ $this, 'handleForm' ] );

    }

    /**
     * @param int $postID
     *
     * @return void
     */
    public function showForm( $postID ) {

        $post = Property::find( $postID );

        $data = [
            'post'                 => $post,
            'checkIn'              => $this->getQueryParam( 'check_in_prop' ),
            'checkInHour'          => $this->getQueryParam( 'check_in_hour' ),
            'checkOut'             => $this->getQueryParam( 'check_out_prop' ),
            'checkOutHour'         => $this->getQueryParam( 'check_out_hour' ),
            'maxGuests'            => $post->meta( 'guest_no' ),
            'instantBooking'       => $post->meta( 'instant_booking' ),
            'overloadGuest'        => $post->getOverloadGuest(),
            'guestNo'              => $this->getQueryParam( 'guest_no_prop', 'all' ),
            'guestList'            => $GLOBALS[ 'guest_list' ],
            'pricePerGuestFromOne' => $post->getPricePerGuestFromOne(),
            'favoriteClass'        => $GLOBALS[ 'favorite_class' ],
            'favoriteText'         => $GLOBALS[ 'favorite_text' ],
            'permalink'            => get_permalink( $post->ID ),
            'guests'               => (int) get_option( 'wp_estate_guest_dropdown_no' ),
            'title'                => $post->post_title,
            'extraOptions'         => $post->meta( 'extra_pay_options' ),
            'pinterest'            => wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'wpestate_property_full_map' ),
            'whereCurrency'        => esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) ),
            'currency'             => esc_html( get_option( 'wp_estate_currency_label_main', '' ) ),
            'hours'                => \Wpk\d565571\Helpers\Booking::getHours(),
        ];

        $this->response->render( 'property.booking-form', $data );

    }

    /**
     * Ajax callback for displaying prices in booking form
     *
     * @return void
     */
    public function getPrices() {

        $requiredFields = [
            'listing_edit',
            'start_hour',
            'end_hour',
            'start_date',
            'end_date',
        ];

        $response = $this->response;

        foreach ( $requiredFields as $field ) {
            if ( empty( $_POST[ $field ] ) ) {
                $response->sendJson();
            }
        }

        $propertyID = (int) $this->getPostParam( 'listing_edit' );
        $property   = Property::find( $propertyID );

        $startHour = $this->getPostParam( 'start_hour' );
        $endHour   = $this->getPostParam( 'end_hour' );

        $startDate = $this->getPostParam( 'start_date' );
        $endDate   = $this->getPostParam( 'end_date' );

        try {

            $startDate = new DateTime( "$startDate $startHour" );
            $endDate   = new DateTime( "$endDate $endHour" );

        } catch ( \Exception $e ) {
            $response->addError( esc_html__( 'Invalid date format', 'wpk' ), 'alert', true );
        }

        $guests = (int) $this->getPostParam( 'guests_no', 1 );

        $currency      = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
        $whereCurrency = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );

        $extraOptions = (array) $this->getPostParam( 'extra_option' );

        $prices = $property->getPrices( [
            'guest_no'            => $guests,
            'start_date'          => $startDate,
            'end_date'            => $endDate,
            'extra_options_array' => $extraOptions,
        ] );

        $pricesDisplay = array_map( function ( $item ) use ( $currency, $whereCurrency ) {

            if ( ! is_array( $item ) ) {
                $item = wpestate_show_price_booking( $item, $currency, $whereCurrency, 1 );
            }

            return $item;

        }, $prices );

        $data = [
            'prices'        => $prices,
            'pricesDisplay' => $pricesDisplay,
        ];

        $view = Core()->view->render( 'property.booking-prices', $data );

        $response->setResult( $view )->sendJson();

    }

    /**
     * Handles booking form
     *
     *
     * @return void
     */
    public function handleForm() {

        $response = $this->response;

        $response->checkLoginStatus();

        $requiredFields = [
            'start_date',
            'start_hour',
            'end_hour',
            'end_date',
            'guests_no',
            'listing_edit',
        ];

        foreach ( $requiredFields as $requiredField ) {
            if ( empty( $this->getPostParam( $requiredField ) ) ) {
                $response->addError( esc_html__( 'You must fill all fields', 'wpk' ), 'alert', true );
            }
        }

        $propertyID = (int) $this->getPostParam( 'listing_edit' );

        if ( empty( $propertyID ) ) {
            $response->addError( esc_html__( 'Invalid property', 'wpk' ), 'alert', true );
        }

        $property = Property::find( $propertyID );

        $startHour = $this->getPostParam( 'start_hour' );
        $endHour   = $this->getPostParam( 'end_hour' );

        $startDate = $this->getPostParam( 'start_date' );
        $endDate   = $this->getPostParam( 'end_date' );

        try {

            $startDate = new \DateTime( "$startDate $startHour" );
            $endDate   = new \DateTime( "$endDate $endHour" );

        } catch ( \Exception $e ) {
            $response->addError(
                esc_html__( 'Invalid date format', 'wpk' ),
                'alert',
                true );
        }

        if ( $startDate->getTimestamp() < time() ) {
            $response->addError(
                esc_html__( 'You can\'t book dates in the past.', 'wpk' ),
                'alert',
                true );
        }

        $minHours = (int) $property->meta( 'min_days_booking' );

        if ( ! empty( $minHours ) ) {

            $diff  = $endDate->diff( $startDate );
            $hours = $diff->h + ( $diff->days * 24 );

            if ( $minHours > $hours ) {

                $message = sprintf(
                    esc_html__( 'You need to book at least %s hours', 'wpk' ),
                    $minHours
                );

                $response->addError( $message, 'alert', true );

            }

        }

        $comment = $this->getPostParam( 'comment', '' );

        $extraOptions = (array) $this->getPostParam( 'extra_option', [] );

        $guests    = (int) $this->getPostParam( 'guests_no', 1 );
        $maxGuests = (int) $property->meta( 'guest_no' );

        if ( $guests > $maxGuests ) {
            $response->addError(
                sprintf(
                    esc_html__( 'The number of attendees is greater than property capacity - %s attendees', 'wpk' ),
                    $maxGuests
                ),
                'alert',
                true
            );
        }

        $booking = BookingModel::createForProperty( [
            'property'      => $property,
            'comment'       => $comment,
            'extra_options' => $extraOptions,
            'start_date'    => $startDate,
            'end_date'      => $endDate,
            'guests'        => $guests,
            'author'        => $this->user->ID,
        ] );

        if ( $booking ) {

            $instantBooking = $property->meta( 'instant_booking' );

            if ( $instantBooking ) {
                //wpestate_send_booking_email( 'bookingconfirmeduser', $this->user->user_email, $propertyID );
            }

            $result = $instantBooking ?
                $booking->meta( 'invoice_html' ) :
                esc_html__( 'Booking sent! Please wait for owner confirmation.', 'wpk' );

            if ( $instantBooking && get_current_user_id() == $property->post_author ) {
                $result = '';
            }

            $response->setResult( [
                'instantBooking' => $instantBooking,
                'message'        => $result,
            ] );
        } else {
            $response->addError( esc_html__( 'Selected dates are not avialable.', 'wpk' ), 'alert' );
        }

        $response->sendJson();

    }

}
