<?php

namespace Wpk\d565571\Controllers\Booking;

use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;

/**
 * @author Przemysław Żydek
 */
class Removal extends Controller {

    /**
     * Removal constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'before_delete_post', [ $this, 'handle' ] );
    }

    /**
     * Remove all connected bookings when property is being removed
     *
     * @param int $postID
     *
     * @return void
     */
    public function handle( $postID ) {

        if ( get_post_type( $postID ) !== 'estate_property' ) {
            return;
        }

        $property = Property::find( $postID );
        $bookings = Booking::getForProperty( $property );

        $bookings->each( function ( $item ) {
            $item->delete();
        } );

    }

}