<?php

namespace Wpk\d565571\Controllers\Booking;

use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Property;

/**
 * Handles creation of internal booking by owner
 *
 * @author Przemysław Żydek
 */
class Internal extends Controller {

    public function __construct() {
        parent::__construct();

        add_action( 'wp_ajax_wpk_owner_book', [ $this, 'handleForm' ] );
    }

    /**
     * @return void
     */
    public function handleForm() {

        $response = $this->response;

        $requiredFields = [
            'booking_from_date',
            'start_hour_owner_book',
            'booking_to_date',
            'end_hour_owner_book',
            'property_id',
        ];

        foreach ( $requiredFields as $requiredField ) {
            if ( empty( $this->getPostParam( $requiredField ) ) ) {
                $response->addError( esc_html__( 'You must fill all fields', 'wpk' ), 'alert', true );
            }
        }

        $propertyID = (int) $this->getPostParam( 'property_id' );
        $property   = Property::find( $propertyID );

        $response->validateAuthor( $this->user, $property );

        $startHour = $this->getPostParam( 'start_hour_owner_book' );
        $endHour   = $this->getPostParam( 'end_hour_owner_book' );

        $startDate = $this->getPostParam( 'booking_from_date' );
        $endDate   = $this->getPostParam( 'booking_to_date' );

        try {

            $startDate = new \DateTime( "$startDate $startHour" );
            $endDate   = new \DateTime( "$endDate $endHour" );

        } catch ( \Exception $e ) {
            $response->addError( esc_html__( 'Invalid date format', 'wpk' ), 'alert', true );
        }

        $booking = \Wpk\d565571\Models\Booking::createForProperty( [
            'property'   => $property,
            'start_date' => $startDate,
            'end_date'   => $endDate,
            'comment'    => $this->getPostParam( 'booking_msg' ),
            'confirmed'  => true,
        ] );

        if ( $booking ) {
            $response->setResult( true );
        }


        $response->sendJson();

    }

}
