<?php

namespace Wpk\d565571\Controllers\Booking;

use Wpk\d565571\Api\Google\Calendar;
use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;

/**
 * Handles creating new event for google calendar
 */
class GoogleCalendar extends Controller {

    /**
     * GoogleCalendar constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/d5657781/bookingConfirmed', [ $this, 'createEvent' ] );
        add_action( 'before_delete_post', [ $this, 'removeEvent' ] );
        add_action( 'wpk/d565581/property/googleCalendarConnected', [ $this, 'createEvents' ] );
    }

    /**
     * Create events in google calendar for each booking when calendar is connected to property
     *
     * @return void
     */
    public function createEvents( $propertyID ) {

        $property = Property::find( $propertyID );
        $bookings = Booking::getForProperty( $property );

        foreach ( $bookings->all() as $booking ) {
            $this->createEvent( $booking );
        }

        Calendar::syncProperty( $property );

    }

    /**
     * On booking confirmation create new event in google calendar
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function createEvent( Booking $booking ) {

        if ( ! empty( $booking->meta( 'google_event_id' ) ) ) {
            return;
        }

        $calendarID = $booking->getProperty()->meta( 'google_calendar_id' );

        if ( empty ( $calendarID ) ) {
            return;
        }

        Calendar::createEventForBooking( $booking, $calendarID );

    }

    /**
     * On booking removal remove event from google calendar
     *
     * @param $postID
     *
     * @return void
     */
    public function removeEvent( $postID ) {

        if ( get_post_type( $postID ) !== 'wpestate_booking' ) {
            return;
        }

        $booking = Booking::find( $postID );

        $calendarID = $booking->meta( 'google_calendar_id' );

        if ( $booking->meta( 'created_from_google_event' ) ) {
            return;
        }

        if ( ! empty( $calendarID ) ) {
            Calendar::removeEventForBooking( $booking, $calendarID );
        }

    }

}
