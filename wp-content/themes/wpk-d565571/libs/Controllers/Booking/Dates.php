<?php

namespace Wpk\d565571\Controllers\Booking;

use Wpk\d565571\Controllers\Controller;
use Wpk\d565571\Models\Property;

/**
 * Handle getting booked dates from property
 *
 * @author Przemysław Żydek
 */
class Dates extends Controller {

    /**
     * BookedDates constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wp_ajax_nopriv_wpk_get_booked_dates', [ $this, 'get' ] );
        add_action( 'wp_ajax_wpk_get_booked_dates', [ $this, 'get' ] );
    }

    /**
     * @return void
     */
    public function get() {

        $response   = $this->response;
        $propertyID = (int) $this->getPostParam( 'propertyID' );

        if ( empty( $propertyID ) ) {
            $response->addError( esc_html__( 'No property provided.', 'wpk' ), 'alert', true );
        }

        $now = time();

        $dates          = Property::find( $propertyID )->getReservationArray();
        $formattedDates = [];

        if ( ! empty( $dates ) ) {

            foreach ( array_keys( $dates ) as $timestamp ) {

                //No need to include past bookings
                if ( $now > $timestamp ) {
                    continue;
                }

                $hour = date( 'H:i', $timestamp );
                $date = date( 'Y-m-d', $timestamp );

                $formattedDates[ $date ][] = $hour;

            }

        }

        $response->setResult( $formattedDates )->sendJson();

    }

}
