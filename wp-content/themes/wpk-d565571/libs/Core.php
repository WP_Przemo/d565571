<?php

namespace Wpk\d565571;

use Wpk\d565571\Api;
use Wpk\d565571\Cron\Crons;
use Wpk\d565571\Helpers\PostType;
use Wpk\d565571\Models\UserReview;

/**
 * Core class
 *
 * @author Przemysław Żydek
 */
final class Core {

    /** @var string Slug for user review post type */
    const USER_REVIEW = 'test';

    /** @var string Slug used for translations */
    const SLUG = 'wpk-d565571';

    /** @var string Stores path to this plugin directory */
    public $dir;

    /** @var string Stores url to this plugin directory */
    public $url;

    /** @var Utility */
    public $utility;

    /** @var Enqueue */
    public $enqueue;

    /** @var View */
    public $view;

    /** @var Settings */
    public $settings;

    /** @var Core Stores class instance */
    private static $instance;

    /**
     * Core constructor.
     */
    private function __construct() {
    }

    /**
     * No serializing
     */
    private function __sleep() {
    }

    /**
     * No unserializing
     */
    private function __wakeup() {
    }

    /**
     * Core init
     *
     * @param string $path Path to job directory
     *
     * @return Core
     */
    public function init( $path = null ) {

        $this->url = get_stylesheet_directory_uri() . $path;
        $this->dir = get_stylesheet_directory() . $path;

        $this->enqueue  = new Enqueue();
        $this->utility  = new Utility();
        $this->view     = new View( $this->getPath( 'views' ) );
        $this->settings = new Settings();

        //Load controllers
        Controllers\Loader::load();

        //Create google client
        Api\Google\Client::create();

        //Load cron tasks
        Crons::load();

        //Register user reviews post type
        $this->registerUserReviews();

        return $this;

    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getPath( $path = '' ) {
        return "{$this->dir}/{$path}";
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function getUrl( $path = '' ) {
        return "{$this->url}/$path";
    }

    /**
     * @return $this
     */
    private function registerUserReviews() {

        $postType = new PostType( self::USER_REVIEW, [
            'labels'             => [
                'name'               => _x( 'User Reviews', 'post type general name', 'wpk' ),
                'singular_name'      => _x( 'User Review', 'post type singular name', 'wpk' ),
                'menu_name'          => _x( 'User Reviews', 'admin menu', 'wpk' ),
                'name_admin_bar'     => _x( 'User Review', 'add new on admin bar', 'wpk' ),
                'add_new'            => _x( 'Add New', 'User Review', 'wpk' ),
                'add_new_item'       => __( 'Add New User Review', 'wpk' ),
                'new_item'           => __( 'New User Review', 'wpk' ),
                'edit_item'          => __( 'Edit User Review', 'wpk' ),
                'view_item'          => __( 'View User Review', 'wpk' ),
                'all_items'          => __( 'All User Reviews', 'wpk' ),
                'search_items'       => __( 'Search User Reviews', 'wpk' ),
                'parent_item_colon'  => __( 'Parent User Reviews:', 'wpk' ),
                'not_found'          => __( 'No User Reviews found.', 'wpk' ),
                'not_found_in_trash' => __( 'No User Reviews found in Trash.', 'wpk' ),
            ],
            'description'        => __( 'Description.', 'wpk' ),
            'public'             => true,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => [ 'slug' => self::USER_REVIEW ],
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => [ 'title', 'editor', 'custom-fields' ],
        ] );

        $postType
            ->addAdminColumn(
                'author',
                esc_html__( 'Author', 'wpk' ),
                function ( $postID ) {

                    echo UserReview::find( $postID )->getAuthor()->user_login;

                } )
            ->addAdminColumn(
                'for',
                esc_html__( 'For' ),
                function ( $postID ) {

                    echo UserReview::find( $postID )->getReviwedUser()->user_login;

                } );

        return $this;

    }

    /**
     * Get Core instance.
     *
     * @return Core
     */
    public static function getInstance() {

        if ( empty( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;

    }
}
