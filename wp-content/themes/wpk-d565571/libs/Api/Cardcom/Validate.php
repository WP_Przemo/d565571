<?php

namespace Wpk\d565571\Api\Cardcom;

use GuzzleHttp\Exception\RequestException;
use Wpk\d565571\Utility;

/**
 * Validates cardcom payment
 *
 * @author Przemysław Żydek
 */
class Validate {

    /** @var string */
    const URL = 'https://secure.cardcom.solutions/Interface/BillGoldGetLowProfileIndicator.aspx';

    /**
     * @param string $lowProfileCode Unique payment code
     *
     * @return bool
     */
    public static function wasSuccessful( $lowProfileCode ) {

        $params = [
            'terminalnumber' => Client::getSetting( 'terminal_number' ),
            'username'       => Client::getSetting( 'user_name' ),
            'lowprofilecode' => $lowProfileCode,
        ];

        $client = Client::get();

        $url = add_query_arg( $params, self::URL );

        try {

            $response = $client->get( $url );

            $stringResponse = $response->getBody();

            if ( $stringResponse ) {
                parse_str( $stringResponse, $result );

                return (int) $result[ 'ResponseCode' ] === 0;
            }

        } catch ( RequestException $e ) {
            Utility::log( $e->getMessage(), __METHOD__ );
        }

        return false;

    }

}
