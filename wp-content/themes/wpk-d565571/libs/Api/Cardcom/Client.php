<?php

namespace Wpk\d565571\Api\Cardcom;

use GuzzleHttp\Client as GuzzleClient;
use Wpk\d565571\Settings;
use Wpk\d565571\Utility;

/**
 * @author Przemysław Żydek
 */
class Client {

    /** @var array Options for Guzzle client */
    const OPTS = [];

    /** @var GuzzleClient */
    protected static $client;

    /**
     * @return GuzzleClient
     */
    public static function get() {

        if ( empty( self::$client ) ) {
            self::$client = new GuzzleClient( self::OPTS );
        }

        return self::$client;

    }

    /**
     * Check whenever required API settings were filled
     *
     * @return bool
     */
    public static function hasFilledSettings() {

        if ( ! empty( self::getSetting( 'terminal_number' ) ) &&
             ! empty( get_permalink( Utility::getPageByTemplate( 'payment_failed.php' ) ) &&
                      ! empty( get_permalink( Utility::getPageByTemplate( 'payment_successful.php' ) ) ) &&
                      ! empty( get_permalink( Utility::getPageByTemplate( 'payment_canceled.php' ) ) ) &&
                      ! empty( self::getSetting( 'user_name' ) )
             )
        ) {
            return true;
        }

        return false;

    }

    /**
     * Helper function for getting cardcom related setting
     *
     * @param $key
     *
     * @return bool|mixed
     */
    public static function getSetting( $key ) {
        return Settings::get( "cardcom_{$key}" );
    }

}
