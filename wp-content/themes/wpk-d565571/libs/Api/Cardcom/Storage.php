<?php

namespace Wpk\d565571\Api\Cardcom;

/**
 * Handles storage of payment actions
 *
 * @author Przemysław Żydek
 */
class Storage {

    /** @var string */
    const OPTION_KEY = 'wpk_cardcom_payments';

    /** @var string */
    const EXPIRES_IN = '+1 day';

    /**
     * Set new payment action in sotrage
     *
     * @param int    $userID
     * @param int    $contextID ID of payment context (for ex. booking ID)
     * @param string $paymentAction
     *
     * @return bool
     */
    public static function set( $userID, $contextID, $paymentAction) {

        $key = self::getKey( $userID );

        $value = [
            'action'     => $paymentAction,
            'context_id' => $contextID,
        ];

        return set_transient( $key, $value, strtotime( self::EXPIRES_IN ) );

    }

    /**
     * Get storage value
     *
     * @param int $userID
     *
     * @return array|false
     */
    public static function get( $userID ) {

        $key   = self::getKey( $userID );
        $value = get_transient( $key );

        return $value ? $value : false;

    }

    /**
     * Remove storage value for provided user
     *
     * @param int $userID
     *
     * @return bool
     */
    public static function delete( $userID ) {
        $key = self::getKey( $userID );

        return delete_transient( $key );
    }

    /**
     * Get option key for provided user ID
     *
     * @param int $userID
     *
     * @return string
     */
    protected static function getKey( $userID ) {
        return self::OPTION_KEY . $userID;
    }

}
