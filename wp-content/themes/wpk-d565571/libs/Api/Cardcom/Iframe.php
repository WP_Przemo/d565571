<?php

namespace Wpk\d565571\Api\Cardcom;

use GuzzleHttp\Exception\RequestException;
use Wpk\d565571\Models\User;
use Wpk\d565571\Utility;

/**
 * Helper class for getting iframe URL from cardcom
 *
 * @author Przemysław Żydek
 */
class Iframe {

    /** @var string */
    const URL = 'https://secure.cardcom.co.il/Interface/LowProfile.aspx';

    /**
     * Get cardcom iframe payment URL
     *
     * @param int|float $price
     * @param string    $description Optional product description
     *
     * @return bool
     */
    public static function getUrl( $price, $description = '' ) {

        $client = Client::get();

        $user = User::current();

        $homeUrl = get_home_url();

        $data = [
            'CoinID'                    => 2,
            'TerminalNumber'            => Client::getSetting( 'terminal_number' ),
            'Operation'                 => 1, //Billing and token creation
            'SumToBill'                 => $price,
            'Language'                  => 'en',
            'APILevel'                  => 10,
            'ProductName'               => $description,
            'IndicatorUrl'              => add_query_arg( 'type', 'indicator', $homeUrl ),
            'ErrorRedirectUrl'          => get_permalink( Utility::getPageByTemplate( 'payment_failed.php' ) ),
            'SuccessRedirectUrl'        => get_permalink( Utility::getPageByTemplate( 'payment_successful.php' ) ),
            'UserName'                  => Client::getSetting( 'user_name' ),
            'CancelUrl'                 => get_permalink( Utility::getPageByTemplate( 'payment_canceled.php' ) ),
            'CancelType'                => 2,
            'CreateTokenDeleteDate'     => date( 'd/m/Y', strtotime( '+1 year' ) ),
            'InvoiceHead.Email'         => $user->user_email,
            'InvoiceHead.CustLinePH'    => $user->meta( 'mobile' ),
            'InvoiceHead.Language'      => 'en',
            'InvoiceHead.CustName'      => "{$user->first_name} {$user->last_name}",
            'InvoiceLines1.Description' => $description,
            'InvoiceLines1.Price'       => $price,
            'InvoiceLines1.Quantity'    => 1,
            'InvoiceHead.SendByEmail'   => 'true',
        ];

        try {
            $response = $client->post( self::URL, [
                'form_params' => $data,
            ] );

            $stringResponse = $response->getBody();

            if ( $stringResponse ) {
                parse_str( $stringResponse, $result );

                return (int) $result[ 'ResponseCode' ] === 0 ? $result[ 'url' ] : false;
            }

        } catch ( RequestException $e ) {
            Utility::log( $e->getMessage(), __METHOD__ );
        }

        return false;


    }

}
