<?php

namespace Wpk\d565571\Api\Google;

use Wpk\d565571\Utility;

/**
 * Wrapper for Google Client
 *
 * @author Przemysław Żydek
 */
class Client {

    /** @var \Google_Client */
    protected static $client;

    /**
     * Setup our google client
     *
     * @param array $args
     *
     * @return \Google_Client
     */
    public static function create( $args = [] ) {

        //Merge args with defaults
        $args += self::getDefaults();

        $client = &self::$client;

        $client = new \Google_Client( $args );
        $client->setAccessType( 'offline' );
        $client->setPrompt( 'consent' );

        return $client;

    }

    /**
     * @return \Google_Client
     */
    public static function get() {
        return self::$client;
    }

    /**
     * Get default arguments for Google_Client
     *
     * @return array
     */
    protected static function getDefaults() {

        return [
            'client_id'     => esc_html( get_option( 'wp_estate_google_oauth_api', '' ) ),
            'client_secret' => esc_html( get_option( 'wp_estate_google_oauth_client_secret', '' ) ),
            'redirect_uri'  => get_permalink( Utility::getPageByTemplate( 'user_dashboard_profile.php' ) ),
            'developer_key' => esc_html( get_option( 'wp_estate_google_api_key', '' ) ),
        ];

    }

}
