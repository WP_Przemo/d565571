<?php


namespace Wpk\d565571\Api\Google;

/**
 * @author Przemysław Żydek
 */
class Auth {

    /**
     * @param array  $scope
     * @param mixed $state
     * @param string $applicationName
     *
     * @return string
     */
    public static function getUrl(
        $scope = [
            'email',
        ], $state = null, $applicationName = 'Login to WpRentals'
    ) {

        $gClient = Client::get();

        if ( ! empty( $applicationName ) ) {
            $gClient->setApplicationName( $applicationName );
        }

        $gClient->setState( $state );

        $gClient->setScopes( $scope );

        return $gClient->createAuthUrl();

    }

}
