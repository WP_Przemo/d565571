<?php

namespace Wpk\d565571\Api\Google;

use Wpk\d565571\Helpers\DateTime;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Owner;
use Wpk\d565571\Models\Property;
use Wpk\d565571\Utility;

/**
 * Helper for google calendar related actions
 *
 * @author Przemysław Żydek
 */
class Calendar {

    /** @var string */
    const ATOM_DATE_FORMAT = 'Y-m-d\TH:i:sP';

    /**
     * Get google service calendar instance for provided token
     *
     * @param Property $property
     *
     * @return \Google_Service_Calendar|false
     */
    protected static function getClient( Property $property ) {

        $token = $property->meta( 'google_access_token' );

        if ( empty( $token ) ) {
            return false;
        }

        $client = Client::get();
        $client->setAccessToken( $token );

        if ( $client->isAccessTokenExpired() ) {

            try {
                $refreshedToken = $client->fetchAccessTokenWithRefreshToken();

                $property->updateMeta( 'google_access_token', $refreshedToken );
            } catch ( \LogicException $e ) {
                $property->deleteMeta( 'google_access_token' );

                return false;
            }
        }

        return new \Google_Service_Calendar( $client );

    }

    /**
     * Get calendar list for current user
     *
     * @param Property $property
     *
     * @return array|bool
     */
    public static function getForProperty( Property $property ) {

        $client = self::getClient( $property );

        if ( ! $client ) {
            return false;
        }

        try {
            $calendarsList = $client->calendarList->listCalendarList();
        } catch ( \Google_Service_Exception $e ) {
            Utility::log( $e->getMessage(), sprintf( '%s google service error', __METHOD__ ) );

            return false;
        }

        return $calendarsList->getItems();


    }

    /**
     * @param Booking $booking
     * @param string  $calendarID
     *
     * @return \Google_Service_Calendar_Event|false
     */
    public static function createEventForBooking( Booking $booking, $calendarID ) {

        $property = $booking->getProperty();

        $renter = $booking->getAuthor();

        $client = self::getClient( $booking->getProperty() );

        if ( ! $client ) {
            return false;
        }

        $event = new \Google_Service_Calendar_Event();

        $startDate = new \Google_Service_Calendar_EventDateTime();
        $endDate   = new \Google_Service_Calendar_EventDateTime();

        $startDate->setDateTime(
            $booking
                ->getStartDate()
                ->format( self::ATOM_DATE_FORMAT )
        );
        $endDate->setDateTime(
            $booking
                ->getEndDate()
                ->format( self::ATOM_DATE_FORMAT )
        );

        $event->setStart( $startDate );
        $event->setEnd( $endDate );

        $description = '';
        $description .= $renter->user_email . '<br>';
        $description .= $renter->meta( 'facebook' ) . '<br>';
        $description .= $renter->meta( 'phone' ) . '<br>';

        $owner = Owner::getForUser( $renter->ID );

        if ( $owner ) {
            $description .= get_permalink( $owner->ID ) . '<br>';
        }

        $event->setSummary(
            sprintf(
                esc_html__( 'Booking %s for property %s', 'wpk' ),
                $booking->ID, $property->post_title ) );

        $event->setDescription( $description );

        try {
            $createdEvent = $client->events->insert( $calendarID, $event );
        } catch ( \Google_Service_Exception $e ) {
            Utility::log( $e->getMessage(), __METHOD__ );

            return false;
        }


        $booking
            //Store event ID in booking meta
            ->updateMeta( 'google_event_id', $createdEvent->getId() )
            //Store calendar ID in case if user would change property calendar
            ->updateMeta( 'google_calendar_id', $calendarID );

        do_action( 'wpk/d5657781/booking/googleEventCreated', $booking, $createdEvent );

        return $createdEvent;

    }

    /**
     * @param Booking $booking
     * @param string  $calendarID
     *
     * @return bool
     */
    public static function removeEventForBooking( Booking $booking, $calendarID ) {

        $client = self::getClient( $booking->getProperty() );

        $eventID = $booking->meta( 'google_event_id' );

        if ( ! $client || empty( $eventID ) ) {
            return false;
        }

        try {
            $client->events->delete( $calendarID, $eventID );

            do_action( 'wpk/d5657781/booking/googleEventRemoved', $booking );

            return true;
        } catch ( \Google_Service_Exception $e ) {
            return false;
        }


    }


    /**
     * Sync event from google calendar with booking
     *
     * @param Booking $booking
     * @param string  $calendarID
     *
     * @return bool
     */
    public static function syncBooking( Booking $booking, $calendarID ) {

        $client = self::getClient( $booking->getProperty() );

        $property = $booking->getProperty();

        $eventID = $booking->meta( 'google_event_id' );

        if ( ! $client || empty( $eventID ) ) {
            return false;
        }

        try {
            $event = $client->events->get( $calendarID, $eventID );
        } catch ( \Google_Service_Exception $e ) {
            return false;
        }

        if ( $event->getStatus() === 'cancelled' ) {
            $booking->delete();

            $property->updateReservationArray();

            return true;
        }

        $eventStartDate = new \DateTime( $event->getStart()->getDateTime() );
        $eventEndDate   = new \DateTime( $event->getEnd()->getDateTime() );

        $property->updateReservationArray();

        if ( $property->post_author == $booking->post_author ) {

            $booking
                ->updateStartDate( $eventStartDate )
                ->updateEndDate( $eventEndDate )
                ->getProperty()
                ->updateReservationArray();

        }

        return true;

    }

    /**
     * Sync google calendar for property
     *
     * @param Property $property
     *
     * @return bool
     */
    public static function syncProperty( Property $property ) {

        $calendarID = $property->meta( 'google_calendar_id' );

        $client = self::getClient( $property );

        if ( empty( $calendarID ) || ! $client ) {
            return false;
        }

        $events = (array) $client->events->listEvents( $calendarID )->getItems();

        if ( empty( $events ) ) {
            return false;
        }

        return self::createBookingsForGoogleEvents( $events, $property, $calendarID );

    }

    /**
     * Creates or modifies bookings based on events from google calendar
     *
     * @param array    $events
     * @param Property $property Property for which bookings will be created
     * @param mixed    $calendarID
     *
     * @return bool
     */
    private static function createBookingsForGoogleEvents( $events, Property $property, $calendarID ) {

        $now = time();

        /** @var \Google_Service_Calendar_Event $event */
        foreach ( $events as $event ) {

            $eventID = $event->getId();

            $bookings = Booking::init()
                               ->hasMetaValue( 'google_event_id', $eventID )
                               ->hasMetaValue( 'booking_id', $property->ID )
                               ->get();

            if ( ! $bookings->empty() ) {
                continue;
            }

            $eventStartDate = new \DateTime( $event->getStart()->getDateTime() );
            $eventEndDate   = new \DateTime( $event->getEnd()->getDateTime() );

            if ( $eventEndDate->getTimestamp() < $now ||
                 $eventStartDate->getTimestamp() === $eventEndDate->getTimestamp() ||
                 ! Booking::isAvailable( $property, clone $eventStartDate, clone $eventEndDate ) ) {
                continue;
            }

            $booking = Booking::createForProperty( [
                'property'    => $property,
                'start_date'  => $eventStartDate,
                'end_date'    => $eventEndDate,
                'confirmed'   => true,
                'send_emails' => false,
                'author'      => $property->post_author,
                'meta'        => [
                    'google_event_id'           => $eventID,
                    'google_calendar_id'        => $calendarID,
                    'created_from_google_event' => true,
                ],
            ] );

            do_action( 'wpk/d5657781/booking/googleEventCreated', $booking, $event );

        }

        $property->updateReservationArray();

        return true;

    }

}
