<?php

namespace Wpk\d565571\Models;

use Wpk\d565571\Core;

/**
 * @author Przemysław Żydek
 */
class UserReview extends Post {

    /**
     * @return bool|Post
     */
    public function create() {
        $this->attributes[ 'post_type' ] = Core::USER_REVIEW;

        return parent::create();
    }

    /**
     * @return Collection
     */
    public function get() {
        $this->attributes[ 'post_type' ] = Core::USER_REVIEW;

        return parent::get();
    }

    /**
     * @return User
     */
    public function getReviwedUser() {
        $for = (int) $this->meta( 'for' );

        return User::find( $for );
    }

    /**
     * Create new review for provided user
     *
     * @param User   $for
     * @param int    $propertyID
     * @param string $stars
     * @param string $content
     *
     * @return bool|self
     */
    public static function createFor( User $for, $propertyID, $stars, $content ) {

        $model = self::init()
                     ->title( sprintf(
                         esc_html__( 'Review for %s', 'wpk' ),
                         $for->user_login
                     ) )
                     ->content( $content )
                     ->addMetas( [
                         'for'          => $for->ID,
                         'review_stars' => $stars,
                         'property_id'  => $propertyID,
                     ] )
                     ->status( 'publish' )
                     ->create();

        return $model;

    }

    /**
     * Get review for user made by provided user
     *
     * @param int      $for
     * @param int      $propertyID
     * @param int|null $author
     *
     * @return bool|Collection
     */
    public static function getReviewFor( $for, $propertyID = null, $author = null ) {

        $review = self::init()
                      ->hasMetaValue( 'for', $for )
                      ->status( 'publish' );

        if ( ! empty( $author ) ) {
            $review->author( $author );
        }

        if ( ! empty( $propertyID ) ) {
            $review->hasMetaValue( 'property_id', $propertyID );
        }

        $review = $review->get();

        return $review->empty() ? false : $review;

    }

    /**
     * This is wrapper for wp_rentals function that calculates ratings
     *
     * @param int $for ID of user for which reviews were created
     * @param int $propertyID Optional property ID for context
     *
     * @return array|false
     */
    public static function getAverageFor( $for, $propertyID = null ) {

        $forType = wpse119881_get_author( $propertyID ) == $for ? 'owner' : 'renter';

        $reviews = self::getReviewFor( $for, $propertyID );

        if ( ! $reviews ) {
            return false;
        }

        $category_fields = wpk_get_review_fields( $forType );
        $stars_in_fields = [];
        $stars_fields    = [];
        $stars_averages  = [];
        $store           = [];
        $count           = $reviews->count();

        foreach ( $reviews->all() as $review ) {

            $raw_comment_rating = $review->meta( 'review_stars' );

            $tmp_rating                   = json_decode( $raw_comment_rating, true );
            $stars_in_fields[ 'total' ][] = $tmp_rating[ 'rating' ];

            // gather all stars per field in an array
            foreach ( $category_fields[ 'fields' ] as $field_key => $field_value ) {
                $stars_in_fields[ $field_key ][] = $tmp_rating[ $field_key ];
            }
        }

        // Sums per fields
        foreach ( $category_fields[ 'fields' ] as $field_key => $field_value ) {
            $stars_fields[ $field_key ]   = array_sum( $stars_in_fields[ $field_key ] );
            $tmp_round                    = round( $stars_fields[ $field_key ] / count( $stars_in_fields[ $field_key ] ), 1 );
            $stars_averages[ $field_key ] = wpestate_round_to_nearest_05( $tmp_round );
            $store[]                      = sprintf( '"%s": %s', $field_key, $stars_averages[ $field_key ] );
        }

        // Calc total rating
        $all_reviews_total          = array_sum( $stars_in_fields[ 'total' ] ) / ( ( $count ) );
        $property_rating[ 'total' ] = wpestate_round_to_nearest_05( $all_reviews_total );

        // Construct rating string for db
        $store[]     = sprintf( '"%s": %s', 'rating', $property_rating[ 'total' ] );
        $star_rating = '{' . implode( ',', $store ) . '}';

        return [
            'star_rating' => $star_rating,
            'ratings'     => $property_rating,
        ];

    }

}
