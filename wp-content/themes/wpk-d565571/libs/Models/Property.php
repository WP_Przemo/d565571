<?php


namespace Wpk\d565571\Models;

use Wpk\d565571\Api\Google\Calendar;
use Wpk\d565571\Helpers\Date;
use Wpk\d565571\Helpers\WpRentals\Price;

/**
 * @author Przemysław Żydek
 */
class Property extends Post {

    /**
     * @return float
     */
    public function getOverloadGuest() {
        return (float) $this->meta( 'overload_guest' );
    }

    /**
     * @return float
     */
    public function getPricePerGuestFromOne() {
        return (float) $this->meta( 'price_per_guest_from_one' );
    }

    /**
     * @return float
     */
    public function getGuestNumber() {
        return (float) $this->meta( 'guest_no' );
    }

    /**
     * @return bool
     */
    public function isInstantBooking() {
        return (bool) $this->meta( 'instant_booking' );
    }

    /**
     * @return float
     */
    public function getPricePerHour() {
        return (float) $this->meta( 'property_price' );
    }

    /**
     * @return array
     */
    public function getCustomPrice() {
        $price = (array) $this->meta( 'custom_price' );

        return empty( $price ) ? (array) $this->meta( "custom_price{$this->ID}" ) : $price;
    }

    /**
     * @return array
     */
    public function getMegaDetails() {
        $mega = (array) $this->meta( 'mega_details' );

        return empty( $mega ) ? (array) $this->meta( "mega_details{$this->ID}" ) : $mega;
    }

    /**
     * @return float
     */
    public function getCleaningFeePerDay() {
        return (float) $this->meta( 'cleaning_fee_per_day' );
    }

    /**
     * @return float
     */
    public function getCityFeePerDay() {
        return (float) $this->meta( 'city_fee_per_day' );
    }

    /**
     * @return float
     */
    public function getPricePerWeekend() {
        return (float) $this->meta( 'price_per_weekeend' );
    }

    /**
     * @return float
     */
    public function getPricePerWeek() {
        $price = (float) $this->meta( 'property_price_per_week' );

        return empty( $price ) ? $this->getPricePerHour() : $price;
    }

    /**
     * @return float
     */
    public function getPricePerMonth() {
        $price = (float) $this->meta( 'property_price_per_month' );

        return empty( $price ) ? $this->getPricePerHour() : $price;
    }

    /**
     * @return float
     */
    public function getCleaningFee() {
        return (float) $this->meta( 'cleaning_fee' );
    }

    /**
     * @return float
     */
    public function getCityFee() {
        return (float) $this->meta( 'city_fee' );
    }

    /**
     * @return float
     */
    public function getCityFeePercent() {
        return (float) $this->meta( 'city_fee_percent' );
    }

    /**
     * @return float
     */
    public function getSecurityDeposit() {
        return (float) $this->meta( 'security_deposit' );
    }

    /**
     * @return float
     */
    public function getExtraPricePerGuest() {
        return (float) $this->meta( 'extra_price_per_guest' );
    }

    /**
     * @return array
     */
    public function getExtraPayOptions() {
        return (array) $this->meta( 'extra_pay_options' );
    }

    /**
     * @return float
     */
    public function getTaxes() {
        return (float) $this->meta( 'property_taxes' );
    }

    /**
     * @return array
     */
    public function getReservationArray() {
        return (array) $this->meta( 'booking_dates' );
    }

    /**
     * Update reservations array based on current bookings
     *
     * @return $this
     */
    public function updateReservationArray() {

        $reservations = [];
        $bookings     = Booking::getForProperty( $this, true );

        $bookedDates = [];

        foreach ( $bookings->all() as $booking ) {
            /**
             * @var Booking $booking
             */

            $startDate = $booking->getStartDate();
            $endDate   = $booking->getEndDate();

            // Last hour should be bookable
            $endDate->modify( '-1 hour' );

            //Fix for invalid time zone
            $endTimestamp   = strtotime( $endDate->format( Booking::DATE_FORMAT ) );
            $startTimestamp = strtotime( $startDate->format( Booking::DATE_FORMAT ) );

            while ( $endTimestamp >= $startTimestamp ) {

                $reservations[ $startTimestamp ] = $booking->ID;

                $bookedDates[ Date::getWithoutHours( $startDate )
                                  ->format( 'Y-m-d' ) ][ $startTimestamp ] = $booking->ID;

                $startDate->modify( '+1 hour' );

                $startTimestamp = strtotime( $startDate->format( Booking::DATE_FORMAT ) );

            }

        }

        foreach ( $bookedDates as $date => $timestampsArray ) {

            //Whole day is booked, let's save this day timestamp in reservations array so WP Rentals will block this day
            if ( count( $timestampsArray ) === 24 ) {
                $bookingID = $timestampsArray[ 0 ];

                $reservations[ strtotime( $date ) ] = $bookingID;
            }

        }

        //Store booked dates in meta as well
        $this->updateMeta( 'booked_dates_formated', $bookedDates );

        return $this->updateMeta( 'booking_dates', $reservations );

    }

    /**
     * @return mixed
     */
    public function getRcapiListingID() {
        return $this->meta( 'rcapi_listing_id' );
    }

    /**
     * Get prices based on provided args
     *
     * @param array $args
     *
     * @return array
     */
    public function getPrices( $args = [] ) {

        $args = wp_parse_args( $args, [
            'invoice_id'          => 0,
            'guest_no'            => 1,
            'start_date'          => '',
            'end_date'            => '',
            'booking_id'          => $this->ID,
            'extra_options_array' => '',
            'manual_expenses'     => '',
        ] );

        $invoiceID = $args[ 'invoice_id' ];
        $bookid    = $args[ 'booking_id' ];

        $mega = $this->getMegaDetails();

        $extraOptionsArray = $args[ 'extra_options_array' ];

        $pricePerWeekend    = $this->getPricePerWeekend();
        $setupWeekendStatus = esc_html( get_option( 'wp_estate_setup_weekend', '' ) );
        $includeExpenses    = esc_html( get_option( 'wp_estate_include_expenses', '' ) );

        $currentGuestsNumber = $args[ 'guest_no' ];

        $manualExpenses = $args[ 'manual_expenses' ];

        /** @var \DateTime $bookingFromDate */
        $bookingFromDate = $args[ 'start_date' ];
        /** @var \DateTime $bookingToDate */
        $bookingToDate = $args[ 'end_date' ];

        /* TODO Get guests from booking? */
        //$total_guests         = floatval( get_post_meta( $bookid, 'booking_guests', true ) );
        $totalGuests = $args[ 'guest_no' ];

        $numberHours = 1;

        /* TODO Include early bird calculations ?*/
        $early_bird_percent = 0;
        $early_bird_days    = 0;

        if ( empty( $invoiceID ) ) {

            $pricePerHour      = $this->getPricePerHour();
            $weekPrice         = $this->getPricePerWeek();
            $monthPrice        = $this->getPricePerMonth();
            $cleaningFee       = $this->getCleaningFee();
            $cityFee           = $this->getCityFee();
            $cityFeePercent    = $this->getCityFeePercent();
            $securityDeposit   = $this->getSecurityDeposit();
            $cleaningFeePerDay = $this->getCleaningFeePerDay();
            $cityFeePerDay     = $this->getCityFeePerDay();

            //$early_bird_percent = floatval( get_post_meta( $property_id, 'early_bird_percent', true ) );
            //$early_bird_days    = floatval( get_post_meta( $property_id, 'early_bird_days', true ) );

        } else {
            //WP Rentals part
            $pricePerHour      = floatval( get_post_meta( $invoiceID, 'default_price', true ) );
            $weekPrice         = floatval( get_post_meta( $invoiceID, 'week_price', true ) );
            $monthPrice        = floatval( get_post_meta( $invoiceID, 'month_price', true ) );
            $cleaningFee       = floatval( get_post_meta( $invoiceID, 'cleaning_fee', true ) );
            $cityFee           = floatval( get_post_meta( $invoiceID, 'city_fee', true ) );
            $cleaningFeePerDay = floatval( get_post_meta( $invoiceID, 'cleaning_fee_per_day', true ) );
            $cityFeePerDay     = floatval( get_post_meta( $invoiceID, 'city_fee_per_day', true ) );
            $cityFeePercent    = floatval( get_post_meta( $invoiceID, 'city_fee_percent', true ) );
            $securityDeposit   = floatval( get_post_meta( $invoiceID, 'security_deposit', true ) );
            // $early_bird_percent         =   floatval(get_post_meta($invoiceID, 'early_bird_percent', true));
            // $early_bird_days            =   floatval(get_post_meta($invoiceID, 'early_bird_days', true));
            //WP Rentals part end
        }

        $fromDateTimestamp = $bookingFromDate->getTimestamp();
        $fromDateDiscount  = $bookingFromDate->getTimestamp();
        //Get date without hour in order to access custom prices from "mega" array
        $fromDateWithoutHour = strtotime( $bookingFromDate->format( 'd.m.Y' ) );


        $toDateTimestamp = $bookingToDate->getTimestamp();
        //Same as above
        $toDateWithoutHour = strtotime( $bookingToDate->format( 'd.m.Y' ) );

        $totalPrice        = 0;
        $internPrice       = 0;
        $hasCustom         = 0;
        $hasWeekendPrice   = 0;
        $coverWeekend      = 0;
        $customPeriodQuest = 0;

        $customPriceArray = $this->getCustomPrice();

        $bookingCustomPrices = [];

        //Get difference in hours between start and end date
        $hoursDiff = round( ( $fromDateTimestamp - $toDateTimestamp ) / 3600, 1 );
        $hoursDiff = abs( $hoursDiff );

        //Difference in days
        $timeDiff = abs( $fromDateWithoutHour - $toDateWithoutHour );

        $countDays = $timeDiff / 86400;  //86400 seconds in one day
        $countDays = (int) $countDays;

        //Check extra price PER guest
        $extraPricePerGuest      = $this->getExtraPricePerGuest();
        $pricePerGuestFromOne    = $this->getPricePerGuestFromOne();
        $overloadGuest           = $this->getOverloadGuest();
        $guestnumber             = $this->getGuestNumber();
        $hasGuestOverload        = 0;
        $totalExtraPricePerGuest = 0;
        $extraGuests             = 0;


        if ( $pricePerGuestFromOne == 0 ) {

            //per day math
            if ( $countDays >= 7 && $weekPrice != 0 ) {
                $pricePerHour = $weekPrice;
            }

            if ( $countDays >= 30 && $monthPrice != 0 ) {
                $pricePerHour = $monthPrice;
            }

            //custom prices - check the first day
            if ( isset( $customPriceArray[ $fromDateWithoutHour ] ) ) {
                $hasCustom = 1;
            }

            if ( isset( $mega[ $fromDateWithoutHour ] ) && isset( $mega[ $fromDateWithoutHour ][ 'period_price_per_weekeend' ] ) && $mega[ $fromDateWithoutHour ][ 'period_price_per_weekeend' ] != 0 ) {
                $hasWeekendPrice = 1;
            }

            if ( $overloadGuest == 1 ) {  // if we allow overload

                if ( $currentGuestsNumber > $guestnumber ) {

                    $hasGuestOverload = 1;
                    $extraGuests      = $currentGuestsNumber - $guestnumber;

                    if ( isset( $mega[ $fromDateWithoutHour ] ) && isset( $mega[ $fromDateWithoutHour ][ 'period_price_per_weekeend' ] ) ) {
                        $totalExtraPricePerGuest = $totalExtraPricePerGuest + $extraGuests * $mega[ $fromDateWithoutHour ][ 'period_extra_price_per_guest' ];

                        $customPeriodQuest = 1;
                    } else {
                        $totalExtraPricePerGuest = $totalExtraPricePerGuest + $extraGuests * $extraPricePerGuest;

                    }

                }

            }

            if ( $pricePerWeekend != 0 ) {
                $hasWeekendPrice = 1;
            }

            //Calculate usable price based on property settings
            $usablePrice = Price::getUsablePrice( $fromDateWithoutHour, $mega, $pricePerWeekend, $customPriceArray, $pricePerHour, $countDays );

            $totalPrice  = $totalPrice + $usablePrice;
            $internPrice = $internPrice + $usablePrice;

            $bookingCustomPrices [ $fromDateWithoutHour ] = $usablePrice;

            $fromDateTimestampFirstDay = $bookingFromDate->getTimestamp();

            $bookingFromDate->modify( '+1 hour' );

            $fromDateTimestamp = $bookingFromDate->getTimestamp();

            $weekday = date( 'N', $fromDateTimestampFirstDay ); // 1-7

            /*TODO */
            if ( wpestate_is_cover_weekend( $weekday, $hasWeekendPrice, $setupWeekendStatus ) ) {
                $coverWeekend = 1;
            }


            // loop trough the dates
            while ( $fromDateTimestamp < $toDateTimestamp ) {

                $numberHours ++;

                $fromDateWithoutHourLoop = strtotime( date( 'd.m.Y', $fromDateTimestamp ) );

                if ( isset( $customPriceArray[ $fromDateWithoutHourLoop ] ) ) {
                    $hasCustom = 1;
                }

                if ( isset( $mega[ $fromDateWithoutHourLoop ] ) && isset( $mega[ $fromDateWithoutHourLoop ][ 'period_price_per_weekeend' ] ) && $mega[ $fromDateWithoutHourLoop ][ 'period_price_per_weekeend' ] != 0 ) {
                    $hasWeekendPrice = 1;
                }

                if ( $overloadGuest == 1 ) {  // if we allow overload

                    if ( $currentGuestsNumber > $guestnumber ) {

                        $hasGuestOverload = 1;
                        $extraGuests      = $currentGuestsNumber - $guestnumber;

                        if ( isset( $mega[ $fromDateWithoutHourLoop ] ) && isset( $mega[ $fromDateWithoutHourLoop ][ 'period_price_per_weekeend' ] ) ) {
                            $totalExtraPricePerGuest = $totalExtraPricePerGuest + $extraGuests * $mega[ $fromDateWithoutHourLoop ][ 'period_extra_price_per_guest' ];
                            $customPeriodQuest       = 1;
                        } else {
                            $totalExtraPricePerGuest = $totalExtraPricePerGuest + $extraGuests * $extraPricePerGuest;

                        }

                    }

                }

                if ( $pricePerWeekend != 0 ) {
                    $hasWeekendPrice = 1;
                }


                $weekday = date( 'N', $fromDateWithoutHourLoop ); // 1-7
                if ( wpestate_is_cover_weekend( $weekday, $hasWeekendPrice, $setupWeekendStatus ) ) {
                    $coverWeekend = 1;
                }

                $usablePrice = Price::getUsablePrice( $fromDateWithoutHourLoop, $mega, $pricePerWeekend, $customPriceArray, $pricePerHour, $countDays );
                $totalPrice  = $totalPrice + $usablePrice;

                $internPrice                                      = $internPrice + $usablePrice;
                $bookingCustomPrices [ $fromDateWithoutHourLoop ] = $usablePrice;

                $bookingFromDate->modify( '+1 hour' );
                $fromDateTimestamp = $bookingFromDate->getTimestamp();

            }

        } else {

            $customPeriodQuest = 0;

            // per guest math
            if ( isset( $mega[ $fromDateWithoutHour ][ 'period_extra_price_per_guest' ] ) ) {
                $totalPrice                                   = $currentGuestsNumber * $mega[ $fromDateWithoutHour ][ 'period_extra_price_per_guest' ];
                $internPrice                                  = $currentGuestsNumber * $mega[ $fromDateWithoutHour ][ 'period_extra_price_per_guest' ];
                $bookingCustomPrices [ $fromDateWithoutHour ] = $currentGuestsNumber * $mega[ $fromDateWithoutHour ][ 'period_extra_price_per_guest' ];
                $customPeriodQuest                            = 1;
            } else {
                $totalPrice  = $currentGuestsNumber * $extraPricePerGuest;
                $internPrice = $currentGuestsNumber * $extraPricePerGuest;
            }


            $bookingFromDate->modify( '+1 hour' );
            $fromDateTimestamp = $bookingFromDate->getTimestamp();


            while ( $fromDateTimestamp < $toDateTimestamp ) {

                $numberHours ++;

                $fromDateWithoutHourLoop = strtotime( ( new \DateTime( $fromDateTimestamp ) )->format( 'd.m.Y' ) );

                if ( isset( $mega[ $fromDateWithoutHourLoop ][ 'period_extra_price_per_guest' ] ) ) {

                    $totalPrice                                       = $totalPrice + $currentGuestsNumber * $mega[ $fromDateWithoutHourLoop ][ 'period_extra_price_per_guest' ];
                    $internPrice                                      = $internPrice + $currentGuestsNumber * $mega[ $fromDateWithoutHourLoop ][ 'period_extra_price_per_guest' ];
                    $bookingCustomPrices [ $fromDateWithoutHourLoop ] = $currentGuestsNumber * $mega[ $fromDateTimestamp ][ 'period_extra_price_per_guest' ];
                    $customPeriodQuest                                = 1;

                } else {
                    $totalPrice  = $totalPrice + $currentGuestsNumber * $extraPricePerGuest;
                    $internPrice = $internPrice + $currentGuestsNumber * $extraPricePerGuest;
                }

                $bookingFromDate->modify( '+1 hour' );
                $fromDateTimestamp = $bookingFromDate->getTimestamp();
            }

        }


        $wpEstateBookDown         = (float) get_option( 'wp_estate_book_down', '' );
        $wpEstateBookDownFixedFee = (float) get_option( 'wp_estate_book_down_fixed_fee', '' );

        $currency      = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
        $whereCurrency = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );

        $customPricesDisplay = [];

        if ( ! empty ( $extraOptionsArray ) ) {

            $extraPayOptions = $this->getExtraPayOptions();

            foreach ( $extraOptionsArray as $key => $value ) {

                if ( isset( $extraPayOptions[ $value ][ 0 ] ) ) {

                    $extra_option_value = wpestate_calculate_extra_options_value( $numberHours, $totalGuests, $extraPayOptions[ $value ][ 2 ], $extraPayOptions[ $value ][ 1 ] );

                    $totalPrice = $totalPrice + $extra_option_value;

                    $customPricesDisplay[ $extraPayOptions[ $value ][ 0 ] ] = wpestate_show_price_booking( $extra_option_value, $currency, $whereCurrency, 1 );
                }

            }
        }


        if ( ! empty ( $manualExpenses ) && is_array( $manualExpenses ) ) {

            foreach ( $manualExpenses as $key => $value ) {
                if ( (float) $value[ 1 ] !== 0 ) {
                    $totalPrice = $totalPrice + (float) $value[ 1 ];
                }
            }

        }

        //extra price per guest
        if ( $hasGuestOverload == 1 && $totalExtraPricePerGuest > 0 ) {
            $totalPrice = $totalPrice + $totalExtraPricePerGuest;
        }


        //early bird discount TODO
        $earlyBirdDiscount = wpestate_early_bird( $this->ID, $early_bird_percent, $early_bird_days, $fromDateDiscount, $totalPrice );

        if ( $earlyBirdDiscount > 0 ) {
            $totalPrice = $totalPrice - $earlyBirdDiscount;
        }


        //security depozit - refundable
        if ( ! empty( $securityDeposit ) ) {
            $totalPrice = $totalPrice + $securityDeposit;
        }


        $totalPriceBeforeExtra = $totalPrice;


        //cleaning or city fee per day

        $cleaningFee = Price::calculateCleaningFee( $numberHours, $currentGuestsNumber, $cleaningFee, $cleaningFeePerDay );
        $cityFee     = Price::calculateCityFee( $numberHours, $currentGuestsNumber, $cityFee, $cityFeePerDay, $cityFeePercent, $internPrice );


        if ( $cleaningFee != 0 && $cleaningFee != '' ) {
            $totalPrice = $totalPrice + $cleaningFee;
        }

        if ( $cityFee != 0 && $cityFee != '' ) {
            $totalPrice = $totalPrice + $cityFee;
        }


        if ( empty( $invoiceID ) ) {
            $priceForServiceFee = $totalPrice - $securityDeposit - floatval( $cityFee ) - floatval( $cleaningFee );
            $serviceFee         = wpestate_calculate_service_fee( $priceForServiceFee, $invoiceID );
        } else {
            /* TODO Get invoice model */
            $serviceFee = get_post_meta( $invoiceID, 'service_fee', true );
        }


        if ( $includeExpenses == 'yes' ) {
            $deposit = wpestate_calculate_deposit( $wpEstateBookDown, $wpEstateBookDownFixedFee, $totalPrice );
        } else {
            $deposit = wpestate_calculate_deposit( $wpEstateBookDown, $wpEstateBookDownFixedFee, $totalPriceBeforeExtra );
        }

        /* TODO get booking object */
        if ( empty( $invoiceID ) ) {
            $youEarn = $totalPrice - $securityDeposit - (float) $cityFee - (float) $cleaningFee - $serviceFee;
            //update_post_meta( $bookid, 'you_earn', $you_earn );
        } else {
            $youEarn = get_post_meta( $bookid, 'you_earn', true );
        }


        $taxes = 0;

        if ( empty( $invoiceID ) ) {
            $taxesValue = $this->getTaxes();
        } else {

            $taxesValue = floatval( get_post_meta( $invoiceID, 'prop_taxed', true ) );
        }
        if ( $taxesValue > 0 ) {
            $taxes = round( $youEarn * $taxesValue / 100, 2 );
        }

        /* TODO Get booking object */
        if ( intval( $invoiceID ) == 0 ) {
            //update_post_meta( $bookid, 'custom_price_array', $customPriceArray );
        } else {
            $customPriceArray = get_post_meta( $bookid, 'custom_price_array', true );
        }

        $balance                                       = $totalPrice - $deposit;
        $return_array                                  = [];
        $return_array[ 'default_price' ]               = $pricePerHour;
        $return_array[ 'week_price' ]                  = $weekPrice;
        $return_array[ 'month_price' ]                 = $monthPrice;
        $return_array[ 'total_price' ]                 = $totalPrice;
        $return_array[ 'inter_price' ]                 = $internPrice;
        $return_array[ 'balance' ]                     = $balance;
        $return_array[ 'deposit' ]                     = $deposit;
        $return_array[ 'from_date' ]                   = $bookingFromDate;
        $return_array[ 'to_date' ]                     = $bookingToDate;
        $return_array[ 'cleaning_fee' ]                = $cleaningFee;
        $return_array[ 'city_fee' ]                    = $cityFee;
        $return_array[ 'has_custom' ]                  = $hasCustom;
        $return_array[ 'custom_price_array' ]          = $bookingCustomPrices;
        $return_array[ 'numberDays' ]                  = $numberHours;
        $return_array[ 'count_days' ]                  = $countDays;
        $return_array[ 'count_hours' ]                 = $numberHours;
        $return_array[ 'has_wkend_price' ]             = $hasWeekendPrice;
        $return_array[ 'has_guest_overload' ]          = $hasGuestOverload;
        $return_array[ 'total_extra_price_per_guest' ] = $totalExtraPricePerGuest;
        $return_array[ 'extra_guests' ]                = $extraGuests;
        $return_array[ 'extra_price_per_guest' ]       = $extraPricePerGuest;
        $return_array[ 'price_per_guest_from_one' ]    = $pricePerGuestFromOne;
        $return_array[ 'curent_guest_no' ]             = $currentGuestsNumber;
        $return_array[ 'cover_weekend' ]               = $coverWeekend;
        $return_array[ 'custom_period_quest' ]         = $customPeriodQuest;
        $return_array[ 'security_deposit' ]            = $securityDeposit;
        $return_array[ 'early_bird_discount' ]         = $earlyBirdDiscount;
        $return_array[ 'taxes' ]                       = $taxes;
        $return_array[ 'service_fee' ]                 = $serviceFee;
        $return_array[ 'youearned' ]                   = $youEarn;
        $return_array[ 'custom_prices_display' ]       = $customPricesDisplay;

        return $return_array;

    }

    /**
     * @param bool $onlyRemoveEvents
     *
     * @return $this
     */
    public function disconnectGoogleCalendar( $onlyRemoveEvents = false ) {

        $bookings = Booking::getForProperty( $this );

        do_action( 'wpk/d5657781/property/beforeGoogleCalendarDisconnect', $this->ID );

        foreach ( $bookings->all() as $booking ) {
            /** @var Booking $booking */

            //Hook to remove cron task
            do_action( 'wpk/d5657781/booking/beforeCalendarDisconnect', $booking->ID );

            if ( $booking->meta( 'created_from_google_event' ) ) {
                $booking->delete();
            } else {
                Calendar::removeEventForBooking( $booking, $booking->meta( 'google_calendar_id' ) );
            }

            $booking
                ->deleteMeta( 'google_calendar_id' )
                ->deleteMeta( 'google_event_id' );

        }


        if ( ! $onlyRemoveEvents ) {
            //Remove google calendar ID from property meta
            $this->deleteMeta( 'google_calendar_id' )
                 ->deleteMeta( 'google_access_token' );
        }

        $this->updateReservationArray();

        return $this;

    }

}
