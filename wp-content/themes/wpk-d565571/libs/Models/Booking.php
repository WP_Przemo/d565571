<?php


namespace Wpk\d565571\Models;

use Wpk\d565571\Helpers\DateTime;

/**
 * Model for property booking
 *
 * @author Przemysław Żydek
 */
class Booking extends Post {

    /** @var string */
    const DATE_FORMAT = 'Y-m-d H:i';

    /**
     * @return bool|Post
     */
    public function create() {
        $this->attributes[ 'post_type' ] = 'wpestate_booking';

        return parent::create();
    }

    /**
     * @return Collection
     */
    public function get() {
        $this->attributes[ 'post_type' ] = 'wpestate_booking';

        return parent::get();
    }

    /**
     * @return \DateTime
     */
    public function getStartDate() {
        return new DateTime( $this->meta( 'booking_from_date' ) );
    }

    /**
     * @return \DateTime
     */
    public function getEndDate() {
        return new DateTime( $this->meta( 'booking_to_date' ) );
    }

    /**
     * @param \DateTime $date
     *
     * @return Booking
     */
    public function updateStartDate( \DateTime $date ) {
        return $this->updateMeta( 'booking_from_date', $date->format( self::DATE_FORMAT ) );
    }

    /**
     * @param \DateTime $date
     *
     * @return Booking
     */
    public function updateEndDate( \DateTime $date ) {
        return $this->updateMeta( 'booking_to_date', $date->format( self::DATE_FORMAT ) );
    }

    /**
     * @return Property
     */
    public function getProperty() {
        return Property::find( $this->meta( 'booking_id' ) );
    }

    /**
     * @return mixed
     */
    public function getRcapiBookingID() {
        return $this->meta( 'rcapi_booking_id' );
    }

    /**
     * @param array $args
     *
     * @return bool|Post
     */
    public static function createForProperty( $args = [] ) {

        $args = wp_parse_args( $args, [
            'property'      => false,
            'start_date'    => new \DateTime(),
            'end_date'      => new \DateTime(),
            'guests'        => 1,
            'comment'       => ' ',
            'author'        => get_current_user_id(),
            'extra_options' => [],
            'confirmed'     => false,
            'send_emails'   => true,
            'meta'          => [],
        ] );

        /** @var Property $property */
        $property = $args[ 'property' ];

        /** @var \DateTime $endDate */
        $endDate = $args[ 'end_date' ];

        /** @var \DateTime $startDate */
        $startDate = $args[ 'start_date' ];

        $author = User::find( $args[ 'author' ] );

        if ( ! self::isAvailable( $property, clone $startDate, clone $endDate ) ) {
            return false;
        }

        $prices = $property->getPrices( [
            'start_date'          => clone $startDate,
            'end_date'            => clone $endDate,
            'guest_no'            => $args[ 'guests' ],
            'extra_options_array' => $args[ 'extra_options' ],
        ] );

        //Meta for booking, exact copy of currently used on from WP Rentals (blame theme developers for this)
        $meta = [
            'booking_status'      => 'pending',
            'booking_id'          => $property->ID,
            'owner_id'            => $property->post_author,
            'booking_from_date'   => $startDate->format( self::DATE_FORMAT ),
            'booking_to_date'     => $endDate->format( self::DATE_FORMAT ),
            'booking_invoice_no'  => 0,
            'booking_pay_ammount' => 0,
            'booking_guests'      => $args[ 'guests' ],
            'extra_options'       => $args[ 'extra_options' ],
            'security_deposit'    => $property->getSecurityDeposit(),
            'full_pay_invoice_id' => 0,
            'to_be_paid'          => $prices[ 'deposit' ],
            'early_bird_percent'  => 0,
            'early_bird_days'     => 0,
            'booking_taxes'       => $prices[ 'taxes' ],
            'service_fee'         => $prices[ 'service_fee' ],
            'taxes'               => $prices[ 'taxes' ],
            'youearned'           => $prices[ 'youearned' ],
            'custom_price_array'  => $prices[ 'custom_price_array' ],
            'balance'             => $prices[ 'custom_price_array' ],
            'total_price'         => $prices[ 'total_price' ],
        ];

        $eventName = esc_html__( 'Booking Request', 'wpestate' );

        $booking = self::init()
                       ->title( $eventName )
                       ->content( empty( $args[ 'comment' ] ) ? '' : $args[ 'comment' ] )
                       ->status( 'publish' )
                       ->author( $args[ 'author' ] )
                       ->addMetas( $meta )
                       ->create();

        if ( ! $booking ) {
            return false;
        }

        $booking->updateMetas( $args[ 'meta' ] );

        //Update booking title to include it's ID in it (like WP Rentals does)

        /**
         * @var Booking $booking
         */
        $booking = $booking->title( "$eventName {$booking->ID}" )->update();

        //For some odd reason when booking is created by owner it contains wrong dates, so we set them again
        $booking->updateMetas( [
            'booking_from_date' => $startDate->format( self::DATE_FORMAT ),
            'booking_to_date'   => $endDate->format( self::DATE_FORMAT ),
        ] );

        if ( $args[ 'confirmed' ] ) {
            $booking->updateMeta( 'booking_status', 'confirmed' );

            do_action( 'wpk/d5657781/bookingConfirmed', $booking );
        }

        if ( $property->post_author == $author->ID ) {

            if ( $args[ 'send_emails' ] ) {

                $subject     = esc_html__( 'You reserved a period', 'wpestate' );
                $description = esc_html__( 'You have reserverd a period on your own listing', 'wpestate' );

                $receiver      = $property->getAuthor();
                $receiverEmail = $receiver->user_email;
                $receiverID    = $receiver->ID;

                wpestate_add_to_inbox( $receiverID, $receiverID, $receiverID, $subject, $description, "internal_book_req" );
                wpestate_send_booking_email( 'mynewbook', $receiverEmail, $property->ID );

            }

            /**
             * @param Booking $booking
             * @param array   $prices Prices array, so that we don't have to calculate them again when using callback
             */
            do_action( 'wpk/d5657781/bookingCreatedInternal', $booking, $prices );


        } else {

            if ( $args[ 'send_emails' ] ) {

                $receiver      = $property->getAuthor();
                $receiverEmail = $receiver->user_email;

                $to         = $property->post_author;
                $receiverID = $author->ID;

                $subject     = esc_html__( 'New Booking Request from ', 'wpestate' );
                $description = sprintf( esc_html__( 'Dear %s, You have received a new booking request from %s. Message sent to %s and %s', 'wpestate' ), $receiver->user_login, $author->user_login, $receiver->user_login, $author->user_login );

                wpestate_add_to_inbox( $receiverID, $receiverID, $to, $subject, $description, "external_book_req" );
                wpestate_send_booking_email( 'newbook', $receiverEmail, $property->ID );

            }
            $rcapiID = $property->getRcapiListingID();

            $details = [
                'booking_status'            => 'pending',
                'original_property_id'      => $property->ID,
                'rcapi_listing_id'          => $rcapiID,
                'book_author'               => $args[ 'author' ],
                'owner_id'                  => $property->post_author,
                'booking_from_date'         => $startDate->format( self::DATE_FORMAT ),
                'booking_to_date'           => $endDate->format( self::DATE_FORMAT ),
                'booking_invoice_no'        => 0,
                'booking_pay_ammount'       => $prices[ 'deposit' ],
                'booking_guests'            => $args[ 'guests' ],
                'extra_options'             => $args[ 'extra_options' ],
                'security_deposit'          => $prices[ 'security_deposit' ],
                'full_pay_invoice_id'       => 0,
                'to_be_paid'                => $prices[ 'deposit' ],
                'youearned'                 => $prices[ 'youearned' ],
                'service_fee'               => $prices[ 'service_fee' ],
                'booking_taxes'             => $prices[ 'taxes' ],
                'total_price'               => $prices[ 'total_price' ],
                'custom_price_array'        => $prices[ 'custom_price_array' ],
                'submission_curency_status' => esc_html( get_option( 'wp_estate_submission_curency', '' ) ),
                'balance'                   => $prices[ 'balance' ],
            ];

            if ( $prices[ 'balance' ] > 0 ) {
                $booking->updateMeta( 'booking_status_full', 'waiting' );

                $details[ 'booking_status_full' ] = 'waiting';
            }

            rcapi_save_booking( $booking->ID, $details );

            /**
             * @param Booking $booking
             * @param array   $prices Prices array, so that we don't have to calculate them again when using callback
             */
            do_action( 'wpk/d5657781/bookingCreated', $booking, $prices );


        }

        return $booking;

    }

    /**
     * Check if provided start date and end date can be booked
     *
     * @param Property  $property
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     *
     * @return bool
     */
    public static function isAvailable( Property $property, \DateTime $startDate, \DateTime $endDate ) {

        $reservationArray = $property->getReservationArray();

        //Fix for invalid time zone
        $endTimestamp   = strtotime( $endDate->format( self::DATE_FORMAT ) );
        $startTimestamp = strtotime( $startDate->format( self::DATE_FORMAT ) );

        while ( $endTimestamp >= $startTimestamp ) {

            if ( array_key_exists( $startTimestamp, $reservationArray ) ) {
                return false;
            }

            $startDate->modify( '+1 hour' );

            $startTimestamp = strtotime( $startDate->format( self::DATE_FORMAT ) );

        }

        return true;

    }

    /**
     * @param Property $property
     * @param bool     $onlyConfirmed Whenever get only confirmed bookings
     *
     * @return Collection
     */
    public static function getForProperty( Property $property, $onlyConfirmed = true ) {

        $model = self::init()->hasMetaValue( 'booking_id', $property->ID );

        if ( $onlyConfirmed ) {
            $model->hasMetaValue( 'booking_status', 'confirmed' );
        }

        return $model->get();

    }

}
