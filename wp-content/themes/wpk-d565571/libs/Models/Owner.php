<?php

namespace Wpk\d565571\Models;

/**
 * @author Przemysław Żydek
 */
class Owner extends Post {

    /**
     * @return bool|Post
     */
    public function create() {
        $this->attributes[ 'post_type' ] = 'estate_agent';

        return parent::create();
    }

    /**
     * @return Collection
     */
    public function get() {
        $this->attributes[ 'post_type' ] = 'estate_agent';

        return parent::get();
    }

    /**
     * @param User $user
     *
     * @return bool|self
     *
     * @throws \Exception
     */
    public static function createForUser( User $user ) {

        return self::init()
                   ->title( $user->user_login )
                   ->content( ' ' )
                   ->status( 'publish' )
                   ->addMetas( [
                       'user_agent_id' => $user->ID,
                   ] )
                   ->create();

    }

    /**
     * @param int $userID
     *
     * @return bool|self
     */
    public static function getForUser( $userID ) {
        $model = self::init()->hasMetaValue( 'user_agent_id', $userID )->get();

        return $model->first();
    }

    /**
     * @param int      $userID
     * @param null|int $propertyID
     * @param int      $authorID
     *
     * @return array|int
     */
    public static function getRatings( $userID, $propertyID = null, $authorID = null ) {

        $args = [
            'post_id' => self::getForUser( $userID )->ID,
        ];

        if ( ! empty( $propertyID ) ) {

            $args[ 'meta_query' ][] = [
                'key'   => 'listing_id',
                'value' => $propertyID,
            ];

        }

        if ( ! empty( $authorID ) ) {

            $args[ 'user_id' ] = $authorID;

        }

        return get_comments( $args );

    }

}
