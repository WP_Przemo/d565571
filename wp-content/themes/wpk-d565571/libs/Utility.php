<?php

namespace Wpk\d565571;

use Wpk\d565571\Api\Cardcom\Iframe;
use Wpk\d565571\Api\Google\Calendar;
use Wpk\d565571\Cron\GoogleCalendar\SyncBooking;
use Wpk\d565571\Cron\GoogleCalendar\SyncProperty;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;
use Wpk\d565571\Traits;

/**
 * Utility class.
 *
 * @author Przemysław Żydek
 */
class Utility {

    use Traits\Request;

    /**@var string Debug option slug. */
    private static $name = 'wpk_debug';

    /**@var mixed Stores debug option. */
    private static $debug;


    /**
     * Utility constructor
     */
    public function __construct() {

        self::$debug = get_option( self::$name, [] );
        add_action( 'admin_menu', [ $this, 'addMenu' ] );

    }

    /**
     * Update debug option
     *
     * @param mixed $value
     */
    public static function updateDebug( $value ) {

        self::$debug = $value;
        update_option( self::$name, self::$debug );

    }

    /**
     * Helper function for logging stuff
     *
     * @param mixed  $log
     * @param string $title
     */
    public static function log( $log, $title = null ) {

        if ( ! empty( $title ) ) {
            error_log( $title );
        }
        if ( is_array( $log ) || is_object( $log ) ) {
            error_log( print_r( $log, true ) );
        } else {
            error_log( $log );
        }


    }

    /**
     * Get debug option value
     *
     * @return mixed
     */
    public static function getDebug() {

        return self::$debug;

    }

    /**
     * Add debug menu.
     */
    public function addMenu() {

        add_menu_page( 'WPK Debug', 'WPK Debug', 'manage_options', 'wpk_debug', [ $this, 'debugMenu' ] );

    }

    /**
     * Debug menu callback function.
     */
    public function debugMenu() {

        Calendar::syncProperty(
            Property::find( 533 )
        );

    }

    /**
     * Display user first and last name, or login
     *
     * @param \WP_User $user
     *
     * @return string User name
     */
    public static function getUserName( \WP_User $user ) {

        if ( empty( $user->first_name ) || empty( $user->last_name ) ) {
            return $user->user_login;
        }

        return $user->first_name . ' ' . $user->last_name;

    }

    /**
     * Get date range from provided dates and return them in selected format.
     *
     * @param \DateTime     $from
     * @param \DateTime     $to
     * @param \DateInterval $interval
     * @param string        $format
     *
     * @return array
     */
    public static function getDateRange( \DateTime $from, \DateTime $to, \DateInterval $interval, $format = 'Y-m-d' ) {

        $range  = new \DatePeriod( $from, $interval, $to );
        $result = [];


        foreach ( $range as $item ) {
            $result[] = $item->format( $format );
        }
        try {
            $last = new \DateTime( end( $result ) );
            $last->modify( 'tomorrow' );
        } catch ( \Exception $e ) {
            $current_year = date( 'Y' );
            $last         = new \DateTime( $current_year . '-' . end( $result ) );
            $last->modify( 'tomorrow' );
        }
        $result[] = $last->format( $format );


        return $result;

    }

    /**
     * Check if element is iframe.
     *
     * @param $string
     *
     * @return bool
     */
    public static function isIframe( $string ) {

        return strpos( $string, '<iframe' ) !== false;

    }

    /**
     *
     *
     * @param int $value
     *
     * @return float|string
     */
    public static function round( $value ) {

        $value    = floatval( $value );
        $exploded = str_split( (string) $value );
        if ( $value >= 1000000 ) {
            return $exploded[ 0 ] . 'm';
        } else if ( $value >= 100000 ) {
            return $exploded[ 0 ] . $exploded[ 1 ] . $exploded[ 2 ] . 'k';
        } else if ( $value >= 10000 ) {
            return $exploded[ 0 ] . $exploded[ 1 ] . 'k';
        } else if ( $value > 999 ) {
            return $exploded[ 0 ] . 'k';
        }

        return $value;

    }

    /**
     * Validate e-mail address
     *
     * @param string $email
     *
     * @return bool
     */
    public static function validateEmail( $email ) {

        $isValid = true;
        $atIndex = strrpos( $email, "@" );

        if ( is_bool( $atIndex ) && ! $atIndex ) {
            $isValid = false;
        } else {
            $domain    = substr( $email, $atIndex + 1 );
            $local     = substr( $email, 0, $atIndex );
            $localLen  = strlen( $local );
            $domainLen = strlen( $domain );

            if ( $localLen < 1 || $localLen > 64 ) {
                // local part length exceeded
                $isValid = false;
            } else if ( $domainLen < 1 || $domainLen > 255 ) {
                // domain part length exceeded
                $isValid = false;
            } else if ( $local[ 0 ] == '.' || $local[ $localLen - 1 ] == '.' ) {
                // local part starts or ends with '.'
                $isValid = false;
            } else if ( preg_match( '/\\.\\./', $local ) ) {
                // local part has two consecutive dots
                $isValid = false;
            } else if ( ! preg_match( '/^[A-Za-z0-9\\-\\.]+$/', $domain ) ) {
                // character not valid in domain part
                $isValid = false;
            } else if ( preg_match( '/\\.\\./', $domain ) ) {
                // domain part has two consecutive dots
                $isValid = false;
            } else if ( ! preg_match( '/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace( "\\\\", "", $local ) ) ) {
                // character not valid in local part unless
                // local part is quoted
                if ( ! preg_match( '/^"(\\\\"|[^"])+"$/',
                    str_replace( "\\\\", "", $local ) ) ) {
                    $isValid = false;
                }
            }

            if ( $isValid && function_exists( "checkdnsrr" ) && ! ( checkdnsrr( $domain, "MX" ) || checkdnsrr( $domain, "A" ) ) ) {
                // domain not found in DNS
                $isValid = false;
            }
        }

        return $isValid;

    }

    /**
     * Check if string contains provided substring
     *
     * @param string       $string
     * @param string|array $substring
     *
     * @return bool
     */
    public static function contains( $string, $substring ) {

        if ( is_array( $substring ) ) {
            foreach ( $substring as $item ) {
                if ( strpos( $string, $item ) === false ) {
                    return false;
                }
            }

            return true;
        } else {
            return strpos( $string, $substring ) !== false;
        }

    }

    /**
     * Helper function for getting templates.
     *
     * @param string $path Path to template (relative to /templates)
     * @param bool   $return Whenever return template content or just load it
     *
     * @return bool|string
     */
    public static function getTemplate( $path, $return = false ) {

        $core         = Core();
        $templatePath = $core->dir . '/templates/';
        $path         = $templatePath . $path;

        if ( ! file_exists( $path ) ) {
            return false;
        }

        if ( $return ) {
            ob_start();
            require_once $path;

            return ob_get_clean();
        } else {
            require_once $path;

            return true;
        }

    }

    /**
     * Perform console log with provided value
     *
     * @param mixed $value
     */
    public static function consoleLog( $value ) {

        $debug = debug_backtrace();
        $debug = $debug[ 0 ][ 'file' ] . ' Line ' . $debug[ 0 ][ 'line' ];

        $hook = is_admin() ? 'admin_footer' : 'wp_footer';

        add_action( $hook, function () use ( $debug, $value ) {
            ?>
			<script>
                'use strict';
                console.log(<?php echo json_encode( $debug ) ?>);
                console.log(<?php echo json_encode( $value ) ?>);
			</script>
            <?php
        } );

    }

    /**
     * Get page by provideded template name
     *
     * @param string $template Template name (for example page.php)
     * @param bool   $single Whenever return only first result, or array with all results.
     *
     * @return array|\WP_Post|false
     */
    public static function getPageByTemplate( $template, $single = true ) {

        $args  = [
            'meta_key'   => '_wp_page_template',
            'meta_value' => $template,
        ];
        $pages = get_pages( $args );

        if ( ! empty( $pages ) ) {
            if ( $single ) {
                return array_shift( $pages );
            }

            return $pages;
        }

        return false;

    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     *
     * @link https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
     *
     * @param float     $latitudeFrom Latitude of start point in [deg decimal]
     * @param float     $longitudeFrom Longitude of start point in [deg decimal]
     * @param float     $latitudeTo Latitude of target point in [deg decimal]
     * @param float     $longitudeTo Longitude of target point in [deg decimal]
     * @param float|int $earthRadius Mean earth radius in [m]
     *
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function haversineGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000
    ) {
        // convert from degrees to radians
        $latFrom = deg2rad( $latitudeFrom );
        $lonFrom = deg2rad( $longitudeFrom );
        $latTo   = deg2rad( $latitudeTo );
        $lonTo   = deg2rad( $longitudeTo );

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin( sqrt( pow( sin( $latDelta / 2 ), 2 ) +
                                 cos( $latFrom ) * cos( $latTo ) * pow( sin( $lonDelta / 2 ), 2 ) ) );

        return $angle * $earthRadius;
    }

}
