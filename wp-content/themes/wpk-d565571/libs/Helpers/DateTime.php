<?php

namespace Wpk\d565571\Helpers;

use DateTimeZone;

/**
 * @author Przemysław Żydek
 */
class DateTime extends \DateTime {

    /**
     * DateTime constructor.
     *
     * @param string            $time
     * @param DateTimeZone|null $timezone
     */
    public function __construct( $time = 'now', $timezone = null ) {

        if ( empty( $timezone ) ) {
            $timezone = new \DateTimeZone( get_option( 'timezone_string' ) );
        }

        parent::__construct( $time, $timezone );
    }

    /**
     * Round not full hours. For example 10:30 will be rounded to 10:00.
     *
     * @return $this
     */
    public function roundHours() {

        $date = $this->format( 'Y-m-d H' );
        $date .= ':00';

        $timestamp = strtotime( $date );

        return $this->setTimestamp( $timestamp );
    }

    /**
     * @param mixed $timestamp
     *
     * @return self
     */
    public static function createForGoogleEvent( $timestamp ) {

        $date = new self( $timestamp );

        return $date->roundHours();

    }

}
