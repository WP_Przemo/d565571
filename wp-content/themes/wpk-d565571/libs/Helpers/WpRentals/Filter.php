<?php


namespace Wpk\d565571\Helpers\WpRentals;

/**
 * Helper class for getting values for WPRentals filter
 *
 * @author Przemysław Żydek
 */
class Filter {

    /**
     * @param string $dateKey
     * @param string $hourKey
     *
     * @return string
     */
    public static function getDateAndHour( $dateKey, $hourKey ) {

        if ( empty( $_REQUEST[ $dateKey ] ) || empty( $_REQUEST[ $hourKey ] ) ) {
            return '';
        }

        $allowed_html = [];

        $value = sanitize_text_field( wp_kses( $_REQUEST[ $dateKey ], $allowed_html ) );
        $value .= ' ';
        $value .= sanitize_text_field( wp_kses( $_REQUEST[ $hourKey ], $allowed_html ) );

        return $value;

    }

}