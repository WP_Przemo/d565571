<?php

namespace Wpk\d565571\Helpers\WpRentals;

use Wpk\d565571\Utility;

/**
 * @author Przemysław Żydek
 */
class Order {

    /**
     * @param  float    $userLat
     * @param  float    $userLng
     * @param  int      $paged
     * @param \WP_Query $query
     *
     * @return \WP_Query
     */
    public static function byUserDistance( $userLat, $userLng, $paged, \WP_Query $query ) {

        $orderCallback = function ( \WP_Post $postA, \WP_Post $postB ) use ( $userLat, $userLng ) {

            $latA = (float) $postA->property_latitude;
            $lngA = (float) $postA->property_longitude;

            $latB = (float) $postB->property_latitude;
            $lngB = (float) $postB->property_longitude;

            $postADistance = Utility::haversineGreatCircleDistance(
                $userLat,
                $userLng,
                $latA,
                $lngA
            );

            $postBDistance = Utility::haversineGreatCircleDistance(
                $userLat,
                $userLng,
                $latB,
                $lngB
            );

            return $postADistance > $postBDistance;

        };

        usort( $query->posts, $orderCallback );

        $postIDs = array_map( function ( $post ) {
            return $post->ID;
        }, $query->posts );

        return new \WP_Query( [
            'post__in'       => $postIDs,
            'post_type'      => 'estate_property',
            'paged'          => $paged,
            'orderby'        => 'post__in',
            'posts_per_page' => intval( get_option( 'wp_estate_prop_no', '' ) ),
        ] );

    }

    /**
     * @param           $paged
     * @param           $order
     * @param \WP_Query $query
     *
     * @return \WP_Query
     */
    public static function byPrice( $paged, $order, \WP_Query $query ) {

        $orderCallback = function ( \WP_Post $postA, \WP_Post $postB ) use ( $order ) {

            $priceA = (float) $postA->property_price;

            $priceB = (float) $postB->property_price;

            return $order === 'ASC' ? $priceA > $priceB : $priceA < $priceB;

        };

        usort( $query->posts, $orderCallback );

        $postIDs = array_map( function ( $post ) {
            return $post->ID;
        }, $query->posts );

        return new \WP_Query( [
            'post__in'       => $postIDs,
            'post_type'      => 'estate_property',
            'paged'          => $paged,
            'orderby'        => 'post__in',
            'posts_per_page' => intval( get_option( 'wp_estate_prop_no', '' ) ),
        ] );


    }

}
