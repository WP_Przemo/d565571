<?php

namespace Wpk\d565571\Helpers\WpRentals;

/**
 * @author Przemysław Żydek
 */
class Dropdown {

    /**
     * Creates WpRentals dropdown
     *
     * @param array $args
     *
     * @return string
     */
    public static function create( array $args ) {

        $args += [
            'options' => [],
            'value'   => null,
            'id'      => '',
            'label'   => '',
        ];

        return Core()->view->render( 'wprentals-dropdown', $args );

    }

}
