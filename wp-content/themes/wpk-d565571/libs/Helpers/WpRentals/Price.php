<?php

namespace Wpk\d565571\Helpers\WpRentals;

/**
 * Helper class for price related actions from WP Rentals
 *
 * @author Przemysław Żydek
 */
class Price {

    /**
     * Refactor for @see wpestate_calculate_cleaning_fee()
     *
     * @param int $countHours
     * @param int $guestsNumber
     * @param int $cleaningFee
     * @param int $cleaningFeePerDay
     *
     * @return float|int
     */
    public static function calculateCleaningFee( $countHours, $guestsNumber, $cleaningFee, $cleaningFeePerDay ) {

        $value = 0;

        $guestsNumber = (int) $guestsNumber;

        if ( $guestsNumber === 0 ) {
            $guestsNumber = 1;
        }

        switch ( $cleaningFeePerDay ) {
            case 0:// single fee
                $value = $cleaningFee;
                break;
            case 1://per night
                $value = $cleaningFee * $countHours;
                break;
            case 2://per guest
                $value = $cleaningFee * $guestsNumber;
                break;
            case 3://per guest and night
                $value = $cleaningFee * $guestsNumber * $countHours;
                break;
        }

        return $value;
    }

    /**
     * Refactor for @see wpestate_calculate_city_fee()
     *
     * @param $countHours
     * @param $guestsNumber
     * @param $cityFee
     * @param $cityFeePerDay
     * @param $cityFeePercent
     * @param $interFee
     *
     * @return float|int
     */
    public static function calculateCityFee( $countHours, $guestsNumber, $cityFee, $cityFeePerDay, $cityFeePercent, $interFee ) {

        $value        = 0;
        $guestsNumber = (int) $guestsNumber;

        if ( $guestsNumber == 0 ) {
            $guestsNumber = 1;
        }

        if ( $cityFeePercent == 0 ) {

            switch ( $cityFeePerDay ) {
                case 0:// single fee
                    $value = $cityFee;
                    break;
                case 1://per night
                    $value = $cityFee * $countHours;
                    break;
                case 2://per guest
                    $value = $cityFee * $guestsNumber;
                    break;
                case 3://per guest and night
                    $value = $cityFee * $guestsNumber * $countHours;
                    break;
            }
        } else {
            $value = $interFee * $cityFee / 100;
        }


        return $value;
    }

    /**
     * Refactor for @see wpestate_return_custom_price()
     *
     * @param $fromDate
     * @param $mega
     * @param $pricePerWeekend
     * @param $priceArray
     * @param $pricePerHour
     * @param $countDays
     *
     * @return mixed|string
     */
    public static function getUsablePrice( $fromDate, $mega, $pricePerWeekend, $priceArray, $pricePerHour, $countDays ) {

        $weekday            = date( 'N', $fromDate );
        $setupWeekendStatus = esc_html( get_option( 'wp_estate_setup_weekend', '' ) );

        if ( $setupWeekendStatus == 0 && ( $weekday == 6 || $weekday == 7 ) ) {
            $newPrice = self::getWeekendPrice( $mega, $fromDate, $pricePerWeekend, $pricePerHour, $priceArray, $countDays );
        } else if ( $setupWeekendStatus == 1 && ( $weekday == 5 || $weekday == 6 ) ) {
            $newPrice = self::getWeekendPrice( $mega, $fromDate, $pricePerWeekend, $pricePerHour, $priceArray, $countDays );
        } else if ( $setupWeekendStatus == 2 && ( $weekday == 5 || $weekday == 6 || $weekday == 7 ) ) {
            $newPrice = self::getWeekendPrice( $mega, $fromDate, $pricePerWeekend, $pricePerHour, $priceArray, $countDays );
        } else {
            $newPrice = self::getClassicPrice( $pricePerHour, $priceArray, $fromDate, $countDays, $mega );
        }

        return $newPrice;

    }

    /**
     * Refactor for @see wpestate_calculate_weekedn_price()
     *
     * @param array $mega
     * @param int $fromDate
     * @param float $pricePerWeekend
     * @param float $pricePerHour
     * @param array $priceArray
     * @param int $countDays
     *
     * @return mixed|string
     */
    public static function getWeekendPrice( $mega, $fromDate, $pricePerWeekend, $pricePerHour, $priceArray, $countDays ) {

        if ( isset( $mega[ $fromDate ] ) && isset( $mega[ $fromDate ][ 'period_price_per_weekeend' ] ) && $mega[ $fromDate ][ 'period_price_per_weekeend' ] != 0 ) {
            $newPrice = $mega[ $fromDate ][ 'period_price_per_weekeend' ];
        } else if ( $pricePerWeekend != 0 ) {
            $newPrice = $pricePerWeekend;
        } else {
            $newPrice = self::getClassicPrice( $pricePerHour, $priceArray, $fromDate, $countDays, $mega );
        }

        return $newPrice;

    }

    /**
     * Refactor for @see wpestate_classic_price_return()
     *
     * @param float $pricePerHour
     * @param array $priceArray
     * @param int $fromDate
     * @param int $countDays
     * @param array $mega
     *
     * @return mixed
     */
    public static function getClassicPrice( $pricePerHour, $priceArray, $fromDate, $countDays, $mega ) {

        if ( $countDays >= 7 && $countDays < 30 && isset( $mega[ $fromDate ][ 'period_price_per_week' ] ) && $mega[ $fromDate ][ 'period_price_per_week' ] != 0 ) {

            return $mega[ $fromDate ][ 'period_price_per_week' ];
        } else if ( $countDays >= 30 && isset( $mega[ $fromDate ][ 'period_price_per_month' ] ) && $mega[ $fromDate ][ 'period_price_per_month' ] != 0 ) {
            return $mega[ $fromDate ][ 'period_price_per_month' ];
        } else if ( isset( $priceArray[ $fromDate ] ) ) {
            return $priceArray[ $fromDate ];
        } else {
            return $pricePerHour;
        }


    }


}