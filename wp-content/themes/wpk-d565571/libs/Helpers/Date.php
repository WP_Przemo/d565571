<?php

namespace Wpk\d565571\Helpers;

/**
 * Dste related helper
 *
 * @author Przemysław Żydek
 */
class Date {

    /**
     * Get date without hours. For example, if date provided is 2018-08-16 13:25 the returned DateTime object will have date
     * as 2018-08-16.
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    public static function getWithoutHours( \DateTime $date ) {
        $date = date( 'Y-m-d', strtotime( $date->format( 'Y-m-d' ) ) );

        return new \DateTime($date);
    }

}