<?php

function wpestate_review_composer( $agent_id ) {

    global $post;
    $prop_no  = intval( get_option( 'wp_estate_prop_no', '' ) );
    $owner_id = get_post_meta( $agent_id, 'user_agent_id', true );
    if ( $owner_id == 0 ) {
        $return_array[ 'list_rating' ]    = 0;
        $return_array[ 'coments_no' ]     = 0;
        $return_array[ 'prop_selection' ] = '';
        $return_array[ 'templates' ]      = '';

        return $return_array;
    }

    $post_array   = [];
    $post_array[] = 0;
    $return_array = [];
    $paged        = 1;

    if ( isset( $_GET[ 'pagelist' ] ) ) {
        $paged = intval( $_GET[ 'pagelist' ] );
    }


    $args = [
        'post_type'      => 'estate_property',
        'author'         => $owner_id,
        'paged'          => $paged,
        'posts_per_page' => $prop_no,
        'post_status'    => 'publish',
    ];


    $prop_selection = new WP_Query( $args );

    $return_array[ 'prop_selection' ] = $prop_selection;
    wp_reset_postdata();
    wp_reset_query();


    $arg2_reviews           = [
        'post_type'      => 'estate_property',
        'author'         => $owner_id,
        'paged'          => 1,
        'posts_per_page' => 100,
        'post_status'    => 'publish',
    ];
    $prop_selection_Reviews = new WP_Query( $arg2_reviews );

    //if ( $prop_selection_Reviews->have_posts() ) {
    while ( $prop_selection_Reviews->have_posts() ):
        $prop_selection_Reviews->the_post();

        $post_array[] = $post->ID;
    endwhile;
    wp_reset_postdata();
    wp_reset_query();

    $args = [
        'number'   => '15',
        'post__in' => $post_array,
    ];


    $comments   = get_comments( $args );
    $coments_no = 0;

    if ( empty( $paged ) || $paged === 1 ) {
        $owner_comments = get_comments( [
            'number'  => 15,
            'post_id' => $owner_id,
        ] );
    }

    $comments = array_merge( $comments, $owner_comments );

    $stars_total      = 0;
    $review_templates = '';

    /**
     * @var WP_Comment $comment
     */
    foreach ( $comments as $comment ) :
        $coments_no ++;
        $userId        = $comment->user_id;
        $userid_agent  = get_user_meta( $userId, 'user_agent_id', true );
        $reviewer_name = get_the_title( $userid_agent );

        if ( $userid_agent == '' ) {
            $reviewer_name = $comment->comment_author;
        }


        if ( $userid_agent == '' ) {
            $user_small_picture_id = get_the_author_meta( 'small_custom_picture', $comment->user_id, true );
            $preview               = wp_get_attachment_image_src( $user_small_picture_id, 'wpestate_user_thumb' );
            $preview_img           = $preview[ 0 ];
        } else {
            $thumb_id    = get_post_thumbnail_id( $userid_agent );
            $preview     = wp_get_attachment_image_src( $thumb_id, 'thumbnail' );
            $preview_img = $preview[ 0 ];
        }


        if ( $preview_img == '' ) {
            $preview_img = get_template_directory_uri() . '/img/default_user_agent.gif';
        }

        $rating     = get_comment_meta( $comment->comment_ID, 'review_stars', true );
        $tmp_rating = json_decode( $rating, true );
        $rating     = wpestate_get_star_total_value( $tmp_rating );

        $for = get_comment_meta( $comment->comment_ID, 'for', true );

        if ( $for === 'owner' ) {
            $for = 'host';
        } else if ( $for === 'renter' ) {
            $for = 'guest';
        }

        $for = ucfirst( $for );

        $rating_type = "For $for";

        $stars_total      += $rating;
        $review_templates .= '
                    <div class="listing-review">
                       
                        <div class="col-md-12 review-list-content norightpadding">
                            <div class="reviewer_image"  style="background-image: url(' . $preview_img . ');"></div>
                            <div class="reviwer-name">' . $reviewer_name . '</div>
                            <div class="property_ratings">';
        $review_templates .= wpestate_display_rating( $rating );
        $review_templates .= ' <span class="ratings-star">(' . wpestate_get_star_total_value( wpestate_get_star_total_rating( $rating ) ) . ' ' . esc_html__( 'of', 'wprentals' ) . ' 5)</span> 
                            </div>
                            <div class="wpk-rating-type">
                                   ' . $rating_type . '
                            </div>

                            <div class="review-content">
                                ' . $comment->comment_content . '

                                <div class="review-date">
                                ' . esc_html__( 'Posted on ', 'wprentals' ) . ' ' . get_comment_date( 'j F Y', $comment->comment_ID ) . ' 
                                </div>
                            </div>



                        </div>
                    </div>   ';

    endforeach;

    $return_array[ 'templates' ] = $review_templates;
    $list_rating                 = 0;
    if ( $coments_no > 0 ) {
        $list_rating = ceil( $stars_total / $coments_no );
    }


    $return_array[ 'list_rating' ] = $list_rating;
    $return_array[ 'coments_no' ]  = $coments_no;

    //}// if has listings


    return $return_array;
}
