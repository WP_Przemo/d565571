<?php

/**
 * Overwrite for theme function to include start and end hours
 *
 * @return void
 */
function wpestate_mess_front_end() {
    //  check_ajax_referer( 'mess_ajax_nonce_front', 'security-register' );
    $current_user      = wp_get_current_user();
    $allowed_html      = [];
    $userID            = $current_user->ID;
    $user_login        = $current_user->user_login;
    $subject           = esc_html__( 'Message from ', 'wpestate' ) . $user_login;
    $message_from_user = esc_html( $_POST[ 'message' ] );
    $property_id       = intval( $_POST[ 'agent_property_id' ] );
    $agent_id          = intval( $_POST[ 'agent_id' ] );

    if ( $agent_id === 0 ) {
        $owner_id = wpsestate_get_author( $property_id );
    } else {
        $owner_id = get_post_meta( $agent_id, 'user_agent_id', true );
    }

    $owner       = get_userdata( $owner_id );
    $owner_email = $owner->user_email;
    $owner_login = $owner->ID;
    $subject     = esc_html__( 'Message from ', 'wpestate' ) . $user_login;


    $booking_guest_no  = intval( $_POST[ 'booking_guest_no' ] );
    $booking_from_date = wp_kses( $_POST[ 'booking_from_date' ], $allowed_html );
    $booking_to_date   = wp_kses( $_POST[ 'booking_to_date' ], $allowed_html );

    $start_hour = wp_kses( $_POST[ 'start_hour' ], $allowed_html );
    $end_hour   = wp_kses( $_POST[ 'end_hour' ], $allowed_html );


    $contact_u_name  = wp_kses( $_POST[ 'contact_u_name' ], $allowed_html );
    $contact_u_email = wp_kses( $_POST[ 'contact_u_email' ], $allowed_html );


    if ( $property_id != 0 && get_post_type( $property_id ) === 'estate_property' ) {
        $message_user .= '<strong>' . esc_html__( ' Sent for property: ', 'wpestate' ) . '</strong>' . get_the_title( $property_id ) . ', ' . esc_html__( 'with the link:', 'wpestate' ) . ' ' . get_permalink( $property_id ) . '<br>';
    }

    $message_user .= '<strong>' . esc_html__( 'Selected dates', 'wpestate' ) . ': </strong>' . $booking_from_date . ' ' . $start_hour . esc_html__( ' to ', 'wpestate' ) . $booking_to_date . ' ' . $end_hour . ",<br>";
    $message_user .= '<strong>' . esc_html__( 'Guests', 'wpestate' ) . ': </strong>' . $booking_guest_no . '.<br>';
    $message_user .= '<strong>' . esc_html__( 'Content', 'wpestate' ) . ': </strong>' . $message_from_user . '.<br>';

    if ( ! is_user_logged_in() ) {
        $message_user .= esc_html__( 'Sent by unregistered user with name: ', 'wpestate' ) . $contact_u_name . esc_html__( ' and email: ', 'wpestate' ) . $contact_u_email;
    }


    wpestate_send_booking_email( 'inbox', $owner_email, $message_user );

    // add into inbox
    wpestate_add_to_inbox( $userID, $userID, $owner_login, $subject, $message_user, 1 );

    esc_html_e( 'Your message was sent! You will be notified by email when a reply is received.', 'wpestate' );
    die();
}
