<?php

/**
 * Overwrite theme function for saving post data to use our method to generate booking dates for property
 *
 * @param int $listingID
 *
 * @return array
 */
function wpestate_get_booking_dates( $listingID ) {
    $property = \Wpk\d565571\Models\Property::find( $listingID );

    return $property->updateReservationArray()->getReservationArray();
}
