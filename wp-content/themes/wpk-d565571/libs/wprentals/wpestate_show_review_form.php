<?php

/**
 * Overwrite for theme function to support ratings for owners and renters
 *
 * @return void
 */
function wpestate_show_review_form() {
    //check owner before delete
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }

    $listing_id = intval( $_POST[ 'listing_id' ] );
    $bookid     = intval( $_POST[ 'bookid' ] );

    if ( empty( $_POST[ 'for' ] ) ) {
        exit();
    }

    $for = sanitize_text_field( $_POST[ 'for' ] );

    $review_fields = wpk_get_review_fields( $for );
    $max_stars     = wpestate_get_max_stars();
    $field_html    = '';

    foreach ( $review_fields[ 'fields' ] as $key => $label ) {
        $fields     = 0;
        $field_html .= sprintf( '<div class="%s">', esc_attr( $key ) ) . PHP_EOL;
        $field_html .= sprintf( '<span class="rating_legend">%s</span>', $label );
        while ( $fields < $max_stars ) {
            $fields ++;
            $field_html .= '<span class="empty_star"></span>' . PHP_EOL;
        }
        $field_html .= sprintf( '</div><!-- end .%s -->', esc_attr( $key ) ) . PHP_EOL;
    }

    print '              
            <div class="create_invoice_form" data-for="' . $for . '">
                    <h3>' . esc_html__( 'Post Review', 'wpestate' ) . '</h3>'
          . $field_html .
          '<textarea placeholder="' . __( 'Describe your experience.', 'wpestate' ) . '" id="review_content" name="review_content" class="form - control"></textarea>

                    <div class="action1_booking" id="post_review" data-bookid="' . $bookid . '" data-listing_id="' . $listing_id . '">' . esc_html__( 'Submit Review', 'wpestate' ) . '</div>
            </div>';
    die();
}

/**
 * Helper function for getting review fields depending on who is being reviewed
 *
 * @param string $for
 *
 * @return mixed
 */
function wpk_get_review_fields( $for = 'property' ) {

    $fieldsFor = [
        'property' => [
            'accuracy'      => esc_html__( 'Accuracy', 'wpestate' ),
            'communication' => esc_html__( 'Communication', 'wpestate' ),
            'cleanliness'   => esc_html__( 'Cleanliness', 'wpestate' ),
            'location'      => esc_html__( 'Location', 'wpestate' ),
            'schedule'      => esc_html__( 'Schedule', 'wpestate' ),
            'equipment'     => esc_html__( 'Equipment', 'wpestate' ),
        ],
        'owner'    => [
            'accuracy'      => esc_html__( 'Accuracy', 'wpestate' ),
            'communication' => esc_html__( 'Communication', 'wpestate' ),
            'recommend'     => esc_html__( 'Would you recommend this owner?', 'wpestate' ),
        ],
        'renter'   => [
            'accuracy'      => esc_html__( 'Accuracy', 'wpestate' ),
            'communication' => esc_html__( 'Communication', 'wpestate' ),
            'cleanliness'   => esc_html__( 'Cleanliness', 'wpestate' ),
            'observance'    => esc_html__( 'Observance of space rules', 'wpestate' ),
            'recommend'     => esc_html__( 'Would you recommend this guest?', 'wpestate' ),
        ],
    ];

    return [
        'fields' => isset( $fieldsFor[ $for ] ) ? $fieldsFor[ $for ] : $fieldsFor[ 'property' ],
        'total'  => esc_html__( 'Total', 'wpestate' ),
    ];
}
