<?php

function wpestate_send_booking_email( $email_type, $receiver_email, $content = '' ) {
    $user_email = $receiver_email;

    if ( $email_type == 'bookingconfirmeduser' ) {
        $arguments = [
            'property_id' => $content,
        ];
        wpestate_select_email_type( $user_email, 'bookingconfirmeduser', $arguments );
    }
    if ( $email_type == 'bookingconfirmed' ) {
        $arguments = [];
        wpestate_select_email_type( $user_email, 'bookingconfirmed', $arguments );
    } else if ( $email_type == 'bookingconfirmed_nodeposit' ) {
        $arguments = [];
        wpestate_select_email_type( $user_email, 'bookingconfirmed_nodeposit', $arguments );
    } else if ( $email_type == 'inbox' ) {
        $arguments = [ 'content' => $content ];
        wpestate_select_email_type( $user_email, 'inbox', $arguments );
    } else if ( $email_type == 'newbook' ) {
        $property_id = intval( $content );
        $arguments   = [
            'booking_property_link' => get_permalink( $property_id ),
        ];
        wpestate_select_email_type( $user_email, 'newbook', $arguments );
    } else if ( $email_type == 'mynewbook' ) {
        $property_id = intval( $content );
        $arguments   = [
            'booking_property_link' => get_permalink( $property_id ),
        ];
        wpestate_select_email_type( $user_email, 'mynewbook', $arguments );
    } else if ( $email_type == 'newinvoice' ) {
        $arguments = [];
        wpestate_select_email_type( $user_email, 'newinvoice', $arguments );
    } else if ( $email_type == 'deletebooking' ) {
        $arguments = [];
        wpestate_select_email_type( $user_email, 'deletebooking', $arguments );
    } else if ( $email_type == 'deletebookinguser' ) {
        $arguments = [];
        wpestate_select_email_type( $user_email, 'deletebookinguser', $arguments );
    } else if ( $email_type == 'deletebookingconfirmed' ) {
        $arguments = [];
        wpestate_select_email_type( $user_email, 'deletebookingconfirmed', $arguments );
    }

    /*
    $email_headers = "From: <noreply@".$_SERVER['HTTP_HOST']."> \r\n Reply-To:<noreply@".$_SERVER['HTTP_HOST'].">";
    $headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
                    'Reply-To: <noreply@'.$_SERVER['HTTP_HOST'].'>\r\n" '.
                    'X-Mailer: PHP/' . phpversion();

    $mail = wp_mail($receiver_email, $subject, $message, $headers);
    */
}

function wpestate_select_email_type( $user_email, $type, $arguments ) {
    $value         = get_option( 'wp_estate_' . $type, '' );
    $value_subject = get_option( 'wp_estate_subject_' . $type, '' );

    if ( function_exists( 'icl_translate' ) ) {
        $value         = icl_translate( 'wpestate', 'wp_estate_email_' . $value, $value );
        $value_subject = icl_translate( 'wpestate', 'wp_estate_email_subject_' . $value_subject, $value_subject );
    }

    $value         = stripslashes( $value );
    $value_subject = stripslashes( $value_subject );

    if ( $type === 'bookingconfirmeduser' && isset( $arguments[ 'property_id' ] ) ) {

        $property_id = $arguments[ 'property_id' ];
        $property    = \Wpk\d565571\Models\Property::find( $property_id );

        $arrival = $property->meta( 'arrival' );
        $phone   = $property->meta( 'phone' );
        $phone1  = $property->meta( 'phone1' );
        $address = $property->meta( 'property_address' );

        $value .= "<p>Arrival directions: $arrival</p>";
        $value .= "<p>Phone: $phone</p>";

        if ( ! empty( $phone1 ) ) {
            $value .= "<p>Phone (additional): $phone1</p>";
        }

        $value .= "<p>Address: $address</p>";

    }


    // send also to sms
    $user_data   = get_user_by( 'email', $user_email );
    $user_mobile = get_the_author_meta( 'mobile', $user_data->ID );
    wpestate_select_sms_type( $user_mobile, $type, $arguments, $user_email, $user_data->ID );

    wpestate_emails_filter_replace( $user_email, $value, $value_subject, $arguments );
}

function wpestate_emails_filter_replace( $user_email, $message, $subject, $arguments ) {
    $arguments [ 'website_url' ]  = get_option( 'siteurl' );
    $arguments [ 'website_name' ] = get_option( 'blogname' );

    if ( isset( $arguments [ 'user_profile_url' ] ) && isset( $arguments[ 'user_id' ] ) ) {
        $user_edit_url                    = esc_url( sprintf( '%suser-edit.php?user_id=%d', admin_url(), absint( $arguments[ 'user_id' ] ) ) );
        $arguments [ 'user_profile_url' ] = sprintf( ' <a href="%s">%s</a>', $user_edit_url, esc_html__( 'Edit user ID verification', 'wprentals' ) );
    }
    //  $arguments ['user_email'] = $user_email;
    //  $user= get_user_by('email',$user_email);
    //  $arguments ['username'] = $user->user_login;


    $current_user               = wp_get_current_user();
    $arguments [ 'user_email' ] = $current_user->user_email;
    $arguments [ 'username' ]   = $current_user->user_login;

    foreach ( $arguments as $key_arg => $arg_val ) {
        $subject = str_replace( '%' . $key_arg, $arg_val, $subject );
        $message = str_replace( '%' . $key_arg, $arg_val, $message );
    }

    wpestate_send_emails( $user_email, $subject, $message );
}

function wpestate_send_emails( $user_email, $subject, $message ) {


    // add_filter( 'wp_mail_content_type', 'wpestate_set_html_mail_content_type' );
    $headers   = [];
    $headers[] = 'From: No Reply <noreply@' . $_SERVER[ 'HTTP_HOST' ] . '>' . "\r\n";
    $headers[] = 'Content-Type: text/html; charset=UTF-8';
    @wp_mail(
        $user_email,
        $subject,
        $message,
        $headers
    );

    // remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );

    $duplicate_email_adr = esc_html( get_option( 'wp_estate_duplicate_email_adr', '' ) );
    if ( $duplicate_email_adr != '' ) {
        $message = $message . ' ' . __( 'Message was also sent to ', 'wprentals' ) . $user_email;
        wp_mail( $duplicate_email_adr, $subject, $message, $headers );
    }
}

;
