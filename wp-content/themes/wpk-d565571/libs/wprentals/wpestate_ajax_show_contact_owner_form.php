<?php

function wpestate_ajax_show_contact_owner_form() {

//        $post_id=intval($_POST['booking_id']);
//        if($post_id==0){
//            $agent_id=intval($_POST['agent_id']);
//        }else{
//            $agent_id=0;
//        }
    global $post;
    if ( is_singular( 'estate_property' ) ) {
        $post_id  = $post->ID;
        $agent_id = 0;
    } else {
        $agent_id = $post->ID;
        $post_id  = 0;
    }


    print'
                <!-- Modal -->
                <form class="modal  fade" id="contact_owner_modal" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">';


    print'
                            <div class="modal-header"> 
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h2 class="modal-title_big">' . esc_html__( 'Contact the owner', 'wpestate' ) . '</h2>
                              <h4 class="modal-title" id="myModalLabel">' . esc_html__( 'Please complete the form below to contact owner.', 'wpestate' ) . '</h4>
                            </div>

                            <div class="modal-body">
                                <div id="booking_form_request_mess_modal" class="ajax"></div>  ';

    if ( ! is_user_logged_in() ) {

        print ' <div class="">
                                    <input type="text" id="contact_u_name" size="40" name="contact_u_name" class="form-control" placeholder="' . esc_html__( 'Your Name', 'wpestate' ) . '" value="">
                                </div>';

        print ' <div class="">
                                    <input type="text" id="contact_u_email" size="40" name="contact_u_email" class="form-control" placeholder="' . esc_html__( 'Your Email', 'wpestate' ) . '" value="">
                                </div>';

    }


    print'
                                <section class="wpk-time-section">
                                    <div class=" has_calendar calendar_icon">
                                        <input type="text" id="booking_from_date" size="40" name="booking_from_date" class="form-control" placeholder="' . esc_html__( 'Check In', 'wpestate' ) . '" value="">
                                    </div>
                                    <div class="wpk-clock-icon wpk-booking-hours col-md-12">
					                    <select name="booking_start_hour" id="booking_start_hour">
						                    <option disabled selected>' . esc_html__( 'Hour' ) . '</option>';

    foreach ( \Wpk\d565571\Helpers\Booking::getHours() as $hour ) {
        print '<option value="' . $hour . '">' . $hour . '</option>';
    }

    print'					                </select>
						            </div>
                                </section>

                                <section class="wpk-time-section">
                                    <div class=" has_calendar calendar_icon">
                                        <input type="text" id="booking_to_date" size="40" name="booking_to_date" class="form-control" placeholder="' . esc_html__( 'Check Out', 'wpestate' ) . '" value="">
                                    </div>
                                     <div class="wpk-clock-icon wpk-booking-hours col-md-12">
					                    <select name="booking_end_hour" id="booking_end_hour">
						                    <option disabled selected>' . esc_html__( 'Hour' ) . '</option>';

    foreach ( \Wpk\d565571\Helpers\Booking::getHours() as $hour ) {
        print '<option value="' . $hour . '">' . $hour . '</option>';
    }

    print'                             </select>
						            </div>          
                             </section>

                                <div class="guest_icon">
                                    <select id="booking_guest_no"  name="booking_guest_no"  class="cd-select form-control" >
                                        <option value="1">1 ' . esc_html__( 'Guest', 'wpestate' ) . '</option>';
    for ( $i = 2; $i <= 14; $i ++ ) {
        print '<option value="' . $i . '">' . $i . ' ' . esc_html__( 'Guests', 'wpestate' ) . '</option>';
    }
    print'
                                    </select>    
                                </div>
                                
                                <input type="hidden" id="property_id" name="property_id" value="' . $post_id . '" />
                                <input name="prop_id" type="hidden"  id="agent_property_id" value="' . $post_id . '">
                                <input name="agent_id" type="hidden"  id="agent_id" value="' . $agent_id . '">

                                <div class="">
                                    <textarea id="booking_mes_mess" name="booking_mes_mess" cols="50" rows="6" placeholder="' . esc_html__( 'Your message', 'wpestate' ) . '" class="form-control"></textarea>
                                </div>
                                   
                                <button type="submit" id="submit_mess_front" class="wpb_button  wpb_btn-info  wpb_regularsize   wpestate_vc_button  vc_button">' . esc_html__( 'Send Message', 'wpestate' ) . '</button>

                            </div><!-- /.modal-body -->';


//
//                            print ' <div class="modal-header">
//                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
//                                        <h2 class="modal-title_big">'.esc_html__( 'Please Login','wpestate').'</h2>
//                                        <h4 class="modal-title" id="myModalLabel">'.esc_html__( 'You need to login in order to send a message','wpestate').'</h4>
//                                    </div>
//                                    <div class="modal-body"  style="text-align:center;">
//                                        '.esc_html__( 'You need to login in order to contact the owner.','wpestate').'
//                                    </div><!-- /.modal-body -->';


    print '
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </form><!-- /.modal -->';


    die();

}
