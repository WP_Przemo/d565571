<?php

function wpestate_ajax_update_listing_location() {
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }

    if ( isset( $_POST[ 'listing_edit' ] ) ) {
        if ( ! is_numeric( $_POST[ 'listing_edit' ] ) ) {
            exit( 'you don\'t have the right to edit this' );
        } else {
            $edit_id  = intval( $_POST[ 'listing_edit' ] );
            $the_post = get_post( $edit_id );

            if ( $current_user->ID != $the_post->post_author ) {
                esc_html_e( "you don't have the right to edit this", "wprentals" );
                die();
            } else {
                $allowed_html = [];

                $property_latitude   = floatval( $_POST[ 'property_latitude' ] );
                $property_longitude  = floatval( $_POST[ 'property_longitude' ] );
                $google_camera_angle = floatval( $_POST[ 'google_camera_angle' ] );
                $property_address    = wp_kses( $_POST[ 'property_address' ], $allowed_html );
                $property_zip        = wp_kses( $_POST[ 'property_zip' ], $allowed_html );
                $property_county     = wp_kses( $_POST[ 'property_county' ], $allowed_html );
                $property_state      = wp_kses( $_POST[ 'property_state' ], $allowed_html );
                $arrival             = wp_kses( trim( $_POST[ 'arrival' ] ), $allowed_html );
                $phone               = wp_kses( $_POST[ 'phone' ], $allowed_html );
                $phone1               = wp_kses( $_POST[ 'phone1' ], $allowed_html );

                update_post_meta( $edit_id, 'property_latitude', $property_latitude );
                update_post_meta( $edit_id, 'property_longitude', $property_longitude );
                update_post_meta( $edit_id, 'google_camera_angle', $google_camera_angle );
                update_post_meta( $edit_id, 'property_address', $property_address );
                update_post_meta( $edit_id, 'property_zip', $property_zip );
                update_post_meta( $edit_id, 'property_state', $property_state );
                update_post_meta( $edit_id, 'property_county', $property_county );
                update_post_meta( $edit_id, 'phone', $phone );
                update_post_meta( $edit_id, 'phone1', $phone1 );
                update_post_meta( $edit_id, 'arrival', $arrival );

                //  RCAPI

                $api_update_details                          = [];
                $api_update_details[ 'property_latitude' ]   = $property_latitude;
                $api_update_details[ 'property_longitude' ]  = $property_longitude;
                $api_update_details[ 'google_camera_angle' ] = $google_camera_angle;
                $api_update_details[ 'property_address' ]    = $property_address;
                $api_update_details[ 'property_zip' ]        = $property_zip;
                $api_update_details[ 'property_state' ]      = $property_state;
                $api_update_details[ 'property_county' ]     = $property_county;


                rcapi_update_listing( $edit_id, $api_update_details );

                // end RCAPI
                $status         = wpestate_global_check_mandatory( $edit_id );
                $message_status = '';
                if ( $status == 'pending' ) {
                    $message_status = esc_html__( 'Your listing is pending. Please complete all the mandatory fields for it to be published!', 'wprentals' );
                }
                echo json_encode( [
                    'edited'   => true,
                    'response' => esc_html__( 'Changes are saved!', 'wprentals' ) . ' ' . $message_status,
                ] );


                die();

            }
        }
    }
}
