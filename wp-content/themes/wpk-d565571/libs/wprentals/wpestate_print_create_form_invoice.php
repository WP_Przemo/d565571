<?php

/**
 * Overwrite for theme function to display "hours" string instead of "nights"
 *
 * @return void
 */
function wpestate_print_create_form_invoice( $guest_price, $booking_guests, $invoice_id, $invoice_saved, $booking_from_date, $booking_to_date, $booking_array, $price_show, $details, $currency, $where_currency, $total_price, $total_price_show, $depozit_show, $balance_show, $booking_prop, $price_per_weekeend_show ) {

    $invoice_deposit_tobe = floatval( get_post_meta( $invoice_id, 'depozit_to_be_paid', true ) );
    $depozit_show         = wpestate_show_price_booking_for_invoice( $invoice_deposit_tobe, $currency, $where_currency, 0, 1 );
    //  $balance                =   $total_price-$invoice_deposit_tobe;
    $balance      = get_post_meta( $invoice_id, 'balance', true );
    $balance_show = wpestate_show_price_booking_for_invoice( $balance, $currency, $where_currency, 0, 1 );
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;
    $owner_see    = 0;

    if ( wpsestate_get_author( $booking_prop ) == $userID ) {
        $total_label = esc_html__( 'User Pays', 'wpestate' );
        $owner_see   = 1;
    } else {
        $total_label = esc_html__( 'You Pay', 'wpestate' );
    }


    print '              
           <div class="create_invoice_form">';

    //print_r($booking_array);

    if ( $invoice_id != 0 ) {
        print '<h3>' . esc_html__( 'Invoice INV', 'wpestate' ) . $invoice_id . '</h3>';
    }
    print '
                <div class="invoice_table">';
    if ( $invoice_id != 0 ) {
        print '<div id="print_invoice" data-invoice_id="' . $invoice_id . '" ><i class="fa fa-print" aria-hidden="true" ></i></div>';
    }
    print'
                    <div class="invoice_data">';
    $extra_price_per_guest = wpestate_show_price_booking( $booking_array[ 'extra_price_per_guest' ], $currency, $where_currency, 1 );

    if ( $invoice_saved == 'Reservation fee' || $invoice_saved == esc_html__( 'Reservation fee', 'wpestate' ) ) {
        print'
                            <div class="date_interval show_invoice_period"><span class="invoice_data_legend">' . esc_html__( 'Period', 'wpestate' ) . ' : </span>
                              <div style="width: 100%; float: none; clear: both;">
                                        '.esc_html__('From '). wpestate_convert_dateformat_reverse( $booking_from_date ).'
                                    </div>
                                    <div style="width: 100%; float: none;">
                                     '.esc_html__('To '). wpestate_convert_dateformat_reverse( $booking_to_date ).'
                                    </div>
                            </div>
                            <span class="date_duration show_invoice_no_nights"><span class="invoice_data_legend">' . esc_html__( 'No of hours', 'wpestate' ) . ': </span>' . $booking_array[ 'numberDays' ] . '</span>
                            <span class="date_duration show_invoice_guests"><span class="invoice_data_legend">' . esc_html__( 'Guests', 'wpestate' ) . ': </span>' . $booking_guests . '</span>';

        if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
            print'<span class="date_duration show_invoice_price_per_quest "><span class="invoice_data_legend">' . esc_html__( 'Price per Guest', 'wpestate' ) . ': </span>';
            if ( $booking_array[ 'custom_period_quest' ] == 1 ) {
                _e( 'custom price', 'wpestate' );
            } else {
                print $extra_price_per_guest;
            }
            print'</span>';
        } else {
            print '<span class="date_duration show_invoice_price_per_night"><span class="invoice_data_legend">' . esc_html__( 'Price per hour', 'wpestate' ) . ': </span>';

            print $price_show;
            if ( $booking_array[ 'has_custom' ] ) {
                print ', ' . esc_html__( 'has custom price', 'wpestate' );
            }
            if ( $booking_array[ 'cover_weekend' ] ) {
                print ', ' . esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
            }

            print '</span>';
        }

        if ( $booking_array[ 'has_custom' ] || $booking_array[ 'custom_period_quest' ] == 1 ) {
            if ( is_array( $booking_array[ 'custom_price_array' ] ) ) {
                print '<span class="invoice_data_legend show_invoice_price_details">' . __( 'Price details:', 'wpestate' ) . '</span>';
                foreach ( $booking_array[ 'custom_price_array' ] as $date => $price ) {
                    $day_price = wpestate_show_price_booking_for_invoice( $price, $currency, $where_currency, 1, 1 );
                    print '<span class="price_custom_explained show_invoice_price_details">' . __( 'on', 'wpestate' ) . ' ' . wpestate_convert_dateformat_reverse( date( "Y-m-d", $date ) ) . ' ' . __( 'price is', 'wpestate' ) . ' ' . $day_price . '</span>';
                }
            }
        }


    }

    print'    
                    </div>

                   <div class="invoice_details">
                        <div class="invoice_row header_legend">
                            <span class="inv_legend">' . esc_html__( 'Cost', 'wpestate' ) . '</span>
                            <span class="inv_data">  ' . esc_html__( 'Price', 'wpestate' ) . '</span>
                            <span class="inv_exp">   ' . esc_html__( 'Detail', 'wpestate' ) . '</span>
                        </div>';


    if ( is_array( $details ) ) {

        foreach ( $details as $detail ) {
            if ( $detail[ 1 ] != 0 ) {
                print'<div class="invoice_row invoice_content">
                                        <span class="inv_legend">  ' . $detail[ 0 ] . '</span>
                                        <span class="inv_data">  ' . wpestate_show_price_booking_for_invoice( $detail[ 1 ], $currency, $where_currency, 0, 1 ) . '</span>
                                        <span class="inv_exp"> ';
                if ( trim( $detail[ 0 ] ) == esc_html__( 'Security Depozit', 'wpestate' ) || trim( $detail[ 0 ] ) == esc_html__( 'Security Deposit', 'wpestate' ) ) {
                    esc_html_e( '*refundable', 'wpestate' );
                }

                if ( trim( $detail[ 0 ] ) == esc_html__( 'Subtotal', 'wpestate' ) ) {
                    if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
                        if ( $booking_array[ 'custom_period_quest' ] == 1 ) {
                            print $booking_array[ 'count_days' ] . ' ' . esc_html__( 'nights', 'wpestate' ) . ' x ' . $booking_array[ 'curent_guest_no' ] . ' ' . esc_html__( 'guests', 'wpestate' ) . ' - ' . esc_html__( " period with custom price per guest", "wpestate" );
                        } else {
                            print  $extra_price_per_guest . ' x ' . $booking_array[ 'count_days' ] . ' ' . esc_html__( 'nights', 'wpestate' ) . ' x ' . $booking_array[ 'curent_guest_no' ] . ' ' . esc_html__( 'guests', 'wpestate' );

                        }
                    } else {
                        print $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'nights', 'wpestate' ) . ' x ';
                        if ( $booking_array[ 'cover_weekend' ] ) {
                            echo esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
                        } else {
                            if ( $booking_array[ 'has_custom' ] != 0 ) {
                                esc_html_e( 'custom price', 'wpestate' );
                            } else {
                                print $price_show;
                            }
                        }
                    }
                }


                if ( $booking_array[ 'custom_period_quest' ] == 1 ) {
                    $new_guest_price = esc_html__( "custom price", "wpestate" );
                } else {
                    $new_guest_price = $guest_price . ' ' . esc_html__( 'per hour', 'wpestate' );
                }

                if ( trim( $detail[ 0 ] ) == esc_html__( 'Extra Guests', 'wpestate' ) ) {
                    print $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'nights', 'wpestate' ) . ' x ' . $booking_array[ 'extra_guests' ] . ' ' . esc_html__( 'extra guests', 'wpestate' ) . ' x ' . $new_guest_price;
                }

                if ( isset( $detail[ 2 ] ) ) {
                    echo $detail[ 2 ];
                }


                print'  </span>
                                       </div>';

            }//end if       if($detail[1]>0)
        }
    } else {

    }

//                        $manual_expenses = get_post_meta($invoice_id, 'manual_expense',true);
//                        if( is_array($manual_expenses) && !empty($manual_expenses)){
//                            foreach($manual_expenses as $key=>$expense){
//                                print'<div class="invoice_row invoice_content">
//                                    <span class="inv_legend">  '.$expense[0].'</span>
//                                    <span class="inv_data">  '. wpestate_show_price_booking_for_invoice($expense[1],$currency,$where_currency,0,1).'</span>
//                                    <span class="inv_exp"> </div>';
//                            }
//                        }

    // show Total row
    print '  
                        <div class="invoice_row invoice_total invoice_create_print_invoice">
                            <span class="inv_legend inv_legend_total"><strong>' . $total_label . '</strong></span>
                            <span class="inv_data" id="total_amm" data-total="' . $total_price . '">' . $total_price_show . '</span>
                            <div class="deposit_show_wrapper total_inv_span"> ';

    if ( $invoice_saved == 'Reservation fee' || $invoice_saved == esc_html__( 'Reservation fee', 'wpestate' ) ) {
        print'
                                <span class="inv_legend">' . esc_html__( 'Reservation Fee Required', 'wpestate' ) . ':</span> <span class="inv_depozit">' . $depozit_show . ' </span></br>
                                <span class="inv_legend">' . esc_html__( 'Balance', 'wpestate' ) . ':</span> <span class="inv_depozit">' . $balance_show . '</span>';
    } else {
        echo $invoice_saved;
    }

    print'
                        </div> ';


    // show earnings for owner

    if ( $owner_see == 1 ) {
        wpestate_show_youearn( $booking_array, $booking_prop, $total_price, $currency, $where_currency );
    }

    print'</div>
                       </div> 
               </div>';
}
