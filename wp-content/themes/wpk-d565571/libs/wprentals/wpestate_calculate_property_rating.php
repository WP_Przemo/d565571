<?php

/**
 * Calculates the property rating
 *
 * @param $property_id
 *
 * @return string|void
 */
function wpestate_calculate_property_rating( $property_id ) {

    if ( ! $property_id ) {
        return;
    }

    $reviews           = get_comments( [ 'post_id' => $property_id ] );
    $category_fields   = wpk_get_review_fields( 'property' );
    $count_old_reviews = 0;
    $count_new_reviews = 0;
    $sum_old_reviews   = 0;
    $stars_in_fields   = [];
    $stars_fields      = [];
    $stars_averages    = [];
    $store             = [];

    foreach ( $reviews as $review ) {
        $raw_comment_rating = get_comment_meta( $review->comment_ID, 'review_stars', true );

        switch ( true ) {
            // Old reviews
            case ( is_numeric( $raw_comment_rating ) ) :
                $count_old_reviews ++;
                $sum_old_reviews              = $sum_old_reviews + intval( $raw_comment_rating );
                $stars_in_fields[ 'total' ][] = intval( $raw_comment_rating );
                break;

            // New reviews
            case ( is_string( $raw_comment_rating ) ):
                $count_new_reviews ++;
                $tmp_rating                   = json_decode( $raw_comment_rating, true );
                $stars_in_fields[ 'total' ][] = $tmp_rating[ 'rating' ];

                // gather all stars per field in an array
                foreach ( $category_fields[ 'fields' ] as $field_key => $field_value ) {
                    $stars_in_fields[ $field_key ][] = $tmp_rating[ $field_key ];
                }

                break;
        }


    }

    // Sums per fields
    foreach ( $category_fields[ 'fields' ] as $field_key => $field_value ) {
        if ( isset( $stars_in_fields[ $field_key ] ) ) {
            $stars_fields[ $field_key ]   = array_sum( $stars_in_fields[ $field_key ] );
            $tmp_round                    = round( $stars_fields[ $field_key ] / count( $stars_in_fields[ $field_key ] ), 1 );
            $stars_averages[ $field_key ] = wpestate_round_to_nearest_05( $tmp_round );
            $store[]                      = sprintf( '"%s": %s', $field_key, $stars_averages[ $field_key ] );
        }
    }

    // Calc total rating
    $all_reviews_total          = array_sum( $stars_in_fields[ 'total' ] ) / ( ( $count_new_reviews + $count_old_reviews ) );
    $property_rating[ 'total' ] = wpestate_round_to_nearest_05( $all_reviews_total );

    // Construct rating string for db
    $store[]     = sprintf( '"%s": %s', 'rating', $property_rating[ 'total' ] );
    $star_rating = '{' . implode( ',', $store ) . '}';
    update_post_meta( $property_id, 'property_stars', $star_rating );

    return $star_rating;
}
