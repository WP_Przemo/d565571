<?php

/**
 * Overwrite for wprentals function in order to use our price calculations
 *
 * @param int $curent_guest_no
 * @param int $invoice_id
 * @param int $property_id
 * @param string $from_date
 * @param string $to_date
 * @param string|int $bookid
 * @param string|array $extra_options_array
 * @param string|array $manual_expenses
 *
 * @return array
 */
function wpestate_booking_price( $curent_guest_no, $invoice_id, $property_id, $from_date, $to_date, $bookid = '', $extra_options_array = '', $manual_expenses = '' ) {

    $property = \Wpk\d565571\Models\Property::find( $property_id );

    return $property->getPrices( [
        'invoice_id'          => $invoice_id,
        'guest_no'            => $curent_guest_no,
        'start_date'          => new DateTime( $from_date ),
        'end_date'            => new DateTime( $to_date ),
        'booking_id'          => $bookid,
        'extra_options_array' => $extra_options_array,
        'manual_expenses'     => $manual_expenses,
    ] );

}