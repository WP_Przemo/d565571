<?php
/**
 * Overwrite for theme function to replace time string
 */
function wpestate_show_custom_details( $edit_id, $is_dash = 0 ) {
    $week_days                = [
        '0' => esc_html__( 'All', 'wpestate' ),
        '1' => esc_html__( 'Monday', 'wpestate' ),
        '2' => esc_html__( 'Tuesday', 'wpestate' ),
        '3' => esc_html__( 'Wednesday', 'wpestate' ),
        '4' => esc_html__( 'Thursday', 'wpestate' ),
        '5' => esc_html__( 'Friday', 'wpestate' ),
        '6' => esc_html__( 'Saturday', 'wpestate' ),
        '7' => esc_html__( 'Sunday', 'wpestate' ),

    ];
    $price_per_guest_from_one = floatval( get_post_meta( $edit_id, 'price_per_guest_from_one', true ) );

    $currency       = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
    $where_currency = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );

    $mega        = wpml_mega_details_adjust( $edit_id );
    $price_array = wpml_custom_price_adjust( $edit_id );


    if ( empty( $mega ) && empty( $price_array ) ) {
        return;
    }


    if ( is_array( $mega ) ) {
        // sort arry by key
        ksort( $mega );


        $flag               = 0;
        $flag_price         = '';
        $flag_min_days      = '';
        $flag_guest         = '';
        $flag_price_week    = '';
        $flag_change_over   = '';
        $flag_checkout_over = '';

        print '<div class="custom_day_wrapper';
        if ( $is_dash == 1 ) {
            print ' custom_day_wrapper_dash ';
        }
        print '">';

        print '
            <div class="custom_day custom_day_header"> 
                <div class="custom_day_from_to">' . esc_html__( 'Period', 'wpestate' ) . '</div>';

        if ( $price_per_guest_from_one != 1 ) {
            print'
                    <div class="custom_price_per_day">' . esc_html__( 'Price per hour', 'wpestate' ) . '</div>    
                    <div class="custom_price_per_day">' . esc_html__( 'Price per hour (7n+)', 'wpestate' ) . '</div>  
                    <div class="custom_price_per_day">' . esc_html__( 'Price per hour (30n+)', 'wpestate' ) . '</div>  
                    <div class="custom_day_min_days">' . esc_html__( 'Minimum Booking days', 'wpestate' ) . '</div>   
                    <div class="custom_day_name_price_per_guest">' . esc_html__( 'Extra price per guest', 'wpestate' ) . '</div>
                    <div class="custom_day_name_price_per_weekedn">' . esc_html__( 'Price per hour in weekends', 'wpestate' ) . '</div>';
        } else {
            print '<div class="custom_day_name_price_per_guest">' . esc_html__( 'Price per guest', 'wpestate' ) . '</div>';
        }


        print'
                <div class="custom_day_name_change_over">' . esc_html__( 'Booking starts only on', 'wpestate' ) . '</div>
                <div class="custom_day_name_checkout_change_over">' . esc_html__( 'Booking starts/ends only on', 'wpestate' ) . '</div>';

        if ( $is_dash == 1 ) {
            print '<div class="delete delete_custom_period"></div>';
        }

        print'</div>';

        //print_r($mega);
        foreach ( $mega as $day => $data_day ) {
            $checker        = 0;
            $from_date      = new DateTime( "@" . $day );
            $to_date        = new DateTime( "@" . $day );
            $tomorrrow_date = new DateTime( "@" . $day );

            $tomorrrow_date->modify( 'tomorrow' );
            $tomorrrow_date = $tomorrrow_date->getTimestamp();

            //we set the flags
            //////////////////////////////////////////////////////////////////////////////////////////////
            if ( $flag == 0 ) {
                $flag = 1;
                if ( isset( $price_array[ $day ] ) ) {
                    $flag_price = $price_array[ $day ];
                }
                $flag_min_days      = $data_day[ 'period_min_days_booking' ];
                $flag_guest         = $data_day[ 'period_extra_price_per_guest' ];
                $flag_price_week    = $data_day[ 'period_price_per_weekeend' ];
                $flag_change_over   = $data_day[ 'period_checkin_change_over' ];
                $flag_checkout_over = $data_day[ 'period_checkin_checkout_change_over' ];

                if ( isset( $data_day[ 'period_price_per_month' ] ) ) {
                    $flag_period_price_per_month = $data_day[ 'period_price_per_month' ];
                }

                if ( isset( $data_day[ 'period_price_per_week' ] ) ) {
                    $flag_period_price_per_week = $data_day[ 'period_price_per_week' ];
                }

                $from_date_unix = $from_date->getTimestamp();
                print' <div class="custom_day">';
                print' <div class="custom_day_from_to"> ' . esc_html__( 'From', 'wpestate' ) . ' ' . wpestate_convert_dateformat_reverse( $from_date->format( 'Y-m-d' ) );
            }


            //we check period chane
            //////////////////////////////////////////////////////////////////////////////////////////////
            if ( ! array_key_exists( $tomorrrow_date, $mega ) ) { // non consecutive days
                $checker = 1;

            } else {
                if ( isset( $price_array[ $tomorrrow_date ] ) && $flag_price != $price_array[ $tomorrrow_date ] ) {
                    // IF PRICE DIFFRES FROM DAY TO DAY
                    $checker = 1;
                }
                if ( $mega[ $tomorrrow_date ][ 'period_min_days_booking' ] != $flag_min_days ||
                     $mega[ $tomorrrow_date ][ 'period_extra_price_per_guest' ] != $flag_guest ||
                     $mega[ $tomorrrow_date ][ 'period_price_per_weekeend' ] != $flag_price_week ||
                     ( isset( $mega[ $tomorrrow_date ][ 'period_price_per_month' ] ) && $mega[ $tomorrrow_date ][ 'period_price_per_month' ] != $flag_period_price_per_month ) ||
                     ( isset( $mega[ $tomorrrow_date ][ 'period_price_per_week' ] ) && $mega[ $tomorrrow_date ][ 'period_price_per_week' ] != $flag_period_price_per_week ) ||
                     $mega[ $tomorrrow_date ][ 'period_checkin_change_over' ] != $flag_change_over ||
                     $mega[ $tomorrrow_date ][ 'period_checkin_checkout_change_over' ] != $flag_checkout_over ) {
                    // IF SOME DATA DIFFRES FROM DAY TO DAY

                    $checker = 1;
                }

            }

            if ( $checker == 0 ) {
                // we have consecutive days, data stays the sa,e- do not print
            } else {
                // no consecutive days - we CONSIDER print


                if ( $flag == 1 ) {

                    //   $to_date->modify('yesterday');
                    $to_date_unix = $from_date->getTimestamp();
                    print ' ' . esc_html__( 'To', 'wpestate' ) . ' ' . wpestate_convert_dateformat_reverse( $from_date->format( 'Y-m-d' ) ) . '</div>';

                    if ( $price_per_guest_from_one != 1 ) {
                        print'
                                <div class="custom_price_per_day">';
                        if ( isset( $price_array[ $day ] ) ) {
                            echo wpestate_show_price_booking( $price_array[ $day ], $currency, $where_currency, 1 );
                        } else {
                            echo '-';
                        }
                        print'</div>';


                        print'
                                <div class="custom_day_name_price_per_week custom_price_per_day">';
                        if ( isset( $flag_period_price_per_week ) && $flag_period_price_per_week != 0 ) {
                            echo wpestate_show_price_booking( $flag_period_price_per_week, $currency, $where_currency, 1 );
                        } else {
                            echo '-';
                        }
                        print '</div>
                                <div class="custom_day_name_price_per_month custom_price_per_day">';
                        if ( isset( $flag_period_price_per_month ) && $flag_period_price_per_month != 0 ) {
                            echo wpestate_show_price_booking( $flag_period_price_per_month, $currency, $where_currency, 1 );
                        } else {
                            echo '-';
                        }
                        print '</div>';


                        print'
                                <div class="custom_day_min_days">';
                        if ( $flag_min_days != 0 ) {
                            echo $flag_min_days;
                        } else {
                            echo '-';
                        }
                        print '</div>   
                                <div class="custom_day_name_price_per_guest">';
                        if ( $flag_guest != 0 ) {
                            echo wpestate_show_price_booking( $flag_guest, $currency, $where_currency, 1 );
                        } else {
                            echo '-';
                        }
                        print '</div>
                                <div class="custom_day_name_price_per_weekedn">';
                        if ( $flag_price_week != 0 ) {
                            echo wpestate_show_price_booking( $flag_price_week, $currency, $where_currency, 1 );
                        } else {
                            echo '-';
                        }
                        print '</div>';


                    } else {
                        print '<div class="custom_day_name_price_per_guest">' . wpestate_show_price_booking( $flag_guest, $currency, $where_currency, 1 ) . '</div>';
                    }

                    print'
                            <div class="custom_day_name_change_over">';
                    if ( intval( $flag_change_over ) != 0 ) {
                        echo $week_days[ $flag_change_over ];
                    } else {
                        esc_html_e( 'All', 'wpestate' );
                    }

                    print '</div>
                            <div class="custom_day_name_checkout_change_over">';
                    if ( intval( $flag_checkout_over ) != 0 ) {
                        echo $week_days[ $flag_checkout_over ];
                    } else {
                        esc_html_e( 'All', 'wpestate' );
                    }

                    print '</div>';

                    if ( $is_dash == 1 ) {
                        print '<div class="delete delete_custom_period" data-editid="' . $edit_id . '"   data-fromdate="' . $from_date_unix . '" data-todate="' . $to_date_unix . '"><a href="#"> ' . esc_html__( 'delete period', 'wpestate' ) . '</a></div>';
                    }

                    print '</div>';
                }
                $flag = 0;
                if ( isset( $price_array[ $day ] ) ) {
                    $flag_price = $price_array[ $day ];
                }
                $flag_min_days      = $data_day[ 'period_min_days_booking' ];
                $flag_guest         = $data_day[ 'period_extra_price_per_guest' ];
                $flag_price_week    = $data_day[ 'period_price_per_weekeend' ];
                $flag_change_over   = $data_day[ 'period_checkin_change_over' ];
                $flag_checkout_over = $data_day[ 'period_checkin_change_over' ];
            }
        }
        print '</div>';
    }
}