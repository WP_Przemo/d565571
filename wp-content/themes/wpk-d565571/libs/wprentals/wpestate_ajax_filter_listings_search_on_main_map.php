<?php

use Wpk\d565571\Helpers\WpRentals\Filter;

/**
 * Overwrite for theme function to use our new booking format
 *
 * @return void
 */
function wpestate_ajax_filter_listings_search_on_main_map() {
    global $post;
    global $show_compare_only;
    global $currency;
    global $where_currency;
    $show_compare_only = 'no';
    $current_user      = wp_get_current_user();
    $userID            = $current_user->ID;
    $currency          = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
    $where_currency    = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
    $area_array        = '';
    $city_array        = '';
    $action_array      = '';
    $categ_array       = '';
    $allowed_html      = [];
    $meta_query        = [];


    //////////////////////////////////////////////////////////////////////////////////////
    ///// city filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'city' ] ) && trim( $_POST[ 'city' ] ) != 'all' && $_POST[ 'city' ] != '' ) {
        $taxcity[]  = sanitize_title( wp_kses( $_POST[ 'city' ], $allowed_html ) );
        $city_array = [
            'taxonomy' => 'property_city',
            'field'    => 'slug',
            'terms'    => $taxcity,
        ];
    }


    if ( isset( $_POST[ 'area' ] ) && trim( $_POST[ 'area' ] ) != 'all' && $_POST[ 'area' ] != '' ) {
        $taxarea[]  = sanitize_title( wp_kses( $_POST[ 'area' ], $allowed_html ) );
        $area_array = [
            'taxonomy' => 'property_area',
            'field'    => 'slug',
            'terms'    => $taxarea,
        ];
    }


    $guest_array = [];
    if ( isset( $_POST[ 'guest_no' ] ) && is_numeric( $_POST[ 'guest_no' ] ) ) {
        $guest_no                 = intval( $_POST[ 'guest_no' ] );
        $guest_array[ 'key' ]     = 'guest_no';
        $guest_array[ 'value' ]   = $guest_no;
        $guest_array[ 'type' ]    = 'numeric';
        $guest_array[ 'compare' ] = '>=';
        $meta_query[]             = $guest_array;
    }

    $country_array = [];
    if ( isset( $_POST[ 'country' ] ) && $_POST[ 'country' ] != '' ) {
        $country                    = sanitize_text_field( wp_kses( $_POST[ 'country' ], $allowed_html ) );
        $country                    = str_replace( '-', ' ', $country );
        $country_array[ 'key' ]     = 'property_country';
        $country_array[ 'value' ]   = $country;
        $country_array[ 'type' ]    = 'CHAR';
        $country_array[ 'compare' ] = 'LIKE';
        $meta_query[]               = $country_array;
    }

    if ( isset( $_POST[ 'city' ] ) && $_POST[ 'city' ] == '' && isset( $_POST[ 'property_admin_area' ] ) && $_POST[ 'property_admin_area' ] != '' ) {
        $admin_area_array              = [];
        $admin_area                    = sanitize_text_field( wp_kses( $_POST[ 'property_admin_area' ], $allowed_html ) );
        $admin_area                    = str_replace( " ", "-", $admin_area );
        $admin_area                    = str_replace( "\'", "", $admin_area );
        $admin_area_array[ 'key' ]     = 'property_admin_area';
        $admin_area_array[ 'value' ]   = $admin_area;
        $admin_area_array[ 'type' ]    = 'CHAR';
        $admin_area_array[ 'compare' ] = 'LIKE';
        $meta_query[]                  = $admin_area_array;

    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// calendar filters
    //////////////////////////////////////////////////////////////////////////////////////


    $book_from = '';
    $book_to   = '';
    //WPK d565571 Use start and end hour values

    if ( ! empty( $_POST[ 'check_in' ] ) && ! empty( $_POST[ 'start_hour' ] ) ) {
        $book_from = Filter::getDateAndHour( 'check_in', 'start_hour' );
    }
    if ( isset( $_POST[ 'check_out' ] ) && ! empty( $_POST[ 'end_hour' ] ) ) {
        $book_to = Filter::getDateAndHour( 'check_out', 'end_hour' );
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// action  filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( ( ! empty( $_POST[ 'action_values' ] ) && trim( $_POST[ 'action_values' ] ) != 'all' ) ) {
        $taxaction_include = sanitize_title( wp_kses( $_POST[ 'action_values' ], $allowed_html ) );
        $action_array      = [
            'taxonomy' => 'property_action_category',
            'field'    => 'slug',
            'terms'    => $taxaction_include,
        ];
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// category filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'category_values' ] ) && trim( $_POST[ 'category_values' ] ) != 'all' ) {
        $taxcateg_include = sanitize_title( wp_kses( $_POST[ 'category_values' ], $allowed_html ) );
        $categ_array      = [
            'taxonomy' => 'property_category',
            'field'    => 'slug',
            'terms'    => $taxcateg_include,
        ];
    }


    //WPK d565571 End

    //////////////////////////////////////////////////////////////////////////////////////
    ///// order details
    //////////////////////////////////////////////////////////////////////////////////////


    $args = [
        'post_type'      => 'estate_property',
        'post_status'    => 'publish',
        'posts_per_page' => - 1,
        'meta_query'     => $meta_query,
        'tax_query'      => [
            'relation' => 'AND',
            $categ_array,
            $action_array,
            $city_array,
            $area_array,

        ],
    ];


    $prop_selection = new WP_Query( $args );
    $counter        = 0;
    $markers        = [];
    while ( $prop_selection->have_posts() ): $prop_selection->the_post();

        if ( ! empty( $book_from ) || ! empty( $book_to ) ) {

            if ( empty( $book_from ) ) {
                $book_from = $book_to;
            }

            if ( empty( $book_to ) ) {
                $book_to = $book_from;
            }

            if ( wpestate_check_booking_valability( $book_from, $book_to, $post->ID ) ) {
                $counter ++;
                $markers[] = wpestate_pin_unit_creation( get_the_ID(), $currency, $where_currency, $counter );

            }

        } else {
            $counter ++;
            $markers[] = wpestate_pin_unit_creation( get_the_ID(), $currency, $where_currency, $counter );
        }


//        if( wpestate_check_booking_valability($book_from,$book_to,$post->ID) ){
//            $counter++;
//            $markers[]=wpestate_pin_unit_creation( get_the_ID(),$currency,$where_currency,$counter );
//
//        }
    endwhile;
    //print 'resutls '.$counter;
    echo json_encode( [
        'added'     => true,
        'arguments' => json_encode( $args ),
        'markers'   => json_encode( $markers ),
        'counter'   => $counter,
    ] );
    die();
}
