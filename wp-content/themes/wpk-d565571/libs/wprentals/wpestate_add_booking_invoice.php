<?php

/**
 * Overwrite for wprentals function in order to modify price calculations
 *
 * @return void
 */
function wpestate_add_booking_invoice() {

    $price = (double) round( floatval( $_POST[ 'price' ] ), 2 );

    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;
    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }


    check_ajax_referer( 'create_invoice_ajax_nonce', 'security' );
    $is_confirmed = intval( $_POST[ 'is_confirmed' ] );
    $bookid       = intval( $_POST[ 'bookid' ] );

    $property = \Wpk\d565571\Models\Booking::find( $bookid )->getProperty();

    $book_from  = get_post_meta( $bookid, 'booking_from_date', true );
    $book_to    = get_post_meta( $bookid, 'booking_to_date', true );
    $listing_id = get_post_meta( $bookid, 'booking_id', true );

    $the_post = get_post( $listing_id );

    if ( $current_user->ID != $the_post->post_author ) {
        exit( 'you don\'t have the right to see this' );
    }

    // prepare
    $full_pay_invoice_id = 0;
    $early_bird_percent  = floatval( get_post_meta( $listing_id, 'early_bird_percent', true ) );
    $early_bird_days     = floatval( get_post_meta( $listing_id, 'early_bird_days', true ) );
    $taxes_value         = floatval( get_post_meta( $listing_id, 'property_taxes', true ) );

    //check if period already reserverd
    $reservation_array = get_post_meta( $listing_id, 'booking_dates', true );
    if ( $reservation_array == '' ) {
        $reservation_array = wpestate_get_booking_dates( $listing_id );
    }

    wpestate_check_for_booked_time( $book_from, $book_to, $reservation_array );
    // end check


    // we proceed with issuing the invoice
    $details         = $_POST[ 'details' ];
    $manual_expenses = isset( $_POST[ 'manual_expenses' ] ) ? $_POST[ 'manual_expenses' ] : [];
    $billing_for     = esc_html__( 'Reservation fee', 'wpestate' );
    $type            = esc_html__( 'One Time', 'wpestate' );
    $pack_id         = $bookid; // booking id

    $time          = time();
    $date          = date( 'Y-m-d H:i:s', $time );
    $user_id       = wpse119881_get_author( $bookid );
    $is_featured   = '';
    $is_upgrade    = '';
    $paypal_tax_id = '';


    // get the booking array
    $invoice_id          = 0;
    $booking_guests      = get_post_meta( $bookid, 'booking_guests', true );
    $extra_options       = esc_html( get_post_meta( $bookid, 'extra_options', true ) );
    $extra_options_array = explode( ',', $extra_options );
    //$booking_array       = wpestate_booking_price( $booking_guests, $invoice_id, $listing_id, $book_from, $book_to, $bookid, $extra_options_array, $manual_expenses );
    $booking_array = $property->getPrices( [
        'start_date'          => new DateTime( $book_from ),
        'end_date'            => new DateTime( $book_to ),
        'guest_no'            => $booking_guests,
        'extra_options_array' => $extra_options_array,
        'booking_id'          => $bookid,
        'manual_expenses'     => $manual_expenses,
    ] );
    // done

    $model = \Wpk\d565571\Models\Booking::find( $bookid );


    $invoice_id                = wpestate_booking_insert_invoice( $billing_for, $type, $pack_id, $date, $user_id, $is_featured, $is_upgrade, $paypal_tax_id, $details, $price );
    $submission_curency_status = wpestate_curency_submission_pick();


    // update booking data
    update_post_meta( $bookid, 'full_pay_invoice_id', $full_pay_invoice_id );
    update_post_meta( $bookid, 'booking_taxes', $taxes_value );
    update_post_meta( $bookid, 'early_bird_percent', $early_bird_percent );
    update_post_meta( $bookid, 'early_bird_days', $early_bird_days );
    update_post_meta( $bookid, 'security_deposit', $booking_array[ 'security_deposit' ] );
    update_post_meta( $bookid, 'booking_taxes', $booking_array[ 'taxes' ] );
    update_post_meta( $bookid, 'service_fee', $booking_array[ 'service_fee' ] );
    update_post_meta( $bookid, 'youearned', $booking_array[ 'youearned' ] );
    update_post_meta( $bookid, 'to_be_paid', $booking_array[ 'deposit' ] );
    update_post_meta( $bookid, 'booking_status', 'waiting' );
    update_post_meta( $bookid, 'booking_invoice_no', $invoice_id );
    update_post_meta( $bookid, 'total_price', $booking_array[ 'total_price' ] );
    update_post_meta( $bookid, 'balance', $booking_array[ 'balance' ] );

    //update invoice data
    update_post_meta( $invoice_id, 'booking_taxes', $taxes_value );
    update_post_meta( $invoice_id, 'security_deposit', $booking_array[ 'security_deposit' ] );
    update_post_meta( $invoice_id, 'early_bird_percent', $early_bird_percent );
    update_post_meta( $invoice_id, 'early_bird_days', $early_bird_days );
    update_post_meta( $invoice_id, 'booking_taxes', $booking_array[ 'taxes' ] );
    update_post_meta( $invoice_id, 'service_fee', $booking_array[ 'service_fee' ] );
    update_post_meta( $invoice_id, 'youearned', $booking_array[ 'youearned' ] );
    update_post_meta( $invoice_id, 'depozit_to_be_paid', $booking_array[ 'deposit' ] );
    update_post_meta( $invoice_id, 'balance', $booking_array[ 'balance' ] );
    update_post_meta( $invoice_id, 'manual_expense', $manual_expenses );

    $cleaning_fee_per_day = floatval( get_post_meta( $listing_id, 'cleaning_fee_per_day', true ) );
    $city_fee_per_day     = floatval( get_post_meta( $listing_id, 'city_fee_per_day', true ) );
    $city_fee_percent     = floatval( get_post_meta( $listing_id, 'city_fee_percent', true ) );

    update_post_meta( $invoice_id, 'cleaning_fee_per_day', $cleaning_fee_per_day );
    update_post_meta( $invoice_id, 'city_fee_per_day', $city_fee_per_day );
    update_post_meta( $invoice_id, 'city_fee_percent', $city_fee_percent );


    $booking_details = [
        'total_price'         => $booking_array[ 'total_price' ],
        'to_be_paid'          => $booking_array[ 'deposit' ],
        'youearned'           => $booking_array[ 'youearned' ],
        'full_pay_invoice_id' => $full_pay_invoice_id,
        'service_fee'         => $booking_array[ 'service_fee' ],
        'booking_taxes'       => $booking_array[ 'taxes' ],
        'security_deposit'    => $booking_array[ 'security_deposit' ],
        'booking_status'      => 'waiting',
        'booking_invoice_no'  => $invoice_id,
        'balance'             => $booking_array[ 'balance' ],
    ];


    $rcapi_booking_id = get_post_meta( $bookid, 'rcapi_booking_id', true );
    update_post_meta( $invoice_id, 'custom_price_array', $booking_array[ 'custom_price_array' ] );


    $invoice_details = [
        "invoice_status"            => "issued",
        "purchase_date"             => $date,
        "buyer_id"                  => $user_id,
        "item_price"                => $booking_array[ 'total_price' ],
        "rcapi_booking_id"          => $rcapi_booking_id,
        "orignal_invoice_id"        => $invoice_id,
        "billing_for"               => $billing_for,
        "type"                      => $type,
        "pack_id"                   => $pack_id,
        "date"                      => $date,
        "user_id"                   => $user_id,
        "is_featured"               => $is_featured,
        "is_upgrade"                => $is_upgrade,
        "paypal_tax_id"             => $paypal_tax_id,
        "details"                   => $details,
        "price"                     => $price,
        "to_be_paid"                => $booking_array[ 'deposit' ],
        "submission_curency_status" => $submission_curency_status,
        "bookid"                    => $bookid,
        "author_id"                 => $author_id,
        "youearned"                 => $booking_array[ 'youearned' ],
        "service_fee"               => $booking_array[ 'service_fee' ],
        "booking_taxes"             => $booking_array[ 'taxes' ],
        "security_deposit"          => $booking_array[ 'security_deposit' ],
        "renting_details"           => $details,
        "custom_price_array"        => $booking_array[ 'custom_price_array' ],
        "balance"                   => $booking_array[ 'balance' ],
        "cleaning_fee_per_day"      => $cleaning_fee_per_day,
        "city_fee_per_day"          => $city_fee_per_day,
        "city_fee_percent"          => $city_fee_percent,
    ];

    if ( $booking_array[ 'balance' ] > 0 ) {
        update_post_meta( $bookid, 'booking_status_full', 'waiting' );
        update_post_meta( $invoice_id, 'invoice_status_full', 'waiting' );
        $booking_details[ 'booking_status_full' ] = 'waiting';
        $booking_details[ 'booking_invoice_no' ]  = $invoice_id;
        $invoice_details[ 'invoice_status_full' ] = 'waiting';
    }

    $wp_estate_book_down = floatval( get_post_meta( $invoice_id, 'invoice_percent', true ) );

    if ( $wp_estate_book_down == 100 ) {
        $booking_details[ 'booking_invoice_no' ] = $invoice_id;
    }


    if ( $is_confirmed == 1 ) {
        update_post_meta( $bookid, 'booking_status', 'confirmed' );
        $booking_details[ 'booking_status' ] = 'confirmed';

        update_post_meta( $invoice_id, 'invoice_status', 'confirmed' );
        update_post_meta( $invoice_id, 'depozit_paid', 0 );
        update_post_meta( $invoice_id, 'depozit_to_be_paid', 0 );
        update_post_meta( $invoice_id, 'balance', $booking_array[ 'balance' ] );
        $invoice_details[ 'invoice_status' ] = 'confirmed';
        $invoice_details[ 'to_be_paid' ]     = 0;
        $invoice_details[ 'balance' ]        = $booking_array[ 'balance' ];

        do_action( 'wpk/d5657781/bookingConfirmed', $model );
    }


    rcapi_invoice_booking( $invoice_id, $invoice_details );
    rcapi_edit_booking( $bookid, $rcapi_booking_id, $booking_details );


    if ( $is_confirmed == 1 ) {
        $curent_listng_id = get_post_meta( $bookid, 'booking_id', true );

        wpestate_get_booking_dates( $curent_listng_id );

    }


    // send notification emails
    if ( $is_confirmed !== 1 ) {
        $receiver       = get_userdata( $user_id );
        $receiver_email = $receiver->user_email;
        $receiver_login = $receiver->user_login;
        $from           = $current_user->user_login;
        $to             = $user_id;
        $subject        = esc_html__( 'New Invoice', 'wpestate' );
        $description    = esc_html__( 'A new invoice was generated for your booking request', 'wpestate' );

        wpestate_add_to_inbox( $userID, $userID, $to, $subject, $description, 1 );
        wpestate_send_booking_email( 'newinvoice', $receiver_email );
    } else {
        //direct confirmation emails
        $user_email = $current_user->user_email;

        $receiver       = get_userdata( $user_id );
        $receiver_email = $receiver->user_email;
        $receiver_login = $receiver->user_login;


        //$receiver_id    =   wpsestate_get_author($booking_id);

        $receiver_email = get_the_author_meta( 'user_email', $user_id );
        $receiver_name  = get_the_author_meta( 'user_login', $user_id );
        wpestate_send_booking_email( "bookingconfirmeduser", $receiver_email );// for user
        wpestate_send_booking_email( "bookingconfirmed_nodeposit", $user_email );// for owner
        // add messages to inbox

        $subject     = esc_html__( 'Booking Confirmation', 'wpestate' );
        $description = esc_html__( 'A booking was confirmed', 'wpestate' );
        wpestate_add_to_inbox( $userID, $receiver_name, $userID, $subject, $description );

        $subject     = esc_html__( 'Booking Confirmed', 'wpestate' );
        $description = esc_html__( 'A booking was confirmed', 'wpestate' );
        wpestate_add_to_inbox( $receiver_id, $username, $receiver_id, $subject, $description );

    }


    print $invoice_id;
    die();

}
