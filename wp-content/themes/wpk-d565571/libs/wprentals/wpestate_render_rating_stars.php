<?php

/**
 * Renders the actual star rating. Overwrite to support $rating param as array
 *
 * @param int|array $rating
 *
 * @return string
 */
function wpestate_render_rating_stars( $rating ) {
    $max_rating = wpestate_get_max_stars();

    if ( is_array( $rating ) ) {
        $rating = $rating[ 'ratings' ][ 'total' ];
    }

    if ( floor( $rating ) < $rating ) { // add '&& 1 == 0' to disable half star rating
        $half_stars = '<i class="fa fa-star-half-o"></i>';
    } else {
        $half_stars = '';
        $rating     = ceil( $rating ); // to fix if half star rating is disabled
    }
    $full_stars  = str_repeat( '<i class="fa fa-star"></i>', $rating );
    $empty_stars = str_repeat( '<i class="far fa-star"></i>', intval( ( $max_rating - $rating ) ) );

    return $full_stars . $half_stars . $empty_stars;
}
