<?php

/**
 * Overwrite for theme function to save google calendar ID in property meta
 *
 * @return void
 */
function wpestate_ajax_update_ical_feed() {
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }


    if ( isset( $_POST[ 'listing_edit' ] ) ) {
        if ( ! is_numeric( $_POST[ 'listing_edit' ] ) ) {
            exit( 'you don\'t have the right to edit this' );
        } else {
            $edit_id  = intval( $_POST[ 'listing_edit' ] );
            $the_post = get_post( $edit_id );

            //WPK d565571 Store calendar ID in property meta
            if ( ! empty( $_POST[ 'google_calendar_id' ] ) ) {

                $property_calendar_id = get_post_meta( 'google_calendar_id', $edit_id, true );

                if ( $property_calendar_id !== $_POST[ 'google_calendar_id' ] ) {

                    \Wpk\d565571\Models\Property::find( $edit_id )->disconnectGoogleCalendar( true );

                    update_post_meta( $edit_id, 'google_calendar_id', filter_var( $_POST[ 'google_calendar_id' ] ) );

                    do_action( 'wpk/d565581/property/googleCalendarConnected', $edit_id );

                }

            }
            //WPK d565571 End

            if ( $current_user->ID != $the_post->post_author ) {
                esc_html_e( "you don't have the right to edit this", "wpestate" );
                die();
            } else {

                $tmp_feed_array = [];
                $all_feeds      = [];
                //array_feeds,array_labels
                foreach ( $_POST[ 'array_feeds' ] as $key => $value ) {
                    if ( $value != '' ) {
                        $tmp_feed_array[ 'feed' ] = esc_url_raw( $value );
                        $tmp_feed_array[ 'name' ] = esc_html( $_POST[ 'array_labels' ][ $key ] );
                        $all_feeds[]              = $tmp_feed_array;
                    }
                }

                if ( ! empty( $all_feeds ) ) {
                    update_post_meta( $edit_id, 'property_icalendar_import_multi', $all_feeds );
                    wpestate_import_calendar_feed_listing_global( $edit_id );
                }


//                    $property_icalendar_import      =esc_url_raw($_POST['property_icalendar_import']);
//                    update_post_meta($edit_id, 'property_icalendar_import', $property_icalendar_import);
//
//                    if ($property_icalendar_import!=''){
//                        wpestate_import_calendar_feed_listing($edit_id);
//                    }
//
                echo json_encode( [
                    'edited'   => true,
                    'response' => esc_html__( 'Changes are saved!', 'wpestate' ),
                ] );
                die();

            }
        }
    }
}
