<?php

function wpestate_global_check_mandatory( $post_id ) {

    $mandatory_fields = ( get_option( 'wp_estate_mandatory_page_fields', '' ) );

    if ( empty( $mandatory_fields ) ) {
        return;
    }

    $admin_submission_status = esc_html( get_option( 'wp_estate_admin_submission', '' ) );
    if ( $admin_submission_status == 'yes' ) { // anmin validate submission so no mandatory check is required
        return;
    }


    $new_status = 'publish';

    $feature_list         = esc_html( get_option( 'wp_estate_feature_list' ) );
    $feature_list_array   = explode( ',', $feature_list );
    $feature_list_array_1 = [];

    foreach ( $feature_list_array as $key => $checker ) {
        $checker                = stripslashes( $checker );
        $post_var_name          = strtolower( str_replace( ' ', '_', trim( $checker ) ) );
        $post_var_name          = sanitize_title( $post_var_name );
        $feature_list_array_1[] = $post_var_name;
    }


    $custom_fields   = get_option( 'wp_estate_custom_fields', true );
    $custom_fields_1 = [];
    $i               = 0;
    if ( ! empty( $custom_fields ) ) {
        while ( $i < count( $custom_fields ) ) {
            $name  = stripslashes( $custom_fields[ $i ][ 0 ] );
            $slug  = str_replace( ' ', '-', $name );
            $label = stripslashes( $custom_fields[ $i ][ 1 ] );

            $slug              = htmlspecialchars( $slug, ENT_QUOTES );
            $slug              = wpestate_limit45( sanitize_title( $slug ) );
            $slug              = sanitize_key( $slug );
            $custom_fields_1[] = strtolower( $slug );
            $i ++;
        }
    }


    foreach ( $mandatory_fields as $key => $value ) {

        if ( $value == 'prop_category_submit' || $value == 'prop_action_category_submit' || $value == 'property_city_front' || $value == 'property_area_front' ) {

            if ( $value == 'prop_category_submit' ) {
                $value = 'property_category';
            } else if ( $value == 'prop_action_category_submit' ) {
                $value = 'property_action_category';
            } else if ( $value == 'property_city_front' ) {
                $value = 'property_city';
            } else if ( $value == 'property_area_front' ) {
                $value = 'property_area';
            }


            $terms = wp_get_post_terms( $post_id, $value );

            if ( ! isset( $terms [ 0 ] ) ) {
                $new_status = 'pending';
            }


        } else if ( $value == 'title' ) {
            $title = get_the_title( $post_id );
            if ( $title == '' ) {
                $new_status = 'pending';
            }
        } else if ( $value == 'property_description' ) {
            $content_post = get_post( $post_id );
            $content      = $content_post->post_content;
            if ( $content == '' ) {
                $new_status = 'pending';
            }

        } else if ( in_array( $value, $feature_list_array_1 ) ) {
            $post_var_name = $value;
            $terms         = intval( get_post_meta( $post_id, $post_var_name, true ) );
            if ( $terms != 1 ) {
                $new_status = 'pending';
            }

        } else if ( in_array( $value, $custom_fields_1 ) ) {
            $terms = get_post_meta( $post_id, $value, true );
            if ( $terms == '' ) {
                $new_status = 'pending';
            }

        } else {

            if ( $value === 'attachid' ) {


                $arguments = [
                    'numberposts'    => - 1,
                    'post_type'      => 'attachment',
                    'post_mime_type' => 'image',
                    'post_parent'    => $post_id,
                    'post_status'    => null,
                    'orderby'        => 'menu_order',
                    'order'          => 'ASC',

                ];

                $post_attachments = get_posts( $arguments );

                if ( empty( $post_attachments ) ) {
                    $new_status = 'pending';
                }

            } else {

                $terms = get_post_meta( $post_id, $value, true );
                if ( $terms == '' ) {
                    $new_status = 'pending';
                }

            }
        }

    }


    $my_post = [
        'ID'          => $post_id,
        'post_status' => $new_status,
    ];
    wp_update_post( $my_post );

    return $new_status;
}
