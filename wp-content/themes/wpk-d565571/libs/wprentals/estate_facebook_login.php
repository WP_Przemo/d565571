<?php

function estate_facebook_login( $get_vars ) {
    //https://developers.facebook.com/docs/php/gettingstarted
    session_start();
    $facebook_api    = esc_html( get_option( 'wp_estate_facebook_api', '' ) );
    $facebook_secret = esc_html( get_option( 'wp_estate_facebook_secret', '' ) );

    $fb     = new Facebook\Facebook( [
        'app_id'                => $facebook_api,
        'app_secret'            => $facebook_secret,
        'default_graph_version' => 'v2.12',
    ] );
    $helper = $fb->getRedirectLoginHelper();


    $secret = $facebook_secret;
    try {
        $accessToken = $helper->getAccessToken();
    } catch ( Facebook\Exceptions\FacebookResponseException $e ) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch ( Facebook\Exceptions\FacebookSDKException $e ) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }


    // Logged in
    // var_dump($accessToken->getValue());

    // The OAuth 2.0 client handler helps us manage access tokens
    $oAuth2Client = $fb->getOAuth2Client();

    // Get the access token metadata from /debug_token
    $tokenMetadata = $oAuth2Client->debugToken( $accessToken );
    //echo '<h3>Metadata</h3>';
    //var_dump($tokenMetadata);

    // Validation (these will throw FacebookSDKException's when they fail)
    $tokenMetadata->validateAppId( $facebook_api );

    // If you know the user ID this access token belongs to, you can validate it here
    //$tokenMetadata->validateUserId('123');
    $tokenMetadata->validateExpiration();

    if ( ! $accessToken->isLongLived() ) {
        // Exchanges a short-lived access token for a long-lived one
        try {
            $accessToken = $oAuth2Client->getLongLivedAccessToken( $accessToken );
        } catch ( Facebook\Exceptions\FacebookSDKException $e ) {
            echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
            exit;
        }

        // echo '<h3>Long-lived</h3>';
        //  var_dump($accessToken->getValue());
    }

    $_SESSION[ 'fb_access_token' ] = (string) $accessToken;

    try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get( '/me?fields=id,email,name,first_name,last_name', $accessToken );
    } catch ( Facebook\Exceptions\FacebookResponseException $e ) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch ( Facebook\Exceptions\FacebookSDKException $e ) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $user = $response->getGraphUser();


    if ( isset( $user[ 'name' ] ) ) {
        $full_name = $user[ 'name' ];
    }
    if ( isset( $user[ 'email' ] ) ) {
        $email = $user[ 'email' ];
    }
    $identity_code = $secret . $user[ 'id' ];
    wpestate_register_user_via_google( $email, $full_name, $identity_code, $user[ 'first_name' ], $user[ 'last_name' ] );
    $info                    = [];
    $info[ 'user_login' ]    = $full_name;
    $info[ 'user_password' ] = $identity_code;
    $info[ 'remember' ]      = true;

    $user_signon = wp_signon( $info, true );


    if ( is_wp_error( $user_signon ) ) {
        wp_redirect( esc_url( home_url() ) );
        exit();
    } else {
        wpestate_update_old_users( $user_signon->ID );
        wpestate_calculate_new_mess();
        wp_redirect( home_url() );
        exit();
    }

}
