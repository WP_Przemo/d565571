<?php

use Wpk\d565571\Helpers\WpRentals\Filter;
use Wpk\d565571\Helpers\WpRentals\Order;
use Wpk\d565571\Settings;
use Wpk\d565571\Utility;

add_action( 'wp_ajax_nopriv_wpestate_ajax_filter_listings_search_onthemap', 'wpestate_ajax_filter_listings_search_onthemap' );
add_action( 'wp_ajax_wpestate_ajax_filter_listings_search_onthemap', 'wpestate_ajax_filter_listings_search_onthemap' );

/**
 * Overwrite for theme function to filter by date and hours and by property category
 *
 * @return void
 */
function wpestate_ajax_filter_listings_search_onthemap() {

    global $post;
    global $options;
    global $show_compare_only;
    global $currency;
    global $where_currency;
    global $listing_type;
    global $property_unit_slider;
    global $curent_fav;
    global $book_from;
    global $book_to;
    global $guest_no;
    $property_unit_slider = esc_html( get_option( 'wp_estate_prop_list_slider', '' ) );

    $listing_type      = get_option( 'wp_estate_listing_unit_type', '' );
    $show_compare_only = 'no';
    $current_user      = wp_get_current_user();
    $userID            = $current_user->ID;
    $user_option       = 'favorites' . $userID;
    $curent_fav        = get_option( $user_option );
    $currency          = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
    $where_currency    = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
    $area_array        = '';
    $city_array        = '';
    $action_array      = '';
    $categ_array       = '';

    $distances = [];

    $options      = wpestate_page_details( intval( $_POST[ 'postid' ] ) );
    $allowed_html = [];

    $userLat = (float) $_POST[ 'user_lat' ];
    $userLng = (float) $_POST[ 'user_lng' ];

    $city      = isset( $_POST[ 'city' ] ) ? $_POST[ 'city' ] : '';
    $country   = isset( $_POST[ 'country' ] ) ? $_POST[ 'country' ] : '';
    $searchLat = null;
    $searchLng = null;

    $searchRadius = (float) Settings::get( 'location_radius', 1000 );

    $response = null;

    if ( ! empty( $city ) || ! empty( $country ) ) {

        $guzzle = new \GuzzleHttp\Client();

        $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json';

        $url = add_query_arg( [
            'sensor'  => false,
            'address' => "$city, $country",
            'key'     => get_option( 'wp_estate_api_key' ),
        ], $googleApiUrl );

        $response = $guzzle->get( $url );

        if ( $response->getStatusCode() === 200 ) {
            $response = json_decode( $response->getBody(), true );

            if ( $response[ 'results' ] ) {
                $result = $response[ 'results' ][ 0 ];

                if ( in_array( 'locality', $result[ 'types' ] ) ) {
                    $location = $result[ 'geometry' ][ 'location' ];


                    $searchLat = $location[ 'lat' ];
                    $searchLng = $location[ 'lng' ];
                }

            }

        }

    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// category filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'category_values' ] ) && trim( $_POST[ 'category_values' ] ) != 'all' ) {
        $taxcateg_include = sanitize_title( wp_kses( $_POST[ 'category_values' ], $allowed_html ) );
        $categ_array      = [
            'taxonomy' => 'property_category',
            'field'    => 'slug',
            'terms'    => $taxcateg_include,
        ];
    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// action  filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( ( ! empty( $_POST[ 'action_values' ] ) && trim( $_POST[ 'action_values' ] ) != 'all' ) ) {
        $taxaction_include = sanitize_title( wp_kses( $_POST[ 'action_values' ], $allowed_html ) );
        $action_array      = [
            'taxonomy' => 'property_action_category',
            'field'    => 'slug',
            'terms'    => $taxaction_include,
        ];
    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// city filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'city' ] ) && trim( $_POST[ 'city' ] ) != 'all' && $_POST[ 'city' ] != '' ) {
        $taxcity[]  = sanitize_title( wp_kses( $_POST[ 'city' ], $allowed_html ) );
        $city_array = [
            'taxonomy' => 'property_city',
            'field'    => 'slug',
            'terms'    => $taxcity,
        ];
    }


    if ( isset( $_POST[ 'area' ] ) && trim( $_POST[ 'area' ] ) != 'all' && $_POST[ 'area' ] != '' ) {
        $taxarea[]  = sanitize_title( wp_kses( $_POST[ 'area' ], $allowed_html ) );
        $area_array = [
            'taxonomy' => 'property_area',
            'field'    => 'slug',
            'terms'    => $taxarea,
        ];
    }


    $meta_query = $rooms = $baths = $price = [];
    if ( isset( $_POST[ 'advanced_rooms' ] ) && is_numeric( $_POST[ 'advanced_rooms' ] ) && intval( $_POST[ 'advanced_rooms' ] != 0 ) ) {
        $rooms[ 'key' ]   = 'property_rooms';
        $rooms[ 'value' ] = floatval( $_POST[ 'advanced_rooms' ] );
        $meta_query[]     = $rooms;
    }

    if ( isset( $_POST[ 'advanced_bath' ] ) && is_numeric( $_POST[ 'advanced_bath' ] ) && intval( $_POST[ 'advanced_bath' ] != 0 ) ) {
        $baths[ 'key' ]   = 'property_bathrooms';
        $baths[ 'value' ] = floatval( $_POST[ 'advanced_bath' ] );
        $meta_query[]     = $baths;
    }


    if ( isset( $_POST[ 'advanced_beds' ] ) && is_numeric( $_POST[ 'advanced_beds' ] ) && intval( $_POST[ 'advanced_beds' ] != 0 ) ) {
        $beds[ 'key' ]   = 'property_bedrooms';
        $beds[ 'value' ] = floatval( $_POST[ 'advanced_beds' ] );
        $meta_query[]    = $beds;
    }

    if ( isset( $_POST[ 'guest_no' ] ) && is_numeric( $_POST[ 'guest_no' ] ) && intval( $_POST[ 'guest_no' ] ) != 0 ) {
        $guest[ 'key' ]     = 'guest_no';
        $guest[ 'value' ]   = floatval( $_POST[ 'guest_no' ] );
        $guest[ 'type' ]    = 'numeric';
        $guest[ 'compare' ] = '>=';
        $meta_query[]       = $guest;
        $guest_no           = intval( $_POST[ 'guest_no' ] );
    }


    $country_array = [];
    /*  if ( isset( $_POST[ 'country' ] ) && $_POST[ 'country' ] != '' ) {
          $country                    = sanitize_text_field( wp_kses( $_POST[ 'country' ], $allowed_html ) );
          $country                    = str_replace( '-', ' ', $country );
          $country_array[ 'key' ]     = 'property_country';
          $country_array[ 'value' ]   = $country;
          $country_array[ 'type' ]    = 'CHAR';
          $country_array[ 'compare' ] = 'LIKE';
          $meta_query[]               = $country_array;
      }*/

    if ( isset( $_POST[ 'city' ] ) && $_POST[ 'city' ] == '' && isset( $_POST[ 'property_admin_area' ] ) && $_POST[ 'property_admin_area' ] != '' ) {
        /*  $admin_area_array              = [];
          $admin_area                    = sanitize_text_field( wp_kses( $_POST[ 'property_admin_area' ], $allowed_html ) );
          $admin_area                    = str_replace( " ", "-", $admin_area );
          $admin_area                    = str_replace( "\'", "", $admin_area );
          $admin_area_array[ 'key' ]     = 'property_admin_area';
          $admin_area_array[ 'value' ]   = $admin_area;
          $admin_area_array[ 'type' ]    = 'CHAR';
          $admin_area_array[ 'compare' ] = 'LIKE';
          $meta_query[]                  = $admin_area_array;*/
    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// chekcers
    //////////////////////////////////////////////////////////////////////////////////////
    $all_checkers = explode( ",", $_POST[ 'all_checkers' ] );

    foreach ( $all_checkers as $cheker ) {
        if ( $cheker != '' ) {
            $check_array              = [];
            $check_array[ 'key' ]     = $cheker;
            $check_array[ 'value' ]   = 1;
            $check_array[ 'compare' ] = 'CHAR';
            $meta_query[]             = $check_array;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// price filters
    //////////////////////////////////////////////////////////////////////////////////////
    $price_low = '';
    if ( isset( $_POST[ 'price_low' ] ) ) {
        $price_low = intval( $_POST[ 'price_low' ] );
    }

    $price_max = '';
    if ( isset( $_POST[ 'price_max' ] ) && is_numeric( $_POST[ 'price_max' ] ) ) {
        $price_max      = intval( $_POST[ 'price_max' ] );
        $price[ 'key' ] = 'property_price';

        $custom_fields = get_option( 'wp_estate_multi_curr', true );
        if ( ! empty( $custom_fields ) && isset( $_COOKIE[ 'my_custom_curr' ] ) && isset( $_COOKIE[ 'my_custom_curr_pos' ] ) && isset( $_COOKIE[ 'my_custom_curr_symbol' ] ) && $_COOKIE[ 'my_custom_curr_pos' ] != - 1 ) {
            $i = intval( $_COOKIE[ 'my_custom_curr_pos' ] );
            if ( $price_low != 0 ) {
                $price_low = $price_low / $custom_fields[ $i ][ 2 ];
            }
            if ( $price_max != 0 ) {
                $price_max = $price_max / $custom_fields[ $i ][ 2 ];
            }

        }

        $price[ 'value' ]   = [ $price_low, $price_max ];
        $price[ 'type' ]    = 'numeric';
        $price[ 'compare' ] = 'BETWEEN';
        $meta_query[]       = $price;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// calendar filters
    //////////////////////////////////////////////////////////////////////////////////////

    $allowed_html = [];
    $book_from    = Filter::getDateAndHour( 'check_in', 'start_hour' );
    $book_to      = Filter::getDateAndHour( 'check_out', 'end_hour' );


    //////////////////////////////////////////////////////////////////////////////////////
    ///// order details
    //////////////////////////////////////////////////////////////////////////////////////
    $meta_order      = 'prop_featured';
    $meta_directions = 'DESC';

    $order = null;

    if ( isset( $_POST[ 'order' ] ) ) {
        $order = (int) wp_kses( $_POST[ 'order' ], $allowed_html );
        switch ( $order ) {
            case 1:
                $meta_order      = 'property_price';
                $meta_directions = 'DESC';
                break;
            case 2:
                $meta_order      = 'property_price';
                $meta_directions = 'ASC';
                break;
            case 3:
                $meta_order      = 'property_size';
                $meta_directions = 'DESC';
                break;
            case 4:
                $meta_order      = 'property_size';
                $meta_directions = 'ASC';
                break;
            case 5:
                $meta_order      = 'property_bedrooms';
                $meta_directions = 'DESC';
                break;
            case 6:
                $meta_order      = 'property_bedrooms';
                $meta_directions = 'ASC';
                break;

            default:
                $meta_order      = 'prop_featured';
                $meta_directions = 'DESC';

        }

    }

    $paged   = intval( $_POST[ 'newpage' ] );
    $prop_no = $order == 7 ? - 1 : intval( get_option( 'wp_estate_prop_no', '' ) );

    ////////////////////////////////////////////////////////////////////////////
    // if we have check in and check out dates we need to double loop
    ////////////////////////////////////////////////////////////////////////////
    if ( ! empty( $book_from ) || ! empty( $book_to ) ) {

        if ( empty( $book_from ) ) {
            $book_from = $book_to;
        }

        if ( empty( $book_to ) ) {
            $book_to = $book_from;
        }

        $args = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'posts_per_page'         => '-1',
            'meta_key'               => 'prop_featured',
            'orderby'                => 'meta_value',
            'meta_query'             => $meta_query,
            'tax_query'              => [
                'relation' => 'AND',
                $categ_array,
                $action_array,
                // $city_array,
                //$area_array,
            ],
        ];


        $prop_selection = new WP_Query( $args );
        $right_array    = [];
        $right_array[]  = 0;
        while ( $prop_selection->have_posts() ): $prop_selection->the_post();
            //       print '</br>we check '.$guest_no.'</br>';
            if ( wpestate_check_booking_valability( $book_from, $book_to, $post->ID ) ) {
                $right_array[] = $post->ID;
            }
        endwhile;

        wp_reset_postdata();
        $args = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'paged'                  => $paged,
            'meta_key'               => $meta_order,
            'order'                  => $meta_directions,
            'orderby'                => 'meta_value',
            'posts_per_page'         => $prop_no,
            'post__in'               => $right_array,
        ];


        add_filter( 'posts_orderby', 'wpestate_my_order' );
        $prop_selection = new WP_Query( $args );
        remove_filter( 'posts_orderby', 'wpestate_my_order' );
    } else {
        $args           = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'paged'                  => $paged,
            'posts_per_page'         => $prop_no,
            'meta_key'               => $meta_order,
            'order'                  => $meta_directions,
            'orderby'                => 'meta_value',
            'meta_query'             => $meta_query,
            'tax_query'              => [
                'relation' => 'AND',
                $categ_array,
                $action_array,
                // $city_array,
                // $area_array,
            ],
        ];
        $prop_selection = new WP_Query( $args );

    }

    // Perform search by location radius
    if ( ! empty( $searchLng ) && ! empty( $searchLat ) && ! empty( $searchRadius ) ) {

        $postIn = [];

        foreach ( $prop_selection->get_posts() as $post ) {

            $propLat = (float) $post->property_latitude;
            $propLng = (float) $post->property_longitude;

            $distance = Utility::haversineGreatCircleDistance(
                $searchLat,
                $searchLng,
                $propLat,
                $propLng
            );

            if ( $searchRadius >= $distance ) {
                $postIn[] = $post->ID;
            }

        }

        if ( empty( array_filter( $postIn ) ) ) {
            $args[ 'meta_query' ] = [
                [
                    'key'   => 'wpk_empty_query',
                    'value' => '9999',
                ],
            ];
        }

        $args[ 'post__in' ] = $postIn;

        $prop_selection = new WP_Query( $args );

    }

    //Custom ordering by distance
    if ( $prop_selection->have_posts() ) {
        if ( $order == 7 ) {

            $prop_selection = Order::byUserDistance( $userLat, $userLng, $paged, $prop_selection );

        } else if ( in_array( $order, [ 1, 2 ] ) ) {

            $prop_selection = Order::byPrice( $paged, $order === 1 ? 'DESC' : 'ASC', $prop_selection );

        }
    }

    $counter        = 0;
    $compare_submit = wpestate_get_template_link( 'compare_listings.php' );
    $markers        = [];
    $return_string  = '';
    ob_start();

    print '<span id="scrollhere"></span>';

    /*
    if( !is_tax() ){
      print '<div class="compare_ajax_wrapper">';
          get_template_part('templates/compare_list');
      print'</div>';
    }*/

    $listing_unit_style_half = get_option( 'wp_estate_listing_unit_style_half', '' );


    if ( $prop_selection->have_posts() ) {
        while ( $prop_selection->have_posts() ): $prop_selection->the_post();
            if ( $listing_unit_style_half == 1 ) {
                get_template_part( 'templates/property_unit_wide' );
            } else {
                get_template_part( 'templates/property_unit' );
            }
            $markers[] = wpestate_pin_unit_creation( get_the_ID(), $currency, $where_currency, $counter );
        endwhile;
        kriesi_pagination_ajax( $prop_selection->max_num_pages, $range = 2, $paged, 'pagination_ajax_search_home' );
    } else {
        print '<span class="no_results">' . esc_html__( "We didn't find any results", "wpestate" ) . '</>';
    }
    // print '</div>';
    $templates = ob_get_contents();
    ob_end_clean();
    $return_string .= '<div class="half_map_results">' . $prop_selection->found_posts . ' ' . esc_html__( ' Results found!', 'wpestate' ) . '</div>';
    $return_string .= $templates;
    echo json_encode( [
        'added'          => true,
        'arguments'      => json_encode( $args ),
        'markers'        => json_encode( $markers ),
        'response'       => $return_string,
        'searchRadius'   => $searchRadius,
        'args'           => $args,
        'googleResponse' => $response ?? [],
    ] );
    die();
}


