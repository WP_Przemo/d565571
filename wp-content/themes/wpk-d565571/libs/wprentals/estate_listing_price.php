<?php

/**
 * Overwrite for wp rentals function in order to replace "nights" with "hours"
 *
 * @param int $post_id
 *
 * @return string
 */
function estate_listing_price( $post_id ) {
    $return_string                = '';
    $property_price               = floatval( get_post_meta( $post_id, 'property_price', true ) );
    $property_price_before_label  = esc_html( get_post_meta( $post_id, 'property_price_before_label', true ) );
    $property_price_after_label   = esc_html( get_post_meta( $post_id, 'property_price_after_label', true ) );
    $property_price_per_week      = floatval( get_post_meta( $post_id, 'property_price_per_week', true ) );
    $property_price_per_month     = floatval( get_post_meta( $post_id, 'property_price_per_month', true ) );
    $cleaning_fee                 = floatval( get_post_meta( $post_id, 'cleaning_fee', true ) );
    $city_fee                     = floatval( get_post_meta( $post_id, 'city_fee', true ) );
    $cleaning_fee_per_day         = floatval( get_post_meta( $post_id, 'cleaning_fee_per_day', true ) );
    $city_fee_percent             = floatval( get_post_meta( $post_id, 'city_fee_percent', true ) );
    $city_fee_per_day             = floatval( get_post_meta( $post_id, 'city_fee_per_day', true ) );
    $price_per_guest_from_one     = floatval( get_post_meta( $post_id, 'price_per_guest_from_one', true ) );
    $overload_guest               = floatval( get_post_meta( $post_id, 'overload_guest', true ) );
    $checkin_change_over          = floatval( get_post_meta( $post_id, 'checkin_change_over', true ) );
    $checkin_checkout_change_over = floatval( get_post_meta( $post_id, 'checkin_checkout_change_over', true ) );
    $min_days_booking             = floatval( get_post_meta( $post_id, 'min_days_booking', true ) );
    $extra_price_per_guest        = floatval( get_post_meta( $post_id, 'extra_price_per_guest', true ) );
    $price_per_weekeend           = floatval( get_post_meta( $post_id, 'price_per_weekeend', true ) );
    $security_deposit             = floatval( get_post_meta( $post_id, 'security_deposit', true ) );
    $early_bird_percent           = floatval( get_post_meta( $post_id, 'early_bird_percent', true ) );
    $early_bird_days              = floatval( get_post_meta( $post_id, 'early_bird_days', true ) );


    $week_days = [
        '0' => esc_html__( 'All', 'wpestate' ),
        '1' => esc_html__( 'Monday', 'wpestate' ),
        '2' => esc_html__( 'Tuesday', 'wpestate' ),
        '3' => esc_html__( 'Wednesday', 'wpestate' ),
        '4' => esc_html__( 'Thursday', 'wpestate' ),
        '5' => esc_html__( 'Friday', 'wpestate' ),
        '6' => esc_html__( 'Saturday', 'wpestate' ),
        '7' => esc_html__( 'Sunday', 'wpestate' ),

    ];

    $currency       = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
    $where_currency = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );

    $th_separator  = get_option( 'wp_estate_prices_th_separator', '' );
    $custom_fields = get_option( 'wp_estate_multi_curr', true );

    $property_price_show           = wpestate_show_price_booking( $property_price, $currency, $where_currency, 1 );
    $property_price_per_week_show  = wpestate_show_price_booking( $property_price_per_week, $currency, $where_currency, 1 );
    $property_price_per_month_show = wpestate_show_price_booking( $property_price_per_month, $currency, $where_currency, 1 );
    $cleaning_fee_show             = wpestate_show_price_booking( $cleaning_fee, $currency, $where_currency, 1 );
    $city_fee_show                 = wpestate_show_price_booking( $city_fee, $currency, $where_currency, 1 );

    $price_per_weekeend_show    = wpestate_show_price_booking( $price_per_weekeend, $currency, $where_currency, 1 );
    $extra_price_per_guest_show = wpestate_show_price_booking( $extra_price_per_guest, $currency, $where_currency, 1 );
    $extra_price_per_guest_show = wpestate_show_price_booking( $extra_price_per_guest, $currency, $where_currency, 1 );
    $security_deposit_show      = wpestate_show_price_booking( $security_deposit, $currency, $where_currency, 1 );

    $setup_weekend_status = esc_html( get_option( 'wp_estate_setup_weekend', '' ) );
    $weekedn              = [
        0 => __( "Sunday and Saturday", "wpestate" ),
        1 => __( "Friday and Saturday", "wpestate" ),
        2 => __( "Friday, Saturday and Sunday", "wpestate" ),
    ];


    if ( $price_per_guest_from_one != 1 ) {

        if ( $property_price != 0 ) {
            $return_string .= '<div class="listing_detail list_detail_prop_price_per_night col-md-6"><span class="item_head">' . esc_html__( 'Price per hour', 'wpestate' ) . ':</span> ' . $property_price_before_label . ' ' . $property_price_show . ' ' . $property_price_after_label . '</div>';
        }

        if ( $property_price_per_week != 0 ) {
            $return_string .= '<div class="listing_detail list_detail_prop_price_per_night_7d col-md-6"><span class="item_head">' . esc_html__( 'Price per hour (7d+)', 'wpestate' ) . ':</span> ' . $property_price_per_week_show . '</div>';
        }

        if ( $property_price_per_month != 0 ) {
            $return_string .= '<div class="listing_detail list_detail_prop_price_per_night_30d col-md-6"><span class="item_head">' . esc_html__( 'Price per hour (30d+)', 'wpestate' ) . ':</span> ' . $property_price_per_month_show . '</div>';
        }

        if ( $price_per_weekeend != 0 ) {
            $return_string .= '<div class="listing_detail list_detail_prop_price_per_night_weekend col-md-6"><span class="item_head">' . esc_html__( 'Price per weekend ', 'wpestate' ) . '(' . $weekedn[ $setup_weekend_status ] . ') ' . ':</span> ' . $price_per_weekeend_show . '</div>';
        }

        if ( $extra_price_per_guest != 0 ) {
            $return_string .= '<div class="listing_detail list_detail_prop_price_per_night_extra_guest col-md-6"><span class="item_head">' . esc_html__( 'Extra Price per guest', 'wpestate' ) . ':</span> ' . $extra_price_per_guest_show . '</div>';
        }
    } else {
        if ( $extra_price_per_guest != 0 ) {
            $return_string .= '<div class="listing_detail list_detail_prop_price_per_night_extra_guest_price col-md-6"><span class="item_head">' . esc_html__( 'Price per guest', 'wpestate' ) . ':</span> ' . $extra_price_per_guest_show . '</div>';
        }
    }
    $options_array = [
        0 => esc_html__( 'Single Fee', 'wpestate' ),
        1 => esc_html__( 'Per hour', 'wpestate' ),
        2 => esc_html__( 'Per Guest', 'wpestate' ),
        3 => esc_html__( 'Per hour per Guest', 'wpestate' ),
    ];
    if ( $cleaning_fee != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_price_cleaning_fee col-md-6"><span class="item_head">' . esc_html__( 'Cleaning Fee', 'wpestate' ) . ':</span> ' . $cleaning_fee_show;

        $return_string .= $options_array[ $cleaning_fee_per_day ];
        //' '.esc_html__('per hour','wpestate');

        $return_string .= '</div>';
    }

    if ( $city_fee != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_price_tax_fee col-md-6"><span class="item_head">' . esc_html__( 'City Tax Fee', 'wpestate' ) . ':</span> ';
        if ( $city_fee_percent == 0 ) {
            $return_string .= $city_fee_show . ' ' . $options_array[ $city_fee_per_day ];
        } else {
            $return_string .= $city_fee . '%' . ' ' . __( 'of price per hour', 'wpestate' );
        }
        $return_string .= '</div>';

    }

    if ( $min_days_booking != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_price_min_hours col-md-6"><span class="item_head">' . esc_html__( 'Minimum no of hours', 'wpestate' ) . ':</span> ' . $min_days_booking . '</div>';
    }

    if ( $overload_guest != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_price_overload_guest col-md-6"><span class="item_head">' . esc_html__( 'Allow more guests than the capacity: ', 'wpestate' ) . ' </span>' . esc_html__( 'yes', 'wpestate' ) . '</div>';
    }


    if ( $checkin_change_over != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_book_starts col-md-6"><span class="item_head">' . esc_html__( 'Booking starts only on', 'wpestate' ) . ':</span> ' . $week_days[ $checkin_change_over ] . '</div>';
    }

    if ( $security_deposit != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_book_starts col-md-6"><span class="item_head">' . esc_html__( 'Security deposit', 'wpestate' ) . ':</span> ' . $security_deposit_show . '</div>';
    }

    if ( $checkin_checkout_change_over != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_book_starts_end col-md-6"><span class="item_head">' . esc_html__( 'Booking starts/ends only on', 'wpestate' ) . ':</span> ' . $week_days[ $checkin_checkout_change_over ] . '</div>';
    }


    if ( $early_bird_percent != 0 ) {
        $return_string .= '<div class="listing_detail list_detail_prop_book_starts_end col-md-6"><span class="item_head">' . esc_html__( 'Early Bird Discount', 'wpestate' ) . ':</span> ' . $early_bird_percent . '% ' . esc_html__( 'discount', 'wpestate' ) . ' ' . esc_html__( 'for bookings made', 'wpestate' ) . ' ' . $early_bird_days . ' ' . esc_html__( 'hours in advance', 'wpestate' ) . '</div>';

    }

    $extra_pay_options = ( get_post_meta( $post_id, 'extra_pay_options', true ) );

    if ( is_array( $extra_pay_options ) && ! empty( $extra_pay_options ) ) {
        $return_string .= '<div class="listing_detail list_detail_prop_book_starts_end col-md-12"><span class="item_head">' . esc_html__( 'Extra options', 'wpestate' ) . ':</span></div>';
        foreach ( $extra_pay_options as $key => $options ) {
            $return_string           .= '<div class="extra_pay_option">';
            $extra_option_price_show = wpestate_show_price_booking( $options[ 1 ], $currency, $where_currency, 1 );
            $return_string           .= '' . $options[ 0 ] . ': ' . $extra_option_price_show . ' ' . $options_array[ $options[ 2 ] ];

            $return_string .= '</div>';

        }
    }


    return $return_string;

}