<?php

/**
 * Overwrite for theme function that replaces "night" strings with "hours"
 *
 * @return void
 */
function wpestate_create_invoice_form() {
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;


    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }


    $invoice_id  = 0;
    $bookid      = intval( $_POST[ 'bookid' ] );
    $lisiting_id = get_post_meta( $bookid, 'booking_id', true );
    $the_post    = get_post( $lisiting_id );

    if ( $current_user->ID != $the_post->post_author ) {
        exit( 'you don\'t have the right to see this' );
    }


    $booking_from_date   = esc_html( get_post_meta( $bookid, 'booking_from_date', true ) );
    $property_id         = esc_html( get_post_meta( $bookid, 'booking_id', true ) );
    $booking_to_date     = esc_html( get_post_meta( $bookid, 'booking_to_date', true ) );
    $extra_options       = esc_html( get_post_meta( $bookid, 'extra_options', true ) );
    $extra_options_array = explode( ',', $extra_options );
    $booking_guests      = get_post_meta( $bookid, 'booking_guests', true );
    $booking_array       = wpestate_booking_price( $booking_guests, $invoice_id, $property_id, $booking_from_date, $booking_to_date, $bookid, $extra_options_array );

    $booking_array = \Wpk\d565571\Models\Property::find( $property_id )->getPrices( [
        'start_date'          => new DateTime( $booking_from_date ),
        'end_date'            => new DateTime( $booking_to_date ),
        'guest_no'            => $booking_guests,
        'extra_options_array' => $extra_options_array,
        'booking_id'          => $bookid,
    ] );

    $where_currency     = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
    $currency           = esc_html( get_option( 'wp_estate_submission_curency', '' ) );
    $currency           = wpestate_curency_submission_pick();
    $include_expeses    = esc_html( get_option( 'wp_estate_include_expenses', '' ) );
    $security_depozit   = floatval( get_post_meta( $property_id, 'security_deposit', true ) );
    $price_per_weekeend = floatval( get_post_meta( $property_id, 'price_per_weekeend', true ) );


    $total_price_comp = $booking_array[ 'total_price' ];

    if ( $include_expeses == 'yes' ) {
        $total_price_comp2 = $total_price_comp;
    } else {
        $total_price_comp2 = $booking_array[ 'total_price' ] - $booking_array[ 'city_fee' ] - $booking_array[ 'cleaning_fee' ];
    }


    $wp_estate_book_down           = esc_html( get_option( 'wp_estate_book_down', '' ) );
    $wp_estate_book_down_fixed_fee = esc_html( get_option( 'wp_estate_book_down_fixed_fee', '' ) );

    $depozit                  = wpestate_calculate_deposit( $wp_estate_book_down, $wp_estate_book_down_fixed_fee, $total_price_comp2 );
    $balance                  = $total_price_comp - $depozit;
    $price_show               = wpestate_show_price_booking_for_invoice( $booking_array[ 'default_price' ], $currency, $where_currency, 0, 1 );
    $price_per_weekeend_show  = wpestate_show_price_booking_for_invoice( $price_per_weekeend, $currency, $where_currency, 0, 1 );
    $total_price_show         = wpestate_show_price_booking_for_invoice( $total_price_comp, $currency, $where_currency, 0, 1 );
    $security_depozit_show    = wpestate_show_price_booking_for_invoice( $security_depozit, $currency, $where_currency, 1, 1 );
    $deposit_show             = wpestate_show_price_booking_for_invoice( $depozit, $currency, $where_currency, 0, 1 );
    $balance_show             = wpestate_show_price_booking_for_invoice( $balance, $currency, $where_currency, 0, 1 );
    $city_fee_show            = wpestate_show_price_booking_for_invoice( $booking_array[ 'city_fee' ], $currency, $where_currency, 1, 1 );
    $cleaning_fee_show        = wpestate_show_price_booking_for_invoice( $booking_array[ 'cleaning_fee' ], $currency, $where_currency, 1, 1 );
    $inter_price_show         = wpestate_show_price_booking_for_invoice( $booking_array[ 'inter_price' ], $currency, $where_currency, 1, 1 );
    $total_guest              = wpestate_show_price_booking_for_invoice( $booking_array[ 'total_extra_price_per_guest' ], $currency, $where_currency, 1, 1 );
    $guest_price              = wpestate_show_price_booking_for_invoice( $booking_array[ 'extra_price_per_guest' ], $currency, $where_currency, 1, 1 );
    $extra_price_per_guest    = wpestate_show_price_booking( $booking_array[ 'extra_price_per_guest' ], $currency, $where_currency, 1 );
    $early_bird_discount_show = wpestate_show_price_booking_for_invoice( $booking_array[ 'early_bird_discount' ], $currency, $where_currency, 1, 1 );


    if ( trim( $deposit_show ) == '' ) {
        $deposit_show = 0;
    }


    //print_r($booking_array);

    print '              
            <div class="create_invoice_form">
                <h3>' . esc_html__( 'Create Invoice', 'wpestate' ) . '</h3>

                <div class="invoice_table">
                    <div class="invoice_data">
                        <div style="display:none" id="property_details_invoice" data-taxes_value="' . floatval( get_post_meta( $property_id, 'property_taxes', true ) ) . '" data-earlyb="' . floatval( get_post_meta( $property_id, 'early_bird_percent', true ) ) . '"></div>
                        <div class="date_interval">
                        <div class="invoice_data_legend">' . esc_html__( 'Period', 'wpestate' ) . ' : </div>
                             <div style="width: 100%; float: none; clear: both;">
                                        '.esc_html__('From '). wpestate_convert_dateformat_reverse( $booking_from_date ).'
                                    </div>
                                    <div style="width: 100%; float: none;">
                                     '.esc_html__('To '). wpestate_convert_dateformat_reverse( $booking_to_date ).'
                                    </div>
                        </div>
                        <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'No of hours', 'wpestate' ) . ': </span>' . $booking_array[ 'count_days' ] . '</span>
                        <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'No of guests', 'wpestate' ) . ': </span>' . $booking_guests . '</span>';
    if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
        print'    
                            <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Price per Guest', 'wpestate' ) . ': </span>';
        print $extra_price_per_guest;
        print'</span>';
    } else {
        print'    
                            <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Price per hour', 'wpestate' ) . ': </span>';
        print __( 'default price:', 'wpestate' ) . ' ' . $price_show;
        if ( $booking_array[ 'has_custom' ] ) {
            print ', ' . esc_html__( 'has custom price', 'wpestate' );

        }
        if ( $booking_array[ 'cover_weekend' ] ) {
            print ', ' . esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
        }
        print'</span>';
        if ( $booking_array[ 'has_custom' ] ) {
            print '<span class="invoice_data_legend">' . __( 'Price details:', 'wpestate' ) . '</span>';
            foreach ( $booking_array[ 'custom_price_array' ] as $date => $price ) {
                $day_price = wpestate_show_price_booking_for_invoice( $price, $currency, $where_currency, 1, 1 );
                print '<span class="price_custom_explained">' . __( 'on', 'wpestate' ) . ' ' . wpestate_convert_dateformat_reverse( date( "Y-m-d", $date ) ) . ' ' . __( 'price is', 'wpestate' ) . ' ' . $day_price . '</span>';
            }
        }


    }

    print '    
                    </div>
                    <div class="invoice_details">
                        <div class="invoice_row header_legend">
                           <span class="inv_legend">    ' . esc_html__( 'Cost', 'wpestate' ) . '</span>
                           <span class="inv_data">      ' . esc_html__( 'Price', 'wpestate' ) . '</span>
                           <span class="inv_exp">       ' . esc_html__( 'Detail', 'wpestate' ) . ' </span>
                        </div>
                        <div class="invoice_row invoice_content">
                            <span class="inv_legend">   ' . esc_html__( 'Subtotal', 'wpestate' ) . '</span>
                            <span class="inv_data">   ' . $inter_price_show . '</span>';

    if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
        print  $extra_price_per_guest . ' x ' . $booking_array[ 'count_days' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' x ' . $booking_array[ 'curent_guest_no' ] . ' ' . esc_html__( 'guests', 'wpestate' );
    } else {

        if ( $booking_array[ 'cover_weekend' ] ) {
            $new_price_to_show = esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
        } else {
            if ( $booking_array[ 'has_custom' ] ) {
                $new_price_to_show = esc_html__( "custom price", "wpestate" );
            } else {
                $new_price_to_show = $price_show . ' ' . esc_html__( 'per hour', 'wpestate' );
            }
        }


        if ( $booking_array[ 'numberDays' ] == 1 ) {
            print ' <span class="inv_exp">   (' . $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hour', 'wpestate' ) . ' | ' . $new_price_to_show . ') </span>';
        } else {
            print ' <span class="inv_exp">   (' . $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' | ' . $new_price_to_show . ') </span>';
        }
    }

    if ( $booking_array[ 'price_per_guest_from_one' ] == 1 && $booking_array[ 'custom_period_quest' ] == 1 ) {
        esc_html_e( " period with custom price per guest", "wpestate" );
    }

    print'            

                            </div>';


    if ( $booking_array[ 'has_guest_overload' ] != 0 && $booking_array[ 'total_extra_price_per_guest' ] != 0 ) {
        print'
                                <div class="invoice_row invoice_content">
                                    <span class="inv_legend">   ' . esc_html__( 'Extra Guests', 'wpestate' ) . '</span>
                                    <span class="inv_data" id="extra-guests" data-extra-guests="' . $booking_array[ 'total_extra_price_per_guest' ] . '">  ' . $total_guest . '</span>
                                    <span class="inv_exp">   (' . $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' | ' . $booking_array[ 'extra_guests' ] . ' ' . esc_html__( 'extra guests', 'wpestate' ) . ' )';

        if ( $booking_array[ 'custom_period_quest' ] == 1 ) {
            echo esc_html__( " period with custom price per guest", "wpestate" );
        }

        print'</span>
                       
                                </div>';
    }

    if ( $booking_array[ 'cleaning_fee' ] != 0 && $booking_array[ 'cleaning_fee' ] != '' ) {
        print'
                               <div class="invoice_row invoice_content">
                                   <span class="inv_legend">   ' . esc_html__( 'Cleaning fee', 'wpestate' ) . '</span>
                                   <span class="inv_data" id="cleaning-fee" data-cleaning-fee="' . $booking_array[ 'cleaning_fee' ] . '">  ' . $cleaning_fee_show . '</span>
                               </div>';
    }

    if ( $booking_array[ 'city_fee' ] != 0 && $booking_array[ 'city_fee' ] != '' ) {
        print'
                               <div class="invoice_row invoice_content">
                                   <span class="inv_legend">   ' . esc_html__( 'City fee', 'wpestate' ) . '</span>
                                   <span class="inv_data" id="city-fee" data-city-fee="' . $booking_array[ 'city_fee' ] . '">  ' . $city_fee_show . '</span>
                               </div>';
    }


    $extra_pay_options = ( get_post_meta( $property_id, 'extra_pay_options', true ) );
    if ( $extra_options != '' ) {
        $extra_options_array = explode( ',', $extra_options );
    }


    $options_array_explanations = [
        0 => esc_html__( 'Single Fee', 'wpestate' ),
        1 => esc_html__( 'Per Night', 'wpestate' ),
        2 => esc_html__( 'Per Guest', 'wpestate' ),
        3 => esc_html__( 'Per Night per Guest', 'wpestate' ),
    ];

    foreach ( $extra_options_array as $key => $value ) {
        if ( isset( $extra_pay_options[ $value ][ 0 ] ) ) {
            $extra_option_value             = wpestate_calculate_extra_options_value( $booking_array[ 'count_days' ], $booking_guests, $extra_pay_options[ $value ][ 2 ], $extra_pay_options[ $value ][ 1 ] );
            $extra_option_value_show        = wpestate_show_price_booking_for_invoice( $extra_option_value, $currency, $where_currency, 1, 1 );
            $extra_option_value_show_single = wpestate_show_price_booking_for_invoice( $extra_pay_options[ $value ][ 1 ], $currency, $where_currency, 0, 1 );

            print'
                                    <div class="invoice_row invoice_content">
                                        <span class="inv_legend">   ' . $extra_pay_options[ $value ][ 0 ] . '</span>
                                        <span class="inv_data invoice_default_extra" data-value="' . $extra_option_value . '" >  ' . $extra_option_value_show . '</span>
                                        <span class="inv_data inv_data_exp">' . $extra_option_value_show_single . ' ' . $options_array_explanations[ $extra_pay_options[ $value ][ 2 ] ] . '</span>
                                    </div>';
        }
    }

    if ( $security_depozit != 0 ) {
        print'
                                <div class="invoice_row invoice_content">
                                    <span class="inv_legend">   ' . __( 'Security Deposit', 'wpestate' ) . '</span>
                                    <span id="security_depozit_row" data-val="' . $security_depozit . '" class="inv_data">  ' . $security_depozit_show . '</span>
                                    <span  class="inv_data">' . __( '*refundable', 'wpestate' ) . '</span>
                                </div>';
    }


    if ( $booking_array[ 'early_bird_discount' ] > 0 ) {
        print'
                                <div class="invoice_row invoice_content">
                                    <span class="inv_legend">   ' . __( 'Early Bird Discount', 'wpestate' ) . '</span>
                                    <span id="erarly_bird_row" data-val="' . $booking_array[ 'early_bird_discount' ] . '"  class="inv_data">  ' . $early_bird_discount_show . '</span>
                                    <span class="inv_data"></span>
                                </div>';
    }


    print'  
                            <div class="invoice_row invoice_total invoice_total_generate_invoice">
                                <div style="display:none;" id="inter_price" data-value="' . $booking_array [ 'inter_price' ] . '"></div>
                                <span class="inv_legend"><strong>' . esc_html__( 'Guest Pays', 'wpestate' ) . '</strong></span>
                                <span class="inv_data" id="total_amm" data-total="' . $total_price_comp . '">' . $total_price_show . '</span>

                                <span class="total_inv_span"><span class="inv_legend"> ' . esc_html__( 'Reservation Fee Required', 'wpestate' ) . ':</span> <span id="inv_depozit" data-value="' . $depozit . '">' . $deposit_show . '</span>
                                <div style="width:100%"></div>
                                <span class="inv_legend">' . esc_html__( 'Balance Owing', 'wpestate' ) . ':</span> <span id="inv_balance" data-val="' . $balance . '">' . $balance_show . '</span>
                            </div>';

    //   $total_price_show       =   wpestate_show_price_booking_for_invoice($total_price_comp,$currency,$where_currency,0,1);


    $taxes_show       = wpestate_show_price_booking_for_invoice( $booking_array [ 'taxes' ], $currency, $where_currency, 0, 1 );
    $you_earn_show    = wpestate_show_price_booking_for_invoice( $booking_array [ 'youearned' ], $currency, $where_currency, 0, 1 );
    $service_fee_show = wpestate_show_price_booking_for_invoice( $booking_array [ 'service_fee' ], $currency, $where_currency, 0, 1 );
    print'  
                            <div class="invoice_row invoice_totalx invoice_total_generate_invoice">
                                <span class="inv_legend"><strong>' . esc_html__( 'You Earn', 'wpestate' ) . '</strong></span>
                                <span class="inv_data" id="youearned" data-youearned="' . $booking_array [ 'youearned' ] . '"><strong>' . $you_earn_show . '</strong></span>
                                

                                <div class="invoice_explantions">' . esc_html__( 'we deduct security deposit, city fees, cleaning fees and website service fee', 'wpestate' ) . '</div>
                                
                                <span class="total_inv_span">
                                    <span class="inv_legend">' . esc_html__( 'Service Fee', 'wpestate' ) . ':</span>
                                    <span id="inv_service_fee" data-value="' . $booking_array [ 'service_fee' ] . '"  data-value-fixed-hidden="' . floatval( get_option( 'wp_estate_service_fee_fixed_fee', '' ) ) . '">' . $service_fee_show . '</span>
                                    
                                    <div style="width:100%"></div>
                                    
                                    <span class="inv_legend">' . esc_html__( 'Taxes', 'wpestate' ) . ':</span>
                                    <span id="inv_taxes" data-value="' . $booking_array [ 'taxes' ] . '" >' . $taxes_show . '</span>
                                </span>    
                                
                                <div class="invoice_explantions">' . esc_html__( '*taxes are included in your earnings and you are responsible for paying these taxes', 'wpestate' ) . '</div>
                            </div>';

    print'</div>   ';


    $book_down           = floatval( get_option( 'wp_estate_book_down', '' ) );
    $book_down_fixed_fee = floatval( get_option( 'wp_estate_book_down_fixed_fee', '' ) );

    if ( $book_down != 0 || $book_down_fixed_fee != 0 ) {
        $label        = esc_html__( 'Send Invoice', 'wpestate' );
        $is_confirmed = 0;
    } else {
        $label        = esc_html__( 'Confirm Booking - No Reservation Fee Required', 'wpestate' );
        $is_confirmed = 1;

    }

    print '<div class="action1_booking" id="invoice_submit" data-is_confirmed="' . $is_confirmed . '" data-bookid="' . $bookid . '">' . $label . '</div>';

    print '</div>';


    print '
                    <div class="invoice_actions">
                        <h4>' . esc_html__( 'Add extra expense', 'wpestate' ) . '</h4>
                        <input type="text" id="inv_expense_name" size="40" name="inv_expense_name" placeholder="' . esc_html__( "type expense name", "wpestate" ) . '">
                        <input type="text" id="inv_expense_value" size="40" name="inv_expense_value" placeholder="' . esc_html__( "type expense value", "wpestate" ) . '">
                        <div class="action1_booking" id="add_inv_expenses" data-include_ex="' . $include_expeses . '">' . esc_html__( 'add', 'wpestate' ) . '</div>

                        <h4>' . esc_html__( 'Add discount', 'wpestate' ) . '</h4>
                        <input type="text" id="inv_expense_discount" size="40" name="inv_expense_discount" placeholder="' . esc_html__( "type discount value", "wpestate" ) . '">
                        <div class="action1_booking" id="add_inv_discount" data-include_ex="' . $include_expeses . '">' . esc_html__( 'add', 'wpestate' ) . '</div>
                    </div>';

    print  wp_nonce_field( 'create_invoice_ajax_nonce', 'security-create_invoice_ajax_nonce' ) . '
            </div>';
    die();
}
