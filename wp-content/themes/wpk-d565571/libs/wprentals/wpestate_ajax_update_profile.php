<?php

function wpestate_ajax_update_profile() {
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;
    $login_name   = $current_user->display_name;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }

    check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );

    $firstname               = sanitize_text_field( $_POST[ 'firstname' ] );
    $secondname              = sanitize_text_field( $_POST[ 'secondname' ] );
    $useremail               = sanitize_text_field( $_POST[ 'useremail' ] );
    $userphone               = sanitize_text_field( $_POST[ 'userphone' ] );
    $usermobile              = sanitize_text_field( $_POST[ 'usermobile' ] );
    $userskype               = sanitize_text_field( $_POST[ 'userskype' ] );
    $about_me                = sanitize_text_field( $_POST[ 'description' ] );
    $profile_image_url_small = sanitize_text_field( $_POST[ 'profile_image_url_small' ] );
    $profile_image_url       = sanitize_text_field( $_POST[ 'profile_image_url' ] );
    $userfacebook            = sanitize_text_field( $_POST[ 'userfacebook' ] );
    $usertwitter             = sanitize_text_field( $_POST[ 'usertwitter' ] );
    $userlinkedin            = sanitize_text_field( $_POST[ 'userlinkedin' ] );
    $userpinterest           = sanitize_text_field( $_POST[ 'userpinterest' ] );
    $live_in                 = sanitize_text_field( $_POST[ 'live_in' ] );
    $i_speak                 = sanitize_text_field( $_POST[ 'i_speak' ] );
    $paypal_payments_to      = sanitize_text_field( $_POST[ 'paypal_payments_to' ] );
    $payment_info            = sanitize_text_field( $_POST[ 'payment_info' ] );
    $profession              = sanitize_text_field( $_POST[ 'profession' ] );
    $bank_account_owner_name = sanitize_text_field( $_POST[ 'bank_account_owner_name' ] );
    $branch                  = sanitize_text_field( $_POST[ 'branch' ] );
    $bank_code               = sanitize_text_field( $_POST[ 'bank_code' ] );

    $bank_account = isset( $_POST[ 'bank_account' ] ) ? sanitize_text_field( $_POST[ 'bank_account' ] ) : '';

    update_user_meta( $userID, 'bank_account', $bank_account );
    update_user_meta( $userID, 'first_name', $firstname );
    update_user_meta( $userID, 'last_name', $secondname );
    update_user_meta( $userID, 'phone', $userphone );
    update_user_meta( $userID, 'skype', $userskype );
    update_user_meta( $userID, 'profession', $profession );
    update_user_meta( $userID, 'bank_account_owner_name', $bank_account_owner_name );
    update_user_meta( $userID, 'branch', $branch );
    update_user_meta( $userID, 'bank_code', $bank_code );

    update_user_meta( $userID, 'custom_picture', $profile_image_url );
    update_user_meta( $userID, 'small_custom_picture', $profile_image_url_small );

    $old_mobile = get_user_meta( $userID, 'mobile', true );
    if ( $old_mobile != $usermobile ) {
        update_user_meta( $userID, 'check_phone_valid', 'no' );
    }

    update_user_meta( $userID, 'mobile', $usermobile );

    update_user_meta( $userID, 'facebook', $userfacebook );
    update_user_meta( $userID, 'twitter', $usertwitter );
    update_user_meta( $userID, 'linkedin', $userlinkedin );
    update_user_meta( $userID, 'pinterest', $userpinterest );
    update_user_meta( $userID, 'description', $about_me );
    update_user_meta( $userID, 'live_in', $live_in );
    update_user_meta( $userID, 'i_speak', $i_speak );
    update_user_meta( $userID, 'paypal_payments_to', $paypal_payments_to );
    update_user_meta( $userID, 'payment_info', $payment_info );

    $agent_id = get_user_meta( $userID, 'user_agent_id', true );

    wpestate_update_user_agent( $agent_id, $firstname, $secondname, $useremail, $userphone, $userskype, $profile_image_url, $usermobile, $about_me, $profile_image_url_small, $userfacebook, $usertwitter, $userlinkedin, $userpinterest, $live_in, $i_speak, $login_name, $payment_info );

    $user_email_rcapi = $useremail;
    if ( $current_user->user_email != $useremail ) {
        $user_id = email_exists( $useremail );
        if ( $user_id ) {
            esc_html_e( 'The email was not saved because it is used by another user. ', 'wpestate' );
        } else if ( $useremail == '' ) {
            esc_html_e( 'The email field cannot be blank. ', 'wpestate' );
        } else if ( filter_var( $useremail, FILTER_VALIDATE_EMAIL ) === false ) {
            print esc_html__( 'The email doesn\'t look right !', 'wpestate' );
        } else {
            $args = [
                'ID'         => $userID,
                'user_email' => $useremail,
            ];

            wp_update_user( $args );
        }
    }


    esc_html_e( 'Profile updated', 'wpestate' );

    $arguments = [
        'user_id'            => $userID,
        'user_email_profile' => $current_user->user_email,
        'user_login'         => $current_user->user_login,
    ];


    $company_email = get_bloginfo( 'admin_email' );
    wpestate_select_email_type( $company_email, 'agent_update_profile', $arguments );


    //User ID verification
    if ( isset( $_POST[ 'user_id_image_id' ] ) && intval( $_POST[ 'user_id_image_id' ] != 0 ) ) {

        // check if already saved user ID
        $old_user_id_image_id = get_user_meta( $userID, 'user_id_image_id', true );
        $user_id_image        = esc_url( $_POST[ 'user_id_url' ] );
        $user_id_image_id     = absint( $_POST[ 'user_id_image_id' ] );

        // Update User ID verification fields
        if ( is_numeric( $_POST[ 'user_id_image_id' ] ) && ( absint( $_POST[ 'user_id_image_id' ] ) != $old_user_id_image_id ) ) {
            update_user_meta( $userID, 'user_id_image', $user_id_image );
            update_user_meta( $userID, 'user_id_image_id', $user_id_image_id );

            // strip user verification if new ID image has been uploaded
            update_user_meta( $userID, 'user_id_verified', 0 );

            // Set up separate user verification email

            $owner_id  = get_user_meta( $userID, 'user_agent_id', true );
            $arguments = [
                'user_profile_url' => get_permalink( $owner_id ),
            ];
            wpestate_select_email_type( $company_email, 'new_user_id_verification', $arguments );

        }
    }


    $rcapiarguments = [
        'login_name'         => $login_name,
        'first_name'         => $firstname,
        'last_name'          => $secondname,
        'user_email'         => $user_email_rcapi,
        'phone'              => $userphone,
        'skype'              => $userskype,
        'mobile'             => $usermobile,
        'live_in'            => $live_in,
        'i_speak'            => $i_speak,
        'description'        => $about_me,
        'paypal_payments_to' => $paypal_payments_to,
    ];

    rcapi_update_user( $userID, $rcapiarguments );

    die();
}
