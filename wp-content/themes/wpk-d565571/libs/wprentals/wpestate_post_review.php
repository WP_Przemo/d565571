<?php

use Wpk\d565571\Models\Owner;
use Wpk\d565571\Models\User;

/**
 * Overwrite for theme function to allow reviewing for owner and renter
 *
 * @return void
 */
function wpestate_post_review() {

    $current_user = wp_get_current_user();
    $allowed_html = [];

    $getOwnerPost = function ( $userID ) {
        $model = Owner::getForUser( $userID );

        return $model ? $model : Owner::createForUser( User::find( $userID ) );
    };

    $bookid = intval( $_POST[ 'bookid' ] );
    $userID = $current_user->ID;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }

    if ( empty( $_POST[ 'for' ] ) ) {
        exit();
    }

    $for = sanitize_text_field( $_POST[ 'for' ] );

    $userID     = $current_user->ID;
    $user_login = $current_user->user_login;
    $user_email = $current_user->user_email;
    $listing_id = intval( $_POST[ 'listing_id' ] );

    $stars   = html_entity_decode( $_POST[ 'stars' ] );
    $content = wp_kses( $_POST[ 'content' ], $allowed_html );

    $time = current_time( 'mysql' );

    if ( $for === 'property' ) {

        $postID = $listing_id;

    } else if ( $for === 'owner' ) {

        $postID = $getOwnerPost( wpse119881_get_author( $listing_id ) )->ID;

    } else if ( $for === 'renter' ) {

        $postID = $getOwnerPost( wpse119881_get_author( $bookid ) )->ID;

    } else {
        return;
    }

    $data = [
        'comment_post_ID'      => $postID,
        'comment_author'       => $user_login,
        'comment_author_email' => $user_email,
        'comment_author_url'   => '',
        'comment_content'      => $content,
        'comment_type'         => 'comment',
        'comment_parent'       => 0,
        'user_id'              => $userID,
        'comment_author_IP'    => '127.0.0.1',
        'comment_agent'        => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)',
        'comment_date'         => $time,
        'comment_approved'     => 1,
    ];

    $comment_id = wp_insert_comment( $data );
    add_comment_meta( $comment_id, 'review_stars', $stars );
    add_comment_meta( $comment_id, 'listing_id', $listing_id );

    if ( $for === 'property' ) {
        update_post_meta( $listing_id, 'review_by_' . $userID, 'has' );
    }

    add_comment_meta( $comment_id, 'for', $for );

    wpestate_calculate_property_rating( $listing_id );

}
