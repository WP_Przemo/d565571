<?php

function estate_listing_address( $post_id ) {

    $property_address    = esc_html( get_post_meta( $post_id, 'property_address', true ) );
    $property_city       = get_the_term_list( $post_id, 'property_city', '', ', ', '' );
    $property_area       = get_the_term_list( $post_id, 'property_area', '', ', ', '' );
    $property_county     = esc_html( get_post_meta( $post_id, 'property_county', true ) );
    $property_state      = esc_html( get_post_meta( $post_id, 'property_state', true ) );
    $property_zip        = esc_html( get_post_meta( $post_id, 'property_zip', true ) );
    $property_country    = esc_html( get_post_meta( $post_id, 'property_country', true ) );
    $property_country_tr = wpestate_return_country_list_translated( strtolower( $property_country ) );

    if ( $property_country_tr != '' && ! is_array( $property_country_tr ) ) {
        $property_country = $property_country_tr;
    }

    $return_string = '';

    if ( $property_address != '' ) {
        $property_address = preg_replace( '/[0-9]/', '', $property_address );

        $return_string .= '<div class="listing_detail list_detail_prop_address col-md-6"><span class="item_head">' . esc_html__( 'Address', 'wprentals' ) . ':</span> ' . $property_address . '</div>';
    }
    if ( $property_city != '' ) {
        $return_string .= '<div class="listing_detail list_detail_prop_city col-md-6"><span class="item_head">' . esc_html__( 'City', 'wprentals' ) . ':</span> ' . $property_city . '</div>';
    }
    if ( $property_area != '' ) {
        $return_string .= '<div class="listing_detail list_detail_prop_area col-md-6"><span class="item_head">' . esc_html__( 'Area', 'wprentals' ) . ':</span> ' . $property_area . '</div>';
    }
    if ( $property_county != '' ) {
        $return_string .= '<div class="listing_detail list_detail_prop_county col-md-6"><span class="item_head">' . esc_html__( 'County', 'wprentals' ) . ':</span> ' . $property_county . '</div>';
    }
    if ( $property_state != '' ) {
        $return_string .= '<div class="listing_detail list_detail_prop_state col-md-6"><span class="item_head">' . esc_html__( 'State', 'wprentals' ) . ':</span> ' . $property_state . '</div>';
    }
    if ( $property_zip != '' ) {
        $return_string .= '<div class="listing_detail list_detail_prop_zip col-md-6"><span class="item_head">' . esc_html__( 'Zip', 'wprentals' ) . ':</span> ' . $property_zip . '</div>';
    }
    if ( $property_country != '' ) {
        $return_string .= '<div class="listing_detail list_detail_prop_contry col-md-6"><span class="item_head">' . esc_html__( 'Country', 'wprentals' ) . ':</span> ' . $property_country . '</div>';
    }

    return $return_string;
}
