<?php

/**
 * Overwrite wpestate function to check booked dates via hours
 *
 * @return void
 */
function wpestate_check_for_booked_time( $book_from, $book_to, $reservation_array ) {

    $from_date      = new DateTime( $book_from );
    $from_date_unix = $from_date->getTimestamp();

    $to_date = new DateTime( $book_to );

    $to_date_unix = $to_date->getTimestamp();


    // checking booking avalability
    while ( $from_date_unix < $to_date_unix ) {
        if ( array_key_exists( $from_date_unix, $reservation_array ) ) {
            print 'stop';
            die();
        }

        //Modify by hour rather than day
        $from_date->modify( '+1 hour' );
        $from_date_unix = $from_date->getTimestamp();
    }
}