<?php

/**
 * Display star rating
 *
 * @param        $rating
 * @param string $type can be total|fields|complete
 * @param string $for
 *
 * @return string
 */
function wpestate_display_rating( $rating, $type = 'total', $for = 'property' ) {

    $rating_fields = wpk_get_review_fields( $for );

    if ( is_string( $rating ) && strlen( $rating ) > 3 ) {
        $tmp_rating = json_decode( $rating, true );
        switch ( $type ) {
            case 'total':
                $stars_total = wpestate_get_star_total_value( $tmp_rating );
                $star_rating = wpestate_render_rating_stars( $stars_total );
                break;
            case 'fields':
                $star_rating = wpestate_render_fields_rating( $tmp_rating, $rating_fields[ 'fields' ] );
                break;
            case 'complete':

                $star_rating = '<div class="property-rating">' . PHP_EOL;
                $stars_total = wpestate_get_star_total_value( $tmp_rating );
                $star_rating .= wpestate_render_rating_stars( $stars_total );
                $star_rating .= wpestate_render_fields_rating( $tmp_rating, $rating_fields[ 'fields' ] );
                $star_rating .= '</div> <!-- end .property-rating -->' . PHP_EOL;
                break;
        }

    } else {
        $star_rating = wpestate_render_rating_stars( $rating );
    }

    return $star_rating;
}

/**
 * Render rating fields
 *
 * @param $rating
 * @param $rating_fields
 *
 * @return string
 */
function wpestate_render_fields_rating( $rating, $rating_fields ) {
    $star_rating = '';
    foreach ( $rating_fields as $field_key => $field_value ) {
        $star_rating .= sprintf( '<div class="%s">', esc_attr( $field_key ) ) . PHP_EOL;
        $star_rating .= sprintf( '<span class="rating_legend">%s</span>', esc_html( $field_value ) ) . PHP_EOL;
        $star_rating .= wpestate_render_rating_stars( intval( $rating[ $field_key ] ) );
        $star_rating .= sprintf( '</div><!-- end .%s -->', esc_attr( $field_key ) ) . PHP_EOL;
    }

    return $star_rating;
}
