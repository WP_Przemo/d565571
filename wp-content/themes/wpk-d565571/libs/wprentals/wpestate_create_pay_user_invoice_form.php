<?php

use Wpk\d565571\Api\Cardcom\Client;

/**
 * Overwrite wprentals function to use "hours" string instead of "nights"
 */
function wpestate_create_pay_user_invoice_form() {
//check owner before delete
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;
    $user_email   = $current_user->user_email;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }


    $bookid     = intval( $_POST[ 'booking_id' ] );
    $the_post   = get_post( $bookid );
    $is_full    = intval( $_POST[ 'is_full' ] );
    $invoice_id = intval( $_POST[ 'invoice_id' ] );
    $bookid     = intval( $_POST[ 'booking_id' ] );

    if ( $current_user->ID != $the_post->post_author ) {
        exit( 'you don\'t have the right to see this' );
    }


    if ( $is_full != 1 ) {
        if ( ! wpestate_check_reservation_period( $bookid ) ) {
            die( '' );
        }
    }


    $currency           = esc_html( get_post_meta( $invoice_id, 'invoice_currency', true ) );
    $default_price      = get_post_meta( $invoice_id, 'default_price', true );
    $where_currency     = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
    $booking_from_date  = esc_html( get_post_meta( $bookid, 'booking_from_date', true ) );
    $property_id        = esc_html( get_post_meta( $bookid, 'booking_id', true ) );
    $booking_to_date    = esc_html( get_post_meta( $bookid, 'booking_to_date', true ) );
    $booking_guests     = floatval( get_post_meta( $bookid, 'booking_guests', true ) );
    $booking_array      = wpestate_booking_price( $booking_guests, $invoice_id, $property_id, $booking_from_date, $booking_to_date, $bookid );
    $price_per_weekeend = floatval( get_post_meta( $property_id, 'price_per_weekeend', true ) );


    if ( $booking_array[ 'numberDays' ] > 7 && $booking_array[ 'numberDays' ] < 30 ) {
        $default_price = $booking_array[ 'week_price' ];
    } else if ( $booking_array[ 'numberDays' ] > 30 ) {
        $default_price = $booking_array[ 'month_price' ];
    }

    $wp_estate_book_down           = get_post_meta( $invoice_id, 'invoice_percent', true );
    $wp_estate_book_down_fixed_fee = get_post_meta( $invoice_id, 'invoice_percent_fixed_fee', true );
    $include_expeses               = esc_html( get_option( 'wp_estate_include_expenses', '' ) );
    $invoice_price                 = floatval( get_post_meta( $invoice_id, 'item_price', true ) );

    if ( $include_expeses == 'yes' ) {
        $total_price_comp = $invoice_price;
    } else {
        $total_price_comp = $invoice_price - $booking_array[ 'city_fee' ] - $booking_array[ 'cleaning_fee' ];
    }


    $depozit                 = wpestate_calculate_deposit( $wp_estate_book_down, $wp_estate_book_down_fixed_fee, $total_price_comp );
    $balance                 = $invoice_price - $depozit;
    $price_show              = wpestate_show_price_booking_for_invoice( $default_price, $currency, $where_currency, 0, 1 );
    $price_per_weekeend_show = wpestate_show_price_booking_for_invoice( $price_per_weekeend, $currency, $where_currency, 0, 1 );
    $total_price_show        = wpestate_show_price_booking_for_invoice( $invoice_price, $currency, $where_currency, 0, 1 );
    $depozit_show            = wpestate_show_price_booking_for_invoice( $depozit, $currency, $where_currency, 0, 1 );
    $balance_show            = wpestate_show_price_booking_for_invoice( $balance, $currency, $where_currency, 0, 1 );
    $city_fee_show           = wpestate_show_price_booking_for_invoice( $booking_array[ 'city_fee' ], $currency, $where_currency, 0, 1 );
    $cleaning_fee_show       = wpestate_show_price_booking_for_invoice( $booking_array[ 'cleaning_fee' ], $currency, $where_currency, 0, 1 );
    $inter_price_show        = wpestate_show_price_booking_for_invoice( $booking_array[ 'inter_price' ], $currency, $where_currency, 0, 1 );
    $total_guest             = wpestate_show_price_booking_for_invoice( $booking_array[ 'total_extra_price_per_guest' ], $currency, $where_currency, 1, 1 );
    $guest_price             = wpestate_show_price_booking_for_invoice( $booking_array[ 'extra_price_per_guest' ], $currency, $where_currency, 1, 1 );
    $extra_price_per_guest   = wpestate_show_price_booking( $booking_array[ 'extra_price_per_guest' ], $currency, $where_currency, 1 );


// print_r($booking_array);

    $depozit_stripe = $depozit * 100;
    $details        = get_post_meta( $invoice_id, 'renting_details', true );


// strip details generation
    require_once get_template_directory() . '/libs/stripe/lib/Stripe.php';
    $stripe_secret_key      = esc_html( get_option( 'wp_estate_stripe_secret_key', '' ) );
    $stripe_publishable_key = esc_html( get_option( 'wp_estate_stripe_publishable_key', '' ) );

    $stripe = [
        "secret_key"      => $stripe_secret_key,
        "publishable_key" => $stripe_publishable_key,
    ];

    Stripe::setApiKey( $stripe[ 'secret_key' ] );


    $pages = get_pages( [
        'meta_key'   => '_wp_page_template',
        'meta_value' => 'stripecharge.php',
    ] );

    if ( $pages ) {
        $processor_link = esc_url( get_permalink( $pages[ 0 ]->ID ) );
    } else {
        $processor_link = esc_html( home_url() );
    }


    print '
<div class="create_invoice_form">
    <h3>' . esc_html__( 'Invoice INV', 'wpestate' ) . $invoice_id . '</h3>';


    print '
    <div class="invoice_table">';
    if ( $invoice_id != 0 ) {
        //     print '<div id="print_invoice" data-invoice_id="'.$invoice_id.'" ><i class="fa fa-print" aria-hidden="true" ></i></div>';
    }

    print'
        <div class="invoice_data">
            <div class="date_interval"><span class="invoice_data_legend">' . esc_html__( 'Period', 'wpestate' ) . ' : </span>
              <div style="width: 100%; float: none; clear: both;">
                                        '.esc_html__('From '). wpestate_convert_dateformat_reverse( $booking_from_date ).'
                                    </div>
                                    <div style="width: 100%; float: none;">
                                     '.esc_html__('To '). wpestate_convert_dateformat_reverse( $booking_to_date ).'
                                    </div>
            </div>
            <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'No of hours', 'wpestate' ) . ': </span>' . $booking_array[ 'numberDays' ] . '</span>
            <span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Guests', 'wpestate' ) . ': </span>' . $booking_guests . '</span>';
    if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
        print'<span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Price per Guest', 'wpestate' ) . ': </span>';
        print $extra_price_per_guest;
        print'</span>';
    } else {
        print '<span class="date_duration"><span class="invoice_data_legend">' . esc_html__( 'Price per hour', 'wpestate' ) . ': </span>';

        print $price_show;
        if ( $booking_array[ 'has_custom' ] ) {
            print ', ' . esc_html__( 'has custom price', 'wpestate' );
        }


        if ( $booking_array[ 'cover_weekend' ] ) {
            print ', ' . esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
        }

        print'</span>';


        print '</span>';

        if ( $booking_array[ 'has_custom' ] ) {
            print '<span class="invoice_data_legend">' . __( 'Price details:', 'wpestate' ) . '</span>';
            foreach ( $booking_array[ 'custom_price_array' ] as $date => $price ) {
                $day_price = wpestate_show_price_booking_for_invoice( $price, $currency, $where_currency, 1, 1 );
                print '<span class="price_custom_explained">' . __( 'on', 'wpestate' ) . ' ' . wpestate_convert_dateformat_reverse( date( "Y-m-d", $date ) ) . ' ' . __( 'price is', 'wpestate' ) . ' ' . $day_price . '</span>';
            }
        }

    }
    print '
        </div>

        <div class="invoice_details">
            <div class="invoice_row header_legend">
                <span class="inv_legend">' . esc_html__( 'Cost', 'wpestate' ) . '</span>
                <span class="inv_data">  ' . esc_html__( 'Price', 'wpestate' ) . '</span>
                <span class="inv_exp">   ' . esc_html__( 'Detail', 'wpestate' ) . '</span>
            </div>';
    $computed_total = 0;


    foreach ( $details as $detail ) {
        print'<div class="invoice_row invoice_content">
                <span class="inv_legend">  ' . $detail[ 0 ] . '</span>
                <span class="inv_data">  ' . wpestate_show_price_booking_for_invoice( $detail[ 1 ], $currency, $where_currency, 0, 1 ) . '</span>
                <span class="inv_exp">';
        if ( trim( $detail[ 0 ] ) == esc_html__( 'Security Deposit', 'wpestate' ) || trim( $detail[ 0 ] ) == esc_html__( 'Security Depozit', 'wpestate' ) ) {
            esc_html_e( '*refundable', 'wpestate' );
        }
        if ( trim( $detail[ 0 ] ) == esc_html__( 'Subtotal', 'wpestate' ) ) {
            if ( $booking_array[ 'price_per_guest_from_one' ] == 1 ) {
                print  $extra_price_per_guest . ' x ' . $booking_array[ 'count_days' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' x ' . $booking_array[ 'curent_guest_no' ] . ' ' . esc_html__( 'guests', 'wpestate' );

                if ( $booking_array[ 'price_per_guest_from_one' ] == 1 && $booking_array[ 'custom_period_quest' ] == 1 ) {
                    echo " - " . esc_html__( "period with custom price per guest", "wpestate" );
                }


            } else {
                echo $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' x ';
                if ( $booking_array[ 'cover_weekend' ] ) {
                    print esc_html__( 'has weekend price of', 'wpestate' ) . ' ' . $price_per_weekeend_show;
                } else {
                    if ( $booking_array[ 'has_custom' ] == 1 ) {
                        print esc_html__( 'custom price', 'wpestate' );
                    } else {
                        print  $price_show;
                    }
                }


            }

        }

        if ( trim( $detail[ 0 ] ) == esc_html__( 'Extra Guests', 'wpestate' ) ) {
            print $booking_array[ 'numberDays' ] . ' ' . esc_html__( 'hours', 'wpestate' ) . ' x ' . $booking_array[ 'extra_guests' ] . ' ' . esc_html__( 'extra guests', 'wpestate' );
            if ( $booking_array[ 'custom_period_quest' ] == 1 ) {
                echo esc_html__( " , period with custom price per guest", "wpestate" );
            }
        }

        if ( isset( $detail[ 2 ] ) ) {
            echo $detail[ 2 ];
        }


        print'
                                    </span>
            </div>';
    }

    print '
            <div class="invoice_row invoice_total total_inv_span total_invoice_for_payment">
                <span class="inv_legend"><strong>' . esc_html__( 'Total', 'wpestate' ) . '</strong></span>
                <span class="inv_data" id="total_amm" data-total="' . $invoice_price . '">' . $total_price_show . '</span></br>

                <span class="inv_legend">' . esc_html__( 'Reservation Fee Required', 'wpestate' ) . ':</span> <span class="inv_depozit depozit_show" data-value="' . $depozit . '"> ' . $depozit_show . '</span></br>
                <span class="inv_legend">' . esc_html__( 'Balance Owing', 'wpestate' ) . ':</span> <span class="inv_depozit balance_show"  data-value="' . $balance . '">' . $balance_show . '</span>
            </div>
        </div>';

    $is_paypal_live            = esc_html( get_option( 'wp_estate_enable_paypal', '' ) );
    $is_stripe_live            = esc_html( get_option( 'wp_estate_enable_stripe', '' ) );
    $submission_curency_status = esc_html( get_option( 'wp_estate_submission_curency', '' ) );

    if ( $is_full != 1 ) {
        if ( $balance > 0 ) {
            print '<div class="invoice_pay_status_note">' . __( 'You are paying only the deposit required to confirm the booking:', 'wpestate' ) . ' ' . $depozit_show . '</div>';
            print '<div class="invoice_pay_status_note">' . __( 'You will need to pay the remain balance until the first day of your period!', 'wpestate' ) . '</div>';

        }

    } else {
        if ( $balance > 0 ) {
            $depozit_stripe = $balance * 100;
            $depozit        = $balance;
            print '<div class="invoice_pay_status_note">' . __( 'You are paying the remaining balance of your invoice:', 'wpestate' ) . ' ' . $balance_show . '</div><input type="hidden" id="is_full_pay" value="' . $balance . '">';
        }
    }


    print '<span class="pay_notice_booking">' . esc_html__( 'Pay Deposit & Confirm Reservation', 'wpestate' ) . '</span>';

    if ( Client::hasFilledSettings() ) {
        echo '<button class="wpk-cardcom-payment" data-payment-action="booking" data-propid="' . $property_id . '" data-bookid="' . $bookid . '" data-invoiceid="' . $invoice_id . '">' . esc_html__( 'Pay with Cardcom', 'wpestate' ) . '</button>';
    }

    if ( $is_stripe_live == 'yes' ) {
        print '
        <form action="' . $processor_link . '" method="post" class="booking_form_stripe">
            <script src="https://checkout.stripe.com/checkout.js"
                    class="stripe-button"
                    data-key="' . $stripe[ 'publishable_key' ] . '"
                    data-amount="' . $depozit_stripe . '"
                    data-zip-code="true"
                    data-locale="auto"
                    data-email="' . $user_email . '"
                    data-currency="' . $submission_curency_status . '"
                    data-label="' . esc_html__( 'Pay with Credit Card', 'wpestate' ) . '"
                    data-description="Reservation Payment">
            </script>
            <input type="hidden" name="booking_id" value="' . $bookid . '">
            <input type="hidden" name="invoice_id" value="' . $invoice_id . '">
            <input type="hidden" name="userID" value="' . $userID . '">
            <input type="hidden" name="depozit" value="' . $depozit_stripe . '">
        </form>';
    }
    if ( $is_paypal_live == 'yes' ) {
        print '<span id="paypal_booking" data-propid="' . $property_id . '" data-deposit="' . $depozit . '" data-bookid="' . $bookid . '" data-invoiceid="' . $invoice_id . '">' . esc_html__( 'Pay with Paypal', 'wpestate' ) . '</span>';
    }
    $enable_direct_pay = esc_html( get_option( 'wp_estate_enable_direct_pay', '' ) );

    if ( $enable_direct_pay == 'yes' ) {
        print '<span id="direct_pay_booking" data-propid="' . $property_id . '" data-bookid="' . $bookid . '" data-invoiceid="' . $invoice_id . '">' . esc_html__( 'Wire Transfer', 'wpestate' ) . '</span>';
    }
    print'
    </div>


</div>';
    die();
}
