<?php

function wpestate_ajax_add_booking() {

    //  check_ajax_referer( 'booking_ajax_nonce','security');
    $current_user  = wp_get_current_user();
    $allowded_html = [];
    $userID        = $current_user->ID;

    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }


    $from    = $current_user->user_login;
    $comment = '';
    $status  = 'pending';

    if ( isset( $_POST[ 'comment' ] ) ) {
        $comment = wp_kses( $_POST[ 'comment' ], $allowded_html );
    }

    $booking_guest_no = 0;
    if ( isset( $_POST[ 'booking_guest_no' ] ) ) {
        $booking_guest_no = intval( $_POST[ 'booking_guest_no' ] );
    }

    if ( isset ( $_POST[ 'confirmed' ] ) ) {
        if ( intval( $_POST[ 'confirmed' ] ) == 1 ) {
            $status = 'confirmed';
        }
    }


    $property_id     = intval( $_POST[ 'listing_edit' ] );
    $instant_booking = floatval( get_post_meta( $property_id, 'instant_booking', true ) );
    $owner_id        = wpsestate_get_author( $property_id );
    $fromdate        = wp_kses( $_POST[ 'fromdate' ], $allowded_html );
    $to_date         = wp_kses( $_POST[ 'todate' ], $allowded_html );

    $start_hour = isset( $_POST[ 'start_hour' ] ) ? $_POST[ 'start_hour' ] : '00:00';
    $end_hour   = isset( $_POST[ 'end_hour' ] ) ? $_POST[ 'end_hour' ] : '00:00';

    try {

        $fromdate = new \DateTime( "$fromdate $start_hour" );
        $to_date  = new \DateTime( "$to_date $end_hour" );

    } catch ( \Exception $e ) {
        die();
    }

    \Wpk\d565571\Models\Booking::createForProperty( [
        'property'      => \Wpk\d565571\Models\Property::find( $property_id ),
        'start_date'    => $fromdate,
        'end_date'      => $to_date,
        'comment'       => $comment,
        'confirmed'     => true,
        'extra_options' => wp_kses( $_POST[ 'extra_options' ], $allowded_html ),
    ] );


    die();
}
