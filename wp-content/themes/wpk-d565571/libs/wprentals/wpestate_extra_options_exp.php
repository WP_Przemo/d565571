<?php

/**
 * Overwrite for theme function to display "Per Hour" instead of "Per Night"
 *
 * @param int $extra_id
 *
 * @return string
 */
function wpestate_extra_options_exp( $extra_id ) {

    $options_array = [
        0 => esc_html__( 'Single Fee', 'wpestate' ),
        1 => esc_html__( 'Per Hour', 'wpestate' ),
        2 => esc_html__( 'Per Guest', 'wpestate' ),
        3 => esc_html__( 'Per Night per Guest', 'wpestate' ),
    ];

    return $options_array[ $extra_id ];
}