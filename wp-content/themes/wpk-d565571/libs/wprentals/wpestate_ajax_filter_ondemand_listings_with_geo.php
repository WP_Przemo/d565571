<?php

use Wpk\d565571\Helpers\WpRentals\Order;

/**
 * Overwrite for theme function to support proper ordering
 *
 * @return void
 */
function wpestate_ajax_filter_ondemand_listings_with_geo() {
    global $post;
    global $options;
    global $show_compare_only;
    global $currency;
    global $where_currency;
    global $listing_type;
    global $property_unit_slider;
    global $curent_fav;
    global $full_page;
    global $guest;
    global $guest_no;
    global $book_from;
    global $book_to;

    $property_unit_slider = esc_html( get_option( 'wp_estate_prop_list_slider', '' ) );
    $listing_type         = get_option( 'wp_estate_listing_unit_type', '' );
    $show_compare_only    = 'no';
    $current_user         = wp_get_current_user();
    $userID               = $current_user->ID;
    $user_option          = 'favorites' . $userID;
    $curent_fav           = get_option( $user_option );
    $currency             = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
    $where_currency       = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
    $area_array           = '';
    $city_array           = '';
    $action_array         = '';
    $categ_array          = '';

    $options      = wpestate_page_details( intval( $_POST[ 'postid' ] ) );
    $allowed_html = [];

    if ( $options[ 'content_class' ] == "col-md-12" ) {
        $full_page = 1;
    }
    if ( basename( get_page_template_slug( intval( $_POST[ 'postid' ] ) ) ) === 'property_list_half.php' ) {
        $full_page = 0;
    }
    $property_list_type_status = esc_html( get_option( 'wp_estate_property_list_type_adv', '' ) );
    if ( basename( get_page_template_slug( intval( $_POST[ 'postid' ] ) ) ) === 'advanced_search_results.php' && $property_list_type_status == 2 ) {
        $full_page = 0;
    }
    //////////////////////////////////////////////////////////////////////////////////////
    ///// category filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'category_values' ] ) && trim( $_POST[ 'category_values' ] ) != 'all' ) {
        $taxcateg_include = sanitize_title( wp_kses( $_POST[ 'category_values' ], $allowed_html ) );
        $categ_array      = [
            'taxonomy' => 'property_category',
            'field'    => 'slug',
            'terms'    => $taxcateg_include,
        ];
    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// action  filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( ( isset( $_POST[ 'action_values' ] ) && trim( $_POST[ 'action_values' ] ) != 'all' ) ) {
        $taxaction_include = sanitize_title( wp_kses( $_POST[ 'action_values' ], $allowed_html ) );
        $action_array      = [
            'taxonomy' => 'property_action_category',
            'field'    => 'slug',
            'terms'    => $taxaction_include,
        ];
    }


    $meta_query = $rooms = $baths = $price = [];
    if ( isset( $_POST[ 'advanced_rooms' ] ) && is_numeric( $_POST[ 'advanced_rooms' ] ) && intval( $_POST[ 'advanced_rooms' ] != 0 ) ) {
        $rooms[ 'key' ]   = 'property_rooms';
        $rooms[ 'value' ] = floatval( $_POST[ 'advanced_rooms' ] );
        $meta_query[]     = $rooms;
    }

    if ( isset( $_POST[ 'advanced_bath' ] ) && is_numeric( $_POST[ 'advanced_bath' ] ) && intval( $_POST[ 'advanced_bath' ] != 0 ) ) {
        $baths[ 'key' ]   = 'property_bathrooms';
        $baths[ 'value' ] = floatval( $_POST[ 'advanced_bath' ] );
        $meta_query[]     = $baths;
    }


    if ( isset( $_POST[ 'advanced_beds' ] ) && is_numeric( $_POST[ 'advanced_beds' ] ) && intval( $_POST[ 'advanced_beds' ] != 0 ) ) {
        $beds[ 'key' ]   = 'property_bedrooms';
        $beds[ 'value' ] = floatval( $_POST[ 'advanced_beds' ] );
        $meta_query[]    = $beds;
    }

    if ( isset( $_POST[ 'guest_no' ] ) && is_numeric( $_POST[ 'guest_no' ] ) && intval( $_POST[ 'guest_no' ] ) != 0 ) {
        $guest[ 'key' ]     = 'guest_no';
        $guest[ 'value' ]   = floatval( $_POST[ 'guest_no' ] );
        $guest[ 'type' ]    = 'numeric';
        $guest[ 'compare' ] = '>=';
        $meta_query[]       = $guest;
        $guest              = $guest_no = floatval( $_POST[ 'guest_no' ] );
    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// chekcers
    //////////////////////////////////////////////////////////////////////////////////////
    $all_checkers = explode( ",", $_POST[ 'all_checkers' ] );

    foreach ( $all_checkers as $cheker ) {
        if ( $cheker != '' ) {
            $check_array              = [];
            $check_array[ 'key' ]     = $cheker;
            $check_array[ 'value' ]   = 1;
            $check_array[ 'compare' ] = 'CHAR';
            $meta_query[]             = $check_array;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// price filters
    //////////////////////////////////////////////////////////////////////////////////////
    $price_low = '';
    if ( isset( $_POST[ 'price_low' ] ) ) {
        $price_low = intval( $_POST[ 'price_low' ] );
    }

    $price_max = '';
    if ( isset( $_POST[ 'price_max' ] ) && is_numeric( $_POST[ 'price_max' ] ) ) {
        $price_max      = intval( $_POST[ 'price_max' ] );
        $price[ 'key' ] = 'property_price';

        $custom_fields = get_option( 'wp_estate_multi_curr', true );
        if ( ! empty( $custom_fields ) && isset( $_COOKIE[ 'my_custom_curr' ] ) && isset( $_COOKIE[ 'my_custom_curr_pos' ] ) && isset( $_COOKIE[ 'my_custom_curr_symbol' ] ) && $_COOKIE[ 'my_custom_curr_pos' ] != - 1 ) {
            $i = intval( $_COOKIE[ 'my_custom_curr_pos' ] );
            if ( $price_low != 0 ) {
                $price_low = $price_low / $custom_fields[ $i ][ 2 ];
            }
            if ( $price_max != 0 ) {
                $price_max = $price_max / $custom_fields[ $i ][ 2 ];
            }

        }

        $price[ 'value' ]   = [ $price_low, $price_max ];
        $price[ 'type' ]    = 'numeric';
        $price[ 'compare' ] = 'BETWEEN';
        $meta_query[]       = $price;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// calendar filters
    //////////////////////////////////////////////////////////////////////////////////////

    $allowed_html = [];
    $book_from    = '';
    $book_to      = '';
    if ( isset( $_POST[ 'check_in' ] ) ) {
        $book_from = sanitize_text_field( wp_kses( $_POST[ 'check_in' ], $allowed_html ) );
    }
    if ( isset( $_POST[ 'check_out' ] ) ) {
        $book_to = sanitize_text_field( wp_kses( $_POST[ 'check_out' ], $allowed_html ) );
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// order details
    //////////////////////////////////////////////////////////////////////////////////////
    $meta_order      = 'prop_featured';
    $meta_directions = 'DESC';
    $order           = null;

    if ( isset( $_POST[ 'order' ] ) ) {
        $order = (int) wp_kses( $_POST[ 'order' ], $allowed_html );
        switch ( $order ) {
            case 1:
                $meta_order      = 'property_price';
                $meta_directions = 'DESC';
                break;
            case 2:
                $meta_order      = 'property_price';
                $meta_directions = 'ASC';
                break;
            case 3:
                $meta_order      = 'property_size';
                $meta_directions = 'DESC';
                break;
            case 4:
                $meta_order      = 'property_size';
                $meta_directions = 'ASC';
                break;
            case 5:
                $meta_order      = 'property_bedrooms';
                $meta_directions = 'DESC';
                break;
            case 6:
                $meta_order      = 'property_bedrooms';
                $meta_directions = 'ASC';
                break;
        }
    }

    $paged   = intval( $_POST[ 'newpage' ] );
    $prop_no = intval( get_option( 'wp_estate_prop_no', '' ) );


    $ne_lat = floatval( $_POST[ 'ne_lat' ] );
    $ne_lng = floatval( $_POST[ 'ne_lng' ] );
    $sw_lat = floatval( $_POST[ 'sw_lat' ] );
    $sw_lng = floatval( $_POST[ 'sw_lng' ] );


    $long_array = [];
    $lat_array  = [];

    $meta_query[ 'relation' ] = 'AND';


    $min_lat = $sw_lat;
    $max_lat = $ne_lat;

    if ( $min_lat > $max_lat ) {
        $min_lat = $ne_lat;
        $max_lat = $sw_lat;
    }


    $min_lng = $sw_lng;
    $max_lng = $ne_lng;

    if ( $min_lng > $max_lng ) {
        $min_lng = $ne_lng;
        $max_lng = $sw_lng;
    }


    $long_array[ 'key' ]     = 'property_longitude';
    $long_array[ 'value' ]   = [ $min_lng, $max_lng ];
    $long_array[ 'type' ]    = 'DECIMAL';
    $long_array[ 'compare' ] = 'BETWEEN';
    $meta_query[]            = $long_array;


    $lat_array[ 'key' ]     = 'property_latitude';
    $lat_array[ 'value' ]   = [ $min_lat, $max_lat ];
    $lat_array[ 'type' ]    = 'DECIMAL';
    $lat_array[ 'compare' ] = 'BETWEEN';
    $meta_query[]           = $lat_array;


    ////////////////////////////////////////////////////////////////////////////
    // if we have check in and check out dates we need to double loop
    ////////////////////////////////////////////////////////////////////////////
    if ( $book_from != '' && $book_from != '' ) {
        $args  = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'posts_per_page'         => '-1',
            'meta_key'               => $meta_order,
            'order'                  => $meta_directions,
            'orderby'                => 'meta_value',
            'meta_query'             => $meta_query,
            'tax_query'              => [
                'relation' => 'AND',
                $categ_array,
                $action_array,
                $city_array,
                $area_array,
            ],
        ];
        $args1 = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'posts_per_page'         => '-1',
            'meta_key'               => $meta_order,
            'order'                  => $meta_directions,
            'orderby'                => 'meta_value',
            'meta_query'             => $meta_query,
            'tax_query'              => [
                'relation' => 'AND',
                $categ_array,
                $action_array,
                $city_array,
                $area_array,
            ],
        ];

        add_filter( 'get_meta_sql', 'cast_decimal_precision' );
        $prop_selection = new WP_Query( $args );
        remove_filter( 'get_meta_sql', 'cast_decimal_precision' );

        $right_array   = [];
        $right_array[] = 0;
        while ( $prop_selection->have_posts() ): $prop_selection->the_post();
            // print '</br>we check '.$post->ID.'</br>';
            if ( wpestate_check_booking_valability( $book_from, $book_to, $post->ID ) ) {
                $right_array[] = $post->ID;
            }
        endwhile;

        wp_reset_postdata();
        $args = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'meta_key'               => $meta_order,
            'order'                  => $meta_directions,
            'orderby'                => 'meta_value',
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'paged'                  => $paged,
            'posts_per_page'         => $prop_no,
            'post__in'               => $right_array,
        ];


        add_filter( 'posts_orderby', 'wpestate_my_order' );
        $prop_selection = new WP_Query( $args );
        remove_filter( 'posts_orderby', 'wpestate_my_order' );
    } else {
        $args = [
            'cache_results'          => false,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'post_type'              => 'estate_property',
            'post_status'            => 'publish',
            'paged'                  => $paged,
            'posts_per_page'         => $prop_no,
            'meta_key'               => $meta_order,
            'order'                  => $meta_directions,
            'orderby'                => 'meta_value',
            'meta_query'             => $meta_query,
            'tax_query'              => [
                'relation' => 'AND',
                $categ_array,
                $action_array,
                $city_array,
                $area_array,
            ],
        ];
        add_filter( 'get_meta_sql', 'cast_decimal_precision' );
        add_filter( 'posts_orderby', 'wpestate_my_order' );
        $prop_selection = new WP_Query( $args );
        remove_filter( 'posts_orderby', 'wpestate_my_order' );
        remove_filter( 'get_meta_sql', 'cast_decimal_precision' );
    }

    //Custom ordering by distance
    if ( $order == 7 ) {

        $userLat = (float) $_POST[ 'user_lat' ];
        $userLng = (float) $_POST[ 'user_lng' ];

        $prop_selection = Order::byUserDistance( $userLat, $userLng, $paged, $prop_selection );
    } else if ( in_array( $order, [ 1, 2 ] ) ) {

        $prop_selection = Order::byPrice( $paged, $order === 1 ? 'DESC' : 'ASC', $prop_selection );

    }

    $counter        = 0;
    $compare_submit = wpestate_get_template_link( 'compare_listings.php' );
    $markers        = [];
    $return_string  = '';
    ob_start();

    print '<span id="scrollhere"></span>';


    $listing_unit_style_half = get_option( 'wp_estate_listing_unit_style_half', '' );


    if ( $prop_selection->have_posts() ) {
        while ( $prop_selection->have_posts() ): $prop_selection->the_post();
            if ( $listing_unit_style_half == 1 &&
                 ( basename( get_page_template_slug( intval( $_POST[ 'postid' ] ) ) ) == 'property_list_half.php' ||
                   ( basename( get_page_template_slug( intval( $_POST[ 'postid' ] ) ) ) == 'advanced_search_results.php' && $property_list_type_status == 2 ) )
            ) {
                get_template_part( 'templates/property_unit_wide' );
            } else {
                get_template_part( 'templates/property_unit' );
            }
            $markers[] = wpestate_pin_unit_creation( get_the_ID(), $currency, $where_currency, $counter );
        endwhile;
        kriesi_pagination_ajax( $prop_selection->max_num_pages, $range = 2, $paged, 'pagination_ajax_search_home' );
    } else {
        print '<span class="no_results">' . esc_html__( "We didn't find any results", "wpestate" ) . '</>';
    }
    // print '</div>';
    $templates = ob_get_contents();
    ob_end_clean();
    //get_page_template_slug(intval($_POST['postid'])).'/'.
    $return_string .= '<div class="half_map_results">' . $prop_selection->found_posts . ' ' . esc_html__( ' Results found!', 'wpestate' ) . '</div>';
    $return_string .= $templates;
    echo json_encode( [
        'added'     => true,
        'arguments' => json_encode( $args ),
        'arg1'      => json_encode( $args1 ),
        'markers'   => json_encode( $markers ),
        'response'  => $return_string,
    ] );
    die();
}
