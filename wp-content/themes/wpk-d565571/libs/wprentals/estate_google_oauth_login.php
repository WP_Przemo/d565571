<?php

/**
 * Overwrite for estate function to save user token in his meta
 *
 * @return void
 */
function estate_google_oauth_login( $get_vars ) {
    // set_include_path( get_include_path() . PATH_SEPARATOR . get_template_directory().'/libs/resources');
    $allowed_html = [];

    //$google_client_id = '789776065323-47hjb2931cl5ag9881gcpfcn0qq9e72n.apps.googleusercontent.com';
    //$google_client_secret = 'q9kSQCz2Pif1e1wLDeaYRoUl';


    $gClient = \Wpk\d565571\Api\Google\Client::get();

    $propertyID = null;

    if ( ! empty( $_GET[ 'state' ] ) ) {
        list( $type, $ID ) = explode( '|', $_GET[ 'state' ] );

        if ( $type === 'property' ) {
            $propertyID = $ID;
        }

    }

    $logged_in = is_user_logged_in();

    $gClient->setApplicationName( 'Login to WpRentals' );

    $google_oauthV2 = new Google_Service_Oauth2( $gClient );

    if ( isset( $_GET[ 'code' ] ) ) {
        $code = sanitize_text_field( wp_kses( $_GET[ 'code' ], $allowed_html ) );
        $gClient->authenticate( $code );
    }


    if ( $token = $gClient->getAccessToken() ) {

        $allowed_html  = [];
        $dashboard_url = wpestate_get_template_link( 'user_dashboard_profile.php' );

        if ( ! $logged_in ) {
            $user      = $google_oauthV2->userinfo->get();
            $full_name = wp_kses( $user[ 'name' ], $allowed_html );
            $email     = wp_kses( $user[ 'email' ], $allowed_html );

            $user_id   = $user[ 'id' ];
            $full_name = wp_kses( $user[ 'name' ], $allowed_html );
            $email     = wp_kses( $user[ 'email' ], $allowed_html );
            $full_name = str_replace( ' ', '.', $full_name );

            $first_name = $last_name = '';
            if ( isset( $user[ 'familyName' ] ) ) {
                $last_name = $user[ 'familyName' ];
            }
            if ( isset( $user[ 'givenName' ] ) ) {
                $first_name = $user[ 'givenName' ];
            }

            if ( empty( $full_name ) ) {
                $full_name = $email;
            }

            wpestate_register_user_via_google( $email, $full_name, $user_id, $first_name, $last_name );
            $wordpress_user_id = username_exists( $full_name );
            wp_set_password( $code, $wordpress_user_id );


            $info                    = [];
            $info[ 'user_login' ]    = $full_name;
            $info[ 'user_password' ] = $code;
            $info[ 'remember' ]      = true;
            $user_signon             = wp_signon( $info, true );
        } else {
            $wordpress_user_id = get_current_user_id();
        }

        //WPK d565571 Save user access token in meta for further use
        if ( $token && $propertyID ) {


            $dashboard_url = get_permalink( \Wpk\d565571\Utility::getPageByTemplate( 'user_dashboard_edit_listing.php' ) );

            $dashboard_url = add_query_arg( [
                'listing_edit' => $propertyID,
                'action'       => 'calendar',
            ], $dashboard_url );

            update_post_meta( $propertyID, 'google_access_token', $token );
        }

        if ( $logged_in ) {
            wp_redirect( $dashboard_url );
            exit();
        }
        //WPK d565571 end


        if ( is_wp_error( $user_signon ) ) {
            wp_redirect( home_url() );
            exit();
        } else {
            wpestate_update_old_users( $user_signon->ID );
            wpestate_calculate_new_mess();
            wp_redirect( home_url() );
            exit();
        }

    }
}
