<?php

/**
 * Overwrite for theme function in order to include our hook for booking activation
 *
 * @param     $booking_id
 * @param     $invoice_id
 * @param     $userId
 * @param     $depozit
 * @param     $userEmail
 * @param int $isStripe
 *
 * @return void
 */
function wpestate_booking_mark_confirmed( $booking_id, $invoice_id, $userId, $depozit, $userEmail, $isStripe = 0 ) {

    $booking_details         = [];
    $booking_status          = get_post_meta( $booking_id, 'booking_status', true );
    $is_full_instant_booking = get_post_meta( $booking_id, 'is_full_instant', true );
    $is_full_instant_invoice = get_post_meta( $invoice_id, 'is_full_instant', true );


    if ( $booking_status != 'confirmed' ) {
        update_post_meta( $booking_id, 'booking_status', 'confirmed' );
        $booking_details[ 'booking_status' ] = 'confirmed';

        do_action( 'wpk/d5657781/bookingConfirmed', \Wpk\d565571\Models\Booking::find( $booking_id ) );
    } else {
        // confirmed_paid_full
        update_post_meta( $booking_id, 'booking_status_full', 'confirmed' );
        $booking_details[ 'booking_status_full' ] = 'confirmed';
        $booking_details[ 'balance' ]             = 0;
        update_post_meta( $booking_id, 'balance', 0 );
    }

    if ( $is_full_instant_booking == 1 ) {
        update_post_meta( $booking_id, 'booking_status_full', 'confirmed' );
        $booking_details[ 'booking_status_full' ] = 'confirmed';
        $booking_details[ 'balance' ]             = 0;
        update_post_meta( $booking_id, 'balance', 0 );
    }

    if ( $isStripe == 1 ) {
        $depozit = ( $depozit / 100 );
    }


    // reservation array
    $curent_listng_id = get_post_meta( $booking_id, 'booking_id', true );

    wpestate_get_booking_dates( $curent_listng_id );


    $invoice_details = [];
    $invoice_status  = get_post_meta( $invoice_id, 'invoice_status', true );


    if ( $invoice_status != 'confirmed' ) {
        update_post_meta( $invoice_id, 'depozit_paid', $depozit );
        update_post_meta( $invoice_id, 'invoice_status', 'confirmed' );
        $invoice_details[ 'invoice_status' ] = 'confirmed';
    } else {
        update_post_meta( $invoice_id, 'invoice_status_full', 'confirmed' );
        $invoice_details[ 'invoice_status_full' ] = 'confirmed';
        $invoice_details[ 'balance' ]             = 0;
        update_post_meta( $invoice_id, 'balance', 0 );
    }


    if ( $is_full_instant_invoice == 1 ) {
        update_post_meta( $invoice_id, 'invoice_status_full', 'confirmed' );
        $invoice_details[ 'invoice_status_full' ] = 'confirmed';
        $invoice_details[ 'balance' ]             = 0;
        update_post_meta( $invoice_id, 'balance', 0 );
    }

    // 100% deposit
    $wp_estate_book_down = floatval( get_post_meta( $invoice_id, 'invoice_percent', true ) );
    $invoice_price       = floatval( get_post_meta( $invoice_id, 'item_price', true ) );

    if ( $wp_estate_book_down == 100 ) {
        update_post_meta( $booking_id, 'booking_status_full', 'confirmed' );
        $booking_details[ 'booking_status_full' ] = 'confirmed';
        $booking_details[ 'balance' ]             = 0;
        update_post_meta( $booking_id, 'balance', 0 );

        update_post_meta( $invoice_id, 'invoice_status_full', 'confirmed' );
        $invoice_details[ 'invoice_status_full' ] = 'confirmed';
        $invoice_details[ 'balance' ]             = 0;
        update_post_meta( $invoice_id, 'balance', 0 );
    }
    // end 100% deposit

    $booking_author = get_userdata( get_post_field( 'post_author', $booking_id ) );

    wpestate_send_booking_email( "bookingconfirmeduser", $booking_author->user_email, $curent_listng_id );

    $receiver_id    = wpsestate_get_author( $invoice_id );
    $receiver_email = get_the_author_meta( 'user_email', $receiver_id );
    $receiver_name  = get_the_author_meta( 'user_login', $receiver_id );
    wpestate_send_booking_email( "bookingconfirmed", $receiver_email );


    // add messages to inbox
    $subject     = esc_html__( 'Booking Confirmation', 'wpestate' );
    $description = esc_html__( 'A booking was confirmed', 'wpestate' );
    wpestate_add_to_inbox( $userId, $userId, $receiver_id, $subject, $description, 1 );


    // rcapi code
    $booking_guests    = floatval( get_post_meta( $booking_id, 'booking_guests', true ) );
    $booking_from_date = esc_html( get_post_meta( $booking_id, 'booking_from_date', true ) );
    $booking_to_date   = esc_html( get_post_meta( $booking_id, 'booking_to_date', true ) );
    $booking_prop      = esc_html( get_post_meta( $booking_id, 'booking_id', true ) );
    $booking_array     = wpestate_booking_price( $booking_guests, $invoice_id, $curent_listng_id, $booking_from_date, $booking_to_date );


    $rcapi_booking_id = get_post_meta( $booking_id, 'rcapi_booking_id', true );
    rcapi_edit_booking( $booking_id, $rcapi_booking_id, $booking_details );

    $rcapi_invoice_id = get_post_meta( $invoice_id, 'rcapi_invoice_id', true );
    rcapi_edit_invoice( $invoice_id, $rcapi_invoice_id, $invoice_details );
    // rcapi_update_invoice_as_paid($booking_id,$invoice_id,$booking_array);

}
