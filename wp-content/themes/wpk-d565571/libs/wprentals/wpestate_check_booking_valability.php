<?php

use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;

/**
 * Overwrite for theme function that checks "valability" (typo comes from the theme developers)
 *
 * @param string $bookFrom
 * @param string $bookTo
 * @param int    $listing_id
 *
 * @return bool
 */
function wpestate_check_booking_valability( $bookFrom, $bookTo, $listing_id ) {

    if ( empty( $bookFrom ) || empty( $bookTo ) ) {
        return true;
    }

    $property = Property::find( $listing_id );
    $bookTo   = new DateTime( $bookTo );
    $bookFrom = new DateTime( $bookFrom );

    return Booking::isAvailable( $property, $bookFrom, $bookTo );

}
