<?php

/**
 * Overwrite for theme function to add filtering by property taxonomies
 *
 * @return void`
 */
function wpestate_ajax_filter_listings_search() {
    global $post;
    global $current_user;
    global $options;
    global $show_compare_only;
    global $currency;
    global $where_currency;
    global $full_page;
    global $listing_type;
    global $property_unit_slider;

    $property_unit_slider = esc_html( get_option( 'wp_estate_prop_list_slider', '' ) );
    $listing_type         = get_option( 'wp_estate_listing_unit_type', '' );
    $full_page            = 0;
    $options              = wpestate_page_details( intval( $_POST[ 'postid' ] ) );

    if ( $options[ 'content_class' ] == "col-md-12" ) {
        $full_page = 1;
    }

    $show_compare_only = 'no';
    $current_user      = wp_get_current_user();
    $userID            = $current_user->ID;
    $user_option       = 'favorites' . $userID;
    $curent_fav        = get_option( $user_option );
    $currency          = esc_html( get_option( 'wp_estate_currency_label_main', '' ) );
    $where_currency    = esc_html( get_option( 'wp_estate_where_currency_symbol', '' ) );
    $area_array        =
    $city_array =
    $action_array = '';
    $categ_array       = '';
    $allowed_html      = [];
    //////////////////////////////////////////////////////////////////////////////////////
    ///// city filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'city' ] ) and trim( $_POST[ 'city' ] ) != 'all' and trim( $_POST[ 'city' ] ) != '' ) {
        $taxcity[]  = sanitize_title( wp_kses( $_POST[ 'city' ], $allowed_html ) );
        $city_array = [
            'taxonomy' => 'property_city',
            'field'    => 'slug',
            'terms'    => $taxcity,
        ];
    }


    //////////////////////////////////////////////////////////////////////////////////////
    ///// area filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'area' ] ) && trim( $_POST[ 'area' ] ) != 'all' && trim( $_POST[ 'area' ] ) != '' ) {
        $taxarea[]  = sanitize_title( wp_kses( $_POST[ 'area' ], $allowed_html ) );
        $area_array = [
            'taxonomy' => 'property_area',
            'field'    => 'slug',
            'terms'    => $taxarea,
        ];
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// action  filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( ( ! empty( $_POST[ 'action_values' ] ) && trim( $_POST[ 'action_values' ] ) != 'all' ) ) {
        $taxaction_include = sanitize_title( wp_kses( $_POST[ 'action_values' ], $allowed_html ) );
        $action_array      = [
            'taxonomy' => 'property_action_category',
            'field'    => 'slug',
            'terms'    => $taxaction_include,
        ];
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ///// category filters
    //////////////////////////////////////////////////////////////////////////////////////

    if ( isset( $_POST[ 'category_values' ] ) && trim( $_POST[ 'category_values' ] ) != 'all' ) {
        $taxcateg_include = sanitize_title( wp_kses( $_POST[ 'category_values' ], $allowed_html ) );
        $categ_array      = [
            'taxonomy' => 'property_category',
            'field'    => 'slug',
            'terms'    => $taxcateg_include,
        ];
    }


    $meta_query  = [];
    $guest_array = [];
    if ( isset( $_POST[ 'guest_no' ] ) && is_numeric( $_POST[ 'guest_no' ] ) && intval( $_POST[ 'guest_no' ] ) != 0 ) {
        $guest_no                 = intval( $_POST[ 'guest_no' ] );
        $guest_array[ 'key' ]     = 'guest_no';
        $guest_array[ 'value' ]   = $guest_no;
        $guest_array[ 'type' ]    = 'numeric';
        $guest_array[ 'compare' ] = '>=';
        $meta_query[]             = $guest_array;
    }


    $country_array = [];
    if ( isset( $_POST[ 'country' ] ) && $_POST[ 'country' ] != '' ) {
        $country                    = sanitize_text_field( wp_kses( $_POST[ 'country' ], $allowed_html ) );
        $country                    = str_replace( '-', ' ', $country );
        $country_array[ 'key' ]     = 'property_country';
        $country_array[ 'value' ]   = $country;
        $country_array[ 'type' ]    = 'CHAR';
        $country_array[ 'compare' ] = 'LIKE';
        $meta_query[]               = $country_array;
    }


    $allowed_html = [];
    $book_from    = '';
    $book_to      = '';
    if ( isset( $_POST[ 'check_in' ] ) ) {
        $book_from = sanitize_text_field( wp_kses( $_POST[ 'check_in' ], $allowed_html ) );
    }
    if ( isset( $_POST[ 'check_out' ] ) ) {
        $book_to = sanitize_text_field( wp_kses( $_POST[ 'check_out' ], $allowed_html ) );
    }

    $paged   = intval( $_POST[ 'newpage' ] );
    $prop_no = intval( get_option( 'wp_estate_prop_no', '' ) );
    $args    = [
        'post_type'      => 'estate_property',
        'post_status'    => 'publish',
        'posts_per_page' => - 1,
        'meta_query'     => $meta_query,
        'tax_query'      => [
            'relation' => 'AND',
            $categ_array,
            $action_array,
            $city_array,
            $area_array,
        ],
    ];


    $prop_selection = new WP_Query( $args );

    $counter        = 0;
    $compare_submit = wpestate_get_template_link( 'compare_listings.php' );

    if ( $prop_selection->have_posts() ) {
        while ( $prop_selection->have_posts() ): $prop_selection->the_post();
            if ( wpestate_check_booking_valability( $book_from, $book_to, $post->ID ) ) {
                get_template_part( 'templates/property_unit' );
            }
        endwhile;

        //  kriesi_pagination_ajax($prop_selection->max_num_pages, $range =2,$paged,'pagination_ajax_search');
    } else {
        print '<span class="no_results">' . esc_html__( "We didn't find any results", "wpestate" ) . '</>';
    }
    die();
}
