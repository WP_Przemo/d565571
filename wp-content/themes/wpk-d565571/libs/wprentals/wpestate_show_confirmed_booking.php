<?php

function wpestate_show_confirmed_booking() {

    //check owner before delete
    $current_user = wp_get_current_user();
    $userID       = $current_user->ID;


    if ( ! is_user_logged_in() ) {
        exit( 'ko' );
    }
    if ( $userID === 0 ) {
        exit( 'out pls' );
    }


    $userID     = $current_user->ID;
    $user_email = $current_user->user_email;
    $invoice_id = intval( $_POST[ 'invoice_id' ] );
    $bookid     = intval( $_POST[ 'booking_id' ] );

    $the_post    = get_post( $bookid );
    $book_author = $the_post->post_author;

    $the_post   = get_post( $invoice_id );
    $inv_author = $the_post->post_author;

    $property = get_post( get_post_meta( $bookid, 'booking_id', true ) );


    if ( $userID != $inv_author && $book_author != $userID && $property->post_author != $userID ) {
        die();
    }


    wpestate_super_invoice_details( $invoice_id );

    die();
}
