<?php


namespace Wpk\d565571\Cron;

/**
 * Loads cron modules
 *
 * @author Przemysław Żydek
 */
class Crons {

    /** @var array */
    protected static $crons = [
        GoogleCalendar\SyncBooking::class,
        GoogleCalendar\SyncProperty::class,
    ];

    /**
     * Cron constructor.
     */
    public static function load() {

        foreach ( self::$crons as $cron ) {
            new $cron();
        }

    }

}
