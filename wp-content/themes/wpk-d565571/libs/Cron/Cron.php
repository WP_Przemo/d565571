<?php


namespace Wpk\d565571\Cron;

/**
 * @author Przemysław Żydek
 */
abstract class Cron {

    /** @var string Hook used for cron task */
    const HOOK = '';

    /**
     * Cron constructor.
     */
    public function __construct() {
        add_action( static::HOOK, [ $this, 'handle' ] );
    }

    /**
     * @param mixed $params
     *
     * @return void
     */
    abstract public function handle( $params );

    /**
     * Callback function for removing cron task
     *
     * @param mixed $param
     *
     * @return void
     */
    abstract public function remove( $param );

}
