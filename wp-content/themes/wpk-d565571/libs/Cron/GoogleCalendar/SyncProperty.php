<?php

namespace Wpk\d565571\Cron\GoogleCalendar;

use Wpk\d565571\Cron\Cron;
use Wpk\d565571\Api\Google\Calendar;
use Wpk\d565571\Models\Booking;
use Wpk\d565571\Models\Property;

/**
 * @author Przemysław Żydek
 */
class SyncProperty extends Cron {

    /** @var string */
    const HOOK = 'wpk_google_calendar_sync_property';

    /** @var string */
    const WHEN = '+1 hour';

    /**
     * Sync constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/d565581/property/googleCalendarConnected', [ $this, 'create' ] );
        add_action( 'before_delete_post', [ $this, 'remove' ], 9 );
        add_action( 'wpk/d5657781/property/beforeGoogleCalendarDisconnect', [ $this, 'remove' ], 9 );

    }

    /**
     * Creates new cron task
     *
     * @param int $propertyID
     *
     * @return void
     */
    public function create( $propertyID ) {

        $params = [
            'property_id' => $propertyID,
        ];

        $timestamp = strtotime( self::WHEN );

        wp_schedule_single_event( $timestamp, self::HOOK, [ $params ] );

        update_post_meta( $propertyID, 'google_calendar_sync_timestamp', $timestamp );

    }

    /**
     * @param array $params
     *
     * @return void
     */
    public function handle( $params = [] ) {

        $propertyID = $params[ 'property_id' ];
        $property   = Property::find( $propertyID );

        //For debugging
        $this->remove( $propertyID );

        if ( Calendar::syncProperty( $property ) ) {
            $this->create( $propertyID );
        }

    }

    /**
     * Callback function for removing cron task
     *
     * @param int $postID
     *
     * @return void
     */
    public function remove( $postID ) {

        if ( get_post_type( $postID ) !== 'estate_property' ) {
            return;
        }

        $params = [
            'property_id' => $postID,
        ];

        $timestamp = get_post_meta( $postID, 'google_calendar_sync_timestamp', true );

        wp_unschedule_event( $timestamp, self::HOOK, [ $params ] );

    }

}
