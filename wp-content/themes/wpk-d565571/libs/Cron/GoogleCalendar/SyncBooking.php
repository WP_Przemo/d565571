<?php

namespace Wpk\d565571\Cron\GoogleCalendar;

use Wpk\d565571\Api\Google\Calendar;
use Wpk\d565571\Cron\Cron;
use Wpk\d565571\Models\Booking;

/**
 * Handles syncing bookings with Google Calendar
 */
class SyncBooking extends Cron {

    /** @var string */
    const HOOK = 'wpk_google_calendar_sync';

    /** @var string */
    const WHEN = '+1 hour';

    /**
     * Sync constructor.
     */
    public function __construct() {
        parent::__construct();

        add_action( 'wpk/d5657781/booking/googleEventCreated', [ $this, 'create' ] );
        add_action( 'before_delete_post', [ $this, 'remove' ], 9 );
        add_action( 'wpk/d5657781/booking/beforeCalendarDisconnect', [ $this, 'remove' ], 9 );
    }

    /**
     * Creates new cron task
     *
     * @param Booking $booking
     *
     * @return void
     */
    public function create( Booking $booking ) {

        $params = [
            'calendar_id' => $booking->meta( 'google_calendar_id' ),
            'booking_id'  => $booking->ID,
        ];

        $timestamp = strtotime( self::WHEN );

        wp_schedule_single_event( $timestamp, self::HOOK, [ $params ] );

        //Save timestamp in meta, it will be useful once we will have to remove the event
        $booking->updateMeta( 'google_sync_timestamp', $timestamp );

    }

    /**
     * @param array $params
     *
     * @return void
     */
    public function handle( $params = [] ) {

        $bookingID  = $params[ 'booking_id' ];
        $calendarID = $params[ 'calendar_id' ];

        $booking = Booking::find( $bookingID );

        //For debugging
        $this->remove( $bookingID );

        if ( Calendar::syncBooking( $booking, $calendarID ) ) {
            //Reschedule event
            $this->create( $booking );
        }

    }

    /**
     * Callback function for removing cron task
     *
     * @param int $postID
     *
     * @return void
     */
    public function remove( $postID ) {

        if ( get_post_type( $postID ) !== 'wpestate_booking' ) {
            return;
        }

        $booking = Booking::find( $postID );

        $calendarID = $booking->meta( 'google_calendar_id' );
        $eventID    = $booking->meta( 'google_event_id' );

        if ( empty( $calendarID ) || empty( $eventID ) ) {
            return;
        }

        $timestamp = (int) $booking->meta( 'google_sync_timestamp' );

        $params = [
            'calendar_id' => $calendarID,
            'booking_id'  => $booking->ID,
        ];

        wp_unschedule_event( $timestamp, self::HOOK, [ $params ] );

    }
}
