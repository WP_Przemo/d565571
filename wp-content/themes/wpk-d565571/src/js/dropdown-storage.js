import { $ } from './constants/jquery';

let storage = JSON.parse( localStorage.getItem( 'dropdowns' ) ) || {};

const $Search = $( '#google_map_prop_list_sidebar' );

function restoreValues() {

    for ( let id in storage ) {

        let item = storage[ id ];

        let target = $( `#${id}` ).parent().find( `li[data-value="${item}"]` );

        target.click();

    }

    storage = {};
    localStorage.setItem( 'dropdowns', JSON.stringify( storage ) );

}

function saveDropdownValue() {

    const $This = $( this );

    const Value = $This.data( 'value' ),
          Name  = $This.closest( '.dropdown' ).find( '.filter_menu_trigger' ).attr( 'id' );

    if ( !Name ) {
        return;
    }

    storage[ Name ] = Value;

    localStorage.setItem( 'dropdowns', JSON.stringify( storage ) );

}

$Search.on( 'click', '.dropdown-menu li', saveDropdownValue );

if ( storage && Object.keys( storage ).length && $Search.length ) {
    setTimeout( restoreValues, 800 );
}
