import { $ } from '../constants/jquery';
import { getVar } from './vars';

/**
 * Perform ajax call.
 *
 * @param {Object} args Additional settings for ajax
 * */
export function ajax( args = [] ) {

    let settings = {
        type:        'POST',
        url:         getVar( 'ajax_url' ),
        processData: false,
        contentType: false,
        error( xhr, ajaxOptions, thrownError ) {
            console.error( thrownError );
        },
    };

    settings = $.extend( settings, args );

    return $.ajax( settings );

}

/**
 * Parse AJAX response
 *
 * @param {Object} response
 *
 * @return void
 * */
export function handleResponse( response ) {

    //Remove all errors first
    $( '.wpk-has-error' ).removeClass( 'wpk-has-error' ).find( '.wpk-error-message' ).remove();

    if ( response == 0 ) {
        alert( 'Invalid server response' );
    } else {

        if ( response.messages.length ) {

            for ( let message of response.messages ) {

                let target = message.target;

                if ( target === 'alert' ) {
                    window.alert( message.message );
                } else {
                    let $target = $( target ).parent();

                    $target.addClass( 'wpk-has-error' ).append( `<span class="wpk-error-message">${message.message}</span>` );
                }

            }

        }

    }

}
