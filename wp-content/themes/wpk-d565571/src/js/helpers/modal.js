import { $ } from '../constants/jquery';

/**
 * Helper function for creating modal
 *
 * @param  {String} content Content of modal
 * @param  {Boolean} closeOnClick Whenever modal should be closed on overlay click
 * @param  {Boolean} fade Whenever modal should have fade in animation
 * @param  {Object} css Additional css for modal
 *
 * @return {Promise}
 */
export function modal(
    content, {
        closeOnClick = true,
        fade = false,
        css = {}
    }
) {

    return new Promise( ( resolve ) => {

        let $container = $( '<div class="wpk-modal"><div class="wpk-close-modal">x</div></div>' ),
            overlay    = '<div class="wpk-modal-overlay"></div>',
            $body      = $( 'body' );

        $container.html( content );

        $body.append( $container );
        $body.append( overlay );

        let $modal = $( '.wpk-modal' );

        $modal.css( css );

        if ( fade ) {
            $modal.fadeIn( 400, function() {
                resolve( $modal );
            } );

        } else {
            $modal.show();
            resolve( $modal );
        }

        //Close modal when clicking overlay
        if ( closeOnClick ) {
            $( 'body, .wpk-modal-overlay' ).on( 'click', closeModal );
        }

        $modal.on( 'click', e => e.stopPropagation() );

    } );

}

/**
 * @return void
 * */
export function closeModal() {

    let $modal = $( '.wpk-modal' );

    $modal.fadeOut( 400, function() {
        $( this ).trigger( 'wpk/modal/close' ).remove();
        $( '.wpk-modal-overlay' ).remove();
    } );

}

