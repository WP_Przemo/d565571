import { isset } from './checkers';
import { Actions } from '../constants/booking';
import { getVar } from './vars';
import { $ } from '../constants/jquery';


/**
 * @param {Number} propertyID
 *
 * @return Promise
 * */
export function getBookedDates( propertyID ) {

    let action = Actions.getBookedDates;

    return new Promise( ( resolve, reject ) => {

        $.post( getVar( 'ajax_url' ), { propertyID, action }, data => {

            if ( isset( data ) && data.result ) {
                resolve( data.result );
            } else {
                reject( data, 'Invalid response' );
            }

        } );

    } );

}