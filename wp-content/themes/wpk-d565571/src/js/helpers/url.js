/**
 * Get current url query
 *
 * @param {String} search
 *
 * @return Object
 * */
export function getQuery( search = window.location.search ) {

    const parse = item => {
        let [ key, param ] = item.split( '=' );

        result[ key ] = param;
    };

    let result = {};

    search.replace( '?', '' ).split( '&' ).map( parse );

    return result;

}

/**
 * Add new query param to url
 *
 * @return void
 * */
export function addQueryParam( name, value, state = {} ) {

    const params = new URLSearchParams( location.search );
    params.set( name, value );
    window.history.pushState( state, '', decodeURIComponent( `${location.pathname}?${params}` ) );

}

/**
 * Removes query param from url
 *
 * @return void
 * */
export function removeQueryParam( name, state = {} ) {

    const params = new URLSearchParams( location.search );

    params.delete( name );

    if ( params.toString() ) {
        window.history.pushState( state, '', decodeURIComponent( `${location.pathname}?${params}` ) );
    } else {
        window.history.pushState( state, '', decodeURIComponent( `${location.pathname}` ) );
    }

}
