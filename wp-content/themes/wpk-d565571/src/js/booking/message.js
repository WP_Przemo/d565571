import { $ } from '../constants/jquery';

const $MessagesContainer = $( '#booking_form_request_mess' );

/**
 * Show message in the form
 *
 * @param {String} message
 *
 * @return void
 * */
export function showMessage( message ) {
    $MessagesContainer.html( message ).show();
}

/**
 * Handles response messages and display them in the form
 *
 * @param {Array} messages
 *
 * @return void
 * */
export function handleResponseMessages( messages ) {

    let html = '';

    for ( let { message, type } of messages ) {
        html += message + '\n';
    }

    showMessage( html );

}