/*global create_payment_action*/

import { $ } from '../constants/jquery';
import { $Form, Actions } from '../constants/booking';
import { isset } from '../helpers/checkers';
import { handleResponseMessages, showMessage } from './message';
import './prices';
import './dates';
import { ajax } from './ajax';

let $bookingModal,
    isInternal;

const $CloseModal = $( '#close_reservation_internal' );

/**
 * Handles booking form submission
 *
 * @return void
 * */
function handleSubmit( e ) {

    e.preventDefault();

    $( '#end_date_owner_book' ).attr( 'disabled', false );

    const $This = $( this );

    isInternal = $This.attr( 'id' ) === 'booking_form_owner';

    const Fd       = new FormData( this ),
          Action   = isInternal ? Actions.handleBookingForOwner : Actions.handleBooking,
          LoggedIn = parseInt( $This.data( 'logged' ), 10 );

    Fd.append( 'action', Action );

    if ( isInternal ) {
        Fd.append( 'confirmed', true );
    }

    if ( !$bookingModal ) {
        $bookingModal = $( '#instant_booking_modal' );
    }

    if ( LoggedIn || $( '#owner_reservation_modal' ).length ) {
        submit( Fd );
    } else {

        saveDataInStorage( Fd );

        show_login_form( 1, 0, 0 );

        $( document )
            .on( 'wpk/loggedIn', ( event, $modal ) => {
                $modal.find( '.close' ).trigger( 'click' );

                $Form.data( 'logged', 1 ).trigger( 'submit' );
            } )
            .on( 'click', '.modal-body .login-links > div', () => {
                localStorage.setItem( 'wpk_redirect', document.location.toString() );
            } );

    }

}

export function restoreDataFromStorage() {

    if ( $( '#owner_reservation_modal' ).length || $( '#wpk_all_in_one_modal' ).length ) {
        return;
    }

    let store = localStorage.getItem( 'wpk_booking_form' );

    const showTimeDropdown = ( $element ) => {

        $element
            .addClass( 'wpk-visible' )
            .parents( '.wpk-time-section' )
            .addClass( 'wpk-flex' );

    };

    const blockedProps = [ 'listing_edit', 'security-register-booking_front', 'action', '_wp_http_referer' ];

    if ( !store ) {
        return;
    }

    store = JSON.parse( store );

    if ( window.location.href.indexOf( 'properties' ) === -1 ) {
        window.location = store.url;
    }

    if ( $( '#listing_edit' ).val() !== store.listing_edit ) {
        return;
    }

    if ( store.start_hour ) {
        $( '#end_date' ).attr( 'disabled', false );

        showTimeDropdown( $( '#end_hour' ).parent() );
        showTimeDropdown( $( '#start_hour' ).parent() );
    }


    for ( let prop in store ) {

        if ( !store.hasOwnProperty( prop ) || blockedProps.indexOf( prop ) > -1 ) {
            continue;
        }

        $( `[name="${prop}"]` ).val( store[ prop ] );

    }

    localStorage.removeItem( 'wpk_booking_form' );

}

/**
 * @param {FormData} fd
 *
 * @return void
 * */
function saveDataInStorage( fd ) {

    let data = {};

    for ( let item of fd.entries() ) {

        let [ key, value ] = item;

        data[ key ] = value;

    }

    data.url = window.location.href;

    localStorage.setItem( 'wpk_booking_form', JSON.stringify( data ) );

}

function redirectBackToProperty() {

    const url = localStorage.getItem( 'wpk_redirect' );

    if ( !url ) {
        return;
    }

    localStorage.removeItem( 'wpk_redirect' );

    window.location = url;

}

/**
 * @param {FormData} Fd
 *
 * @return void
 * */
function submit( Fd ) {

    ajax( {
        data: Fd,
        success( data ) {

            if ( isset( data ) ) {

                if ( isset( data.messages ) && data.messages.length ) {
                    handleResponseMessages( data.messages );
                }

                if ( data.result ) {

                    if ( $bookingModal.length ) {

                        if ( parseInt( data.result.instantBooking ) ) {

                            if ( data.result.message ) {

                                $bookingModal
                                    .find( '.modal-body' )
                                    .html( data.result.message )
                                    .end()
                                    .modal( {
                                        backdrop: 'static',
                                        keyboard: false
                                    } );

                                //Use WP Rentals function for payment
                                create_payment_action();

                            }

                        } else {
                            showMessage( data.result.message );
                        }

                    } else {
                        isInternal ? window.location.reload() : $CloseModal.trigger( 'click' );
                    }

                }

            }

        }
    } );

}

$Form.not( '#main_search, .advanced_search_map_list_container' ).on( 'submit', handleSubmit );
$( document ).on( 'ready', redirectBackToProperty );

