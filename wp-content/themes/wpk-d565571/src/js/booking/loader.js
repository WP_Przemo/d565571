import { $Form } from '../constants/booking';

/**
 * Show loader in form
 *
 * @return void
 * */
export function showLoader() {
    $Form.append( '<div class="wpk-loader" /> ' ).find( 'input, select, button' ).attr( 'disabled', true );
}

/**
 * Remove loader from form
 *
 * @return void
 * */
export function hideLoader() {
    $Form.find( '.wpk-loader' ).remove().end().find( 'input, select, button' ).attr( 'disabled', false );
}