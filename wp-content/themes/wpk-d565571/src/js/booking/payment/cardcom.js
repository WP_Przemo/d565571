import { $ } from '../../constants/jquery';
import { ajax, handleResponse } from '../../helpers/ajax';
import { modal } from '../../helpers/modal';
import { Actions } from '../../constants/cardcom';
import './cardcom-response-handler';

/**
 * @return void
 * */
function handleClick( e ) {

    e.preventDefault();

    const $This         = $( this ),
          $Close        = $( '.close' ),
          PaymentAction = $This.data( 'payment-action' ),
          Fd            = new FormData();

    Fd.append( 'payment_action', PaymentAction );
    Fd.append( 'booking_id', $This.data( 'bookid' ) );
    Fd.append( 'action', Actions.getIframeUrl );

    if ( PaymentAction === 'submit-property' ) {

        const $Parent = $This.parent();

        Fd.append(
            'is_featured',
            $Parent.find( '.extra_featured' ).is( ':checked' ) ? 1 : 0
        );

        Fd.append( 'property_id', $Parent.data( 'property-id' ) );

    }

    ajax( {
        data: Fd,
        beforeSend() {
            $This.attr( 'disabled', true );
        },
        success( data ) {

            handleResponse( data );

            if ( data && data.result ) {
                $Close.trigger( 'click' );

                openIframe( data.result );
            }

        },
        complete() {
            $This.attr( 'disabled', false );
        }
    } );

}

/**
 * @param {String} url
 *
 * @return void
 * */
function openIframe( url ) {

    const Iframe = document.createElement( 'iframe' );

    Iframe.src = url;
    Iframe.classList.add( 'wpk-iframe' );

    modal( Iframe.outerHTML, { closeOnClick: true } );

}

$( document ).on( 'click', '.wpk-cardcom-payment', handleClick );
