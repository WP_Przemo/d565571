import { $ } from '../../constants/jquery';

/**
 * @return void
 * */
function handleRedirect( e ) {
    e.preventDefault();

    window.top.location = $( this ).attr( 'href' );
}

$( document ).on( 'click', '.wpk-payment-redirect', handleRedirect );
