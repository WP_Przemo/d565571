import { Actions } from '../constants/booking';
import { $ } from '../constants/jquery';
import { ajax } from '../helpers/ajax';

const RequiredFields = [ 'start_date', 'start_hour', 'end_date', 'end_hour' ];

/**
 * On form change display calculated prices
 *
 * @return void
 * */
export function getPrices() {

    const Fd = new FormData( this );

    Fd.append( 'action', Actions.getPrices );

    if ( validateRequiredFields( Fd ) ) {

        ajax( {
            data: Fd,
            success( data ) {
                $( '#show_cost_form,.cost_row_instant' ).remove();

                $( '#add_costs_here' ).before( data.result );
            }
        } );

    }

}

/**
 * @param {FormData} formData
 *
 * @return {Boolean}
 * */
function validateRequiredFields( formData ) {

    for ( let field of RequiredFields ) {
        if ( !formData.get( field ) ) {
            return false;
        }
    }

    return true;

}