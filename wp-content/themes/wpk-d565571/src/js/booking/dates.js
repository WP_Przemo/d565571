import { isset } from '../helpers/checkers';
import { $ } from '../constants/jquery';
import { getBookedDates } from '../helpers/booking';
import { hideLoader, showLoader } from './loader';
import { getPrices } from './prices';
import { $Form } from '../constants/booking';
import { showBookedHoursOnCalendarClick } from '../property/show-availability';
import { restoreDataFromStorage } from './form';

/**
 * @return void
 * */
function onReady() {

    const $StartDate = $( '#start_date, #start_date_owner_book, #check_in, #check_in_list, #booking_from_date' ),
          $StartHour = $( '#start_hour, #start_hour_owner_book, #booking_start_hour' ),
          $EndDate   = $( '#end_date, #end_date_owner_book, #check_out, #check_out_list, #booking_to_date' ),
          $EndHour   = $( '#end_hour, #end_hour_owner_book, #booking_end_hour' );

    let bookedDates = [];

    /**
     * Format hour to integer value
     *
     * @return number
     * */
    const formatHour = hour => hour ? parseInt( hour.replace( ':', '' ) ) : 0;

    /**
     * @return void
     * */
    function blockHoursOnDateChange() {

        const StartDate = $StartDate.val(),
              EndDate   = $EndDate.val();

        /**
         * If dropdown has selected option set as disabled, find first not disabled one and set as it's value
         *
         * @param {jQuery} $el
         *
         * @return void
         * */
        const checkIfHourIsDisabled = $el => {

            const Value           = $el.val(),
                  $SelectedOption = $el.find( `option[value="${Value}"]` );

            if ( !Value || $SelectedOption.is( ':disabled' ) ) {
                $el.val( $el.find( 'option:not([disabled="disabled"])' ).first().val() );
            }

        };

        /**
         * Set all hours that are booked as disabled
         *
         * @param {jQuery} $el
         * @param {Array} hours
         *
         * @return void
         * */
        const filterHours = ( $el, hours ) => {

            for ( let hour of hours ) {
                const $Option = $el.find( `option[value="${hour}"]` );

                $Option.attr( 'disabled', true );
            }

            checkIfHourIsDisabled( $el );

        };

        if ( StartDate && isset( bookedDates[ StartDate ] ) ) {
            filterHours( $StartHour, bookedDates[ StartDate ] );
        }

        if ( EndDate && isset( bookedDates[ EndDate ] ) ) {
            filterHours( $EndHour, bookedDates[ EndDate ] );
        }

    }

    /**
     * On start hour change disable smaller hours in end hour dropdown
     *
     * @return void
     * */
    function disableSmallerHours() {

        const disable = function( index ) {

            let $startDate = $( this ),
                $endDate   = $EndDate.eq( index ),
                $endHour   = $EndHour.eq( index ),
                startHour  = formatHour( $StartHour.eq( index ).val() ),
                endHour    = $endHour.val();

            if ( $startDate.val() !== $endDate.val() ) {

                if ( !$endHour.data( 'hoursSet' ) ) {
                    $endHour.val( '08:00' ).data( 'hoursSet', 1 );
                }

                return;
            }

            /**
             * @return void
             * */
            const filterOptions = function() {

                const $This = $( this ),
                      Hour  = formatHour( $This.val() );

                if ( startHour >= Hour ) {
                    $This.attr( 'disabled', true );
                } else {
                    $This.attr( 'disabled', false );
                }

            };

            $endHour.eq( index ).find( 'option' ).each( filterOptions );

            if ( formatHour( endHour ) <= startHour ) {
                $endHour.val( $endHour.find( 'option:not([disabled="disabled"])' ).eq( 2 ).val() );
            }

        };

        $StartDate.each( disable );

    }

    /**
     * Launches as first durning form change, marks all hours as enabled again
     *
     * @return void
     * */
    function refreshHours() {

        const refresh = $el => $el.find( 'option' ).attr( 'disabled', false );

        refresh( $StartHour );
        refresh( $EndHour );

    }

    /**
     * On dates changes show associated time dropdown if date is not empty
     *
     * @return void
     * */
    function showTimeOnDateChange() {

        const showOrHide = ( $dateEl, $timeEl ) => {

            if ( $dateEl.val() ) {

                $timeEl
                    .parent()
                    .addClass( 'wpk-visible' )
                    .parents( '.wpk-time-section' )
                    .addClass( 'wpk-flex' );

                const $Section = $dateEl.parents( '.wpk-time-section' );

                if ( $Section.index( '.wpk-time-section' ) === 0 && !$timeEl.val() ) {

                    const date = new Date();

                    let hour = date.getHours();

                    let year  = date.getFullYear(),
                        month = date.getMonth() + 1,
                        day   = date.getDate();

                    month = month < 10 ? `0${month}` : month;
                    day = day < 10 ? `0${day}` : day;

                    const todayDate = `${year}-${month}-${day}`;

                    if ( hour >= 8 && todayDate === $dateEl.val() ) {
                        hour += 2;

                        hour = hour < 10 ? `0${hour}` : hour;

                        $timeEl.val( `${hour}:00` );
                    } else {
                        $timeEl.val( '08:00' ).trigger( 'change' );
                    }

                    $timeEl.trigger( 'change' );

                }

            }

        };

        $StartDate.each( function( index ) {
            showOrHide( $( this ), $StartHour.eq( index ) );
        } );

        $EndDate.each( function( index ) {
            showOrHide( $( this ), $EndHour.eq( index ) );
        } );

        /* showOrHide( $StartDate, $StartHour );
         showOrHide( $EndDate, $EndHour );*/

    }

    /**
     * Enables end date input if start date and hour have been provided
     *
     * @return void
     * */
    function enableEndDate() {

        const $This      = $( this ),
              $StartDate = $This.parents( '.wpk-time-section' ).find( '.has_calendar input' ),
              $EndDate   = $This.parents( '.wpk-time-section' ).next().find( '.has_calendar input' );

        if ( $StartDate.val() && $This.val() !== 'Hour' ) {
            $EndDate.attr( 'disabled', false );
        }

    }

    /**
     * Trigger changes in date inputs if they are not empty
     *
     * @return void
     * */
    function triggerChanges() {

        const EndDate = $EndDate.val();

        const trigger = $el => $el.val() !== '' ? $el.trigger( 'change' ) : false;

        trigger( $StartDate );
        trigger( $EndDate );

        //Restore previous value after triggering changes
        $EndDate.val( EndDate );

    }


    const PropertyID = parseInt( $( '#property_id' ).val() );

    const $ContactForm = $( '#contact_owner_modal' );

    if ( $ContactForm.length ) {
        $ContactForm
            .on( 'change', disableSmallerHours )
            .on( 'change', showTimeOnDateChange )
            .trigger( 'change' );
    }

    $StartHour.on( 'change', enableEndDate );

    if ( !PropertyID ) {

        $Form
            .on( 'change', disableSmallerHours )
            .on( 'change', showTimeOnDateChange )
            .trigger( 'change' );

        $EndDate.attr( 'disabled', true );

        return;
    }

    /**
     * Setup form dates related events after receiving booked dates from server
     *
     * @param {Array} dates
     *
     * @return void
     * */
    const setupEvents = dates => {

        bookedDates = dates;

        showBookedHoursOnCalendarClick( bookedDates );

        $Form
            .on( 'change', refreshHours )
            .on( 'change', disableSmallerHours )
            .on( 'change', showTimeOnDateChange )
            .on( 'change', getPrices )
            .on( 'change', blockHoursOnDateChange );

        hideLoader();

        if ( !$EndDate.val() ) {
            $EndDate.attr( 'disabled', true );
        }

        if ( $StartDate.val() && $StartHour.val() ) {
            $EndDate.attr( 'disabled', false );
        }

        restoreDataFromStorage();
    };

    showLoader();
    triggerChanges();

    getBookedDates( PropertyID ).then( setupEvents ).catch( e => console.error( e ) );

}

setTimeout( onReady, 700 );

