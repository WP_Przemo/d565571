import { hideLoader, showLoader } from './loader';
import { $ } from '../constants/jquery';
import { ajax as helperAjax } from '../helpers/ajax';

/**
 * Helper ajax function for booking form
 *
 * @param {Object} opts
 * */
export function ajax( opts = {} ) {

    let defaults = {
        beforeSend: showLoader,
        complete:   hideLoader
    };

    return helperAjax( $.extend( defaults, opts ) );

}
