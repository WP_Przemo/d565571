import { $ } from './constants/jquery';
import { addQueryParam, removeQueryParam } from './helpers/url';

const $MapContainer    = $( '#google_map_prop_list_wrapper' ),
      $FilterContainer = $( '#advanced_search_map_list' ),
      $ListingWrapper  = $( '#listing_ajax_container,.pagination' );

const $Switches = $( '.wpk-mobile-switches' );

const $Wrapper = $( '<div class="wpk-mobile-fold" />' );

const $Window = $( window );

let folded     = false,
    mapZoomSet = false;

function foldItems() {

    const Width = $Window.width();

    if ( Width <= 700 && !folded ) {
        $MapContainer.wrapAll( $Wrapper );
        $FilterContainer.wrapAll( $Wrapper );

        $( '.wpk-mobile-fold' ).append( '<button class="wpk-close-fold">X</button>' );
        $( '.wpk-close-fold' ).on( 'click', unfold );

        folded = true;
    }

}

function unfold() {

    const $This = $( this );

    const params = new URLSearchParams( location.search );

    $This.parents( '.wpk-mobile-fold' ).removeClass( 'wpk-active' );

    removeQueryParam( 'switch', { Switch: params.get( 'switch' ) } );

    $ListingWrapper.removeClass( 'wpk-hidden' );
    $Switches.removeClass( 'wpk-hidden' );

}

/**
 * @return void
 * */
function handlePopState() {

    const $Target = $( '.wpk-close-fold:visible' );

    if ( $Target.length ) {
        $Target.trigger( 'click' );
    }

}

function handleSwitches() {

    const $This  = $( this ),
          Switch = $This.data( 'switch' );

    addQueryParam( 'switch', Switch, { Switch } );

    const hideOtherElements = () => {
        $ListingWrapper.addClass( 'wpk-hidden' );

        $Switches.addClass( 'wpk-hidden' );
    };

    switch ( Switch ) {

        case 'map':
            $MapContainer.parents( '.wpk-mobile-fold' ).addClass( 'wpk-active' );

            hideOtherElements();

            if ( !mapZoomSet && typeof map === 'object' ) {

                if ( gmarkers && gmarkers.length ) {
                    map.setCenter( gmarkers[ 0 ].getPosition() );
                }

                map.setZoom( 10 );

                mapZoomSet = true;

            }

            if ( map.getZoom() < 7 ) {
                map.setZoom( 7 );
            }

            break;

        case 'filters':
            $FilterContainer.parents( '.wpk-mobile-fold' ).addClass( 'wpk-active' );

            hideOtherElements();

            break;

    }

}

foldItems();

export function sortByPrice() {

    let $order = $( '#order' );

    if ( !$order.data( 'value' ) ) {
        $order.siblings( 'ul' ).find( 'li' ).eq( 1 ).click();
    }

}

export function sortByDistance() {

    let $order = $( '#order' );

    if ( $order.attr( 'data-value' ) === '' ) {
        $order.siblings( 'ul' ).find( 'li' ).eq( 3 ).click();
    }

}

$Window.on( 'load', () =>setTimeout( sortByDistance, 1000 ) );
$Window.on( 'resize', foldItems );

$Switches.find( '.wpk-switch' ).on( 'click', handleSwitches );

window.addEventListener( 'popstate', handlePopState );

