import { $ } from './jquery';

export const $Form = $( '.wpk-request-form, #booking_form_owner, #main_search, .advanced_search_map_list_container, #contact_owner_modal' );
export const Actions = {
    getBookedDates:        'wpk_get_booked_dates',
    getPrices:             'wpk_booking_get_prices',
    handleBooking:         'wpk_book_property',
    handleBookingForOwner: 'wpk_owner_book'
};
