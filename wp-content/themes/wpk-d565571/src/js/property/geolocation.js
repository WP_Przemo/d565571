import { $ } from '../constants/jquery';

let $target = $( '.wpk-geolocation-target' );

/**
 * Callback for navigator
 *
 * @return void
 * */
function getUserLocation( position ) {

    const UserLat = position.coords.latitude,
          UserLng = position.coords.longitude;

    $( '#user_lat' ).val( UserLat );
    $( '#user_lng' ).val( UserLng );

    /**
     * Displays user distance from provided property
     *
     * @return void
     * */
    const displayGeolocation = function() {

        const $This = $( this );

        let { longitude, latitude } = $This.data();

        const Distance = getDistance(
            latitude,
            longitude,
            UserLat,
            UserLng
        ).toFixed( 2 );

        $This.text( `${Distance} km` );

    };

    const handleAjax = ( event, xhr, settings ) => {

        let { data } = settings;

        if ( !data ) {
            return;
        }

        const Params = new URLSearchParams( data );

        const Actions = [ 'wpestate_ajax_filter_ondemand_listings_with_geo', 'wpestate_ajax_filter_listings_search_onthemap' ];

        if ( Actions.indexOf( Params.get( 'action' ) ) > -1 ) {

            setTimeout( () => {
                $target = $( '.wpk-geolocation-target' );

                $target.each( displayGeolocation );
            }, 500 );

        }

    };

    $target.each( displayGeolocation );

    $( document ).ajaxComplete( handleAjax );

}

function handleError( err ) {
    console.error( err );
}

function getDistance( lat1, lon1, lat2, lon2, unit = 'K' ) {

    let radlat1 = Math.PI * lat1 / 180;

    let radlat2 = Math.PI * lat2 / 180;

    let theta = lon1 - lon2;

    let radtheta = Math.PI * theta / 180;

    let dist = Math.sin( radlat1 ) * Math.sin( radlat2 ) + Math.cos( radlat1 ) * Math.cos( radlat2 ) * Math.cos( radtheta );

    if ( dist > 1 ) {
        dist = 1;
    }

    dist = Math.acos( dist );
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    if ( unit === 'K' ) {
        dist = dist * 1.609344;
    }
    if ( unit === 'N' ) {
        dist = dist * 0.8684;
    }

    return dist;

};

if ( $target.length ) {
    navigator.geolocation.getCurrentPosition( getUserLocation, handleError );
}
