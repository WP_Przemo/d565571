import { $ } from '../constants/jquery';
import { modal } from '../helpers/modal';

export const showBookedHoursOnCalendarClick = dates => {

    const $DateItem = $( '#listing_calendar' ).next().find( '.end_reservation, .calendar-today' );

    function showBookedHours() {

        const $This       = $( this ),
              ClickedDate = new Date(
                  $This.data( 'curent-date' ) * 1000
              );

        let month = ClickedDate.getMonth() + 1,
            day   = ClickedDate.getDate(),
            year  = ClickedDate.getFullYear();

        day = day < 10 ? `0${day}` : day;
        month = month < 10 ? `0${month}` : month;

        const DateFormated = `${year}-${month}-${day}`;

        if ( DateFormated in dates ) {
            const Hours = dates[ DateFormated ];

            displayModalForHours( Hours, DateFormated );
        }

    }

    $DateItem.on( 'click', showBookedHours );


};

function displayModalForHours( hours = [], date ) {

    const $Table = $( `
        <table class="wpk-hours-table">
            <thead>
                <tr>
                    <td>    
                        Booked hours for day <span class="wpk-date">${date}</span>
                    </td>
                </tr>
            </thead>
            <tbody> 
            </tbody>
        </table>
    ` );

    const $Body = $Table.find( 'tbody' );

    const appendHour = hour => {
        $Body.append( `
            <tr>    
                <td>    
                    ${hour}
                </td>
            </tr>
        ` );
    };

    hours.forEach( appendHour );

    setTimeout( () => modal( $Table.get( 0 ).outerHTML, {
        css:  {
            width:   $( window ).width() < 600 ? '80%' : '40%',
            height:  '40%',
            top:     '20%',
            opacity: '0.9'
        },
        fade: true
    } ), 500 );

}
