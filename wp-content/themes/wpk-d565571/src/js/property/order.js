import { $ } from '../constants/jquery';

const TargetActions = [ 'wpestate_ajax_filter_listings_search_onthemap', 'wpestate_ajax_filter_ondemand_listings_with_geo' ];

let action = TargetActions[ 0 ];

function updateRecentAction( event, xhr, settings ) {

    let { data } = settings;

    if ( !data ) {
        return;
    }

    const Params = new URLSearchParams( data );

    if ( TargetActions.indexOf( Params.get( 'action' ) ) > -1 ) {
        action = Params.get( 'action' );
    }

}

function triggerFiltering() {

    setTimeout( () => {
        switch ( action ) {

            case TargetActions[ 0 ]:
                start_filtering_ajax_map( 1 );
                break;

            case TargetActions[ 1 ]:
                wpestate_ondenamd_map_moved();
                break;

            default:
                throw new Error( 'Invalid action provided' );

        }
    }, 200 );

}

$( document ).ajaxComplete( updateRecentAction );

$( '#wpk_order li' ).on( 'click', triggerFiltering );


