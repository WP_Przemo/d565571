import { $ } from '../constants/jquery';

const $ConnectBtn = $( '.wpk-google-connect' );

const $SaveBtn = $( '.wpk-save-calendar' ),
      $Target  = $( '#edit_calendar' );

function openConnectionModal( e ) {

    e.preventDefault();

    const $This = $( this ),
          link  = $This.attr( 'href' );

    localStorage.setItem( 'isGoogleConnection', 1 );

    window.open( link, 'Google Connection', 'fullscreen=no,height=600,width=600' );

}

function reloadPage() {

    if ( window.opener && localStorage.getItem( 'isGoogleConnection' ) && window.name === 'Google Connection' ) {

        localStorage.removeItem( 'isGoogleConnection' );

        const $Calendar = $( '.wpk-google-calendar-sync ' );

        $Calendar
            .prependTo( 'body' )
            .addClass( 'wpk-full-calendar' )
            .prepend( '<label for="google_calendars">Please select a calendar for this property, and then click save</label>' );

        $( '.website-wrapper, .wpk-disconnect-google-calendar' ).hide();

        $( 'body' ).addClass( 'wpk-hide-all' );

        $( '.wpk-save-calendar' ).on( 'click', () => {

            $( document ).ajaxComplete( () => {
                window.close();

                window.opener.location.reload();
            } );

        } );

        window.onunload = () => window.opener.location.reload();

    }

}

reloadPage();

$ConnectBtn.on( 'click', openConnectionModal );

$SaveBtn.on( 'click', () => $Target.trigger( 'click' ) );
