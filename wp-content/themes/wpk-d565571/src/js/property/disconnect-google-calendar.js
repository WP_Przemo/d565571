import { $ } from '../constants/jquery';
import { Actions } from '../constants/property';
import { ajax } from '../helpers/ajax';

const $Button = $( '.wpk-disconnect-google-calendar' ),
      PropID  = $( '#property_id' ).val();

/**
 * @return void
 * */
function disconnectCalendar() {

    const $This = $( this ),
          Fd    = new FormData(),
          Nonce = $( '#wpk_disconnect_nonce' ).val();

    Fd.append( 'action', Actions.disconnectGoogleCalendar );
    Fd.append( 'property_id', PropID );
    Fd.append( 'nonce', Nonce );

    ajax( {
        beforeSend() {
            $This.attr( 'disabled', true );
        },
        data: Fd,
        success( data ) {

            if ( data && data.result ) {
                window.location.reload();
            }

        },
        complete() {
            $This.attr( 'disabled', false );
        }
    } );

}

$Button.on( 'click', disconnectCalendar );
