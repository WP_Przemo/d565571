import { $ } from './constants/jquery';

const $SearchWrapper       = $( '#search_wrapper.type2' ),
      $MobileSearchWrapper = $( '#adv-search-mobile' ),
      $DesktopAfterTarget  = $( '#gmap_wrapper' );

const $Window     = $( window ),
      TargetWidth = 900;

/**
 * On certain screen width replace default mobile search form with desktop one
 *
 * @return void
 * */
function replaceSearchForms() {

    if ( $Window.width() > TargetWidth ) {
        $SearchWrapper.insertAfter( $DesktopAfterTarget );
    } else {
        $SearchWrapper.insertAfter( $MobileSearchWrapper );
    }


}

$Window.on( 'resize', replaceSearchForms );
