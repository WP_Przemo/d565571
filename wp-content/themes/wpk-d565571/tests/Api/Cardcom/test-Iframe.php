<?php

namespace Tests\Api\Cardcom;

use Tests\Helpers\Settings;
use Tests\TestCase;
use Wpk\d565571\Api\Cardcom\Iframe;

class IframeTest extends TestCase {

    /**
     * Tests whenever we are getting proper iframe URL from cardcom
     *
     * @covers Iframe::getUrl
     */
    public function testIsGettingProperIframeUrl() {

        Settings::setForCardcom();

        $url = Iframe::getUrl( 200 );

        $this->assertInternalType( 'string', $url );
        $this->assertContains( 'https://secure.cardcom.solutions', $url );
        $this->assertContains( 'LowProfileCode', $url );

    }

}
