<?php

namespace Wpk;

// disable xdebug backtrace
if ( function_exists( 'xdebug_disable' ) ) {
   // xdebug_disable();
}


define( 'WP_PLUGIN_DIR', getenv( 'WP_PLUGIN_DIR' ) );
define( 'WP_THEMES_DIR', getenv( 'WP_THEMES_DIR' ) );
define( 'WP_CONTENT_DIR', 'C:\wamp64\www\jobs\d565571\wp-content' );


if ( false !== getenv( 'WP_DEVELOP_DIR' ) ) {
    require getenv( 'WP_DEVELOP_DIR' ) . 'tests/phpunit/includes/bootstrap.php';
}

update_option( 'stylesheet', 'wpk-d565571' );
update_option( 'template', 'wpk' );
activate_plugin( 'advanced-custom-fields-pro/acf.php' );

$GLOBALS[ 'current_user' ] = new \WP_User();

require_once WP_THEMES_DIR . 'wpk-d565571/functions.php';
require_once WP_THEMES_DIR . 'wprentals/functions.php';



