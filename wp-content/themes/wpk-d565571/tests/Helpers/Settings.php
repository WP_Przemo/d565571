<?php

namespace Tests\Helpers;

use Wpk\d565571\Settings as AcfSettings;

/**
 * Helper class that manages ACF settings for our tests
 *
 * @author Przemysław Żydek
 */
class Settings {

    /**
     * Set default settings for cardcom
     *
     * @return void
     */
    public static function setForCardcom() {

        AcfSettings::updateArray( [
            'cardcom_user_name'            => getenv( 'CARDCOM_USER_NAME' ),
            'cardcom_terminal_number'      => getenv( 'CARDCOM_TERMINAL_NUMBER' ),
            'cardcom_indicator_url'        => 'http://talion.ct8.pl/d565571/properties/property-1/',
            'cardcom_error_redirect_url'   => 'http://talion.ct8.pl/d565571/properties/property-1/',
            'cardcom_success_redirect_url' => 'http://talion.ct8.pl/d565571/properties/property-1/',

        ] );

    }

}
