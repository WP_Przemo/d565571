<?php

namespace Tests;

use Wpk\d565571\Models\Property;

abstract class TestCase extends \WP_Ajax_UnitTestCase {

    public function setUp() {
        $this->loginAs();
    }

    /**
     * @param string $role
     *
     * @return void
     */
    protected function loginAs( $role = 'administrator' ) {

        global $current_user;

        $user = $this->factory()->user->create( [
            'role' => $role,
        ] );

        wp_set_current_user( $user );

        $current_user = get_userdata( $user );

    }

    /**
     * @return array|null
     */
    protected function getLastResponse() {
        return json_decode( $this->_last_response, true );
    }

    /**
     * Create new property
     *
     * @param array $args
     *
     * @return Property
     */
    protected function createProperty( $args = [] ) {

        $author = $this->factory()->user->create();

        $defaults = [
            'post_author'  => $author,
            'post_content' => 'Test',
            'post_status'  => 'publish',
            'post_type'    => 'estate_property',
            'meta_input'   => [
                'original_author'              => $author,
                'guest_no'                     => '1',
                'property_country'             => 'Poland',
                'prop_featured'                => '0',
                'instant_booking'              => '1',
                'property_admin_area'          => '',
                'pay_status'                   => 'paid',
                'page_custom_zoom'             => '16',
                'sidebar_option'               => 'right',
                'sidebar_select'               => 'primary-widget-area',
                'rcapi_listing_id'             => 'a:3:{s:4:"code";s:14:"rest_forbidden";s:7:"message";s:38:"Sorry, you are not allowed to do that.";s:4:"data";a:1:{s:6:"status";i:401;}}',
                'property_price'               => '25',
                'cleaning_fee'                 => '25',
                'city_fee'                     => '25',
                'property_price_per_week'      => '0',
                'property_price_per_month'     => '0',
                'cleaning_fee_per_day'         => '2',
                'city_fee_per_day'             => '2',
                'price_per_guest_from_one'     => '0',
                'price_per_weekeend'           => '35',
                'checkin_change_over'          => '0',
                'checkin_checkout_change_over' => '0',
                'min_days_booking'             => '0',
                'extra_price_per_guest'        => '0',
                'overload_guest'               => '0',
                'city_fee_percent'             => '0',
                'security_deposit'             => '0',
                'property_price_after_label'   => '',
                'property_price_before_label'  => '$',
                'extra_pay_options'            => 'a:1:{i:0;a:3:{i:0;s:4:"Test";i:1;s:2:"25";i:2;s:1:"0";}}',
                'early_bird_days'              => '0',
                'early_bird_percent'           => '0',
                'property_taxes'               => '0',
                'unique_code_ica'              => '823036e488bebbb491f1c162c8ce3f4e',
                'google_calendar_id'           => 'tl1s96ae4uk7iumlro271som64@group.calendar.google.com',
                '_edit_lock'                   => '1535613230:1',
                '_edit_last'                   => '1',
                'property_address'             => '',
                'property_county'              => '',
                'property_state'               => '',
                'property_zip'                 => '',
                'property_status'              => 'normal',
                'property_size'                => '',
                'property_rooms'               => '',
                'property_bedrooms'            => '',
                'property_bathrooms'           => '',
                'embed_video_type'             => 'vimeo',
                'embed_video_id'               => '',
                'check-in-hour'                => '',
                'check-out-hour'               => '',
                'late-check-in'                => '',
                'optional-services'            => '',
                'outdoor-facilities'           => '',
                'extra-people'                 => '',
                'cancellation'                 => '',
                'property_latitude'            => '0',
                'property_longitude'           => '0',
                'google_camera_angle'          => '0',
                'kitchen'                      => '',
                'internet'                     => '',
                'smoking_allowed'              => '',
                'tv'                           => '',
                'wheelchair_accessible'        => '',
                'elevator_in_building'         => '',
                'indoor_fireplace'             => '',
                'heating'                      => '',
                'essentials'                   => '',
                'doorman'                      => '',
                'pool'                         => '',
                'washer'                       => '',
                'hot_tub'                      => '',
                'dryer'                        => '',
                'gym'                          => '',
                'free_parking_on_premises'     => '',
                'wireless_internet'            => '',
                'pets_allowed'                 => '',
                'family-kid_friendly'          => '',
                'suitable_for_events'          => '',
                'non_smoking'                  => '',
                'phone_booth-lines'            => '',
                'projectors'                   => '',
                'bar_-_restaurant'             => '',
                'air_conditioner'              => '',
                'scanner_-_printer'            => '',
                'fax'                          => '',
                'property_agent'               => '3',
                'adv_filter_search_action'     => '',
                'adv_filter_search_category'   => '',
                'current_adv_filter_city'      => '',
                'current_adv_filter_area'      => '',
            ],
        ];
        $args = wp_parse_args( $args, $defaults );

        $id = $this->factory()->post->create( $args );

        return Property::find( $id );

    }

}
